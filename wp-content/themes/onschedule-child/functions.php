<?php

/*** Child Theme Function  ***/
if ( ! function_exists( 'onschedule_edge_child_theme_enqueue_scripts' ) ) {
	function onschedule_edge_child_theme_enqueue_scripts()
	{
		$parent_style = 'onschedule-edge-style-default-style';
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
		wp_enqueue_style('onschedule-edge-style-child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
	}

	add_action('wp_enqueue_scripts', 'onschedule_edge_child_theme_enqueue_scripts');
}

