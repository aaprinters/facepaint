<?php
$edgtf_blog_type = onschedule_edge_get_archive_blog_list_layout();
onschedule_edge_include_blog_helper_functions('lists', $edgtf_blog_type);
$edgtf_holder_params = onschedule_edge_get_holder_params_blog();
?>
<?php get_header(); ?>
<?php 
$calendar_id = get_queried_object_id();
$calendar = get_term_by('id',$calendar_id, 'booked_custom_calendars');
echo do_shortcode('[edgtf_booked_calendar calendar="'.$calendar_id.'" calendar_title_tag="" calendar_skin="default" calendar_size="large" calendar_style="calendar" calendar_title="'.$calendar->name.'"]'); ?>
<?php get_footer(); ?>