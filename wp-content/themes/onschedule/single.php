<?php get_header(); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

        <?php
        onschedule_edge_include_blog_helper_functions('singles', 'standard');
        $edgtf_holder_params = onschedule_edge_get_holder_params_blog();
        ?>

        <?php onschedule_edge_get_title('blog'); ?>
            <?php get_template_part('slider'); ?>
            <div class="<?php echo esc_attr($edgtf_holder_params['holder']); ?>">
                <?php do_action('onschedule_edge_action_after_container_open'); ?>
                <div class="<?php echo esc_attr($edgtf_holder_params['inner']); ?>">
                    <?php onschedule_edge_get_blog_single('standard'); ?>
                </div>
            <?php do_action('onschedule_edge_action_before_container_close'); ?>
            </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>