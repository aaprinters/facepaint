<?php get_header(); ?>
<?php
$onschedule_sidebar_layout = onschedule_edge_sidebar_layout();
$onschedule_sidebar_classes = onschedule_edge_sidebar_columns_class();

$enable_search_page_sidebar = true;
if (onschedule_edge_options()->getOptionValue('enable_search_page_sidebar') === "no") {
    $enable_search_page_sidebar = false;
}
?>
<?php onschedule_edge_get_title(); ?>
<div class="edgtf-container">
    <?php do_action('onschedule_edge_action_after_container_open'); ?>
    <div class="edgtf-container-inner clearfix">
        <div class="edgtf-container">
            <?php do_action('onschedule_edge_action_after_container_open'); ?>
            <div class="edgtf-container-inner">
                <div class="edgtf-columns-wrapper <?php echo esc_attr($onschedule_sidebar_classes); ?>">
                    <div class="edgtf-columns-inner">
                        <div class="edgtf-column-content edgtf-column-content1">
                            <div class="edgtf-search-page-holder">
                                <form action="<?php echo esc_url(home_url('/')); ?>" class="edgtf-search-page-form" method="get">
                                    <h2 class="edgtf-search-title"><?php esc_html_e('Search results:', 'onschedule'); ?></h2>
                                    <div class="edgtf-form-holder">
                                        <div class="edgtf-column-left">
                                            <input type="text" name="s" class="edgtf-search-field" autocomplete="off" value="" placeholder="<?php esc_attr_e('Type here', 'onschedule'); ?>"/>
                                        </div>
                                        <div class="edgtf-column-right">
                                            <button type="submit" class="edgtf-search-submit"><span class="icon_search"></span></button>
                                        </div>
                                    </div>
                                    <div class="edgtf-search-label">
                                        <?php esc_html_e("If you are not happy with the results below please do another search", "onschedule"); ?>
                                    </div>
                                </form>
                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                        <div class="edgtf-post-content">
                                            <?php if (has_post_thumbnail()) { ?>
                                                <div class="edgtf-post-image">
                                                    <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                        <?php the_post_thumbnail('thumbnail'); ?>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                            <div class="edgtf-post-title-area <?php if (!has_post_thumbnail()) { echo esc_attr('edgtf-no-thumbnail'); } ?>">
                                                <div class="edgtf-post-title-area-inner">
                                                    <h4 itemprop="name" class="edgtf-post-title entry-title">
                                                        <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                                    </h4>
                                                    <?php
                                                    $my_excerpt = get_the_excerpt();
                                                    if ($my_excerpt != '') { ?>
                                                        <p itemprop="description" class="edgtf-post-excerpt"><?php echo esc_html($my_excerpt); ?></p>
                                                    <?php }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                <?php endwhile; ?>
                                <?php else: ?>
                                    <p class="edgtf-blog-no-posts"><?php esc_html_e('No posts were found.', 'onschedule'); ?></p>
                                <?php endif; ?>
                                <?php
                                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                                    else { $paged = 1; }

                                    $params['max_num_pages'] = onschedule_edge_get_max_number_of_pages();
                                    $params['paged'] = $paged;
                                    onschedule_edge_get_module_template_part('templates/parts/pagination/standard', 'blog', '', $params);
                                ?>
                            </div>
                            <?php do_action('onschedule_edge_page_after_content'); ?>
                        </div>
                        <?php if($onschedule_sidebar_layout !== 'no-sidebar') { ?>
                            <div class="edgtf-column-content edgtf-column-content2">
                                <?php get_sidebar(); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
				<?php do_action('onschedule_edge_action_before_container_close'); ?>
            </div>
        </div>
    </div>
    <?php do_action('onschedule_edge_action_before_container_close'); ?>
</div>
<?php get_footer(); ?>