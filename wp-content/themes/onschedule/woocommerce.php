<?php 
/*
Template Name: WooCommerce
*/ 
?>
<?php
$onschedule_sidebar_layout  = onschedule_edge_sidebar_layout();
$onschedule_sidebar_classes = onschedule_edge_sidebar_columns_class();

get_header();
onschedule_edge_get_title();

//Woocommerce content
if ( ! is_singular('product') ) {
	get_template_part('slider');
	?>
	<div class="edgtf-container">
		<div class="edgtf-container-inner clearfix">
			<div class="edgtf-columns-wrapper <?php echo esc_attr($onschedule_sidebar_classes); ?>">
				<div class="edgtf-columns-inner">
					<div class="edgtf-column-content edgtf-column-content1">
						<?php onschedule_edge_woocommerce_content(); ?>
					</div>
					<?php if($onschedule_sidebar_layout !== 'no-sidebar') { ?>
						<div class="edgtf-column-content edgtf-column-content2">
							<?php get_sidebar(); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>			
<?php } else { ?>
	<div class="edgtf-container">
		<div class="edgtf-container-inner clearfix">
			<?php onschedule_edge_woocommerce_content(); ?>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>