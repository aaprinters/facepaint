<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    /**
     * onschedule_edge_action_header_meta hook
     *
     * @see onschedule_edge_header_meta() - hooked with 10
     * @see onschedule_edge_user_scalable_meta - hooked with 10
     */
    do_action('onschedule_edge_action_header_meta');

    wp_head(); ?>
</head>
<body <?php body_class();?> itemscope itemtype="//schema.org/WebPage">
    <?php
    /**
     * onschedule_edge_action_after_body_tag hook
     *
     * @see onschedule_edge_get_side_area() - hooked with 10
     * @see onschedule_edge_smooth_page_transitions() - hooked with 10
     */
    do_action('onschedule_edge_action_after_body_tag'); ?>

    <div class="edgtf-wrapper">
        <div class="edgtf-wrapper-inner">
            <?php onschedule_edge_get_header(); ?>
	
	        <?php
	        /**
	         * onschedule_edge_action_after_header_area hook
	         *
	         * @see onschedule_edge_back_to_top_button() - hooked with 10
	         * @see onschedule_edge_get_full_screen_menu() - hooked with 10
	         */
	        do_action('onschedule_edge_action_after_header_area'); ?>
	        
            <div class="edgtf-content">
                <div class="edgtf-content-inner">