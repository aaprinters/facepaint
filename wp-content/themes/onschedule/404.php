<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    /**
     * onschedule_edge_action_header_meta hook
     *
     * @see onschedule_edge_header_meta() - hooked with 10
     * @see onschedule_edge_user_scalable_meta - hooked with 10
     */
    do_action('onschedule_edge_action_header_meta');

    wp_head(); ?>
</head>
<body <?php body_class();?> itemscope itemtype="//schema.org/WebPage">
	<?php
	/**
	 * onschedule_edge_action_after_body_tag hook
	 *
	 * @see onschedule_edge_get_side_area() - hooked with 10
	 * @see onschedule_edge_smooth_page_transitions() - hooked with 10
	 */
	do_action('onschedule_edge_action_after_body_tag'); ?>
	
	<div class="edgtf-wrapper edgtf-404-page">
	    <div class="edgtf-wrapper-inner">
		    <?php onschedule_edge_get_header(); ?>
		    
			<div class="edgtf-content" <?php onschedule_edge_content_elem_style_attr(); ?>>
	            <div class="edgtf-content-inner">
					<div class="edgtf-page-not-found">
						<?php
							$title_image_404 = onschedule_edge_options()->getOptionValue('404_page_title_image');
							$title_404       = onschedule_edge_options()->getOptionValue('404_title');
							$subtitle_404    = onschedule_edge_options()->getOptionValue('404_subtitle');
							$text_404        = onschedule_edge_options()->getOptionValue('404_text');
							$button_label    = onschedule_edge_options()->getOptionValue('404_back_to_home');
						?>
						
						<?php if (!empty($title_image_404)) { ?>
							<div class="edgtf-404-title-image"><img src="<?php echo esc_url($title_image_404); ?>" alt="<?php esc_attr_e('404 Title Image', 'onschedule'); ?>" /></div>
						<?php } ?>
						
						<h1 class="edgtf-page-not-found-title">
							<?php if(!empty($title_404)) {
								echo esc_html($title_404);
							} else {
								esc_html_e('404', 'onschedule');
							} ?>
						</h1>
						
						<h3 class="edgtf-page-not-found-subtitle">
							<?php if(!empty($subtitle_404)){
								echo esc_html($subtitle_404);
							} else {
								esc_html_e('Page not found', 'onschedule');
							} ?>
						</h3>
						
						<p class="edgtf-page-not-found-text">
							<?php if(!empty($text_404)){
								echo esc_html($text_404);
							} else {
								esc_html_e('Oops! The page you are looking for does not exist. It might have been moved or deleted.', 'onschedule');
							} ?>
						</p>
						
						<?php
							$params = array();
							$params['text'] = !empty($button_label) ? $button_label : esc_html__('BACK TO HOME', 'onschedule');
							$params['link'] = esc_url(home_url('/'));
							$params['target'] = '_self';
							$params['size'] = 'large';

						echo onschedule_edge_execute_shortcode('edgtf_button',$params);?>
					</div>
				</div>	
			</div>
		</div>
	</div>		
	<?php wp_footer(); ?>
</body>
</html>