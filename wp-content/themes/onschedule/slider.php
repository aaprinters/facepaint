<?php
do_action('onschedule_edge_action_before_slider_action');

$onschedule_slider_shortcode = get_post_meta(onschedule_edge_get_page_id(), 'edgtf_page_slider_meta', true);
if (!empty($onschedule_slider_shortcode)) { ?>
	<div class="edgtf-slider">
		<div class="edgtf-slider-inner">
			<?php echo do_shortcode(wp_kses_post($onschedule_slider_shortcode)); // XSS OK ?>
		</div>
	</div>
<?php }

do_action('onschedule_edge_action_after_slider_action');
?>