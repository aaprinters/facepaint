<?php get_header(); ?>
<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php onschedule_edge_get_title(); ?>
		<?php get_template_part('slider'); ?>
		<div class="edgtf-container">
			<?php do_action('onschedule_edge_action_after_container_open'); ?>
			<div class="edgtf-container-inner">
				<?php onschedule_edge_restaurant_get_menu_item_single(); ?>
			</div>
			<?php do_action('onschedule_edge_action_before_container_close'); ?>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
