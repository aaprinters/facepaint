(function($) {
    'use strict';

    var shortcodes = {};

    edgtf.modules.shortcodes = shortcodes;
	shortcodes.edgtfInitParallax = edgtfInitParallax;
	
    shortcodes.edgtfOnDocumentReady = edgtfOnDocumentReady;
    shortcodes.edgtfOnWindowLoad = edgtfOnWindowLoad;
    shortcodes.edgtfOnWindowResize = edgtfOnWindowResize;
    shortcodes.edgtfOnWindowScroll = edgtfOnWindowScroll;

    $(document).ready(edgtfOnDocumentReady);
    $(window).load(edgtfOnWindowLoad);
    $(window).resize(edgtfOnWindowResize);
    $(window).scroll(edgtfOnWindowScroll);

    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function edgtfOnDocumentReady() {
        edgtfInitAccordions();
	    edgtfInitAnimationHolder();
        edgtfInitBlogSlider();
	    edgtfInitBookedCalendar();
        edgtfInitBoxedBlogListHover();
        edgtfButton().init();
	    edgtfInitCalendarResponsiveStyle();
	    edgtfInitClientsCarousel();
	    edgtfInitCountdown();
	    edgtfInitCounter();
	    edgtfInitCustomFontResize();
	    edgtfInitElementsHolderResponsiveStyle();
	    edgtfInitFrameSlider();
	    edgtfShowGoogleMap();
	    edgtfIcon().init();
        edgtfInitIconList().init();
	    edgtfInitImageGallery();
	    edgtfInitImageSlider();
	    edgtfInitImageSliderWidget();
	    edgtfInitInfoCards();
	    edgtfInitInfoCards();
	    edgtfInitItemShowcase();
	    edgtfInitPieChart();
	    edgtfInitProgressBars();
	    edgtfRestaurantDatePicker();
        edgtfSocialIconWidget().init();
	    edgtfInitStackedImages();
        edgtfInitTabs();
	    edgtfInitTestimonials();
	    edgtfInitVerticalSplitSlider();
	    edgtfInstagramCarousel();
	    edgtfTwitterSlider();
	    edgtfInitPricingTables();
	    edgtfImageWithText();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function edgtfOnWindowLoad() {
	    edgtfInitCharts();
	    edgtfInitExpandedImageGallery();
	    edgtfInitParallax();
	    if(edgtf.body.hasClass('wpb-js-composer')) {
		    window.vc_rowBehaviour(); //call vc row behavior on load, this is for parallax on row since it is not loaded after some other shortcodes are loaded
	    }
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function edgtfOnWindowResize() {
	    edgtfInitExpandedImageGallery();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function edgtfOnWindowScroll() {
    }

	/**
	 * Init accordions shortcode
	 */
	function edgtfInitAccordions(){
		var accordion = $('.edgtf-accordion-holder');
		if(accordion.length){
			accordion.each(function(){

				var thisAccordion = $(this);

				if(thisAccordion.hasClass('edgtf-accordion')){

					thisAccordion.accordion({
						animate: "easeInOutQuart",
						collapsible: true,
						active: 0,
						icons: "",
						heightStyle: "content"
					});
				}

				if(thisAccordion.hasClass('edgtf-toggle')){

					var toggleAccordion = $(this);
					var toggleAccordionTitle = toggleAccordion.find('.edgtf-title-holder');
					var toggleAccordionContent = toggleAccordionTitle.next();

					toggleAccordion.addClass("accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset");
					toggleAccordionTitle.addClass("ui-accordion-header ui-state-default ui-corner-top ui-corner-bottom");
					toggleAccordionContent.addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();

					toggleAccordionTitle.each(function(){
						var thisTitle = $(this);
						thisTitle.on('mouseenter mouseleave', function(){
							thisTitle.toggleClass("ui-state-hover");
						});

						thisTitle.on('click',function(){
							thisTitle.toggleClass('ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom');
							thisTitle.next().toggleClass('ui-accordion-content-active').slideToggle(400);
						});
					});
				}
			});
		}
	}
	
	/*
	 *	Init animation holder shortcode
	 */
	function edgtfInitAnimationHolder(){
		
		var elements = $('.edgtf-grow-in, .edgtf-fade-in-down, .edgtf-element-from-fade, .edgtf-element-from-left, .edgtf-element-from-right, .edgtf-element-from-top, .edgtf-element-from-bottom, .edgtf-flip-in, .edgtf-x-rotate, .edgtf-z-rotate, .edgtf-y-translate, .edgtf-fade-in, .edgtf-fade-in-left-x-rotate'),
			animationClass,
			animationData,
			animationDelay;
		
		if(elements.length){
			elements.each(function(){
				var thisElement = $(this);
				
				thisElement.appear(function() {
					animationData = thisElement.data('animation');
					animationDelay = parseInt(thisElement.data('animation-delay'));
					
					if(typeof animationData !== 'undefined' && animationData !== '') {
						animationClass = animationData;
						var newClass = animationClass+'-on';
						
						setTimeout(function(){
							thisElement.addClass(newClass);
						},animationDelay);
					}
				},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
			});
		}
	}

    function edgtfInitBlogSlider() {
        var blogSlider = $('.edgtf-blog-slider-holder .edgtf-blog-slider');

        blogSlider.owlCarousel({
            responsive : {
                0: {
                    loop: true,
                    items: 1,
                    center: false,
                    margin: 0,
                    dots: true,
                    nav: false
                },
                1025: {
                    loop: true,
                    items: 2,
                    startPosition: 1,
                    center: true,
                    margin: 15,
                    dots: true,
                    nav: true,
                    navText: [
                        '<span class="edgtf-prev-icon"><span class="arrow arrow_left"></span></span>',
                        '<span class="edgtf-next-icon"><span class="arrow arrow_right"></span></span>'
                    ]
                }
            }
        });
    }
    
    function edgtfInitBookedCalendar() {
	    var bookedCalendar = $('.edgtf-booked-calendar-holder'),
		    bookedSliderHolder = $('.edgtf-booked-slider-holder');
	    
	    if(bookedSliderHolder.hasClass('edgtf-custom-style') || bookedCalendar.hasClass('edgtf-custom-style')) {
	    	edgtf.body.addClass('edgtf-booked-custom-style');
	    }
	    
	    if(bookedCalendar.length) {
		    bookedCalendar.each(function(){
			    var thisBookedCalendar = $(this).find('.booked-calendar-wrap'),
				    bookedCalendarIsInParallax = thisBookedCalendar.parents('.edgtf-parallax-holder');
			
			    if(bookedCalendarIsInParallax.length) {
				
				    thisBookedCalendar.ajaxComplete(function() {
					    setTimeout(function(){
						    var parallaxHeight = bookedCalendarIsInParallax.children().outerHeight();
						    console.log(parallaxHeight);
						    bookedCalendarIsInParallax.css({'height': parallaxHeight + 'px'});
						    edgtfInitParallax();
					    }, 1000);
				    });
			    }
		    });
	    }
    }

	function edgtfInitBoxedBlogListHover() {
		var boxed = $('.edgtf-bl-boxed');

		if( boxed.length ) {
			var title = $('.edgtf-post-title');
			var image = $('.edgtf-post-image');

			title.on('mouseenter', function(){
				if( ! image.hasClass('edgtf-active') ) {
					image.addClass('edgtf-active');
				}
			});

			title.on('mouseleave', function(){
				if( image.hasClass('edgtf-active') ) {
					image.removeClass('edgtf-active');
				}
			});
		}
	}

	/**
	 * Button object that initializes whole button functionality
	 * @type {Function}
	 */
	var edgtfButton = edgtf.modules.shortcodes.edgtfButton = function() {
		//all buttons on the page
		var buttons = $('.edgtf-btn');

		/**
		 * Initializes button hover color
		 * @param button current button
		 */
		var buttonHoverColor = function(button) {
			if(typeof button.data('hover-color') !== 'undefined') {
				var changeButtonColor = function(event) {
					event.data.button.css('color', event.data.color);
				};

				var originalColor = button.css('color');
				var hoverColor = button.data('hover-color');

				button.on('mouseenter', { button: button, color: hoverColor }, changeButtonColor);
				button.on('mouseleave', { button: button, color: originalColor }, changeButtonColor);
			}
		};

		/**
		 * Initializes button hover background color
		 * @param button current button
		 */
		var buttonHoverBgColor = function(button) {
			if(typeof button.data('hover-bg-color') !== 'undefined') {
				var changeButtonBg = function(event) {
					event.data.button.css('background-color', event.data.color);
				};

				var originalBgColor = button.css('background-color');
				var hoverBgColor = button.data('hover-bg-color');

				button.on('mouseenter', { button: button, color: hoverBgColor }, changeButtonBg);
				button.on('mouseleave', { button: button, color: originalBgColor }, changeButtonBg);
			}
		};

		/**
		 * Initializes button border color
		 * @param button
		 */
		var buttonHoverBorderColor = function(button) {
			if(typeof button.data('hover-border-color') !== 'undefined') {
				var changeBorderColor = function(event) {
					event.data.button.css('border-color', event.data.color);
				};

				var originalBorderColor = button.css('borderTopColor'); //take one of the four sides
				var hoverBorderColor = button.data('hover-border-color');

				button.on('mouseenter', { button: button, color: hoverBorderColor }, changeBorderColor);
				button.on('mouseleave', { button: button, color: originalBorderColor }, changeBorderColor);
			}
		};

		return {
			init: function() {
				if(buttons.length) {
					buttons.each(function() {
						buttonHoverColor($(this));
						buttonHoverBgColor($(this));
						buttonHoverBorderColor($(this));
					});
				}
			}
		};
	};

	/*
	 **	Elements Holder responsive style
	 */
	function edgtfInitCalendarResponsiveStyle(){

		var calendar = $('.edgtf-bs-calendar-content');
		var n = 1;

		if(calendar.length){
			calendar.each(function() {
				var thisCalendar = $(this),
					side = $(this).hasClass('edgtf-right-position') ? 'right' : 'left',
					itemClass = 'edgtf-bs-calendar-content'+n,
					style = '',
					responsiveStyle = '',
					laptopWidth = '',
					laptopTop = '',
					laptopSide = '',
					ipadWidthLand = '',
					ipadTopLand = '',
					ipadSideLand = '',
					ipadWidth = '',
					ipadTop = '',
					ipadSide = '',
					mobileWidth = '',
					mobileTop = '',
					mobileSide = '';

				$(this).addClass(itemClass);

				if (typeof thisCalendar.data('width-laptop') !== 'undefined' && thisCalendar.data('width-laptop') !== false) {
					laptopWidth = thisCalendar.data('width-laptop');
				}
				if (typeof thisCalendar.data('top-offset-laptop') !== 'undefined' && thisCalendar.data('top-offset-laptop') !== false) {
					laptopTop = thisCalendar.data('top-offset-laptop');
				}
				if (typeof thisCalendar.data('side-offset-laptop') !== 'undefined' && thisCalendar.data('side-offset-laptop') !== false) {
					laptopSide = thisCalendar.data('side-offset-laptop');
				}
				if (typeof thisCalendar.data('width-ipad-landscape') !== 'undefined' && thisCalendar.data('width-ipad-landscape') !== false) {
					ipadWidthLand = thisCalendar.data('width-ipad-landscape');
				}
				if (typeof thisCalendar.data('top-offset-ipad-landscape') !== 'undefined' && thisCalendar.data('top-offset-ipad-landscape') !== false) {
					ipadTopLand = thisCalendar.data('top-offset-ipad-landscape');
				}
				if (typeof thisCalendar.data('side-offset-ipad-landscape') !== 'undefined' && thisCalendar.data('side-offset-ipad-landscape') !== false) {
					ipadSideLand = thisCalendar.data('side-offset-ipad-landscape');
				}
				if (typeof thisCalendar.data('width-ipad') !== 'undefined' && thisCalendar.data('width-ipad') !== false) {
					ipadWidth = thisCalendar.data('width-ipad');
				}
				if (typeof thisCalendar.data('top-offset-ipad') !== 'undefined' && thisCalendar.data('top-offset-ipad') !== false) {
					ipadTop = thisCalendar.data('top-offset-ipad');
				}
				if (typeof thisCalendar.data('side-offset-ipad') !== 'undefined' && thisCalendar.data('side-offset-ipad') !== false) {
					ipadSide = thisCalendar.data('side-offset-ipad');
				}
				if (typeof thisCalendar.data('width-mobile') !== 'undefined' && thisCalendar.data('width-mobile') !== false) {
					mobileWidth = thisCalendar.data('width-mobile');
				}
				if (typeof thisCalendar.data('top-offset-mobile') !== 'undefined' && thisCalendar.data('top-offset-mobile') !== false) {
					mobileTop = thisCalendar.data('top-offset-mobile');
				}
				if (typeof thisCalendar.data('side-offset-mobile') !== 'undefined' && thisCalendar.data('side-offset-mobile') !== false) {
					mobileSide = thisCalendar.data('side-offset-mobile');
				}

				if(laptopWidth.length || laptopTop.length || laptopSide.length || ipadWidthLand.length || ipadTopLand.length || ipadSideLand.length || ipadWidth.length || ipadTop.length || ipadSide.length || mobileWidth.length || mobileTop.length || mobileSide.length) {

					if (laptopWidth.length) {
						responsiveStyle += "@media only screen and (max-width: 1280px) {.edgtf-bs-calendar-content." + itemClass + " { width: " + laptopWidth + " !important; } }";
					}
					if (laptopTop.length) {
						responsiveStyle += "@media only screen and (max-width: 1280px) {.edgtf-bs-calendar-content." + itemClass + " { top: " + laptopTop + " !important; } }";
					}
					if (laptopSide.length) {
						responsiveStyle += "@media only screen and (max-width: 1280px) {.edgtf-bs-calendar-content." + itemClass + " { "+side+": " + laptopSide + " !important; } }";
					}
					if (ipadWidthLand.length) {
						responsiveStyle += "@media only screen and (max-width: 1024px) {.edgtf-bs-calendar-content." + itemClass + " { width: " + ipadWidthLand + " !important; } }";
					}
					if (ipadTopLand.length) {
						responsiveStyle += "@media only screen and (max-width: 1024px) {.edgtf-bs-calendar-content." + itemClass + " { top: " + ipadTopLand + " !important; } }";
					}
					if (ipadSideLand.length) {
						responsiveStyle += "@media only screen and (max-width: 1024px) {.edgtf-bs-calendar-content." + itemClass + " { "+side+": " + ipadSideLand + " !important; } }";
					}
					if (ipadWidth.length) {
						responsiveStyle += "@media only screen and (max-width: 768px) {.edgtf-bs-calendar-content." + itemClass + " { width: " + ipadWidth + " !important; } }";
					}
					if (ipadTop.length) {
						responsiveStyle += "@media only screen and (max-width: 768px) {.edgtf-bs-calendar-content." + itemClass + " { top: " + ipadTop + " !important; } }";
					}
					if (ipadSide.length) {
						responsiveStyle += "@media only screen and (max-width: 768px) {.edgtf-bs-calendar-content." + itemClass + " { "+side+": " + ipadSide + " !important; } }";
					}
					if (mobileWidth.length) {
						responsiveStyle += "@media only screen and (max-width: 600px) {.edgtf-bs-calendar-content." + itemClass + " { width: " + mobileWidth + " !important; } }";
					}
					if (mobileTop.length) {
						responsiveStyle += "@media only screen and (max-width: 600px) {.edgtf-bs-calendar-content." + itemClass + " { top: " + mobileTop + " !important; } }";
					}
					if (mobileSide.length) {
						responsiveStyle += "@media only screen and (max-width: 600px) {.edgtf-bs-calendar-content." + itemClass + " { "+side+": " + mobileSide + " !important; } }";
					}
				}

				if(responsiveStyle.length) {
					style = '<style type="text/css" data-type="onschedule_edge_style_shortcodes_custom_css">'+responsiveStyle+'</style>';
				}

				if(style.length) {
					$('head').append(style);
				}

				n++;
			});
		}
	}

	// init charts shortcode
	function edgtfInitCharts() {
		var chartHolder = $('.edgtf-charts');

		if (chartHolder.length) {
			chartHolder.each(function () {
				var thisChartHolder = $(this);
				var thisChartCanvasId = thisChartHolder.find('canvas').attr('id');

				thisChartHolder.height(thisChartHolder.width() / 2);

				//////////////////////////////////////////////////////////////////////////////
				// prep vars from data atts

				var chartType = thisChartHolder.data('type'),
					noOfDatasets = thisChartHolder.data('no_of_used_datasets'),
					pointGroupLabels = thisChartHolder.data('point_group_labels'),
					colorScheme = '',
					legendDisplay = thisChartHolder.data('legend_display'),
					legendPosition = thisChartHolder.data('legend_position'),
					startAtZero = '';

				if (chartType == 'line' || chartType == 'horizontalBar' || chartType == 'bar') {
					startAtZero = {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}],
						xAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					};
				}

				if(legendDisplay == 'hide') {
					legendDisplay = false;
				} else {
					legendDisplay = true;
				}

				//////////////////////////////////////////////////////////////////////////////

				var pointGroupColors = '',
					dataset_1,
					dataset_1_color,
					dataset_2,
					dataset_2_color,
					dataset_3,
					dataset_3_color,
					datasets;

				if (thisChartHolder.data('color_scheme') == 'dataset') {
					dataset_1_color = thisChartHolder.data('dataset_1_color');
				}
				else {
					dataset_1_color = thisChartHolder.data('point_group_colors').split(',');
				}

				dataset_1 = {
					label: thisChartHolder.data('dataset_1_label'),
					backgroundColor: dataset_1_color,
					data: thisChartHolder.data('dataset_1').split(','),
					cubicInterpolationMode: 'monotone',
				};

				datasets = [dataset_1];

				if (noOfDatasets >= 2) {
					if (thisChartHolder.data('color_scheme') == 'dataset') {
						dataset_2_color = thisChartHolder.data('dataset_2_color');
					}
					else {
						dataset_2_color = thisChartHolder.data('point_group_colors').split(',');
					}

					dataset_2 = {
						label: thisChartHolder.data('dataset_2_label'),
						backgroundColor: dataset_2_color,
						data: thisChartHolder.data('dataset_2').split(','),
						cubicInterpolationMode: 'monotone'
					};

					datasets = [dataset_1, dataset_2];
				}

				if (noOfDatasets >= 3) {
					if (thisChartHolder.data('color_scheme') == 'dataset') {
						dataset_3_color = thisChartHolder.data('dataset_3_color');
					}
					else {
						dataset_3_color = thisChartHolder.data('point_group_colors').split(',');
					}

					dataset_3 = {
						label: thisChartHolder.data('dataset_3_label'),
						backgroundColor: dataset_3_color,
						data: thisChartHolder.data('dataset_3').split(','),
						cubicInterpolationMode: 'monotone'
					};

					datasets = [dataset_1, dataset_2, dataset_3];
				}

				// there is probably better way of doing init than the following one
				var thisChartParams = {
					labels: pointGroupLabels.split(','),
					datasets: datasets
				};

				//////////////////////////////////////////////////////////////////////////////

				var ctx = document.getElementById(thisChartCanvasId).getContext('2d');


				thisChartHolder.appear(function () {
					thisChartHolder.addClass('edgtf-appeared');

					setTimeout(function () {

						window.myBar = new Chart(ctx, {
							type: chartType,
							data: thisChartParams,
							options: {
								responsive: true,
								legend: {
									display: legendDisplay,
									position: legendPosition,
								},
								scales: startAtZero,
							},
						});


					}, 500);
				}, {accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});


			});
		}
	}
	
	/**
	 * Init clients carousel shortcode
	 */
	function edgtfInitClientsCarousel(){
		
		var carouselHolder = $('.edgtf-clients-carousel-holder');
		
		if(carouselHolder.length){
			carouselHolder.each(function(){
				
				var thisCarouselHolder = $(this),
					thisCarousel = thisCarouselHolder.children('.edgtf-cc-inner'),
					numberOfItems = 4,
					autoplay = true,
					autoplayTimeout = 5000,
					loop = true,
					speed = 650;
				
				if (typeof thisCarouselHolder.data('number-of-items') !== 'undefined' && thisCarouselHolder.data('number-of-items') !== false) {
					numberOfItems = parseInt(thisCarouselHolder.data('number-of-items'));
				}
				
				if (typeof thisCarouselHolder.data('autoplay') !== 'undefined' && thisCarouselHolder.data('autoplay') !== false) {
					autoplay = thisCarouselHolder.data('autoplay');
				}
				
				if (typeof thisCarouselHolder.data('autoplay-timeout') !== 'undefined' && thisCarouselHolder.data('autoplay-timeout') !== false) {
					autoplayTimeout = thisCarouselHolder.data('autoplay-timeout');
				}
				
				if (typeof thisCarouselHolder.data('loop') !== 'undefined' && thisCarouselHolder.data('loop') !== false) {
					loop = thisCarouselHolder.data('loop');
				}
				
				if (typeof thisCarouselHolder.data('speed') !== 'undefined' && thisCarouselHolder.data('speed') !== false) {
					speed = thisCarouselHolder.data('speed');
				}
				
				if(numberOfItems === 1) {
					autoplay = false;
					loop = false;
				}

				var responsiveNumberOfItems1 = 1,
					responsiveNumberOfItems2 = 2,
					responsiveNumberOfItems3 = 3;
				
				if (numberOfItems < 3) {
					responsiveNumberOfItems2 = numberOfItems;
					responsiveNumberOfItems3 = numberOfItems;
				}
				
				thisCarousel.owlCarousel({
					items: numberOfItems,
					autoplay: autoplay,
					autoplayTimeout: autoplayTimeout,
					loop: loop,
					smartSpeed: speed,
					nav: false,
					dots: false,
					responsive: {
						0: {
							items: responsiveNumberOfItems1,
						},
						600: {
							items: responsiveNumberOfItems2
						},
						768: {
							items: responsiveNumberOfItems3,
						},
						1025: {
							items: numberOfItems
						}
					}
				});
				
				thisCarousel.css({'visibility': 'visible'});
			});
		}
	}
	
	/**
	 * Countdown Shortcode
	 */
	function edgtfInitCountdown() {
		
		var countdowns = $('.edgtf-countdown'),
			year,
			month,
			day,
			hour,
			minute,
			timezone,
			monthLabel,
			dayLabel,
			hourLabel,
			minuteLabel,
			secondLabel;
		
		if (countdowns.length) {
			countdowns.each(function(){
				//Find countdown elements by id-s
				var countdownId = $(this).attr('id'),
					countdown = $('#'+countdownId),
					digitFontSize,
					labelFontSize;
				
				//Get data for countdown
				year = countdown.data('year');
				month = countdown.data('month');
				day = countdown.data('day');
				hour = countdown.data('hour');
				minute = countdown.data('minute');
				timezone = countdown.data('timezone');
				monthLabel = countdown.data('month-label');
				dayLabel = countdown.data('day-label');
				hourLabel = countdown.data('hour-label');
				minuteLabel = countdown.data('minute-label');
				secondLabel = countdown.data('second-label');
				digitFontSize = countdown.data('digit-size');
				labelFontSize = countdown.data('label-size');
				
				//Initialize countdown
				countdown.countdown({
					until: new Date(year, month - 1, day, hour, minute, 44),
					labels: ['Years', monthLabel, 'Weeks', dayLabel, hourLabel, minuteLabel, secondLabel],
					format: 'ODHMS',
					timezone: timezone,
					padZeroes: true,
					onTick: setCountdownStyle
				});
				
				function setCountdownStyle() {
					countdown.find('.countdown-amount').css({
						'font-size' : digitFontSize+'px',
						'line-height' : digitFontSize+'px'
					});
					countdown.find('.countdown-period').css({
						'font-size' : labelFontSize+'px'
					});
				}
			});
		}
	}
	
	/**
	 * Counter Shortcode
	 */
	function edgtfInitCounter() {
		var counterHolder = $('.edgtf-counter-holder');
		
		if (counterHolder.length) {
			counterHolder.each(function() {
				var thisCounterHolder = $(this),
					thisCounter = thisCounterHolder.find('.edgtf-counter');
				
				thisCounterHolder.appear(function() {
					thisCounterHolder.css('opacity', '1');
					
					//Counter zero type
					if (thisCounter.hasClass('edgtf-zero-counter')) {
						var max = parseFloat(thisCounter.text());
						thisCounter.countTo({
							from: 0,
							to: max,
							speed: 1500,
							refreshInterval: 100
						});
					} else {
						thisCounter.absoluteCounter({
							speed: 2000,
							fadeInDelay: 1000
						});
					}
				},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
			});
		}
	}
	
	/*
	 **	Custom Font shortcode resizing
	 */
	function edgtfInitCustomFontResize(){
		var customFont = $('.edgtf-custom-font-holder');
		
		if (customFont.length){
			customFont.each(function(){
				var thisCustomFont = $(this);
				var fontSize;
				var lineHeight;
				var coef1 = 1;
				var coef2 = 1;
				
				if (edgtf.windowWidth < 1200){
					coef1 = 0.8;
				}
				
				if (edgtf.windowWidth < 1024){
					coef1 = 0.7;
				}
				
				if (edgtf.windowWidth < 768){
					coef1 = 0.6;
					coef2 = 0.7;
				}
				
				if (edgtf.windowWidth < 600){
					coef1 = 0.5;
					coef2 = 0.6;
				}
				
				if (edgtf.windowWidth < 480){
					coef1 = 0.4;
					coef2 = 0.5;
				}
				
				if (typeof thisCustomFont.data('font-size') !== 'undefined' && thisCustomFont.data('font-size') !== false) {
					fontSize = parseInt(thisCustomFont.data('font-size'));
					
					if (fontSize > 70) {
						fontSize = Math.round(fontSize*coef1);
					}
					else if (fontSize > 35) {
						fontSize = Math.round(fontSize*coef2);
					}
					
					thisCustomFont.css('font-size', fontSize + 'px');
				}
				
				if (typeof thisCustomFont.data('line-height') !== 'undefined' && thisCustomFont.data('line-height') !== false) {
					lineHeight = parseInt(thisCustomFont.data('line-height'));
					
					if (lineHeight > 70 && edgtf.windowWidth < 1200) {
						lineHeight = '1.2em';
					}
					else if (lineHeight > 35 && edgtf.windowWidth < 768) {
						lineHeight = '1.2em';
					}
					else{
						lineHeight += 'px';
					}
					
					thisCustomFont.css('line-height', lineHeight);
				}
			});
		}
	}
	
	/*
	 **	Elements Holder responsive style
	 */
	function edgtfInitElementsHolderResponsiveStyle(){

		var elementsHolder = $('.edgtf-elements-holder');

		if(elementsHolder.length){
			elementsHolder.each(function() {
				var thisElementsHolder = $(this),
					elementsHolderItem = thisElementsHolder.children('.edgtf-eh-item'),
					style = '',
					responsiveStyle = '';

				elementsHolderItem.each(function() {
					var thisItem = $(this),
						itemClass = '',
						largeLaptop = '',
						smallLaptop = '',
						ipadLandscape = '',
						ipadPortrait = '',
						mobileLandscape = '',
						mobilePortrait = '';

					if (typeof thisItem.data('item-class') !== 'undefined' && thisItem.data('item-class') !== false) {
						itemClass = thisItem.data('item-class');
					}
					if (typeof thisItem.data('1280-1600') !== 'undefined' && thisItem.data('1280-1600') !== false) {
						largeLaptop = thisItem.data('1280-1600');
					}
					if (typeof thisItem.data('1024-1280') !== 'undefined' && thisItem.data('1024-1280') !== false) {
						smallLaptop = thisItem.data('1024-1280');
					}
					if (typeof thisItem.data('768-1024') !== 'undefined' && thisItem.data('768-1024') !== false) {
						ipadLandscape = thisItem.data('768-1024');
					}
					if (typeof thisItem.data('600-768') !== 'undefined' && thisItem.data('600-768') !== false) {
						ipadPortrait = thisItem.data('600-768');
					}
					if (typeof thisItem.data('480-600') !== 'undefined' && thisItem.data('480-600') !== false) {
						mobileLandscape = thisItem.data('480-600');
					}
					if (typeof thisItem.data('480') !== 'undefined' && thisItem.data('480') !== false) {
						mobilePortrait = thisItem.data('480');
					}

					if(largeLaptop.length || smallLaptop.length || ipadLandscape.length || ipadPortrait.length || mobileLandscape.length || mobilePortrait.length) {

						if(largeLaptop.length) {
							responsiveStyle += "@media only screen and (min-width: 1280px) and (max-width: 1600px) {.edgtf-eh-item-content."+itemClass+" { padding: "+largeLaptop+" !important; } }";
						}
						if(smallLaptop.length) {
							responsiveStyle += "@media only screen and (min-width: 1024px) and (max-width: 1280px) {.edgtf-eh-item-content."+itemClass+" { padding: "+smallLaptop+" !important; } }";
						}
						if(ipadLandscape.length) {
							responsiveStyle += "@media only screen and (min-width: 768px) and (max-width: 1024px) {.edgtf-eh-item-content."+itemClass+" { padding: "+ipadLandscape+" !important; } }";
						}
						if(ipadPortrait.length) {
							responsiveStyle += "@media only screen and (min-width: 600px) and (max-width: 768px) {.edgtf-eh-item-content."+itemClass+" { padding: "+ipadPortrait+" !important; } }";
						}
						if(mobileLandscape.length) {
							responsiveStyle += "@media only screen and (min-width: 480px) and (max-width: 600px) {.edgtf-eh-item-content."+itemClass+" { padding: "+mobileLandscape+" !important; } }";
						}
						if(mobilePortrait.length) {
							responsiveStyle += "@media only screen and (max-width: 480px) {.edgtf-eh-item-content."+itemClass+" { padding: "+mobilePortrait+" !important; } }";
						}
					}
				});

				if(responsiveStyle.length) {
					style = '<style type="text/css" data-type="onschedule_edge_style_shortcodes_custom_css">'+responsiveStyle+'</style>';
				}

				if(style.length) {
					$('head').append(style);
				}
			});
		}
	}
	
	/**
	 * Init expanded image gallery shortcode
	 */
	function edgtfInitExpandedImageGallery() {
		var holder = $('.edgtf-expanded-image-gallery');
		
		if (holder.length) {
			holder.each(function () {
				var thisHolder = $(this),
					filterHolder = thisHolder.children('.edgtf-eig-filter'),
					standardView = filterHolder.children('.edgtf-eig-standard-view'),
					openView = filterHolder.children('.edgtf-eig-open-view'),
					innerHolder = thisHolder.children('.edgtf-eig-grid'),
					imageWrapper = innerHolder.find('.edgtf-eig-image-inner'),
					imageHolder = imageWrapper.find('img'),
					imageHeight = imageHolder.length ? parseInt(imageHolder.outerHeight()) : 0,
					imageSmallHeight = 0,
					expandingImageSpeed = 500;
				
				if (typeof innerHolder.data('image-height') !== 'undefined' && innerHolder.data('image-height') !== false) {
					imageSmallHeight = parseInt(innerHolder.data('image-height'));
				}
				
				if (typeof innerHolder.data('image-speed') !== 'undefined' && innerHolder.data('image-speed') !== false) {
					expandingImageSpeed = parseInt(innerHolder.data('image-speed'));
				}
				
				if (imageSmallHeight > 0 && imageHeight > 0 && imageSmallHeight < imageHeight) {
					imageWrapper.css({'height': imageSmallHeight + 'px'});
					
					standardView.on('click', function (e) {
						e.preventDefault();
						
						filterHolder.children().removeClass('edgtf-eig-active');
						$(this).addClass('edgtf-eig-active');
						
						imageWrapper.stop().animate({'height': imageSmallHeight + 'px'}, expandingImageSpeed, 'easeOutCubic', function(){
							edgtfInitParallax();
						});
					});
					
					openView.on('click', function (e) {
						e.preventDefault();
						
						filterHolder.children().removeClass('edgtf-eig-active');
						$(this).addClass('edgtf-eig-active');
						
						imageWrapper.stop().animate({'height': imageHeight + 'px'}, expandingImageSpeed, 'easeInOutBack', function(){
							edgtfInitParallax();
						});
					});
				}
				
				thisHolder.css({'visibility': 'visible'});

				if (innerHolder.hasClass('edgtf-eig-appear-effect') && !edgtf.htmlEl.hasClass('touch')) {
					thisHolder.appear(function(){
						var imageElements = thisHolder.find('.edgtf-eig-image-inner'),
							filterClicked = false;


						imageElements.each(function(i){
							var imageElement = $(this);

							setTimeout(function(){
								imageElement.addClass('edgtf-appeared');

								setTimeout(function(){
									imageElement.addClass('edgtf-animated');
								}, 1100)
							}, i*100);
						});

						filterHolder.find('a').on('click', function(){
							if (!filterClicked) {
								imageElements.each(function(){
									var imageElement = $(this);

									if(!imageElement.hasClass('edgtf-appeared')) {
										imageElement.addClass('edgtf-appeared edgtf-animated');
									}
								});
								filterClicked = true;
							}
						});
                	}, {accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
				}

			});
		}
	}
	
	/*
	 **	Init Frame Slider shortcode
	 */
	function edgtfInitFrameSlider() {
		var sliders = $('.edgtf-frame-slider-holder');
		
		sliders.each(function() {
			var slider = $(this),
				autoplay = true,
				autoplayTimeout = 3500;
			
			if(typeof slider.data('autoplay') !== 'undefined' && slider.data('autoplay') === 'no'){
				autoplay = false;
			}
			
			if(typeof slider.data('autoplay-speed') !== 'undefined' && slider.data('autoplay-speed') !== ''){
				autoplayTimeout = slider.data('autoplay-speed');
			}
			
			var owlConfig = {
				items: 1,
				autoplay: autoplay,
				autoplayTimeout: autoplayTimeout,
				autoplayHoverPause: false,
				smartSpeed: 100,
				animateOut: 'fadeOut',
				animateIn: 'fadeIn',
				loop: true,
				dots: false,
				nav: false,
				mouseDrag: false,
				touchDrag: false,
				pullDrag: false,
				freeDrag: false
			};
			
			sliders.waitForImages(function () {
				slider.find('.edgtf-fs-desktop-images').owlCarousel(owlConfig);
				slider.find('.edgtf-fs-mobile-images').owlCarousel(owlConfig);
				
				slider.css('visibility', 'visible');
			});
		});
	}
	
	/*
	 **	Show Google Map
	 */
	function edgtfShowGoogleMap(){
		var googleMap = $('.edgtf-google-map');
		
		if(googleMap.length){
			googleMap.each(function(){
				var element = $(this);
				
				var customMapStyle;
				if(typeof element.data('custom-map-style') !== 'undefined') {
					customMapStyle = element.data('custom-map-style');
				}
				
				var colorOverlay;
				if(typeof element.data('color-overlay') !== 'undefined' && element.data('color-overlay') !== false) {
					colorOverlay = element.data('color-overlay');
				}
				
				var saturation;
				if(typeof element.data('saturation') !== 'undefined' && element.data('saturation') !== false) {
					saturation = element.data('saturation');
				}
				
				var lightness;
				if(typeof element.data('lightness') !== 'undefined' && element.data('lightness') !== false) {
					lightness = element.data('lightness');
				}
				
				var zoom;
				if(typeof element.data('zoom') !== 'undefined' && element.data('zoom') !== false) {
					zoom = element.data('zoom');
				}
				
				var pin;
				if(typeof element.data('pin') !== 'undefined' && element.data('pin') !== false) {
					pin = element.data('pin');
				}
				
				var mapHeight;
				if(typeof element.data('height') !== 'undefined' && element.data('height') !== false) {
					mapHeight = element.data('height');
				}
				
				var uniqueId;
				if(typeof element.data('unique-id') !== 'undefined' && element.data('unique-id') !== false) {
					uniqueId = element.data('unique-id');
				}
				
				var scrollWheel;
				if(typeof element.data('scroll-wheel') !== 'undefined') {
					scrollWheel = element.data('scroll-wheel');
				}
				var addresses;
				if(typeof element.data('addresses') !== 'undefined' && element.data('addresses') !== false) {
					addresses = element.data('addresses');
				}
				
				var map = "map_"+ uniqueId;
				var geocoder = "geocoder_"+ uniqueId;
				var holderId = "edgtf-map-"+ uniqueId;
				
				edgtfInitializeGoogleMap(customMapStyle, colorOverlay, saturation, lightness, scrollWheel, zoom, holderId, mapHeight, pin,  map, geocoder, addresses);
			});
		}
	}
	
	/*
	 **	Init Google Map
	 */
	function edgtfInitializeGoogleMap(customMapStyle, color, saturation, lightness, wheel, zoom, holderId, height, pin,  map, geocoder, data){
		
		var mapStyles = [
			{
				stylers: [
					{hue: color },
					{saturation: saturation},
					{lightness: lightness},
					{gamma: 1}
				]
			}
		];

		var googleMapStyleId;
		
		if(customMapStyle === 'yes'){
			googleMapStyleId = 'edgtf-style';
		} else {
			googleMapStyleId = google.maps.MapTypeId.ROADMAP;
		}
		
		if(wheel === 'yes'){
			wheel = true;
		} else {
			wheel = false;
		}
		
		var qoogleMapType = new google.maps.StyledMapType(mapStyles,
			{name: "Edge Google Map"});
		
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(-34.397, 150.644);
		
		if (!isNaN(height)){
			height = height + 'px';
		}

		var myOptions = {
			zoom: zoom,
			scrollwheel: wheel,
			center: latlng,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.RIGHT_CENTER
			},
			scaleControl: false,
			scaleControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			streetViewControl: false,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeControl: false,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'edgtf-style'],
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeId: googleMapStyleId
		};
		
		map = new google.maps.Map(document.getElementById(holderId), myOptions);
		map.mapTypes.set('edgtf-style', qoogleMapType);
		
		var index;
		
		for (index = 0; index < data.length; ++index) {
			edgtfInitializeGoogleAddress(data[index], pin, map, geocoder);
		}
		
		var holderElement = document.getElementById(holderId);
		holderElement.style.height = height;
	}
	
	/*
	 **	Init Google Map Addresses
	 */
	function edgtfInitializeGoogleAddress(data, pin, map, geocoder){
		if (data === '') {
			return;
		}
		
		var contentString = '<div id="content">'+
			'<div id="siteNotice">'+
			'</div>'+
			'<div id="bodyContent">'+
			'<p>'+data+'</p>'+
			'</div>'+
			'</div>';
		
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		
		geocoder.geocode( { 'address': data}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location,
					icon:  pin,
					title: data['store_title']
				});
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map,marker);
				});
				
				google.maps.event.addDomListener(window, 'resize', function() {
					map.setCenter(results[0].geometry.location);
				});
			}
		});
	}

    /**
     * Object that represents icon shortcode
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var edgtfIcon = edgtf.modules.shortcodes.edgtfIcon = function() {
        //get all icons on page
        var icons = $('.edgtf-icon-shortcode');

        /**
         * Function that triggers icon animation and icon animation delay
         */
        var iconAnimation = function(icon) {
            if(icon.hasClass('edgtf-icon-animation')) {
                icon.appear(function() {
                    icon.parent('.edgtf-icon-animation-holder').addClass('edgtf-icon-animation-show');
                }, {accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
            }
        };

        /**
         * Function that triggers icon hover color functionality
         */
        var iconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon.find('.edgtf-icon-element');
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        /**
         * Function that triggers icon holder background color hover functionality
         */
        var iconHolderBackgroundHover = function(icon) {
            if(typeof icon.data('hover-background-color') !== 'undefined') {
                var changeIconBgColor = function(event) {
                    event.data.icon.css('background-color', event.data.color);
                };

                var hoverBackgroundColor = icon.data('hover-background-color');
                var originalBackgroundColor = icon.css('background-color');

                if(hoverBackgroundColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBackgroundColor}, changeIconBgColor);
                    icon.on('mouseleave', {icon: icon, color: originalBackgroundColor}, changeIconBgColor);
                }
            }
        };

        /**
         * Function that initializes icon holder border hover functionality
         */
        var iconHolderBorderHover = function(icon) {
            if(typeof icon.data('hover-border-color') !== 'undefined') {
                var changeIconBorder = function(event) {
                    event.data.icon.css('border-color', event.data.color);
                };

                var hoverBorderColor = icon.data('hover-border-color');
                var originalBorderColor = icon.css('border-color');

                if(hoverBorderColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBorderColor}, changeIconBorder);
                    icon.on('mouseleave', {icon: icon, color: originalBorderColor}, changeIconBorder);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        iconAnimation($(this));
                        iconHoverColor($(this));
                        iconHolderBackgroundHover($(this));
                        iconHolderBorderHover($(this));
                    });
                }
            }
        };
    };

	/**
	 * Button object that initializes icon list with animation
	 * @type {Function}
	 */
	var edgtfInitIconList = edgtf.modules.shortcodes.edgtfInitIconList = function() {
		var iconList = $('.edgtf-animate-list');

		/**
		 * Initializes icon list animation
		 * @param list current slider
		 */
		var iconListInit = function(list) {
			setTimeout(function(){
				list.appear(function(){
					list.addClass('edgtf-appeared');
				},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
			},30);
		};

		return {
			init: function() {
				if(iconList.length) {
					iconList.each(function() {
						iconListInit($(this));
					});
				}
			}
		};
	};
	
	/**
	 * Init image gallery shortcode
	 */
	function edgtfInitImageGallery() {
		
		var galleries = $('.edgtf-image-gallery');
		
		if (galleries.length) {
			galleries.each(function () {
				var gallery = $(this).find('.edgtf-ig-slider'),
					numberOfItems = gallery.data('number-of-visible-items'),
					autoplay = gallery.data('autoplay'),
					animation = (gallery.data('animation') === 'slide') ? false : gallery.data('animation'),
					navigation = (gallery.data('navigation') === 'yes'),
					pagination = (gallery.data('pagination') === 'yes');
				
				//Responsive breakpoints
				var items = numberOfItems;
				
				var responsiveNumberOfItems1 = 1,
					responsiveNumberOfItems2 = 2,
					responsiveNumberOfItems3 = 3;
				
				if (items < 3) {
					responsiveNumberOfItems2 = items;
					responsiveNumberOfItems3 = items;
				}
				
				gallery.owlCarousel({
					autoplay: true,
					autoplayTimeout: autoplay * 1000,
				    autoplayHoverPause: true,
					loop: true,
					smartSpeed: 600,
					animateIn : animation, //fade, fadeUp, backSlide, goDown
					nav: navigation,
					dots: pagination,
					navText: [
						'<span class="edgtf-prev-icon"><span class="edgtf-icon-arrow ion-ios-arrow-left"></span></span>',
						'<span class="edgtf-next-icon"><span class="edgtf-icon-arrow ion-ios-arrow-right"></span></span>'
					],
					responsive:{
						0: {
							items: responsiveNumberOfItems1,
						},
						680: {
							items: responsiveNumberOfItems2
						},
						769: {
							items: responsiveNumberOfItems3
						},
						1025: {
							items: items
						}
					},
					onTranslate:function() {
			        	gallery.find(".owl-item a").css("z-index",-1);
			    	},
			    	onTranslated: function() {
			    	    gallery.find(".owl-item a").css("z-index",0);
			    	}
				});
				
				gallery.css({'visibility': 'visible'});
			});
		}
	}
	
	/**
	 * Init image slider shortcode
	 */
	function edgtfInitImageSlider() {
		var sliderHolder = $('.edgtf-image-slider-holder');
		
		if (sliderHolder.length) {
			sliderHolder.each(function () {
				var slider = $(this).find('.edgtf-is-slider-inner');
				
				slider.owlCarousel({
					items: 1,
					autoplay: true,
					autoplayTimeout: 6000,
					loop: true,
					smartSpeed: 600,
					animateIn : 'fade',
					nav: true,
					dots: false,
					navText: [
						'<span class="edgtf-prev-icon"><span class="edgtf-icon-arrow ion-ios-arrow-left"></span></span>',
						'<span class="edgtf-next-icon"><span class="edgtf-icon-arrow ion-ios-arrow-right"></span></span>'
					]
				});
				
				slider.css({'visibility': 'visible'});
			});
		}
	}
	
	/**
	 * Init image slider widget
	 */
	function edgtfInitImageSliderWidget() {
		var sliderHolder = $('.edgtf-image-slider-widget');
		
		if (sliderHolder.length) {
			sliderHolder.each(function () {
				var slider = $(this).find('.edgtf-is-widget-inner');
				
				slider.owlCarousel({
					items: 1,
					autoplay: true,
					autoplayTimeout: 4000,
					loop: true,
					smartSpeed: 600,
					nav: false,
					dots: true
				});
				
				slider.css({'visibility': 'visible'});
			});
		}
	}
	
	/*
	 **	Info Cards shortcode style
	 */
	function edgtfInitInfoCards(){
		var holder = $('.edgtf-info-cards-holder.edgtf-ic-animation');
		
		if(holder.length){
			holder.each(function() {
				var thisHolder = $(this);
				
				thisHolder.appear(function() {
					thisHolder.addClass('edgtf-ic-show-items');
				}, {accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
			});
		}
	}
	
	/**
	 * Init item showcase shortcode
	 */
	function edgtfInitItemShowcase() {
		var itemShowcase = $('.edgtf-item-showcase-holder');
		
		if (itemShowcase.length) {
			itemShowcase.each(function(){
				var thisItemShowcase = $(this),
					leftItems = thisItemShowcase.find('.edgtf-is-left'),
					rightItems = thisItemShowcase.find('.edgtf-is-right'),
					itemImage = thisItemShowcase.find('.edgtf-is-image');
				
				//logic
				leftItems.wrapAll( "<div class='edgtf-is-item-holder edgtf-is-left-holder' />");
				rightItems.wrapAll( "<div class='edgtf-is-item-holder edgtf-is-right-holder' />");
				thisItemShowcase.animate({opacity:1},200);
				
				setTimeout(function(){
					thisItemShowcase.appear(function(){
						itemImage.addClass('edgtf-appeared');
						thisItemShowcase.on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
							function(e) {
								if(edgtf.windowWidth > 1280) {
									itemAppear('.edgtf-is-left-holder .edgtf-is-item');
									itemAppear('.edgtf-is-right-holder .edgtf-is-item');
								} else {
									itemAppear('.edgtf-is-item');
								}
							});
					},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
				},100);
				
				//appear animation trigger
				function itemAppear(itemCSSClass) {
					thisItemShowcase.find(itemCSSClass).each(function(i){
						var thisListItem = $(this);
						setTimeout(function(){
							thisListItem.addClass('edgtf-appeared');
						}, i*150);
					});
				}
			});
		}
	}
	
	/*
	 ** Sections with parallax background image
	 */
	function edgtfInitParallax(){
		var parallaxHolder = $('.edgtf-parallax-holder');
		
		if(parallaxHolder.length){
			parallaxHolder.each(function() {
				var parallaxElement = $(this),
					speed = parallaxElement.data('parallax-speed')*0.4;
				
				parallaxElement.parallax('50%', speed);
			});
		}
	}
	
	/**
	 * Init Pie Chart shortcode
	 */
	function edgtfInitPieChart() {
		var pieChartHolder = $('.edgtf-pie-chart-holder');
		
		if (pieChartHolder.length) {
			pieChartHolder.each(function () {
				var thisPieChartHolder = $(this),
					pieChart = thisPieChartHolder.children('.edgtf-pc-percentage'),
					barColor = '#2ac4ea',
					trackColor = '#e8e8e8',
					lineWidth = 8,
					size = 196;
				
				if(typeof pieChart.data('size') !== 'undefined' && pieChart.data('size') !== '') {
					size = pieChart.data('size');
				}
				
				if(typeof pieChart.data('bar-color') !== 'undefined' && pieChart.data('bar-color') !== '') {
					barColor = pieChart.data('bar-color');
				}
				
				if(typeof pieChart.data('track-color') !== 'undefined' && pieChart.data('track-color') !== '') {
					trackColor = pieChart.data('track-color');
				}
				
				pieChart.appear(function() {
					initToCounterPieChart(pieChart);
					thisPieChartHolder.css('opacity', '1');
					
					pieChart.easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'round',
						lineWidth: lineWidth,
						animate: 1500,
						size: size
					});
				},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
			});
		}
	}
	
	/*
	 **	Counter for pie chart number from zero to defined number
	 */
	function initToCounterPieChart(pieChart){
		var counter = pieChart.find('.edgtf-pc-percent'),
			max = parseFloat(counter.text());
		
		counter.countTo({
			from: 0,
			to: max,
			speed: 1500,
			refreshInterval: 50
		});
	}
	
	/*
	 **	Horizontal progress bars shortcode
	 */
	function edgtfInitProgressBars(){
		var progressBar = $('.edgtf-progress-bar');
		
		if(progressBar.length){
			
			progressBar.each(function() {
				
				var thisBar = $(this),
					thisBarContent = thisBar.find('.edgtf-pb-content'),
					percentage = thisBarContent.data('percentage');
				
				thisBar.appear(function() {
					edgtfInitToCounterProgressBar(thisBar, percentage);
					
					thisBarContent.css('width', '0%');
					thisBarContent.animate({'width': percentage+'%'}, 2000);
				});
			});
		}
	}
	
	function edgtfRestaurantDatePicker() {
		var datepicker = $('.edgtf-ot-date');
		
		if(datepicker.length) {
			datepicker.each(function() {
				$(this).datepicker({
					prevText: '<span class="edgtf-icon-arrow ion-ios-arrow-left"></span>',
					nextText: '<span class="edgtf-icon-arrow ion-ios-arrow-right"></span>'
				});
			});
		}
	}

	/**
	 * Init item showcase shortcode
	 */
	function edgtfInitStackedImages() {
		var stackedImages = $('.edgtf-stacked-images-holder');

		if (stackedImages.length) {
			stackedImages.each(function(){
				var thisStackedImages = $(this),
					itemImage = thisStackedImages.find('.edgtf-si-images');

				//logic
				thisStackedImages.animate({opacity:1},200);

				setTimeout(function(){
					thisStackedImages.appear(function(){
						itemImage.addClass('edgtf-appeared');
					},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
				},100);
			});
		}
	}
	
	/*
	 **	Counter for horizontal progress bars percent from zero to defined percent
	 */
	function edgtfInitToCounterProgressBar(progressBar, $percentage){
		var percentage = parseFloat($percentage),
			percent = progressBar.find('.edgtf-pb-percent');
		
		if(percent.length) {
			percent.each(function() {
				var thisPercent = $(this);
				thisPercent.css('opacity', '1');
				
				thisPercent.countTo({
					from: 0,
					to: percentage,
					speed: 2000,
					refreshInterval: 50
				});
			});
		}
	}

    /**
     * Object that represents social icon widget
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var edgtfSocialIconWidget = edgtf.modules.shortcodes.edgtfSocialIconWidget = function() {
        //get all social icons on page
        var icons = $('.edgtf-social-icon-widget-holder:not(.edgtf-has-custom-hover)');

        /**
         * Function that triggers icon hover color functionality
         */
        var socialIconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon;
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');
                if(typeof icon.data('original-color') !== 'undefined') {
                    originalColor = icon.data('original-color');
                }

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        socialIconHoverColor($(this));
                    });
                }
            }
        };
    };
	
	/*
	 **	Init tabs shortcode
	 */
	function edgtfInitTabs(){
		var tabs = $('.edgtf-tabs');
		
		if(tabs.length){
			tabs.each(function(){
				var thisTabs = $(this);
				
				thisTabs.children('.edgtf-tab-container').each(function(index){
					index = index + 1;
					var that = $(this),
						link = that.attr('id'),
						navItem = that.parent().find('.edgtf-tabs-nav li:nth-child('+index+') a'),
						navLink = navItem.attr('href');
					
					link = '#'+link;
					
					if(link.indexOf(navLink) > -1) {
						navItem.attr('href',link);
					}
				});
				
				thisTabs.tabs();
			});
		}
	}

	/**
	 * Init testimonials shortcode
	 */
	function edgtfInitTestimonials(){
		
		var testimonialsHolder = $('.edgtf-testimonials-holder');
		
		if(testimonialsHolder.length){
			testimonialsHolder.each(function(){
				
				var thisTestimonials = $(this),
					testimonials = thisTestimonials.children('.edgtf-testimonials'),
					numberOfItems = 1,
					loop = true,
					autoplay = true,
					number = 0,
					speed = 5000,
					animationSpeed = 600,
					navArrows = true,
					navDots = true,
					margin = 25;
				
				if (typeof testimonials.data('number') !== 'undefined' && testimonials.data('number') !== false) {
					number = parseInt(testimonials.data('number'));
				}

				if (typeof testimonials.data('number-visible') !== 'undefined' && testimonials.data('number-visible') !== false) {
					numberOfItems = parseInt(testimonials.data('number-visible'));
				}
				
				if (typeof testimonials.data('speed') !== 'undefined' && testimonials.data('speed') !== false) {
					speed = testimonials.data('speed');
				}
				
				if (typeof testimonials.data('animation-speed') !== 'undefined' && testimonials.data('animation-speed') !== false) {
					animationSpeed = testimonials.data('animation-speed');
				}
				
				if (typeof testimonials.data('nav-arrows') !== 'undefined' && testimonials.data('nav-arrows') !== false && testimonials.data('nav-arrows') === 'no') {
					navArrows = false;
				}

				if (typeof testimonials.data('nav-dots') !== 'undefined' && testimonials.data('nav-dots') !== false && testimonials.data('nav-dots') === 'no') {
					navDots = false;
				}
				
				if(number === 1) {
					loop = false;
					autoplay = false;
					navArrows = false;
					navDots = false;
				}
				
				var responsiveNumberOfItems1 = 1,
					responsiveNumberOfItems2 = 2,
					responsiveNumberOfItems3 = 3;
				
				if (numberOfItems < 3) {
					responsiveNumberOfItems2 = numberOfItems;
					responsiveNumberOfItems3 = numberOfItems;
				}
				
				testimonials.owlCarousel({
					items: numberOfItems,
					loop: loop,
					autoplay: autoplay,
					autoplayTimeout: speed,
					smartSpeed: animationSpeed,
					margin: margin,
					nav: navArrows,
					dots: navDots,
					navText: [
						'<span class="edgtf-prev-icon"><span class="edgtf-icon-arrow ion-ios-arrow-left"></span></span>',
						'<span class="edgtf-next-icon"><span class="edgtf-icon-arrow ion-ios-arrow-right"></span></span>'
					],
					responsive: {
						0: {
							items: responsiveNumberOfItems1,
						},
						600: {
							items: responsiveNumberOfItems2
						},
						1024: {
							items: responsiveNumberOfItems3
						},
						1280: {
							items: numberOfItems
						}
					},
					onInitialize: function () {
						edgtfInitParallax();
					}
				});
				
				thisTestimonials.css({'visibility': 'visible'});
			});
		}
	}
	
	/*
	 **	Vertical Split Slider
	 */
	function edgtfInitVerticalSplitSlider() {
		
		var slider = $('.edgtf-vertical-split-slider');
		
		if (slider.length) {
			
			if (edgtf.body.hasClass('edgtf-vss-initialized')) {
				edgtf.body.removeClass('edgtf-vss-initialized');
				$.fn.multiscroll.destroy();
			}
			
			slider.height(edgtf.windowHeight).animate({opacity: 1}, 300);
			
			var defaultHeaderStyle = '';
			if (edgtf.body.hasClass('edgtf-light-header')) {
				defaultHeaderStyle = 'light';
			} else if (edgtf.body.hasClass('edgtf-dark-header')) {
				defaultHeaderStyle = 'dark';
			}
			
			slider.multiscroll({
				scrollingSpeed: 700,
				easing: 'easeInOutQuart',
				navigation: true,
				useAnchorsOnLoad: false,
				sectionSelector: '.edgtf-vss-ms-section',
				leftSelector: '.edgtf-vss-ms-left',
				rightSelector: '.edgtf-vss-ms-right',
				afterRender: function () {
					edgtfCheckVerticalSplitSectionsForHeaderStyle($('.edgtf-vss-ms-left .edgtf-vss-ms-section:last-child').data('header-style'), defaultHeaderStyle);
					edgtf.body.addClass('edgtf-vss-initialized');
					
					var contactForm7 = $('div.wpcf7 > form');
					if (contactForm7.length) {
						contactForm7.each(function(){
                            var thisForm = $(this);
                            
                            thisForm.find('.wpcf7-submit').off().on('click', function(e){
                                e.preventDefault();
                                wpcf7.submit(thisForm);
                            });
                        });
                    }
					
					//prepare html for smaller screens - start //
					var verticalSplitSliderResponsive = $('<div class="edgtf-vss-responsive"></div>'),
						leftSide = slider.find('.edgtf-vss-ms-left > div'),
						rightSide = slider.find('.edgtf-vss-ms-right > div');
					
					slider.after(verticalSplitSliderResponsive);
					
					for (var i = 0; i < leftSide.length; i++) {
						verticalSplitSliderResponsive.append($(leftSide[i]).clone(true));
						verticalSplitSliderResponsive.append($(rightSide[leftSide.length - 1 - i]).clone(true));
					}
					
					//prepare google maps clones
					if ($('.edgtf-vss-responsive .edgtf-google-map').length) {
						$('.edgtf-vss-responsive .edgtf-google-map').each(function () {
							var map = $(this);
							map.empty();
							var num = Math.floor((Math.random() * 100000) + 1);
							map.attr('id', 'edgtf-map-' + num);
							map.data('unique-id', num);
						});
					}
					
					edgtfButton().init();
					edgtfInitElementsHolderResponsiveStyle();
					edgtfShowGoogleMap();
					edgtfInitProgressBars();
					edgtfInitTestimonials();
				},
				onLeave: function (index, nextIndex, direction) {
					edgtfCheckVerticalSplitSectionsForHeaderStyle($($('.edgtf-vss-ms-left .edgtf-vss-ms-section')[$(".edgtf-vss-ms-left .edgtf-vss-ms-section").length - nextIndex]).data('header-style'), defaultHeaderStyle);
				}
			});
			
			if (edgtf.windowWidth <= 1024) {
				$.fn.multiscroll.destroy();
			} else {
				$.fn.multiscroll.build();
			}
			
			$(window).resize(function () {
				if (edgtf.windowWidth <= 1024) {
					$.fn.multiscroll.destroy();
				} else {
					$.fn.multiscroll.build();
				}
			});
		}
	}
	
	/*
	 **	Check slides on load and slide change for header style changing
	 */
	function edgtfCheckVerticalSplitSectionsForHeaderStyle(section_header_style, default_header_style) {
		
		if (section_header_style !== undefined && section_header_style !== '') {
			edgtf.body.removeClass('edgtf-light-header edgtf-dark-header').addClass('edgtf-' + section_header_style + '-header');
		} else if (default_header_style !== '') {
			edgtf.body.removeClass('edgtf-light-header edgtf-dark-header').addClass('edgtf-' + default_header_style + '-header');
		} else {
			edgtf.body.removeClass('edgtf-light-header edgtf-dark-header');
		}
	}

	function edgtfInstagramCarousel() {

		var instagramCarousels = $('.edgtf-instagram-feed.edgtf-instagram-carousel');

		if (instagramCarousels.length) {
			instagramCarousels.each(function(){

				var carousel = $(this),
					items = 6,
					loop = true,
					margin;

				if (typeof carousel.data('items') !== 'undefined' && carousel.data('items') !== false) {
					items = carousel.data('items');
				}

				// Fix for the issue with the carousels holding only one item - the carousel's core issue
				if (carousel.children().length == 1) { loop = false; }

				if(items === 1) {
					margin = 0;
				} else if((carousel.data('space-between-items') === 'normal')) {
					margin = 20;
				} else if((carousel.data('space-between-items') === 'small')) {
					margin = 10;
				} else if((carousel.data('space-between-items') === 'tiny')) {
					margin = 5;
				} else if((carousel.data('space-between-items') === 'no')) {
					margin = 0;
				}

				var responsiveItems1 = items;
				var responsiveItems2 = 4;
				var responsiveItems3 = 3;
				var responsiveItems4 = 2;

				if (items > 5) {
					responsiveItems1 = 5;
				}

				if(items < 4) {
					responsiveItems2 = items;
				}

				if (items < 3) {
					responsiveItems3 = items;
				}

				if (items === 1) {
					responsiveItems4 = items;
				}

				carousel.owlCarousel({
					autoplay: true,
					autoplayHoverPause: true,
					autoplayTimeout: 5000,
					smartSpeed: 600,
					items: items,
					margin: margin,
					loop: loop,
					dots: false,
					nav: false,
					responsive:{
						1200:{
							items: items
						},
						1024:{
							items: responsiveItems1
						},
						769:{
							items: responsiveItems2
						},
						601:{
							items: responsiveItems3
						},
						0:{
							items: responsiveItems4
						}
					},
					onInitialized: function() {
						carousel.css({'opacity': 1});
					}
				});

			});
		}
	}

	function edgtfTwitterSlider(){

		var twitterSlider = $('.edgtf-twitter-slider');

		if(twitterSlider.length){
			twitterSlider.each(function(){

				var thisTwitterSlider = $(this),
				//tweets = thisTwitterSlider.children('.edgtf-tweet-holder'),
					loop = true,
					autoplay = true,
					items = 0,
					speed = 5000,
					animationSpeed = 600,
					navigation = true;

				if(items === 1) {
					loop = false;
					autoplay = false;
					navigation = false;
				}

				thisTwitterSlider.owlCarousel({
					items: 1,
					loop: loop,
					autoplay: autoplay,
					autoplayTimeout: speed,
					smartSpeed: animationSpeed,
					nav: true,
					dots: navigation,
					navText: [
						'<span class="edgtf-prev-icon"><span class="edgtf-icon-arrow ion-ios-arrow-left"></span></span>',
						'<span class="edgtf-next-icon"><span class="edgtf-icon-arrow ion-ios-arrow-right"></span></span>'
					]
				});

				thisTwitterSlider.css({'visibility': 'visible'});
			});
		}
	}

	function edgtfInitPricingTables() {
		var pTables = $('.edgtf-pricing-tables.edgtf-pt-appear-effect');

		if (pTables.length) {
			pTables.appear(function(){
				var tableElements = $(this).find('.edgtf-pt-inner');

				tableElements.each(function(i){
					var currentElement = $(this);
					
					setTimeout(function(){
						currentElement.addClass('edgtf-pt-appeared');
					}, i * 200);
				});
			},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
		}
	}

	function edgtfImageWithText() {
		var iwt = $('.edgtf-image-with-text-holder.edgtf-iwt-appear-effect');

		if (iwt.length && !edgtf.htmlEl.hasClass('touch')) {
			iwt.appear(function(){
				var currentIwt = $(this),
					currentIwtItem = currentIwt.find('.edgtf-iwt-image');

				currentIwt.addClass('edgtf-animating');
				currentIwtItem.addClass('edgtf-appeared');

				currentIwtItem.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
					currentIwt.removeClass('edgtf-animating');
				});
			},{accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
		}
	}

})(jQuery);