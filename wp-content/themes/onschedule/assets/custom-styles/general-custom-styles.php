<?php
if(!function_exists('onschedule_edge_design_styles')) {
    /**
     * Generates general custom styles
     */
    function onschedule_edge_design_styles() {

		if (onschedule_edge_options()->getOptionValue('google_fonts')){
			$font_family = onschedule_edge_options()->getOptionValue('google_fonts');
			if(onschedule_edge_is_font_option_valid($font_family)) {
				$font_selector = array(
					'body',
					'.edgtf-title .edgtf-title-holder .edgtf-page-title',
					'.edgtf-btn.edgtf-btn-small',
					'.widget.widget_tag_cloud a',
					'footer .widget p, footer .widget a'
				);
				
				echo onschedule_edge_dynamic_css($font_selector, array('font-family' => onschedule_edge_get_font_option_val($font_family)));
			}
		}

        if(onschedule_edge_options()->getOptionValue('first_color') !== "") {
            $color_selector = array(
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'h6 a:hover',
                'a:hover',
                'p a:hover',
	            '.edgtf-comment-holder .edgtf-comment-text .replay',
				'.edgtf-comment-holder .edgtf-comment-text .comment-reply-link',
				'.edgtf-comment-holder .edgtf-comment-text .comment-edit-link',
	            '.edgtf-comment-holder .edgtf-comment-text .edgtf-comment-date',
	            '.edgtf-owl-slider .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-owl-slider .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-owl-slider .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-owl-slider .owl-nav .owl-next:hover .edgtf-next-icon',
	            '.edgtf-main-menu ul li a:hover',
	            '.edgtf-main-menu > ul > li.edgtf-active-item > a',
	            '.edgtf-drop-down .second .inner ul li.current-menu-ancestor > a',
				'.edgtf-drop-down .second .inner ul li.current-menu-item > a',
	            '.edgtf-drop-down .wide .second .inner > ul > li.current-menu-ancestor > a',
				'.edgtf-drop-down .wide .second .inner > ul > li.current-menu-item > a',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li a:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li h5:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor > a',
				'.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item > a',
	            '.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid > ul > li.edgtf-active-item > a',
	            '.edgtf-side-menu-button-opener.opened',
	            '.edgtf-side-menu-button-opener:hover',
	            'nav.edgtf-fullscreen-menu ul li a:hover',
	            'nav.edgtf-fullscreen-menu ul li ul li.current-menu-ancestor > a',
				'nav.edgtf-fullscreen-menu ul li ul li.current-menu-item > a',
	            'nav.edgtf-fullscreen-menu > ul > li.edgtf-active-item > a',
	            '.edgtf-search-page-holder article.sticky .edgtf-post-title-area h3 a',
	            '.edgtf-pl-standard-pagination ul li.edgtf-pl-pag-active a',
	            '.edgtf-portfolio-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
	            '.edgtf-portfolio-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
	            '.edgtf-portfolio-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
	            '.edgtf-portfolio-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
	            '.edgtf-portfolio-single-holder .edgtf-ps-info-holder .edgtf-ps-info-item a:not(.edgtf-share-link):hover',
	            '.edgtf-blog-holder article.sticky .edgtf-post-title a',
	            '.edgtf-blog-holder article .edgtf-post-info-top a',
	            '.edgtf-bl-standard-pagination ul li.edgtf-bl-pag-active a',
	            '.edgtf-single-links-pages .edgtf-single-links-pages-inner > a:hover',
				'.edgtf-single-links-pages .edgtf-single-links-pages-inner > span:hover',
	            '.edgtf-related-posts-holder .edgtf-related-post .edgtf-post-info a',
	            '.edgtf-blog-list-holder .edgtf-bli-info-top a',
	            '.edgtf-blog-list-holder.edgtf-bl-simple .edgtf-post-info-date',
	            '.edgtf-blog-list-holder.edgtf-bl-minimal .edgtf-post-info-date',
	            '.edgtf-blog-slider-holder .edgtf-blog-slider-item .edgtf-item-info-section > div a:hover',
	            '.edgtf-blog-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
	            '.edgtf-btn.edgtf-btn-simple',
	            '.edgtf-btn.edgtf-btn-outline',
	            '.edgtf-image-gallery .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-image-gallery .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-image-gallery .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-image-gallery .owl-nav .owl-next:hover .edgtf-next-icon',
	            '.edgtf-image-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
	            '.edgtf-social-share-holder.edgtf-dropdown .edgtf-social-share-dropdown-opener:hover',
	            '.edgtf-team-holder .edgtf-team-social-holder .edgtf-team-icon .edgtf-icon-element:hover',
	            '.edgtf-testimonials-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-next:hover .edgtf-next-icon',
				'aside.edgtf-sidebar .widget.widget_pages ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_archive ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_categories ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_meta ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_recent_comments ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_recent_entries ul li a:hover',
	            'aside.edgtf-sidebar .widget.widget_nav_menu ul li a:hover',
				'.wpb_widgetised_column .widget.widget_pages ul li a:hover',
				'.wpb_widgetised_column .widget.widget_archive ul li a:hover',
				'.wpb_widgetised_column .widget.widget_categories ul li a:hover',
				'.wpb_widgetised_column .widget.widget_meta ul li a:hover',
				'.wpb_widgetised_column .widget.widget_recent_comments ul li a:hover',
				'.wpb_widgetised_column .widget.widget_recent_entries ul li a:hover',
				'.wpb_widgetised_column .widget.widget_nav_menu ul li a:hover',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-twitter-icon',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-twitter-icon',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text a',
				'.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text span',
	            '.edgtf-single-menu-item-holder .edgtf-smi-related-items .edgtf-smi-related-list .edgtf-smi-related-post-date',
	            '.edgtf-working-hours-holder .edgtf-wh-title .edgtf-wh-title-accent-word',
	            '.edgtf-blog-holder article .edgtf-post-info-bottom .edgtf-social-share-holder li a:hover',
	            '.edgtf-blog-list-holder.edgtf-bl-boxed .edgtf-bl-item:hover .edgtf-bli-info-top a',
	            '.edgtf-single-menu-item-holder .edgtf-smi-share-holder .edgtf-social-share-holder.edgtf-list li a:hover',
	            '.edgtf-drop-down .second .inner ul li a .item_outer:before',
	            '.edgtf-side-menu .widget.widget_nav_menu ul li a:hover',
	            '.edgtf-top-bar .widget a:hover',
	            '.edgtf-booked-custom-style .booked-modal .bm-window a:not(.close)',
	            '.edgtf-booked-custom-style .booked-modal .bm-window p i.fa',
	            '.edgtf-booked-custom-style .booked-modal .bm-window p.appointment-title'
            );

            $woo_color_selector = array();
            if(onschedule_edge_is_woocommerce_installed()) {
                $woo_color_selector = array(
					'.edgtf-woocommerce-page .woocommerce-message > a:hover',
					'.edgtf-woocommerce-page .woocommerce-info > a:hover',
					'.edgtf-woocommerce-page .woocommerce-error > a:hover',
	                '.edgtf-woocommerce-page .woocommerce-info .showcoupon:hover',
	                '.woocommerce-pagination .page-numbers li a.current, .woocommerce-pagination .page-numbers li a:hover',
					'.woocommerce-pagination .page-numbers li span.current',
					'.woocommerce-pagination .page-numbers li span:hover',
	                '.edgtf-woo-view-all-pagination a:before',
	                '.edgtf-woocommerce-page .edgtf-content .variations .reset_variations',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .price',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .product_meta > span',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .product_meta > span a:hover',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .edgtf-woo-social-share-holder > span',
	                '.edgtf-woo-single-page .edgtf-single-product-summary p.stock.out-of-stock',
					'.edgtf-woo-single-page .edgtf-single-product-summary p.stock.in-stock',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs > li.active a',
	                '.edgtf-woo-single-page .woocommerce-tabs table th',
	                '.edgtf-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a:before',
	                '.edgtf-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a.active:after',
	                'ul.products > .product .edgtf-product-list-categories a:hover',
	                '.edgtf-woocommerce-page table.cart thead tr th',
	                '.edgtf-woocommerce-page table.cart tr.cart_item td.product-remove a:hover',
	                '.edgtf-woocommerce-page .cart-empty',
	                '.edgtf-woocommerce-page.woocommerce-order-received .woocommerce ul.order_details li strong',
	                '.edgtf-woocommerce-page.woocommerce-account .woocommerce form.edit-account fieldset > legend',
	                '.edgtf-woocommerce-page.woocommerce-account .vc_row .woocommerce form.login p label:not(.inline)',
	                '.edgtf-woocommerce-page.edgtf-woocommerce-order-tracking .woocommerce > .track_order .form-row-first label',
					'.edgtf-woocommerce-page.edgtf-woocommerce-order-tracking .woocommerce > .track_order .form-row-last label',
	                '.edgtf-content .woocommerce.add_to_cart_inline del',
					'.edgtf-content .woocommerce.add_to_cart_inline ins',
	                '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin',
	                '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin:hover',
	                '.edgtf-pl-holder .edgtf-pli .edgtf-pli-category a:hover',
	                '.edgtf-pl-holder .edgtf-pli .edgtf-pli-excerpt',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .button',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .added_to_cart',
	                '.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-title',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-excerpt',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-rating',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-rating span',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-price',
	                '.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-category a',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-category a:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-excerpt',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .button',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .added_to_cart',
	                '.edgtf-plc-holder .owl-nav .edgtf-prev-icon',
					'.edgtf-plc-holder .owl-nav .edgtf-next-icon',
	                'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li.active a',
	                'div.woocommerce > .single-product .woocommerce-tabs table th',
	                'div.woocommerce > .single-product .woocommerce-tabs #reviews .comment-respond .stars a:before',
	                'div.woocommerce > .single-product .woocommerce-tabs #reviews .comment-respond .stars a.active:after',
	                '.widget.woocommerce.widget_product_tag_cloud .tagcloud a',
	                '.edgtf-light-header .edgtf-page-header > div:not(.edgtf-sticky-header):not(.fixed) .edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-count',
	                '.edgtf-shopping-cart-dropdown .edgtf-item-info-holder .remove:hover',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-subtotal-holder .edgtf-total-amount',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-cart-description .edgtf-cart-description-inner span',
	                '.woocommerce .star-rating',
	                '.edgtf-shopping-cart-holder .edgtf-header-cart:hover',
	                '.select2-container--default.select2-container--open .select2-selection--single',
	                '.select2-container--default .select2-results__option[aria-selected=true]',
	                '.select2-container--default .select2-results__option--highlighted[aria-selected]'
                );
            }

            $color_selector = array_merge($color_selector, $woo_color_selector);

	        $color_important_selector = array(
		        '.edgtf-fullscreen-menu-opener:hover',
		        '.edgtf-fullscreen-menu-opener.edgtf-fm-opened',
		        '.edgtf-blog-slider-holder .edgtf-blog-slider-item .edgtf-section-button-holder a:hover',
		        '.edgtf-social-icon-widget-holder.edgtf-has-custom-hover:hover'
	        );

            $background_color_selector = array(
                '.edgtf-st-loader .pulse',
                '.edgtf-st-loader .double_pulse .double-bounce1',
                '.edgtf-st-loader .double_pulse .double-bounce2',
                '.edgtf-st-loader .cube',
                '.edgtf-st-loader .rotating_cubes .cube1',
                '.edgtf-st-loader .rotating_cubes .cube2',
                '.edgtf-st-loader .stripes > div',
                '.edgtf-st-loader .wave > div',
                '.edgtf-st-loader .two_rotating_circles .dot1',
                '.edgtf-st-loader .two_rotating_circles .dot2',
                '.edgtf-st-loader .five_rotating_circles .container1 > div',
                '.edgtf-st-loader .five_rotating_circles .container2 > div',
                '.edgtf-st-loader .five_rotating_circles .container3 > div',
                '.edgtf-st-loader .atom .ball-1:before',
                '.edgtf-st-loader .atom .ball-2:before',
                '.edgtf-st-loader .atom .ball-3:before',
                '.edgtf-st-loader .atom .ball-4:before',
                '.edgtf-st-loader .clock .ball:before',
                '.edgtf-st-loader .mitosis .ball',
                '.edgtf-st-loader .lines .line1',
                '.edgtf-st-loader .lines .line2',
                '.edgtf-st-loader .lines .line3',
                '.edgtf-st-loader .lines .line4',
                '.edgtf-st-loader .fussion .ball',
                '.edgtf-st-loader .fussion .ball-1',
                '.edgtf-st-loader .fussion .ball-2',
                '.edgtf-st-loader .fussion .ball-3',
                '.edgtf-st-loader .fussion .ball-4',
                '.edgtf-st-loader .wave_circles .ball',
                '.edgtf-st-loader .pulse_circles .ball',
	            '#submit_comment',
				'.post-password-form input[type="submit"]',
				'input.wpcf7-form-control.wpcf7-submit',
				'#edgtf-back-to-top > span',
	            '#ui-datepicker-div .ui-datepicker-today',
	            '.edgtf-search-page-holder .edgtf-search-page-form .edgtf-form-holder button',
	            '.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls > .mejs-time-rail .mejs-time-total .mejs-time-current',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls > a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
	            '.edgtf-blog-holder article.format-quote .edgtf-post-quote-holder',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-title-holder.ui-state-active',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-title-holder.ui-state-hover',
	            '.edgtf-accordion-holder.edgtf-ac-simple .edgtf-title-holder:not(.ui-state-active) .edgtf-accordion-mark',
	            '.edgtf-btn.edgtf-btn-solid',
	            '.edgtf-icon-shortcode.edgtf-circle',
	            '.edgtf-icon-shortcode.edgtf-square',
	            '.edgtf-icon-shortcode.edgtf-dropcaps.edgtf-circle',
	            '.edgtf-image-slider-holder .edgtf-is-slider-inner .edgtf-is-content > ul li:before',
	            '.edgtf-progress-bar .edgtf-pb-content-holder .edgtf-pb-content',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-active a',
				'.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-hover a',
	            '.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-active a',
				'.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-hover a',
	            '.widget.widget_search button:hover',
	            '.widget.widget_tag_cloud a:hover',
	            '.edgtf-single-menu-item-holder .edgtf-smi-prep-info-top .edgtf-prep-info-icon',
	            '.edgtf-single-menu-item-holder .edgtf-smi-prep-info-bottom .edgtf-prep-info-price-holder',
	            '.edgtf-menu-list .edgtf-ml-title-holder .edgtf-ml-label-holder .edgtf-ml-label',
	            '.edgtf-menu-grid .edgtf-mg-image-holder .edgtf-mg-price-holder .edgtf-mg-price',
	            '.edgtf-menu-grid .edgtf-mg-image-holder .edgtf-mg-label-holder',
	            '.edgtf-btn.edgtf-btn-solid:not(.edgtf-btn-custom-hover-bg)',
	            '.edgtf-image-gallery .edgtf-ig-grid .edgtf-ig-image a:after',
	            '.edgtf-booked-custom-skin #booked-profile-page input[type=submit].button-primary',
	            '.edgtf-booked-custom-style #ui-datepicker-div.booked_custom_date_picker .ui-datepicker-header',
	            '.edgtf-booked-custom-style #ui-datepicker-div table.ui-datepicker-calendar tbody td #ui-datepicker-div.booked_custom_date_picker table.ui-datepicker-calendar tbody td a.ui-state-active:hover',
	            '.edgtf-booked-custom-style #ui-datepicker-div table.ui-datepicker-calendar tbody td a.ui-state-active'
            );

            $woo_background_color_selector = array();
            if(onschedule_edge_is_woocommerce_installed()) {
                $woo_background_color_selector = array(
	                '.woocommerce-page .edgtf-content a.button',
					'.woocommerce-page .edgtf-content a.added_to_cart',
					'.woocommerce-page .edgtf-content input[type="submit"]',
					'.woocommerce-page .edgtf-content button[type="submit"]',
					'.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
					'div.woocommerce a.button',
					'div.woocommerce a.added_to_cart',
					'div.woocommerce input[type="submit"]',
					'div.woocommerce button[type="submit"]',
					'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
	                '.woocommerce .edgtf-onsale',
					'.woocommerce .edgtf-out-of-stock',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs > li.active a:after',
	                '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin:hover',
	                '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-image .edgtf-pli-onsale',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-image .edgtf-pli-out-of-stock',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .button',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .added_to_cart',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .button:hover',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .added_to_cart:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-image-outer .edgtf-plc-image .edgtf-plc-onsale',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-image-outer .edgtf-plc-image .edgtf-plc-out-of-stock',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .button:hover',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .added_to_cart:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .button',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .added_to_cart',
	                '.edgtf-plc-holder .owl-dots .owl-dot.active span',
	                'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li.active a:after',
	                '.widget.woocommerce.widget_product_search .woocommerce-product-search button',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-view-cart:hover',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-checkout'
                );
            }

            $background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);

	        $background_color_important_selector = array(
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-hover-bg):hover',
		        '.edgtf-price-table.edgtf-pt-button-dark .edgtf-pt-inner .edgtf-btn.edgtf-btn-solid:hover',
		        '#edgtf-back-to-top > span',
		        '.edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-count',
		        '.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range',
		        'div.woocommerce button[type="submit"]:hover',
		        '.woocommerce-page .edgtf-content button[type="submit"]:hover',
		        '.woocommerce-page .edgtf-content a.button:hover',
		        '.woocommerce-page .edgtf-content a.added_to_cart:hover',
		        '.woocommerce-page .edgtf-content input[type="submit"]:hover',
		        '.woocommerce-page .edgtf-content button[type="submit"]:hover',
		        '.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
		        'div.woocommerce a.button:hover',
		        'div.woocommerce a.added_to_cart:hover',
		        'div.woocommerce input[type="submit"]:hover',
		        'div.woocommerce button[type="submit"]:hover',
		        'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
		        '.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
		        '.edgtf-image-slider-holder .edgtf-is-slider-inner .edgtf-is-content > ul li:before',
		        '.edgtf-booked-custom-style .booked-modal input[type=submit].button-primary',
		        '.edgtf-booked-custom-style .booked-modal button.cancel:hover',
		        '.edgtf-booked-custom-skin.edgtf-booked-user-appointments-holder #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a',
		        '.edgtf-booked-custom-skin.edgtf-booked-user-profile-holder #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a',
		        '.edgtf-booked-custom-skin.edgtf-booked-user-profile-holder #booked-profile-page input[type=submit].button-primary',
		        '.edgtf-booked-custom-skin.edgtf-booked-login-holder #booked-profile-page #profile-login input[type=submit].button-primary',
				'.edgtf-booked-custom-skin.edgtf-booked-login-holder #booked-profile-page #profile-forgot input[type=submit].button-primary'
	        );

            $border_color_selector = array(
                '.edgtf-st-loader .pulse_circles .ball',
                '#edgtf-back-to-top > span',
	            '.edgtf-drop-down .second .inner',
	            '.edgtf-drop-down .narrow .second .inner ul li ul',
	            '.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
	            '.edgtf-btn.edgtf-btn-solid',
	            '.edgtf-btn.edgtf-btn-outline',
	            '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin:hover',
	            '.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin',
	            '.edgtf-shopping-cart-dropdown',
	            '.edgtf-color-spinner .edgtf-cs-line',
	            '.edgtf-booked-calendar-holder.edgtf-custom-style table.booked-calendar td.today .date span',
	            '.edgtf-booked-custom-style .booked-modal .bm-window'
            );

	        $border_color_important_selector = array(
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-border-hover):hover',
		        '.edgtf-price-table.edgtf-pt-button-dark .edgtf-pt-inner .edgtf-btn.edgtf-btn-solid:hover',
		        '.edgtf-booked-custom-skin #booked-profile-page input[type=submit].button-primary'
	        );

            echo onschedule_edge_dynamic_css($color_selector, array('color' => onschedule_edge_options()->getOptionValue('first_color')));
	        echo onschedule_edge_dynamic_css($color_important_selector, array('color' => onschedule_edge_options()->getOptionValue('first_color').'!important'));
	        echo onschedule_edge_dynamic_css($background_color_selector, array('background-color' => onschedule_edge_options()->getOptionValue('first_color')));
	        echo onschedule_edge_dynamic_css($background_color_important_selector, array('background-color' => onschedule_edge_options()->getOptionValue('first_color').'!important'));
	        echo onschedule_edge_dynamic_css($border_color_selector, array('border-color' => onschedule_edge_options()->getOptionValue('first_color')));
	        echo onschedule_edge_dynamic_css($border_color_important_selector, array('border-color' => onschedule_edge_options()->getOptionValue('first_color').'!important'));

	        echo onschedule_edge_dynamic_css('::selection', array('background' => onschedule_edge_options()->getOptionValue('first_color')));
	        echo onschedule_edge_dynamic_css('::-moz-selection', array('background' => onschedule_edge_options()->getOptionValue('first_color')));
        }

		if (onschedule_edge_options()->getOptionValue('page_background_color') !== '') {
			$background_color_selector = array(
				'.edgtf-wrapper-inner',
				'.edgtf-content'
			);
			echo onschedule_edge_dynamic_css($background_color_selector, array('background-color' => onschedule_edge_options()->getOptionValue('page_background_color')));
		}

		if (onschedule_edge_options()->getOptionValue('selection_color') !== '') {
			echo onschedule_edge_dynamic_css('::selection', array('background' => onschedule_edge_options()->getOptionValue('selection_color')));
			echo onschedule_edge_dynamic_css('::-moz-selection', array('background' => onschedule_edge_options()->getOptionValue('selection_color')));
		}

		$boxed_background_style = array();
		if (onschedule_edge_options()->getOptionValue('page_background_color_in_box') !== '') {
			$boxed_background_style['background-color'] = onschedule_edge_options()->getOptionValue('page_background_color_in_box');
		}

		if (onschedule_edge_options()->getOptionValue('boxed_background_image') !== '') {
			$boxed_background_style['background-image'] = 'url('.esc_url(onschedule_edge_options()->getOptionValue('boxed_background_image')).')';
			$boxed_background_style['background-position'] = 'center 0px';
			$boxed_background_style['background-repeat'] = 'no-repeat';
		}

		if (onschedule_edge_options()->getOptionValue('boxed_pattern_background_image') !== '') {
			$boxed_background_style['background-image'] = 'url('.esc_url(onschedule_edge_options()->getOptionValue('boxed_pattern_background_image')).')';
			$boxed_background_style['background-position'] = '0px 0px';
			$boxed_background_style['background-repeat'] = 'repeat';
		}

		if (onschedule_edge_options()->getOptionValue('boxed_background_image_attachment') !== '') {
			$boxed_background_style['background-attachment'] = (onschedule_edge_options()->getOptionValue('boxed_background_image_attachment'));
		}

		echo onschedule_edge_dynamic_css('.edgtf-boxed .edgtf-wrapper', $boxed_background_style);

        $paspartu_style = array();
        if (onschedule_edge_options()->getOptionValue('paspartu_color') !== '') {
            $paspartu_style['background-color'] = onschedule_edge_options()->getOptionValue('paspartu_color');
        }

        if (onschedule_edge_options()->getOptionValue('paspartu_width') !== '') {
            $paspartu_style['padding'] = onschedule_edge_options()->getOptionValue('paspartu_width').'%';
        }

        echo onschedule_edge_dynamic_css('.edgtf-paspartu-enabled .edgtf-wrapper', $paspartu_style);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_design_styles');
}

if(!function_exists('onschedule_edge_content_styles')) {
    /**
     * Generates content custom styles
     */
    function onschedule_edge_content_styles() {

        $content_style = array();
	    
	    $padding_top = onschedule_edge_options()->getOptionValue('content_top_padding');
	    if ($padding_top !== '') {
            $content_style['padding-top'] = onschedule_edge_filter_px($padding_top).'px';
        }

        $content_selector = array(
            '.edgtf-content .edgtf-content-inner > .edgtf-full-width > .edgtf-full-width-inner',
        );

        echo onschedule_edge_dynamic_css($content_selector, $content_style);

        $content_style_in_grid = array();
	    
	    $padding_top_in_grid = onschedule_edge_options()->getOptionValue('content_top_padding_in_grid');
	    if ($padding_top_in_grid !== '') {
            $content_style_in_grid['padding-top'] = onschedule_edge_filter_px($padding_top_in_grid).'px';

        }

        $content_selector_in_grid = array(
            '.edgtf-content .edgtf-content-inner > .edgtf-container > .edgtf-container-inner',
        );

        echo onschedule_edge_dynamic_css($content_selector_in_grid, $content_style_in_grid);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_content_styles');
}

if (!function_exists('onschedule_edge_h1_styles')) {

    function onschedule_edge_h1_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h1_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h1_margin_bottom');
	    
	    $item_styles = onschedule_edge_get_typography_styles('h1');
	    
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	    
	    $item_selector = array(
		    'h1'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h1_styles');
}

if (!function_exists('onschedule_edge_h2_styles')) {

    function onschedule_edge_h2_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h2_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h2_margin_bottom');
	
	    $item_styles = onschedule_edge_get_typography_styles('h2');
	
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	
	    $item_selector = array(
		    'h2'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h2_styles');
}

if (!function_exists('onschedule_edge_h3_styles')) {

    function onschedule_edge_h3_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h3_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h3_margin_bottom');
	
	    $item_styles = onschedule_edge_get_typography_styles('h3');
	
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	
	    $item_selector = array(
		    'h3'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h3_styles');
}

if (!function_exists('onschedule_edge_h4_styles')) {

    function onschedule_edge_h4_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h4_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h4_margin_bottom');
	
	    $item_styles = onschedule_edge_get_typography_styles('h4');
	
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	
	    $item_selector = array(
		    'h4',
		    '.edgtf-blog-holder article.format-quote .edgtf-post-quote-holder .edgtf-post-title',
		    '.edgtf-blog-holder article.format-link .edgtf-post-link-holder .edgtf-post-title'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h4_styles');
}

if (!function_exists('onschedule_edge_h5_styles')) {

    function onschedule_edge_h5_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h5_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h5_margin_bottom');
	
	    $item_styles = onschedule_edge_get_typography_styles('h5');
	
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	
	    $item_selector = array(
		    'h5'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h5_styles');
}

if (!function_exists('onschedule_edge_h6_styles')) {

    function onschedule_edge_h6_styles() {
	    $margin_top = onschedule_edge_options()->getOptionValue('h6_margin_top');
	    $margin_bottom = onschedule_edge_options()->getOptionValue('h6_margin_bottom');
	
	    $item_styles = onschedule_edge_get_typography_styles('h6');
	
	    if($margin_top !== '') {
		    $item_styles['margin-top'] = onschedule_edge_filter_px($margin_top).'px';
	    }
	    if($margin_bottom !== '') {
		    $item_styles['margin-bottom'] = onschedule_edge_filter_px($margin_bottom).'px';
	    }
	
	    $item_selector = array(
		    'h6',
		    '.edgtf-expanded-image-gallery .edgtf-eig-image .edgtf-eig-image-label',
		    '.edgtf-side-menu .widget.widget_nav_menu ul li a'
	    );
	
	    $woo_item_selector = array();
	    if(onschedule_edge_is_woocommerce_installed()) {
		    $woo_item_selector = array(
	    		'.widget.woocommerce.widget_products ul li a',
			    '.widget.woocommerce.widget_recently_viewed_products ul li a',
			    '.widget.woocommerce.widget_recent_reviews ul li a',
			    '.widget.woocommerce.widget_top_rated_products ul li a',
			    '.widget.woocommerce.widget_shopping_cart .widget_shopping_cart_content ul li a:not(.remove)',
			    '.edgtf-woocommerce-page table.cart tr.cart_item td.product-name a'
		    );
	    }
	
	    $item_selector = array_merge($item_selector, $woo_item_selector);
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_h6_styles');
}

if (!function_exists('onschedule_edge_text_styles')) {

    function onschedule_edge_text_styles() {
	    $item_styles = onschedule_edge_get_typography_styles('text');
	
	    $item_selector = array(
		    'p'
	    );
	
	    echo onschedule_edge_dynamic_css($item_selector, $item_styles);
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_text_styles');
}

if (!function_exists('onschedule_edge_link_styles')) {

    function onschedule_edge_link_styles() {

        $link_styles = array();

        if(onschedule_edge_options()->getOptionValue('link_color') !== '') {
            $link_styles['color'] = onschedule_edge_options()->getOptionValue('link_color');
        }
        if(onschedule_edge_options()->getOptionValue('link_fontstyle') !== '') {
            $link_styles['font-style'] = onschedule_edge_options()->getOptionValue('link_fontstyle');
        }
        if(onschedule_edge_options()->getOptionValue('link_fontweight') !== '') {
            $link_styles['font-weight'] = onschedule_edge_options()->getOptionValue('link_fontweight');
        }
        if(onschedule_edge_options()->getOptionValue('link_fontdecoration') !== '') {
            $link_styles['text-decoration'] = onschedule_edge_options()->getOptionValue('link_fontdecoration');
        }

        $link_selector = array(
            'a',
            'p a'
        );

        if (!empty($link_styles)) {
            echo onschedule_edge_dynamic_css($link_selector, $link_styles);
        }
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_link_styles');
}

if (!function_exists('onschedule_edge_link_hover_styles')) {

    function onschedule_edge_link_hover_styles() {

        $link_hover_styles = array();

        if(onschedule_edge_options()->getOptionValue('link_hovercolor') !== '') {
            $link_hover_styles['color'] = onschedule_edge_options()->getOptionValue('link_hovercolor');
        }
        if(onschedule_edge_options()->getOptionValue('link_hover_fontdecoration') !== '') {
            $link_hover_styles['text-decoration'] = onschedule_edge_options()->getOptionValue('link_hover_fontdecoration');
        }

        $link_hover_selector = array(
            'a:hover',
            'p a:hover'
        );

        if (!empty($link_hover_styles)) {
            echo onschedule_edge_dynamic_css($link_hover_selector, $link_hover_styles);
        }

        $link_heading_hover_styles = array();

        if(onschedule_edge_options()->getOptionValue('link_hovercolor') !== '') {
            $link_heading_hover_styles['color'] = onschedule_edge_options()->getOptionValue('link_hovercolor');
        }

        $link_heading_hover_selector = array(
            'h1 a:hover',
            'h2 a:hover',
            'h3 a:hover',
            'h4 a:hover',
            'h5 a:hover',
            'h6 a:hover'
        );

        if (!empty($link_heading_hover_styles)) {
            echo onschedule_edge_dynamic_css($link_heading_hover_selector, $link_heading_hover_styles);
        }
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_link_hover_styles');
}

if (!function_exists('onschedule_edge_smooth_page_transition_styles')) {

    function onschedule_edge_smooth_page_transition_styles($style) {
	    $id = onschedule_edge_get_page_id();
	    $loader_style = array();
	    $current_style = '';
	
	    if(onschedule_edge_get_meta_field_intersect('smooth_pt_bgnd_color',$id) !== '') {
		    $loader_style['background-color'] = onschedule_edge_get_meta_field_intersect('smooth_pt_bgnd_color',$id);
	    }
	
	    $loader_selector = array('.edgtf-smooth-transition-loader');
	
	    if (!empty($loader_style)) {
		    $current_style .= onschedule_edge_dynamic_css($loader_selector, $loader_style);
	    }
	
	    $spinner_style = array();
	    $spinner_border_style = array();
	    
	    if(onschedule_edge_get_meta_field_intersect('smooth_pt_spinner_color',$id) !== '') {
		    $spinner_style['background-color'] = onschedule_edge_get_meta_field_intersect('smooth_pt_spinner_color',$id);
		    $spinner_border_style['border-color'] = onschedule_edge_get_meta_field_intersect('smooth_pt_spinner_color',$id);
	    }
	
	    $spinner_selectors = array(
		    '.edgtf-st-loader .edgtf-rotate-circles > div',
		    '.edgtf-st-loader .pulse',
		    '.edgtf-st-loader .double_pulse .double-bounce1',
		    '.edgtf-st-loader .double_pulse .double-bounce2',
		    '.edgtf-st-loader .cube',
		    '.edgtf-st-loader .rotating_cubes .cube1',
		    '.edgtf-st-loader .rotating_cubes .cube2',
		    '.edgtf-st-loader .stripes > div',
		    '.edgtf-st-loader .wave > div',
		    '.edgtf-st-loader .two_rotating_circles .dot1',
		    '.edgtf-st-loader .two_rotating_circles .dot2',
		    '.edgtf-st-loader .five_rotating_circles .container1 > div',
		    '.edgtf-st-loader .five_rotating_circles .container2 > div',
		    '.edgtf-st-loader .five_rotating_circles .container3 > div',
		    '.edgtf-st-loader .atom .ball-1:before',
		    '.edgtf-st-loader .atom .ball-2:before',
		    '.edgtf-st-loader .atom .ball-3:before',
		    '.edgtf-st-loader .atom .ball-4:before',
		    '.edgtf-st-loader .clock .ball:before',
		    '.edgtf-st-loader .mitosis .ball',
		    '.edgtf-st-loader .lines .line1',
		    '.edgtf-st-loader .lines .line2',
		    '.edgtf-st-loader .lines .line3',
		    '.edgtf-st-loader .lines .line4',
		    '.edgtf-st-loader .fussion .ball',
		    '.edgtf-st-loader .fussion .ball-1',
		    '.edgtf-st-loader .fussion .ball-2',
		    '.edgtf-st-loader .fussion .ball-3',
		    '.edgtf-st-loader .fussion .ball-4',
		    '.edgtf-st-loader .wave_circles .ball',
		    '.edgtf-st-loader .pulse_circles .ball'
	    );
	    
	    $spinner_border_selectors = array(
	    	'.edgtf-st-loader .edgtf-color-spinner .edgtf-cs-line'
	    );
	
	    if (!empty($spinner_style)) {
		    $current_style .= onschedule_edge_dynamic_css($spinner_selectors, $spinner_style);
	    }
	
	    if (!empty($spinner_border_style)) {
		    $current_style .= onschedule_edge_dynamic_css($spinner_border_selectors, $spinner_border_style);
	    }
	
	    $current_style = $current_style . $style;
	
	    return $current_style;
    }

    add_action('onschedule_edge_action_style_dynamic', 'onschedule_edge_smooth_page_transition_styles');
}