<aside class="edgtf-sidebar">
    <?php
        $sidebar = onschedule_edge_get_sidebar();
    
        if (is_active_sidebar($sidebar)) {
            dynamic_sidebar($sidebar);
        }
    ?>
</aside>