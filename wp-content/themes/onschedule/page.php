<?php
$onschedule_sidebar_layout  = onschedule_edge_sidebar_layout();
$onschedule_sidebar_classes = onschedule_edge_sidebar_columns_class();

get_header();
onschedule_edge_get_title();
get_template_part('slider');
?>
<div class="edgtf-container edgtf-default-page-template">
	<?php do_action('onschedule_edge_action_after_container_open'); ?>
	<div class="edgtf-container-inner clearfix">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="edgtf-columns-wrapper <?php echo esc_attr($onschedule_sidebar_classes); ?>">
				<div class="edgtf-columns-inner">
					<div class="edgtf-column-content edgtf-column-content1">
						<?php
							the_content();
							do_action('onschedule_edge_action_page_after_content');
						?>
					</div>
					<?php if($onschedule_sidebar_layout !== 'no-sidebar') { ?>
						<div class="edgtf-column-content edgtf-column-content2">
							<?php get_sidebar(); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</div>
	<?php do_action('onschedule_edge_action_before_container_close'); ?>
</div>
<?php get_footer(); ?>