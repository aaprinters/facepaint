<?php if($related_posts): ?>
	<?php if($related_posts->have_posts()) : ?>
		<div class="edgtf-smi-related-items">
			<h4 class="edgtf-smi-related-posts-title"><?php esc_html_e('You might also like', 'onschedule' ); ?></h4>

			<div class="edgtf-smi-related-items-content">
				<ul class="edgtf-smi-related-list">
					<?php while($related_posts->have_posts()) : $related_posts->the_post(); ?>
						<li <?php post_class('clearfix'); ?>>
							<div class="edgtf-smi-related-content-holder">
								<h6 itemprop="name" class="edgtf-smi-related-title entry-title">
									<a itemprop="url" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h6>
								<span itemprop="dateCreated" class="edgtf-smi-related-post-date entry-date published updated">
									<?php the_time(get_option('date_format')); ?>
								</span>
							</div>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>