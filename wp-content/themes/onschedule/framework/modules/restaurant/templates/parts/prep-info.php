<?php if(is_array($meta) && count($meta)) : ?>
	<?php extract($meta); ?>
	<div class="edgtf-smi-prep-info-holder">
	    <?php if(!empty($prep_time) ||!empty($cook_time) || !empty($servings_num) || !empty($calories_num ) ): ?>
		<div class="edgtf-smi-prep-info-top">
			<div class="edgtf-prep-info-row clearfix">
				<div class="edgtf-prep-info-column">
					<?php if(!empty($prep_time)) : ?>
						<span class="edgtf-prep-info-icon">
							<?php echo onschedule_edge_icon_collections()->renderIcon('icon-hourglass', 'simple_line_icons'); ?>
						</span>
						<span class="edgtf-prep-info-label"><?php esc_html_e('Prep', 'onschedule'); ?> <?php echo esc_html($prep_time); ?></span>
					<?php endif; ?>
				</div>
				<div class="edgtf-prep-info-column">
					<?php if(!empty($cook_time)) : ?>
						<span class="edgtf-prep-info-icon">
							<?php echo onschedule_edge_icon_collections()->renderIcon('icon-fire', 'simple_line_icons'); ?>
						</span>
						<span class="edgtf-prep-info-label"><?php esc_html_e('Cook', 'onschedule'); ?> <?php echo esc_html($cook_time); ?></span>
					<?php endif; ?>
				</div>
			</div>
			<div class="edgtf-prep-info-row clearfix">
				<div class="edgtf-prep-info-column">
					<?php if(!empty($servings_num)) : ?>
						<span class="edgtf-prep-info-icon">
							<?php echo onschedule_edge_icon_collections()->renderIcon('icon-fire', 'simple_line_icons'); ?>
						</span>
						<span class="edgtf-prep-info-label"><?php echo esc_html($servings_num) . ' ' . esc_html__('Servings', 'onschedule'); ?></span>
					<?php endif; ?>
				</div>
				<div class="edgtf-prep-info-column">
					<?php if(!empty($calories_num)) : ?>
						<span class="edgtf-prep-info-icon">
							<?php echo onschedule_edge_icon_collections()->renderIcon('icon-chart', 'simple_line_icons'); ?>
						</span>
						<span class="edgtf-prep-info-label"><?php echo esc_html($calories_num) . ' ' . esc_html__('kcal', 'onschedule'); ?></span>
					<?php endif; ?>
				</div>
			</div>
		</div>
	    <?php endif; ?>
		<div class="edgtf-smi-prep-info-bottom">
			<div class="edgtf-prep-info-time-holder">
				<?php if(!empty($ready_in)) : ?>
					<span class="edgtf-prep-info-time-label"><?php esc_html_e('Ready in', 'onschedule'); ?> <?php echo esc_html($ready_in); ?></span>
				<?php endif; ?>
			</div>
			<?php if(!empty($price)) : ?>
				<div class="edgtf-prep-info-price-holder">
					<?php echo esc_html($price); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>