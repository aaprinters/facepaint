<div class="edgtf-smi-share-holder cleafix">
	<span class="edgtf-smi-share-label"><?php esc_html_e('Share', 'onschedule'); ?></span>
	<?php echo onschedule_edge_get_social_share_html(array('type' => 'list', 'icon_type' => 'circle')) ?>
</div>