<?php if(is_array($ingredients) && count($ingredients)) : ?>
	<div class="edgtf-smi-ingredients-holder">
		<h4><?php esc_html_e('Ingredients', 'onschedule'); ?></h4>

		<div class="edgtf-smi-ingredients-content">
			<ul class="edgtf-smi-ingredients-list">
				<?php foreach($ingredients as $ingredient) : ?>
					<li><?php printf('<span class="edgtf-smi-check"><i class="ion-android-done"></i></span><span>%s</span>', esc_html($ingredient) ); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>