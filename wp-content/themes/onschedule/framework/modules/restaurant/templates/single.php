<div class="edgtf-single-menu-item-holder">
	<div class="edgtf-columns-wrapper edgtf-content-columns-66-33 edgtf-content-sidebar-right edgtf-content-has-sidebar edgtf-columns-normal-space">
		<div class="edgtf-columns-inner">
			<div class="edgtf-column-content edgtf-column-content1">
				<div class="edgtf-smi-content-top">
					<?php onschedule_edge_restaurant_get_menu_item_gallery(); ?>
					<div class="edgtf-smi-content-holder">
						<?php the_content(); ?>
					</div>

					<?php onschedule_edge_restaurant_get_share(); ?>
				</div>
				<?php onschedule_edge_restaurant_get_single_author_info(); ?>
				<?php onschedule_edge_restaurant_get_single_nav(); ?>
				<?php onschedule_edge_restaurant_get_comments(); ?>
			</div>
			<div class="edgtf-column-content edgtf-column-content2">
				<div class="edgtf-smi-sidebar-item edgtf-smi-sidebar-title-item">
					<div class="edgtf-smi-title-holder">
						<h4 itemprop="name" class="edgtf-smi-title entry-title"><?php the_title(); ?></h4>
					</div>
					<div itemprop="description" class="edgtf-smi-excerpt-holder">
						<?php if( has_excerpt() ) {
							the_excerpt();
						} ?>
					</div>
				</div>
				<div class="edgtf-smi-sidebar-item">
					<?php onschedule_edge_restaurant_get_menu_item_prep_info(); ?>
				</div>
				<div class="edgtf-smi-sidebar-item">
					<?php onschedule_edge_restaurant_get_menu_item_ingredients(); ?>
				</div>
				<div class="edgtf-smi-sidebar-item">
					<?php onschedule_edge_restaurant_get_menu_item_related(); ?>
				</div>
			</div>
		</div>
	</div>
</div>