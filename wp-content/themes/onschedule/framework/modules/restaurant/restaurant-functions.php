<?php

if(!function_exists('onschedule_edge_restaurant_get_menu_item_single')) {
	/**
	 * Loads menu item single template
	 */
	function onschedule_edge_restaurant_get_menu_item_single() {
		onschedule_edge_get_module_template_part('templates/single', 'restaurant', '', array());
	}
}

if(!function_exists('onschedule_edge_restaurant_get_menu_item_prep_info')) {
	/**
	 * Loads menu item preparation info template part
	 */
	function onschedule_edge_restaurant_get_menu_item_prep_info() {
		if(onschedule_edge_options()->getOptionValue('menu_item_show_preparation_info') === 'yes') {
			$price         = get_post_meta(get_the_ID(), 'edgtf_menu_item_price_meta', true);
			$prep_time     = get_post_meta(get_the_ID(), 'edgtf_menu_item_prep_time_meta', true);
			$cook_time     = get_post_meta(get_the_ID(), 'edgtf_menu_item_cook_time_meta', true);
			$servings_time = get_post_meta(get_the_ID(), 'edgtf_menu_item_servings_num_meta', true);
			$calories_num  = get_post_meta(get_the_ID(), 'edgtf_menu_item_calories_num_meta', true);
			$ready_in      = get_post_meta(get_the_ID(), 'edgtf_menu_item_ready_in_meta', true);

			$params = array(
				'meta' => array(
					'price'        => $price,
					'prep_time'    => $prep_time,
					'cook_time'    => $cook_time,
					'servings_num' => $servings_time,
					'calories_num' => $calories_num,
					'ready_in'     => $ready_in
				)
			);

			if( '' != $price || '' != $prep_time || '' != $cook_time || '' != $servings_time || '' != $calories_num || '' != $ready_in  )
			  onschedule_edge_get_module_template_part('templates/parts/prep-info', 'restaurant', '', $params);
		}
	}
}

if(!function_exists('onschedule_edge_restaurant_get_menu_item_ingredients')) {
	/**
	 * Loads menu item's ingredients template part
	 */
	function onschedule_edge_restaurant_get_menu_item_ingredients() {
		$ingredients_text = get_post_meta(get_the_ID(), 'edgtf_menu_item_ingredients_meta', true);
		$ingredients_list = '';
		if($ingredients_text !== '') {
			$ingredients_list = explode("\r\n", $ingredients_text);
		}

		$params = array(
			'ingredients' => $ingredients_list
		);

		onschedule_edge_get_module_template_part('templates/parts/ingredients', 'restaurant', '', $params);
	}
}

if(!function_exists('onschedule_edge_restaurant_get_menu_item_related')) {
	/**
	 * Loads menu item's related items
	 */
	function onschedule_edge_restaurant_get_menu_item_related() {
		if(onschedule_edge_options()->getOptionValue('menu_item_show_related') === 'yes') {
			$related_posts = onschedule_edge_get_related_post_type(get_the_ID(), array(
				'posts_per_page' => 4
			));

			$image_size = onschedule_edge_options()->getOptionValue('menu_item_related_image_size');

			$params = array(
				'related_posts' => $related_posts,
				'image_size'    => $image_size
			);

			onschedule_edge_get_module_template_part('templates/parts/related-posts', 'restaurant', '', $params);
		}
	}
}

if(!function_exists('onschedule_edge_restaurant_get_menu_item_gallery')) {
	/**
	 * Loads menu item's gallery template part
	 */
	function onschedule_edge_restaurant_get_menu_item_gallery() {
		$images_ids = get_post_meta(get_the_ID(), 'edgtf_menu_item_gallery_meta', true);
		$params     = array();

		if(!empty($images_ids)) {
			$images_ids = explode(',', $images_ids);
		}

		$params['images_ids'] = $images_ids;

		onschedule_edge_get_module_template_part('templates/parts/gallery', 'restaurant', '', $params);
	}
}

if(!function_exists('onschedule_edge_restaurant_get_comments')) {
	/**
	 * Loads comments template
	 */
	function onschedule_edge_restaurant_get_comments() {
		if(onschedule_edge_options()->getOptionValue('menu_item_enable_comments') === 'yes') {
			onschedule_edge_get_module_template_part('templates/parts/comments', 'restaurant');
		}
	}
}

if(!function_exists('onschedule_edge_restaurant_get_share')) {
	/**
	 * Loads social share template for single menu item page if enabled
	 */
	function onschedule_edge_restaurant_get_share() {
		$share_enabled = onschedule_edge_options()->getOptionValue('enable_social_share') === 'yes'
		                 && onschedule_edge_options()->getOptionValue('enable_social_share_on_restaurant-menu-item') === 'yes';

		if($share_enabled) {
			onschedule_edge_get_module_template_part('templates/parts/share', 'restaurant', '', array());
		}
	}
}

if(!function_exists('onschedule_edge_restaurant_get_single_nav')) {
	/**
	 * Loads prev / next pagination for single menu item
	 */
	function onschedule_edge_restaurant_get_single_nav() {
		if(onschedule_edge_options()->getOptionValue('menu_item_show_navigation') === 'yes') {
			onschedule_edge_get_module_template_part('templates/parts/navigation', 'restaurant', '', array());
		}
	}
}

if(!function_exists('onschedule_edge_restaurant_get_single_author_info')) {
	/**
	 * Loads author info template if enabled
	 */
	function onschedule_edge_restaurant_get_single_author_info() {
		if(onschedule_edge_options()->getOptionValue('menu_item_show_author_info') === 'yes') {
			onschedule_edge_get_module_template_part('templates/parts/author-info', 'restaurant', '', array());
		}
	}
}

if ( ! function_exists('onschedule_edge_get_related_post_type')) {
	/**
	 * Function for returning latest posts types
	 *
	 * @param $post_id
	 * @param array $options
	 * @return WP_Query
	 */
	function onschedule_edge_get_related_post_type($post_id, $options = array()) {
		$categories = wp_get_object_terms($post_id, 'restaurant-menu-section');
		
		$category_ids = array();
		if ($categories) {
			foreach ($categories as $category) {
				$category_ids[] = $category->term_id;
			}
		}
		
		if ($categories) {
			$related_by_category = onschedule_edge_get_related_custom_post_type_by_param($post_id, $category_ids, 'restaurant-menu-section', $options);
			
			if (!empty($related_by_category->posts)) {
				return $related_by_category;
			}
		}
	}
}

if ( ! function_exists('onschedule_edge_get_related_custom_post_type_by_param') ) {
	/**
	 * @param $post_id - Post ID
	 * @param $term_ids - Category or Tag IDs
	 * @param $taxonomy
	 * @param array $options
	 * @return WP_Query
	 */
	function onschedule_edge_get_related_custom_post_type_by_param($post_id, $term_ids, $taxonomy, $options = array()) {
		$args = array(
			'post__not_in'  => array($post_id),
			'order'         => 'DESC',
			'orderby'       => 'date',
			'tax_query'     => array(
				array(
					'taxonomy'  => $taxonomy,
					'field'     => 'term_id',
					'terms'     => $term_ids,
				),
			)
		);
		
		if(is_array($options) && count($options)) {
			if(array_key_exists('posts_per_page', $options)) {
				$args['posts_per_page'] = $options['posts_per_page'];
			}
		}
		
		$related_by_taxonomy = new WP_Query($args);
		
		return $related_by_taxonomy;
	}
}