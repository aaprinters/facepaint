<?php

class OnScheduleEdgeClassFullScreenMenuOpener extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_full_screen_menu_opener',
	        esc_html__('Edge Full Screen Menu Opener', 'onschedule'),
	        array( 'description' => esc_html__( 'Display a "hamburger" icon that opens the full screen menu', 'onschedule'))
        );

		$this->setParams();
    }
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'textfield',
				'name'        => 'widget_elements_color',
				'title'       => esc_html__('Widget Color', 'onschedule'),
				'description' => esc_html__('Define color for full screen menu opener', 'onschedule')
			),
			array(
				'type'        => 'textfield',
				'name'        => 'widget_elements_margin',
				'title'       => esc_html__('Widget Margin', 'onschedule'),
				'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
			)
		);
	}
	
	public function widget($args, $instance) {
		$holder_styles = array();
		if (!empty($instance['widget_elements_color'])) {
			$holder_styles[] = 'color: ' . $instance['widget_elements_color'];
		}
		if (!empty($instance['widget_elements_margin'])) {
			$holder_styles[] = 'margin: ' . $instance['widget_elements_margin'];
		}
		?>
		<a href="javascript:void(0)" class="edgtf-fullscreen-menu-opener" <?php onschedule_edge_inline_style($holder_styles); ?>>
            <span class="edgtf-fm-lines">
                <span class="edgtf-fm-line edgtf-line-1"></span>
                <span class="edgtf-fm-line edgtf-line-2"></span>
                <span class="edgtf-fm-line edgtf-line-3"></span>
            </span>
		</a>
	<?php }
}