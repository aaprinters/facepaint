<?php

class OnScheduleEdgeClassSearchOpener extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_search_opener',
	        esc_html__('Edge Search Opener', 'onschedule'),
	        array( 'description' => esc_html__( 'Display a "search" icon that opens the search form', 'onschedule'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
	            'type'        => 'textfield',
	            'name'        => 'search_icon_size',
                'title'       => esc_html__('Icon Size (px)', 'onschedule'),
                'description' => esc_html__('Define size for search icon', 'onschedule')
            ),
            array(
	            'type'        => 'textfield',
	            'name'        => 'search_icon_color',
                'title'       => esc_html__('Icon Color', 'onschedule'),
                'description' => esc_html__('Define color for search icon', 'onschedule')
            ),
            array(
	            'type'        => 'textfield',
	            'name'        => 'search_icon_hover_color',
                'title'       => esc_html__('Icon Hover Color', 'onschedule'),
                'description' => esc_html__('Define hover color for search icon', 'onschedule')
            ),
	        array(
		        'type' => 'textfield',
		        'name' => 'search_icon_margin',
		        'title' => esc_html__('Icon Margin', 'onschedule'),
		        'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
	        ),
            array(
	            'type'        => 'dropdown',
	            'name'        => 'show_label',
                'title'       => esc_html__('Enable Search Icon Text', 'onschedule'),
                'description' => esc_html__('Enable this option to show search text next to search icon in header', 'onschedule'),
                'options'     => onschedule_edge_get_yes_no_select_array()
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        global $onschedule_edge_global_options, $onschedule_edge_global_IconCollections;

	    $search_type_class    = 'edgtf-search-opener';
	    $styles = array();
	    $show_search_text     = $instance['show_label'] == 'yes' || $onschedule_edge_global_options['enable_search_icon_text'] == 'yes' ? true : false;

	    if(!empty($instance['search_icon_size'])) {
		    $styles[] = 'font-size: '.intval($instance['search_icon_size']).'px';
	    }

	    if(!empty($instance['search_icon_color'])) {
		    $styles[] = 'color: '.$instance['search_icon_color'].';';
	    }

	    if (!empty($instance['search_icon_margin'])) {
		    $styles[] = 'margin: ' . $instance['search_icon_margin'].';';
	    }
	    ?>

	    <a <?php onschedule_edge_inline_attr($instance['search_icon_hover_color'], 'data-hover-color'); ?> <?php onschedule_edge_inline_style($styles); ?>
		    <?php onschedule_edge_class_attribute($search_type_class); ?> href="javascript:void(0)">
            <span class="edgtf-search-opener-wrapper">
                <?php if(isset($onschedule_edge_global_options['search_icon_pack'])) {
	                $onschedule_edge_global_IconCollections->getSearchIcon($onschedule_edge_global_options['search_icon_pack'], false);
                } ?>
	            <?php if($show_search_text) { ?>
		            <span class="edgtf-search-icon-text"><?php esc_html_e('Search', 'onschedule'); ?></span>
	            <?php } ?>
            </span>
	    </a>
    <?php }
}