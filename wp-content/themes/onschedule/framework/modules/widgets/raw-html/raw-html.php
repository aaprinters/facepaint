<?php

class OnScheduleEdgeClassRawHTMLWidget extends OnScheduleEdgeClassWidget {
	/**
	 * Set basic widget options and call parent class construct
	 */
	public function __construct() {
		parent::__construct(
			'edgtf_raw_html_widget',
			esc_html__( 'Edge Raw HTML Widget', 'onschedule' ),
			array( 'description' => esc_html__( 'Add raw HTML holder to widget areas', 'onschedule' ) )
		);
		
		$this->setParams();
	}
	
	/**
	 * Sets widget options
	 */
	protected function setParams() {
		$this->params = array(
			array(
				'type'  => 'textfield',
				'name'  => 'extra_class',
				'title' => esc_html__( 'Extra Class Name', 'onschedule' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'widget_title',
				'title' => esc_html__( 'Widget Title', 'onschedule' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'widget_grid',
				'title'   => esc_html__('Widget Grid', 'onschedule'),
				'options' => array(
					''     => esc_html__('Full Width', 'onschedule'),
					'auto' => esc_html__('Auto', 'onschedule')
				)
			),
			array(
				'type'  => 'textarea',
				'name'  => 'content',
				'title' => esc_html__( 'Content', 'onschedule' )
			)
		);
	}
	
	/**
	 * Generates widget's HTML
	 *
	 * @param array $args args from widget area
	 * @param array $instance widget's options
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		
		$extra_class   = array();
		$extra_class[] = !empty( $instance['extra_class'] ) ? $instance['extra_class'] : '';
		$extra_class[] = !empty( $instance['widget_grid'] ) && $instance['widget_grid'] === 'auto' ? 'edgtf-grid-auto-width' : '';
		?>
		
		<div class="widget edgtf-raw-html-widget <?php echo esc_attr( implode($extra_class) ); ?>">
			<?php
			if ( ! empty( $instance['widget_title'] ) ) {
				echo onschedule_edge_get_module_part($args['before_title'] . esc_html($instance['widget_title']) . $args['after_title']);
			}
			if ( ! empty( $instance['content'] ) ) {
				echo wp_kses( $instance['content'], array(
					'div'    => array(
						'class' => true,
						'style' => true,
						'id'    => true
					),
					'p'      => array(
						'class' => true,
						'style' => true,
						'id'    => true
					),
					'span'   => array(
						'class' => true,
						'style' => true,
						'id'    => true
					),
					'a'      => array(
						'class'  => true,
						'style'  => true,
						'id'     => true,
						'href'   => true,
						'target' => true
					),
					'br'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array()
				) );
			}
			?>
		</div>
		<?php
	}
}