<?php

class OnScheduleEdgeClassSeparatorWidget extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_separator_widget',
	        esc_html__('Edge Separator Widget', 'onschedule'),
	        array( 'description' => esc_html__( 'Add a separator element to your widget areas', 'onschedule'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type' => 'dropdown',
                'name' => 'type',
                'title' => esc_html__('Type', 'onschedule'),
                'options' => array(
                    'normal' => esc_html__('Normal', 'onschedule'),
                    'full-width' => esc_html__('Full Width', 'onschedule')
                )
            ),
            array(
                'type' => 'dropdown',
                'name' => 'position',
                'title' => esc_html__('Position', 'onschedule'),
                'options' => array(
                    'center' => esc_html__('Center', 'onschedule'),
                    'left' => esc_html__('Left', 'onschedule'),
                    'right' => esc_html__('Right', 'onschedule')
                )
            ),
            array(
                'type' => 'dropdown',
                'name' => 'border_style',
                'title' => esc_html__('Style', 'onschedule'),
                'options' => array(
                    'solid' => esc_html__('Solid', 'onschedule'),
                    'dashed' => esc_html__('Dashed', 'onschedule'),
                    'dotted' => esc_html__('Dotted', 'onschedule')
                )
            ),
            array(
                'type' => 'textfield',
                'name' => 'color',
                'title' => esc_html__('Color', 'onschedule')
            ),
            array(
                'type' => 'textfield',
                'name' => 'width',
                'title' => esc_html__('Width (px or %)', 'onschedule')
            ),
            array(
                'type' => 'textfield',
                'name' => 'thickness',
                'title' => esc_html__('Thickness (px)', 'onschedule')
            ),
            array(
                'type' => 'textfield',
                'name' => 'top_margin',
                'title' => esc_html__('Top Margin', 'onschedule')
            ),
            array(
                'type' => 'textfield',
                'name' => 'bottom_margin',
                'title' => esc_html__('Bottom Margin', 'onschedule')
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        extract($args);

        //prepare variables
        $params = '';

        //is instance empty?
        if(is_array($instance) && count($instance)) {
            //generate shortcode params
            foreach($instance as $key => $value) {
                $params .= " $key='$value' ";
            }
        }

        echo '<div class="widget edgtf-separator-widget">';
            echo do_shortcode("[edgtf_separator $params]"); // XSS OK
        echo '</div>';
    }
}