<?php

class OnScheduleEdgeClassButtonWidget extends OnScheduleEdgeClassWidget {
	public function __construct() {
		parent::__construct(
			'edgtf_button_widget',
			esc_html__('Edge Button Widget', 'onschedule'),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'onschedule'))
		);

		$this->setParams();
	}

	/**
	 * Sets widget options
	 */
	protected function setParams() {
		$this->params = array(
			array(
				'type' => 'dropdown',
				'name' => 'type',
				'title' => esc_html__('Type', 'onschedule'),
				'options' => array(
					'solid' => esc_html__('Solid', 'onschedule'),
					'outline' => esc_html__('Outline', 'onschedule'),
					'simple' => esc_html__('Simple', 'onschedule')
				)
			),
			array(
				'type' => 'dropdown',
				'name' => 'size',
				'title' => esc_html__('Size', 'onschedule'),
				'options' => array(
					'small' => esc_html__('Small', 'onschedule'),
					'medium' => esc_html__('Medium', 'onschedule'),
					'large' => esc_html__('Large', 'onschedule'),
					'huge' => esc_html__('Huge', 'onschedule')
				),
				'description' => esc_html__('This option is only available for solid and outline button type', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'text',
				'title' => esc_html__('Text', 'onschedule'),
				'default' => esc_html__('Button Text', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name'  => 'link',
				'title' => esc_html__('Link', 'onschedule')
			),
			array(
				'type' => 'dropdown',
				'name' => 'target',
				'title' => esc_html__('Link Target', 'onschedule'),
				'options' => array(
					'_self' => esc_html__('Same Window', 'onschedule'),
					'_blank' => esc_html__('New Window', 'onschedule')
				)
			),
			array(
				'type' => 'textfield',
				'name' => 'color',
				'title' => esc_html__('Color', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'hover_color',
				'title' => esc_html__('Hover Color', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'background_color',
				'title' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('This option is only available for solid button type', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'hover_background_color',
				'title' => esc_html__('Hover Background Color', 'onschedule'),
				'description' => esc_html__('This option is only available for solid button type', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'border_color',
				'title' => esc_html__('Border Color', 'onschedule'),
				'description' => esc_html__('This option is only available for solid and outline button type', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'hover_border_color',
				'title' => esc_html__('Hover Border Color', 'onschedule'),
				'description' => esc_html__('This option is only available for solid and outline button type', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'margin',
				'title' => esc_html__('Margin', 'onschedule'),
				'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
			)
		);
	}

	/**
	 * Generates widget's HTML
	 *
	 * @param array $args args from widget area
	 * @param array $instance widget's options
	 */
	public function widget($args, $instance) {
		$params = '';

		if (!is_array($instance)) { $instance = array(); }

		// Filter out all empty params
		$instance = array_filter($instance, function($array_value) { return trim($array_value) != ''; });

		// Default values
		if (!isset($instance['text'])) { $instance['text'] = 'Button Text'; }

		// Generate shortcode params
		foreach($instance as $key => $value) {
			$params .= " $key='$value' ";
		}

		echo '<div class="widget edgtf-button-widget">';
			echo do_shortcode("[edgtf_button $params]"); // XSS OK
		echo '</div>';
	}
}