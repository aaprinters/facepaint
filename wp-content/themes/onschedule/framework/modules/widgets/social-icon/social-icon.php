<?php

class OnScheduleEdgeClassSocialIconWidget extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_social_icon_widget',
            esc_html__('Edge Social Icon Widget', 'onschedule'),
            array( 'description' => esc_html__( 'Add social network icons to widget areas', 'onschedule'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array_merge(
            onschedule_edge_icon_collections()->getSocialIconWidgetParamsArray(),
            array(
                array(
                    'type' => 'textfield',
                    'name' => 'link',
                    'title' => esc_html__('Link', 'onschedule')
                ),
                array(
                    'type' => 'dropdown',
                    'name' => 'target',
                    'title' => esc_html__('Target', 'onschedule'),
                    'options' => array(
                        '_self' => esc_html__('Same Window', 'onschedule'),
                        '_blank' => esc_html__('New Window', 'onschedule')
                    )
                ),
                array(
                    'type' => 'textfield',
                    'name' => 'icon_size',
                    'title' => esc_html__('Icon Size (px)', 'onschedule')
                ),
                array(
                    'type' => 'textfield',
                    'name' => 'color',
                    'title' => esc_html__('Color', 'onschedule')
                ),
                array(
                    'type' => 'textfield',
                    'name' => 'hover_color',
                    'title' => esc_html__('Hover Color', 'onschedule')
                ),
	            array(
		            'type' => 'dropdown',
		            'name' => 'custom_hover_color',
		            'title' => esc_html__('Use Custom Hover Color', 'onschedule'),
		            'options' => onschedule_edge_get_yes_no_select_array(false, true)
	            ),
                array(
                    'type' => 'textfield',
                    'name' => 'margin',
                    'title' => esc_html__('Margin', 'onschedule'),
                    'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
                ),
	            array(
		            'type' => 'textfield',
		            'name' => 'padding',
		            'title' => esc_html__('Padding', 'onschedule'),
		            'description' => esc_html__('Insert padding in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
	            ),
	            array(
		            'type' => 'textfield',
		            'name' => 'border',
		            'title' => esc_html__('Border Right', 'onschedule'),
		            'description' => esc_html__('Insert border right width (e.g 1px). Leave empty to exclude border.', 'onschedule')
	            ),
	            array(
		            'type' => 'textfield',
		            'name' => 'border_color',
		            'title' => esc_html__('Border Color', 'onschedule')
	            )
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        $icon_styles = array();
	    $holder_styles = array();

        if (!empty($instance['color'])) {
            $icon_styles[] = 'color: '.$instance['color'].';';
        }

        if (!empty($instance['icon_size'])) {
            $icon_styles[] = 'font-size: '.onschedule_edge_filter_px($instance['icon_size']).'px;';
        }

        if (!empty($instance['margin'])) {
            $icon_styles[] = 'margin: '.$instance['margin'].';';
        }

	    if (!empty($instance['padding'])) {
		    $holder_styles[] = 'padding: '.$instance['padding'].';';
	    }

	    if (!empty($instance['border']) && !empty($instance['border_color']) ) {
		    $holder_styles[] = 'border-right: '.$instance['border'].' solid '.$instance['border_color'].';';
	    } else if( !empty($instance['border']) ) {
		    $holder_styles[] = 'border-right: '.$instance['border'].' solid #ddd;';
	    }
	    
	    $holder_classes = '';
	
	    $instance['custom_hover_color'] = isset($instance['custom_hover_color']) && !empty($instance['custom_hover_color']) ? $instance['custom_hover_color'] : 'yes';
        if(isset($instance['custom_hover_color']) && $instance['custom_hover_color'] === 'yes') {
	        $holder_classes .= ' edgtf-has-custom-hover';
        }

        $link = '#';
        if (!empty($instance['link'])) {
            $link = $instance['link'];
        }

        $target = '_self';
        if (!empty($instance['target'])) {
            $target = $instance['target'];
        }

        $original_color = '';
        if (!empty($instance['color'])) {
            $original_color = $instance['color'];
        }

        $hover_color = '';
        if (!empty($instance['hover_color'])) {
            $hover_color = $instance['hover_color'];
        }

        $icon_html = 'fa-facebook';
        $icon_holder_html = '';
        if (!empty($instance['icon_pack']) && $instance['icon_pack'] !== '') {
            if (!empty($instance['fa_icon']) && $instance['fa_icon'] !== '' && $instance['icon_pack'] === 'font_awesome') {
                $icon_html = $instance['fa_icon'];
                $icon_holder_html = '<i class="edgtf-social-icon-widget fa '.$icon_html.'" '.onschedule_edge_get_inline_style($holder_styles).'></i>';
            } else if (!empty($instance['fe_icon']) && $instance['fe_icon'] !== '' && $instance['icon_pack'] === 'font_elegant') {
                $icon_html = $instance['fe_icon'];
                $icon_holder_html = '<span class="edgtf-social-icon-widget '.$icon_html.'" '.onschedule_edge_get_inline_style($holder_styles).'></span>';
            } else if (!empty($instance['ion_icon']) && $instance['ion_icon'] !== '' && $instance['icon_pack'] === 'ion_icons') {
                $icon_html = $instance['ion_icon'];
                $icon_holder_html = '<span class="edgtf-social-icon-widget '.$icon_html.'" '.onschedule_edge_get_inline_style($holder_styles).'></span>';
            } else if (!empty($instance['simple_line_icons']) && $instance['simple_line_icons'] !== '' && $instance['icon_pack'] === 'simple_line_icons') {
                $icon_html = $instance['simple_line_icons'];
                $icon_holder_html = '<span class="edgtf-social-icon-widget '.$icon_html.'" '.onschedule_edge_get_inline_style($holder_styles).'></span>';
            } else {
                $icon_holder_html = '<i class="edgtf-social-icon-widget fa '.$icon_html.'" '.onschedule_edge_get_inline_style($holder_styles).'></i>';
            }
        }
        ?>

        <a class="edgtf-social-icon-widget-holder <?php echo esc_attr($holder_classes); ?>" <?php echo onschedule_edge_get_inline_attr($hover_color, 'data-hover-color'); ?> <?php echo onschedule_edge_get_inline_attr($original_color, 'data-original-color'); ?> <?php onschedule_edge_inline_style($icon_styles) ?> href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
            <?php echo onschedule_edge_get_module_part($icon_holder_html); ?>
        </a>
    <?php
    }
}