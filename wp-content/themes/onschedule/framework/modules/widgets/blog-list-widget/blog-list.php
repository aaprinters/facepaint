<?php

class OnScheduleEdgeClassBlogListWidget extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_blog_list_widget',
            esc_html__('Edge Blog List Widget', 'onschedule'),
            array( 'description' => esc_html__( 'Display a list of your blog posts', 'onschedule'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type'  => 'textfield',
                'name'  => 'widget_title',
                'title' => esc_html__('Widget Title', 'onschedule')
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'type',
                'title'   => esc_html__('Type', 'onschedule'),
                'options' => array(
                    'simple' => esc_html__('Simple', 'onschedule'),
                    'minimal' => esc_html__('Minimal', 'onschedule')
                )
            ),
            array(
                'type'  => 'textfield',
                'name'  => 'number_of_posts',
                'title' => esc_html__('Number of Posts', 'onschedule')
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'space_between_columns',
                'title'   => esc_html__('Space Between items', 'onschedule'),
                'options' => array(
                    'normal' => esc_html__('Normal', 'onschedule'),
                    'small' => esc_html__('Small', 'onschedule'),
                    'tiny' => esc_html__('Tiny', 'onschedule'),
                    'no' => esc_html__('No Space', 'onschedule')
                )
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'order_by',
                'title'   => esc_html__('Order By', 'onschedule'),
                'options' => array(
                    'title' => esc_html__('Title', 'onschedule'),
                    'date' => esc_html__('Date', 'onschedule'),
                    'rand' => esc_html__('Random', 'onschedule'),
                    'name' => esc_html__('Post Name', 'onschedule')
                )
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'order',
                'title'   => esc_html__('Order', 'onschedule'),
                'options' => array(
                    'ASC' => esc_html__('ASC', 'onschedule'),
                    'DESC' => esc_html__('DESC', 'onschedule')
                )
            ),
            array(
                'type'        => 'textfield',
                'name'        => 'category',
                'title'       => esc_html__('Category Slug', 'onschedule'),
                'description' => esc_html__('Leave empty for all or use comma for list', 'onschedule')
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'title_tag',
                'title'   => esc_html__('Title Tag', 'onschedule'),
                'options' => onschedule_edge_get_title_tag(true)
            ),
            array(
                'type'    => 'dropdown',
                'name'    => 'title_transform',
                'title'   => esc_html__('Title Text Transform', 'onschedule'),
                'options' => onschedule_edge_get_text_transform_array(true)
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        $params = '';

        if (!is_array($instance)) { $instance = array(); }

        $instance['post_info_section'] = 'yes';
        $instance['number_of_columns'] = '1';

        // Filter out all empty params
        $instance = array_filter($instance, function($array_value) { return trim($array_value) != ''; });

        //generate shortcode params
        foreach($instance as $key => $value) {
            $params .= " $key='$value' ";
        }

        $available_types = array('simple', 'classic');

        if (!in_array($instance['type'], $available_types)) {
            $instance['type'] = 'simple';
        }

        echo '<div class="widget edgtf-blog-list-widget">';
            if(!empty($instance['widget_title'])) {
				echo onschedule_edge_get_module_part($args['before_title'] . $instance['widget_title'] . $args['after_title']);
            }

            echo do_shortcode("[edgtf_blog_list $params]"); // XSS OK
        echo '</div>';
    }
}