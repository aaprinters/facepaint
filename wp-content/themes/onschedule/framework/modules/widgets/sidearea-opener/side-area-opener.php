<?php

class OnScheduleEdgeClassSideAreaOpener extends OnScheduleEdgeClassWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_side_area_opener',
	        esc_html__('Edge Side Area Opener', 'onschedule'),
	        array( 'description' => esc_html__( 'Display a "hamburger" icon that opens the side area', 'onschedule'))
        );

        $this->setParams();
    }
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'textfield',
				'name'        => 'widget_elements_color',
				'title'       => esc_html__('Widget Color', 'onschedule'),
				'description' => esc_html__('Define color for side area opener', 'onschedule')
			),
			array(
				'type'        => 'textfield',
				'name'        => 'widget_elements_margin',
				'title'       => esc_html__('Widget Margin', 'onschedule'),
				'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule')
			),
			array(
				'type' => 'textfield',
				'name' => 'widget_title',
				'title' => esc_html__('Side Area Opener Title', 'onschedule')
			)
		);
	}
	
	public function widget($args, $instance) {
		$holder_styles = array();
		if (!empty($instance['widget_elements_color'])) {
			$holder_styles[] = 'color: ' . $instance['widget_elements_color'].';';
		}
		if (!empty($instance['widget_elements_margin'])) {
			$holder_styles[] = 'margin: ' . $instance['widget_elements_margin'];
		}
		?>
		<a class="edgtf-side-menu-button-opener" href="javascript:void(0)" <?php onschedule_edge_inline_style($holder_styles); ?>>
			<?php if (!empty($instance['widget_title'])) { ?>
				<h5 class="edgtf-side-menu-title"><?php echo esc_html($instance['widget_title']); ?></h5>
			<?php } ?>
			<span class="edgtf-side-menu-icon ion-android-menu"></span>
		</a>
	<?php }
}