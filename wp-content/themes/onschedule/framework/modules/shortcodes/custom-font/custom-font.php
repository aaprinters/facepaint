<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\CustomFont;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class CustomFont
 */
class CustomFont implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'edgtf_custom_font';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map( array(
				'name'      => esc_html__('Edge Custom Font', 'onschedule'),
				'base'      => $this->getBase(),
				'category'  => esc_html__('by EDGE', 'onschedule'),
				'icon'      => 'icon-wpb-custom-font extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_tag',
						'heading'     => esc_html__('Title Tag', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_title_tag(true)),
						'save_always' => true
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'font_family',
						'heading'    => esc_html__('Font Family', 'onschedule')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'font_size',
						'heading'    => esc_html__('Font Size (px)', 'onschedule')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'line_height',
						'heading'    => esc_html__('Line Height (px)', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'font_weight',
						'heading'     => esc_html__('Font Weight', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_font_weight_array(true)),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'font_style',
						'heading'     => esc_html__('Font Style', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_font_style_array(true)),
						'save_always' => true
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'letter_spacing',
						'heading'    => esc_html__('Letter Spacing (px)', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'text_transform',
						'heading'     => esc_html__('Text Transform', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_text_transform_array(true)),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'text_decoration',
						'heading'     => esc_html__('Text Decoration', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_text_decorations(true)),
						'save_always' => true
					),
					array(
						'type'       => 'colorpicker',
						'param_name' => 'color',
						'heading'    => esc_html__('Color', 'onschedule')
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'text_align',
						'heading'    => esc_html__('Text Align', 'onschedule'),
						'value'      => array(
							esc_html__('Default', 'onschedule')   => '',
							esc_html__('Left', 'onschedule') => 'left',
							esc_html__('Center', 'onschedule') => 'center',
							esc_html__('Right', 'onschedule') => 'right',
							esc_html__('Justify', 'onschedule') => 'justify'
						),
						'save_always' => true
					),
					array(
						'type'       => 'textarea',
						'param_name' => 'content_text',
						'heading'    => esc_html__('Content', 'onschedule')
					)
				)
		) );
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'title_tag'       => 'h2',
			'font_family'     => '',
			'font_size'       => '',
			'line_height'     => '',
			'font_weight'     => '',
			'font_style'      => '',
			'letter_spacing'  => '',
			'text_transform'  => '',
			'text_decoration' => '',
			'color'           => '',
			'text_align'      => '',
			'content_text'    => ''
		);

		$params = shortcode_atts($args, $atts);

		$params['holder_styles'] = $this->getHolderStyles($params);
		$params['holder_data']   = $this->getHolderData($params);
		
		$params['title_tag']     = !empty($params['title_tag']) ? $params['title_tag'] : $args['title_tag'];

		//Get HTML from template
		$html = onschedule_edge_get_shortcode_module_template_part('templates/custom-font', 'custom-font', '', $params);

		return $html;
	}

	/**
	 * Return holder styles
	 *
	 * @param $params
	 * @return string
	 */
	private function getHolderStyles($params) {
		$styles = array();

		if ($params['font_family'] !== '') {
			$styles[] = 'font-family: '.$params['font_family'];
		}
		
		if (!empty($params['font_size'])) {
			$styles[] = 'font-size: '.onschedule_edge_filter_px($params['font_size']).'px';
		}
		
		if (!empty($params['line_height'])) {
			$styles[] = 'line-height: '.onschedule_edge_filter_px($params['line_height']).'px';
		}
		
		if (!empty($params['font_weight'])) {
			$styles[] = 'font-weight: '.$params['font_weight'];
		}

		if (!empty($params['font_style'])) {
			$styles[] = 'font-style: '.$params['font_style'];
		}
		
		if (!empty($params['letter_spacing'])) {
			$styles[] = 'letter-spacing: '.onschedule_edge_filter_px($params['letter_spacing']).'px';
		}
		
		if (!empty($params['text_transform'])) {
			$styles[] = 'text-transform: '.$params['text_transform'];
		}
		
		if (!empty($params['text_decoration'])) {
			$styles[] = 'text-decoration: '.$params['text_decoration'];
		}
		
		if (!empty($params['text_align'])) {
			$styles[] = 'text-align: '.$params['text_align'];
		}
		
		if (!empty($params['color'])) {
			$styles[] = 'color: '.$params['color'];
		}

		return implode(';', $styles);
	}

	/**
	 * Return holder data attr
	 *
	 * @param $params
	 * @return string
	 */
	private function getHolderData($params) {
		$data = array();
		
		if (!empty($params['font_size'])) {
			$data[] = 'data-font-size='.onschedule_edge_filter_px($params['font_size']);
		}
		
		if (!empty($params['line_height'])) {
			$data[] = 'data-line-height='.onschedule_edge_filter_px($params['line_height']);
		}
		
		return implode(' ', $data);
	}
}