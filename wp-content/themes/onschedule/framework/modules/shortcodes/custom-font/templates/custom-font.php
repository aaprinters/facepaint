<<?php echo esc_attr($title_tag); ?> class="edgtf-custom-font-holder" <?php onschedule_edge_inline_style($holder_styles); ?> <?php echo esc_attr($holder_data); ?>>
	<?php echo esc_html($content_text); ?>
</<?php echo esc_attr($title_tag); ?>>