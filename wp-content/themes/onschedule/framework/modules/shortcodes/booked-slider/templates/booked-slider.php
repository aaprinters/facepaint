<div class="edgtf-booked-slider-holder <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-bs-rev-holder">
		<?php echo do_shortcode($content); ?>
	</div>
	<div class="edgtf-bs-calendar-holder">
		<?php if($calendar_in_grid) { ?>
			<div class="edgtf-grid">
		<?php } ?>
			<div class="edgtf-bs-calendar-content" <?php echo onschedule_edge_get_inline_style($content_styles); ?> <?php echo onschedule_edge_get_inline_attrs($calendar_responsive_data); ?>>
				<?php if(!empty($calendar_title)) { ?>
					<<?php echo esc_attr($calendar_title_tag); ?> class="edgtf-bs-calendar-title"><?php echo esc_html($calendar_title); ?></<?php echo esc_attr($calendar_title_tag); ?>>
				<?php }	?>
				<?php echo do_shortcode('[booked-calendar '.$calendar_attrs.']'); ?>
			</div>
		<?php if($calendar_in_grid) { ?>
			</div>
		<?php } ?>
	</div>
</div>