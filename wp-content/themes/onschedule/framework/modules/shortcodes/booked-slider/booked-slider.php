<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\BookedSlider;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class BookedSlider implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;
	
	public function __construct() {
		$this->base = 'edgtf_booked_slider';
		
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'            => esc_html__('Edge Booked Slider', 'onschedule'),
			'base'            => $this->base,
			'category'        => esc_html__('by EDGE', 'onschedule'),
			'icon'            => 'icon-wpb-booked-slider extended-custom-icon',
			'js_view'         => 'VcColumnView',
			'as_parent'       => array('only' => 'rev_slider_vc'),
			'content_element' => true,
			'params'          => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar',
					'heading'     => esc_html__('Calendar', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_booked_calendar_array()),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'calendar_title',
					'heading'     => esc_html__('Calendar Title', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_title_tag',
					'heading'     => esc_html__('Calendar Title Tag', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_title_tag(true)),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_skin',
					'heading'     => esc_html__('Calendar Skin', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_booked_calendar_skins()),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_size',
					'heading'     => esc_html__('Calendar Size', 'onschedule'),
					'value'       => array(
						esc_html__('Large', 'onschedule') => 'large',
						esc_html__('Small', 'onschedule') => 'small'
					),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_style',
					'heading'     => esc_html__('Calendar Style', 'onschedule'),
					'value'       => array(
						esc_html__('Calendar', 'onschedule') => 'calendar',
						esc_html__('Calendar Simple', 'onschedule') => 'calendar_simple',
						esc_html__('List', 'onschedule')     => 'list'
					),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_position',
					'heading'     => esc_html__('Calendar Position', 'onschedule'),
					'value'       => array(
						esc_html__('Left', 'onschedule')  => 'left',
						esc_html__('Right', 'onschedule') => 'right',
						esc_html__('Center', 'onschedule') => 'center'
					),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'calendar_width',
					'heading'     => esc_html__('Calendar Width (px or %)', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_offset',
					'heading'     => esc_html__('Calendar Top Offset (px or %)', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'side_offset',
					'heading'     => esc_html__('Calendar Side Offset (px or %)', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_is_middle',
					'heading'     => esc_html__('Set Calendar Vertical Align Middle', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false)),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_in_grid',
					'heading'     => esc_html__('Set Calendar In Grid', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'width_laptop',
					'heading'     => esc_html__('Set calendar width for laptop screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Laptop', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_offset_laptop',
					'heading'     => esc_html__('Set calendar top offset for laptop screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Laptop', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'side_offset_laptop',
					'heading'     => esc_html__('Set calendar side offset for laptop screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Laptop', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'width_ipad_landscape',
					'heading'     => esc_html__('Set calendar width for tablet landscape screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Landscape', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_offset_ipad_landscape',
					'heading'     => esc_html__('Set calendar top offset for tablet landscape screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Landscape', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'side_offset_ipad_landscape',
					'heading'     => esc_html__('Set calendar side offset for tablet landscape screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Landscape', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'width_ipad',
					'heading'     => esc_html__('Set calendar width for tablet portrait screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Portrait', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_offset_ipad',
					'heading'     => esc_html__('Set calendar top offset for tablet portrait screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Portrait', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'side_offset_ipad',
					'heading'     => esc_html__('Set calendar side offset for tablet portrait screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Tablet Portrait', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'width_mobile',
					'heading'     => esc_html__('Set calendar width for mobile devices screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Mobile', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_offset_mobile',
					'heading'     => esc_html__('Set calendar top offset for mobile devices screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Mobile', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'side_offset_mobile',
					'heading'     => esc_html__('Set calendar top offset for mobile devices screen size and smaller', 'onschedule'),
					'description' => esc_html__('Please insert value with px or %', 'onschedule'),
					'group'       => esc_html__('Mobile', 'onschedule')
				),
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'calendar'                   => '',
			'calendar_title'             => '',
			'calendar_title_tag'         => 'h4',
			'calendar_skin'              => 'default',
			'calendar_size'              => 'large',
			'calendar_style'             => 'calendar',
			'calendar_position'          => 'right',
			'calendar_width'             => '',
			'top_offset'                 => '',
			'side_offset'                => '',
			'calendar_is_middle'         => 'no',
			'calendar_in_grid'           => 'yes',
			'width_laptop'               => '',
			'top_offset_laptop'          => '',
			'side_offset_laptop'         => '',
			'width_ipad_landscape'       => '',
			'top_offset_ipad_landscape'  => '',
			'side_offset_ipad_landscape' => '',
			'width_ipad'                 => '',
			'top_offset_ipad'            => '',
			'side_offset_ipad'           => '',
			'width_mobile'               => '',
			'top_offset_mobile'          => '',
			'side_offset_mobile'         => ''
		);
		$params = shortcode_atts($args, $atts);

		$params['holder_classes']  = $this->getHolderClasses($params);
		$params['calendar_responsive_data'] = $this->getCalendarResponsiveData($params);
		$params['content_styles']  = $this->getContentStyles($params);
		
		$params['content']          = $content;
		$params['calendar_in_grid'] = $params['calendar_in_grid'] === 'yes' ? true : false;
		$params['calendar_attrs']   = $this->getCalendarAttributes($params);
		
		$params['calendar_title_tag'] = !empty($params['calendar_title_tag']) ? $params['calendar_title_tag'] : $args['calendar_title_tag'];
		
		return onschedule_edge_get_shortcode_module_template_part('templates/booked-slider', 'booked-slider', '', $params);
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holderClasses = array();

		if(!empty($params['calendar_position'])){
			$holderClasses[] = 'edgtf-'.$params['calendar_position'].'-position';
		}

		if(!empty($params['calendar_skin'])){
			$holderClasses[] = 'edgtf-'.$params['calendar_skin'];
		}

		if( $params['calendar_skin'] !== 'default' ){
			$holderClasses[] = 'edgtf-custom-style';
		}

		if(!empty($params['calendar_style'])){
			$holderClasses[] = 'edgtf-calendar-style-'.$params['calendar_style'];
		}
		
		if(!empty($params['calendar_style']) && $params['calendar_style'] === 'calendar_simple'){
			$holderClasses[] = 'edgtf-calendar-simple';
		}
		
		if($params['calendar_is_middle'] === 'yes') {
			$holderClasses[] = 'edgtf-bs-calendar-is-middle';
		}

		if($params['calendar_in_grid'] === 'yes'){
			$holderClasses[] = 'edgtf-calendar-in-grid';
		}

		return implode(' ', $holderClasses);
	}
	
	/**
	 * Generates content styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getContentStyles($params) {
		$styles = array();
		
		if ($params['calendar_width'] !== '') {
			$styles[] = 'width: '.$params['calendar_width'];
		}
		
		if ($params['top_offset'] !== '' && $params['calendar_is_middle'] !== 'yes') {
			$styles[] = 'top: '.$params['top_offset'];
		} else if ($params['top_offset'] !== '' && $params['calendar_is_middle'] === 'yes') {
			$styles[] = 'margin-top: '.$params['top_offset'];
		}
		
		if ($params['side_offset'] !== '' && $params['calendar_position'] === 'left') {
			$styles[] = 'left: '.$params['side_offset'];
		}
		
		if ($params['side_offset'] !== '' && $params['calendar_position'] === 'right') {
			$styles[] = 'right: '.$params['side_offset'];
		}
		
		return implode(';', $styles);
	}
	
	/**
	 * Generates calendar attributes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCalendarAttributes($params){
		$attrs = array();
		
		if(!empty($params['calendar'])){
			$attrs[] = 'calendar="'.esc_attr($params['calendar']).'"';
		}
		
		if(!empty($params['calendar_size'])){
			$attrs[] = 'size="'.esc_attr($params['calendar_size']).'"';
		}
		
		if(!empty($params['calendar_style'])){
			$calendar_style = $params['calendar_style'] === 'calendar_simple' ? 'calendar' : $params['calendar_style'];
			
			$attrs[] = 'style="'.esc_attr($calendar_style).'"';
		}
		
		return implode(' ', $attrs);
	}

	/**
	 * Return Calendar Responsive data
	 *
	 * @param $params
	 * @return array
	 */
	private function getCalendarResponsiveData($params) {
		$data = array();

		if ($params['width_laptop'] !== '') {
			$data['data-width-laptop'] = $params['width_laptop'];
		}

		if ($params['top_offset_laptop'] !== '') {
			$data['data-top-offset-laptop'] = $params['top_offset_laptop'];
		}

		if ($params['side_offset_laptop'] !== '') {
			$data['data-side-offset-laptop'] = $params['side_offset_laptop'];
		}
		
		if ($params['width_ipad_landscape'] !== '') {
			$data['data-width-ipad-landscape'] = $params['width_ipad_landscape'];
		}
		
		if ($params['top_offset_ipad_landscape'] !== '') {
			$data['data-top-offset-ipad-landscape'] = $params['top_offset_ipad_landscape'];
		}
		
		if ($params['side_offset_ipad_landscape'] !== '') {
			$data['data-side-offset-ipad-landscape'] = $params['side_offset_ipad_landscape'];
		}

		if ($params['width_ipad'] !== '') {
			$data['data-width-ipad'] = $params['width_ipad'];
		}

		if ($params['top_offset_ipad'] !== '') {
			$data['data-top-offset-ipad'] = $params['top_offset_ipad'];
		}

		if ($params['side_offset_ipad'] !== '') {
			$data['data-side-offset-ipad'] = $params['side_offset_ipad'];
		}

		if ($params['width_mobile'] !== '') {
			$data['data-width-mobile'] = $params['width_mobile'];
		}

		if ($params['top_offset_mobile'] !== '') {
			$data['data-top-offset-mobile'] = $params['top_offset_mobile'];
		}

		if ($params['side_offset_mobile'] !== '') {
			$data['data-side-offset-mobile'] = $params['side_offset_mobile'];
		}

		return $data;
	}
}