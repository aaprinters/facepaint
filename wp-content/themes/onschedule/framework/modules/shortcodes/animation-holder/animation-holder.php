<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\AnimationHolder;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
* class Animation Holder
*/
class AnimationHolder implements ShortcodeInterface{
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'edgtf_animation_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return	$this->base;
	}

	public function vcMap() {
		vc_map( array(
			'name' =>  esc_html__('Edge Animation Holder', 'onschedule'),
			'base' => $this->base,
			"as_parent" => array('except' => 'vc_row'),
			'content_element' => true,
			'category' => esc_html__('by EDGE', 'onschedule'),
			'icon' => 'icon-wpb-animation-holder extended-custom-icon',
			'show_settings_on_create' => true,
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type'       => 'dropdown',
					'param_name' => 'animation',
					'heading'    => esc_html__('Animation Type', 'onschedule'),
					'value'      => array(
						esc_html__('Element Grow In', 'onschedule') => 'edgtf-grow-in',
						esc_html__('Element Fade In Down', 'onschedule') => 'edgtf-fade-in-down',
						esc_html__('Element From Fade', 'onschedule') => 'edgtf-element-from-fade',
						esc_html__('Element From Left', 'onschedule') => 'edgtf-element-from-left',
						esc_html__('Element From Right', 'onschedule') => 'edgtf-element-from-right',
						esc_html__('Element From Top', 'onschedule') => 'edgtf-element-from-top',
						esc_html__('Element From Bottom', 'onschedule') => 'edgtf-element-from-bottom',
						esc_html__('Element Flip In', 'onschedule') => 'edgtf-flip-in',
						esc_html__('Element X Rotate', 'onschedule') => 'edgtf-x-rotate',
						esc_html__('Element Z Rotate', 'onschedule') => 'edgtf-z-rotate',
						esc_html__('Element Y Translate', 'onschedule') => 'edgtf-y-translate',
						esc_html__('Element Fade In X Rotate', 'onschedule') => 'edgtf-fade-in-left-x-rotate',
					)
				),
                array(
                    'type'       => 'textfield',
                    'param_name' => 'animation_delay',
                    'heading'    => esc_html__('Animation Delay (ms)', 'onschedule')
                )
			)
		) );
	}

	public function render($atts, $content = null) {
		$args = array(
			'animation'       => '',
			'animation_delay' => ''
        );

        extract(shortcode_atts($args, $atts));

        $html = '<div class="edgtf-animation-holder '. esc_attr($animation) .'" data-animation="'.esc_attr($animation).'" data-animation-delay="'.esc_attr($animation_delay).'"><div class="edgtf-animation-inner">'.do_shortcode($content).'</div></div>';

        return $html;
	}
}