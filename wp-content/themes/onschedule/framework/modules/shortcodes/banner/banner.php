<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Banner;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class Banner implements ShortcodeInterface{

	private $base;

	/**
	 * Banner constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_banner';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Banner', 'onschedule'),
			'base'                      => $this->getBase(),
			'category'                  => esc_html__('by EDGE', 'onschedule'),
			'icon' 						=> 'icon-wpb-banner extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'		  => 'attach_image',
					'param_name'  => 'image',
					'heading'	  => esc_html__('Image', 'onschedule'),
					'description' => esc_html__('Select image from media library', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'link',
					'heading'     => esc_html__('Link', 'onschedule')
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'target',
					'heading'    => esc_html__('Target', 'onschedule'),
					'value'      => array(
						esc_html__('Same Window', 'onschedule')  => '_self',
						esc_html__('New Window', 'onschedule') => '_blank'
					),
					'dependency'  => array('element' => 'link', 'not_empty' => true),
				),
				array(
					'type'		  => 'attach_image',
					'param_name'  => 'content_image',
					'heading'	  => esc_html__('Content Image', 'onschedule'),
					'description' => esc_html__('Select image from media library', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'content_padding',
					'heading'     => esc_html__('Content Padding (px or %)', 'onschedule'),
					'description' => esc_html__('Insert padding in format: top right bottom left (e.g. 10px 10px 10px 10px)', 'onschedule')
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'title',
					'heading'    => esc_html__('Title', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'title_tag',
					'heading'     => esc_html__('Title Tag', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_title_tag(true, array('p' => 'p'))),
					'save_always' => true,
					'dependency'  => array('element' => 'title', 'not_empty' => true)
				),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'title_color',
					'heading'    => esc_html__('Title Color', 'onschedule'),
					'dependency' => array('element' => 'title', 'not_empty' => true)
				),
				array(
					'type'       => 'textarea',
					'param_name' => 'text',
					'heading'    => esc_html__('Text', 'onschedule')
				),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'text_color',
					'heading'    => esc_html__('Text Color', 'onschedule'),
					'dependency' => array('element' => 'text', 'not_empty' => true)
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'text_top_margin',
					'heading'    => esc_html__('Text Top Margin (px)', 'onschedule'),
					'dependency' => array('element' => 'text', 'not_empty' => true)
				)
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'image'			   => '',
			'link'             => '',
			'target'           => '_self',
			'content_image'    => '',
			'content_padding'  => '',
			'title'			   => '',
			'title_tag'	 	   => 'h5',
			'title_color'      => '',
			'text'			   => '',
			'text_color'       => '',
			'text_top_margin'  => ''
		);

		$params = shortcode_atts($args, $atts);
		
		$params['content_styles'] = $this->getContentStyles($params);
		
		$params['title_tag']    = !empty($title_tag) ? $params['title_tag'] : $args['title_tag'];
		$params['title_styles'] = $this->getTitleStyles($params);
		
		$params['text_styles']  = $this->getTextStyles($params);

		$html = onschedule_edge_get_shortcode_module_template_part('templates/banner', 'banner', '', $params);

		return $html;
	}
	
	private function getContentStyles($params) {
		$styles = array();
		
		if (!empty($params['content_padding'])) {
			$styles[] = 'padding: '.$params['content_padding'];
		}
		
		return implode(';', $styles);
	}
	
	private function getTitleStyles($params) {
		$styles = array();
		
		if (!empty($params['title_color'])) {
			$styles[] = 'color: '.$params['title_color'];
		}
		
		return implode(';', $styles);
	}
	
	private function getTextStyles($params) {
		$styles = array();
		
		if (!empty($params['text_color'])) {
			$styles[] = 'color: '.$params['text_color'];
		}
		
		if (!empty($params['text_top_margin'])) {
			$styles[] = 'margin-top: '.onschedule_edge_filter_px($params['text_top_margin']).'px';
		}
		
		return implode(';', $styles);
	}
}