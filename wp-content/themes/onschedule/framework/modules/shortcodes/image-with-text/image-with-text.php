<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ImageWithText;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageWithText implements ShortcodeInterface{

	private $base;

	/**
	 * Image With Text constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_image_with_text';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Image With Text', 'onschedule'),
			'base'                      => $this->getBase(),
			'category'                  => esc_html__('by EDGE', 'onschedule'),
			'icon' 						=> 'icon-wpb-image-with-text extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'		  => 'attach_image',
					'param_name'  => 'image',
					'heading'	  => esc_html__('Image', 'onschedule'),
					'description' => esc_html__('Select image from media library', 'onschedule')
				),
				array(
					'type'		  => 'textfield',
					'param_name'  => 'image_size',
					'heading'	  => esc_html__('Image Size', 'onschedule'),
					'description' => esc_html__('Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size', 'onschedule')
				),
				array(
                    'type'       => 'textfield',
                    'param_name' => 'title',
                    'heading'    => esc_html__('Title', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'title_tag',
                    'heading'     => esc_html__('Title Tag', 'onschedule'),
                    'value'       => array_flip(onschedule_edge_get_title_tag(true)),
                    'save_always' => true,
                    'dependency'  => array('element' => 'title', 'not_empty' => true)
                ),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'title_color',
					'heading'    => esc_html__('Title Color', 'onschedule'),
					'dependency' => array('element' => 'title', 'not_empty' => true)
				),
                array(
                    'type'       => 'textarea',
                    'param_name' => 'text',
                    'heading'    => esc_html__('Text', 'onschedule')
                ),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'text_color',
					'heading'    => esc_html__('Text Color', 'onschedule'),
					'dependency' => array('element' => 'text', 'not_empty' => true)
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'link',
					'heading'    => esc_html__('Link', 'onschedule'),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'link_target',
					'heading'    => esc_html__('Link Target', 'onschedule'),
					'value'      => array(
						esc_html__('Same Window', 'onschedule')  => '_self',
						esc_html__('New Window', 'onschedule') => '_blank'
					),
					'dependency'  => array('element' => 'link', 'not_empty' => true),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'appear_effect',
					'heading'    => esc_html__('Appear Effect', 'onschedule'),
					'value'      => array(
						esc_html__('Yes', 'onschedule')  => 'yes',
						esc_html__('No', 'onschedule') => 'no'
					),
                    'save_always' => true,
					'group'		=> esc_html__('Behavior', 'onschedule')
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'appear_effect_delay',
					'heading'    => esc_html__('Appear Effect delay', 'onschedule'),
					'dependency'  => array('element' => 'appear_effect', 'value' =>array('yes')),
					'group'		=> esc_html__('Behavior', 'onschedule')

				),
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'image'			  => '',
			'image_size'	  => 'full',
			'enable_lightbox' => 'no',
			'title'			  => '',
			'title_tag'	 	  => 'h5',
			'title_color'     => '',
			'text'			  => '',
			'text_color'      => '',
			'link'			  => '',
			'link_target'     => '_self',
			'appear_effect'	  => 'yes',
			'appear_effect_delay' => ''
		);

		$params = shortcode_atts($args, $atts);
		
		$params['image'] = $this->getImage($params);
		$params['image_size'] = $this->getImageSize($params['image_size']);
		$params['title_tag'] = !empty($title_tag) ? $params['title_tag'] : $args['title_tag'];
		$params['title_styles'] = $this->getTitleStyles($params);
		$params['text_styles'] = $this->getTextStyles($params);
		$params['animation_styles'] = $this->getAnimationStyles($params);
		$params['holder_classes'] = $this->getHolderClasses($params);

		$html = onschedule_edge_get_shortcode_module_template_part('templates/image-with-text', 'image-with-text', '', $params);

		return $html;
	}

	/**
	 * Return image for shortcode
	 *
	 * @param $params
	 * @return array
	 */
	private function getImage($params) {
        $image = array();

        if (!empty($params['image'])) {
            $id = $params['image'];

            $image['image_id'] = $id;
            $image_original = wp_get_attachment_image_src($id, 'full');
            $image['url'] = $image_original[0];
	        $image['alt'] = get_post_meta($id, '_wp_attachment_image_alt', true);
        }

		return $image;
	}

	/**
	 * Return image size or custom image size array
	 *
	 * @param $image_size
	 * @return array
	 */
	private function getImageSize($image_size) {
		$image_size = trim($image_size);
		//Find digits
		preg_match_all( '/\d+/', $image_size, $matches );
		if(in_array( $image_size, array('thumbnail', 'thumb', 'medium', 'large', 'full'))) {
			return $image_size;
		} elseif(!empty($matches[0])) {
			return array(
					$matches[0][0],
					$matches[0][1]
			);
		} else {
			return 'thumbnail';
		}
	}
	
	private function getTitleStyles($params) {
		$styles = array();
		
		if (!empty($params['title_color'])) {
			$styles[] = 'color: '.$params['title_color'];
		}
		
		return implode(';', $styles);
	}
	
	private function getTextStyles($params) {
		$styles = array();
		
		if (!empty($params['text_color'])) {
			$styles[] = 'color: '.$params['text_color'];
		}
		
		return implode(';', $styles);
	}

	private function getHolderClasses($params) {
		$holderClasses = array();

		if ($params['appear_effect'] == 'yes') {
			$holderClasses[] = 'edgtf-iwt-appear-effect';
		}

		return implode(' ', $holderClasses);
	}

	private function getAnimationStyles($params) {
	    $styles = array();

	    if(($params['appear_effect'] == 'yes') && ($params['appear_effect_delay'] !== '')) {
	        $styles[] = '-webkit-animation-delay: '.$params['appear_effect_delay'].'ms';
	        $styles[] = 'animation-delay: '.$params['appear_effect_delay'].'ms';
	    }

	    return $styles;
	}
}