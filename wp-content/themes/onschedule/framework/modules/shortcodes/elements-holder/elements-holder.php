<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ElementsHolder;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class ElementsHolder implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Elements Holder', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-elements-holder extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'as_parent' => array('only' => 'edgtf_elements_holder_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type'       => 'colorpicker',
					'param_name' => 'background_color',
					'heading'    => esc_html__('Background Color', 'onschedule')
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'number_of_columns',
					'heading'    => esc_html__('Columns', 'onschedule'),
					'value'      => array(
						esc_html__('1 Column', 'onschedule')  => 'one-column',
						esc_html__('2 Columns', 'onschedule') => 'two-columns',
						esc_html__('3 Columns', 'onschedule') => 'three-columns',
						esc_html__('4 Columns', 'onschedule') => 'four-columns',
						esc_html__('5 Columns', 'onschedule') => 'five-columns',
						esc_html__('6 Columns', 'onschedule') => 'six-columns'
					)
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'items_float_left',
					'heading'    => esc_html__('Items Float Left', 'onschedule'),
					'value'      => array('Make Items Float Left?' => 'yes')
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'switch_to_one_column',
					'heading'    => esc_html__('Switch to One Column', 'onschedule'),
					'value'      => array(
						esc_html__('Default', 'onschedule')   	=> '',
						esc_html__('Below 1280px', 'onschedule') 	=> '1280',
						esc_html__('Below 1024px', 'onschedule')  => '1024',
						esc_html__('Below 768px', 'onschedule')   => '768',
						esc_html__('Below 600px', 'onschedule')   => '600',
						esc_html__('Below 480px', 'onschedule')   => '480',
						esc_html__('Never', 'onschedule')    		=> 'never'
					),
					'description' => esc_html__('Choose on which stage item will be in one column', 'onschedule'),
					'save_always' => true
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'alignment_one_column',
					'heading'    => esc_html__('Choose Alignment In Responsive Mode', 'onschedule'),
					'value'      => array(
						esc_html__('Default', 'onschedule') => '',
						esc_html__('Left', 'onschedule') 	  => 'left',
						esc_html__('Center', 'onschedule')  => 'center',
						esc_html__('Right', 'onschedule')   => 'right'
					),
					'description' => esc_html__('Alignment When Items are in One Column', 'onschedule'),
					'save_always' => true
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'number_of_columns' 	=> '',
			'switch_to_one_column'	=> '',
			'alignment_one_column' 	=> '',
			'items_float_left' 		=> '',
			'background_color' 		=> ''
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '';

		$elements_holder_classes = array();
		$elements_holder_classes[] = 'edgtf-elements-holder';
		$elements_holder_style = '';

		if($number_of_columns != ''){
			$elements_holder_classes[] .= 'edgtf-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$elements_holder_classes[] = 'edgtf-responsive-mode-' . $switch_to_one_column ;
		} else {
			$elements_holder_classes[] = 'edgtf-responsive-mode-768' ;
		}

		if($alignment_one_column != ''){
			$elements_holder_classes[] = 'edgtf-one-column-alignment-' . $alignment_one_column ;
		}

		if($items_float_left !== ''){
			$elements_holder_classes[] = 'edgtf-ehi-float';
		}

		if($background_color != ''){
			$elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$elements_holder_class = implode(' ', $elements_holder_classes);

		$html .= '<div ' . onschedule_edge_get_class_attribute($elements_holder_class) . ' ' . onschedule_edge_get_inline_attr($elements_holder_style, 'style'). '>';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;
	}
}
