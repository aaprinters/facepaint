<div class="edgtf-booked-calendar-holder  <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-calendar-content">
		<?php if(!empty($calendar_title)) { ?>
			<<?php echo esc_attr($calendar_title_tag); ?> class="edgtf-calendar-title"><?php echo esc_html($calendar_title); ?></<?php echo esc_attr($calendar_title_tag); ?>>
		<?php }	?>
		<?php echo do_shortcode('[booked-calendar '.$calendar_attrs.']'); ?>
	</div>
</div>