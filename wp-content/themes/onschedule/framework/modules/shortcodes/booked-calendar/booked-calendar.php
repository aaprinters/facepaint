<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\BookedCalendar;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class BookedCalendar implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;
	
	public function __construct() {
		$this->base = 'edgtf_booked_calendar';
		
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'            => esc_html__('Edge Booked Calendar', 'onschedule'),
			'base'            => $this->base,
			'category'        => esc_html__('by EDGE', 'onschedule'),
			'icon'            => 'icon-wpb-booked-calendar extended-custom-icon',
			'params'          => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar',
					'heading'     => esc_html__('Calendar', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_booked_calendar_array()),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'calendar_title',
					'heading'     => esc_html__('Calendar Title', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_title_tag',
					'heading'     => esc_html__('Calendar Title Tag', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_title_tag(true)),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_skin',
					'heading'     => esc_html__('Calendar Skin', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_booked_calendar_skins()),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_size',
					'heading'     => esc_html__('Calendar Size', 'onschedule'),
					'value'       => array(
						esc_html__('Large', 'onschedule') => 'large',
						esc_html__('Small', 'onschedule') => 'small'
					),
					'save_always' => true
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'calendar_style',
					'heading'     => esc_html__('Calendar Style', 'onschedule'),
					'value'       => array(
						esc_html__('Calendar', 'onschedule') => 'calendar',
						esc_html__('Calendar Simple', 'onschedule') => 'calendar_simple',
						esc_html__('List', 'onschedule')     => 'list'
					),
					'save_always' => true
				)
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'calendar'           => '',
			'calendar_title'     => '',
			'calendar_title_tag' => 'h4',
			'calendar_skin'      => 'default',
			'calendar_size'      => 'large',
			'calendar_style'     => 'calendar'
		);

		$params = shortcode_atts($args, $atts);
		
		$params['holder_classes']  = $this->getHolderClasses($params);
		
		$params['content']          = $content;
		$params['calendar_attrs']   = $this->getCalendarAttributes($params);
		
		$params['calendar_title_tag'] = !empty($params['calendar_title_tag']) ? $params['calendar_title_tag'] : $args['calendar_title_tag'];
		
		return onschedule_edge_get_shortcode_module_template_part('templates/booked-calendar', 'booked-calendar', '', $params);
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holderClasses = array();

		if(!empty($params['calendar_skin'])){
			$holderClasses[] = 'edgtf-'.$params['calendar_skin'];
		}

		if( $params['calendar_skin'] !== 'default' ){
			$holderClasses[] = 'edgtf-custom-style';
		}
		
		if(!empty($params['calendar_style']) && $params['calendar_style'] === 'calendar_simple'){
			$holderClasses[] = 'edgtf-calendar-simple';
		}

		if(!empty($params['calendar_style'])){
			$holderClasses[] = 'edgtf-calendar-style-'.$params['calendar_style'];
		}

		return implode(' ', $holderClasses);
	}
	
	/**
	 * Generates calendar attributes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCalendarAttributes($params){
		$attrs = array();
		
		if(!empty($params['calendar'])){
			$attrs[] = 'calendar="'.esc_attr($params['calendar']).'"';
		}
		
		if(!empty($params['calendar_size'])){
			$attrs[] = 'size="'.esc_attr($params['calendar_size']).'"';
		}
		
		if(!empty($params['calendar_style'])){
			$calendar_style = $params['calendar_style'] === 'calendar_simple' ? 'calendar' : $params['calendar_style'];
			
			$attrs[] = 'style="'.esc_attr($calendar_style).'"';
		}
		
		return implode(' ', $attrs);
	}
}