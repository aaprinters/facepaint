<div class="edgtf-is-item">
	<div class="edgtf-is-item-inner">
		<?php if(!empty($image)) { ?>
			<div class="edgtf-is-image">
				<?php echo wp_get_attachment_image($image, 'full'); ?>
			</div>
		<?php } ?>
		<?php if(!empty($content)) { ?>
			<div class="edgtf-is-content" <?php echo onschedule_edge_get_inline_style($content_styles); ?>>
				<?php echo do_shortcode($content); ?>
			</div>
		<?php } ?>
	</div>
</div>