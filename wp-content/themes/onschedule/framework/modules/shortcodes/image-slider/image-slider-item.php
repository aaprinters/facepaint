<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ImageSliderItem;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageSliderItem implements ShortcodeInterface{
	private $base;

	/**
	 * Image Gallery constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_image_slider_item';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map(array(
			'name'      => esc_html__('Edge Image Slider Item', 'onschedule'),
			'base'      => $this->getBase(),
			'category'  => esc_html__('by EDGE', 'onschedule'),
			'icon' 		=> 'icon-wpb-image-slider-item extended-custom-icon',
			'as_child'  => array('only' => 'edgtf_image_slider'),
			'as_parent' => array('except' => 'vc_row'),
			'params'                    => array(
				array(
					'type'		  => 'attach_image',
					'param_name'  => 'image',
					'heading'	  => esc_html__('Image', 'onschedule'),
					'description' => esc_html__('Select image from media library', 'onschedule')
				),
				array(
					'type'		  => 'colorpicker',
					'param_name'  => 'content_background',
					'heading'	  => esc_html__('Content Background Color', 'onschedule')
				),
				array(
					'type'		  => 'textarea_html',
					'param_name'  => 'content',
					'heading'	  => esc_html__('Content', 'onschedule')
				)
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'image'	             => '',
			'content'            => '',
			'content_background' => ''
		);

		$params = shortcode_atts($args, $atts);
		
		$params['content'] = preg_replace('#^<\/p>|<p>$#', '', $content);
		$params['content_styles'] = $this->getContentStyles($params);
		
		$html = onschedule_edge_get_shortcode_module_template_part('templates/image-slider-item', 'image-slider', '', $params);

		return $html;
	}
	
	private function getContentStyles($params) {
		$styles = array();
		
		if (!empty($params['content_background'])) {
			$styles[] = 'background-color: '.$params['content_background'];
		}
		
		return implode(';', $styles);
	}
}