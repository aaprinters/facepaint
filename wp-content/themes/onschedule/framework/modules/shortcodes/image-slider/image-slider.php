<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ImageSlider;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageSlider implements ShortcodeInterface{
	private $base;

	/**
	 * Image Gallery constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_image_slider';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map(array(
			'name' => esc_html__('Edge Image Slider', 'onschedule'),
			'base' => $this->getBase(),
			'category' => esc_html__('by EDGE', 'onschedule'),
			'icon' => 'icon-wpb-image-slider extended-custom-icon',
			'as_parent' => array('only' => 'edgtf_image_slider_item'),
			'content_element' => true,
			'show_settings_on_create' => false,
			'js_view' => 'VcColumnView'
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array();
		
		$params = shortcode_atts($args, $atts);
		
		$params['content'] = $content;
		
		$html = onschedule_edge_get_shortcode_module_template_part('templates/image-slider', 'image-slider', '', $params);
		
		return $html;
	}
}