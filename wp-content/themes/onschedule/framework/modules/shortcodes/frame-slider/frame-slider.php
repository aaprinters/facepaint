<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\FrameSlider;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class FrameSlider implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_frame_slider';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Frame Slider', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-frame-slider extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'as_parent' => array('only' => 'edgtf_frame_slide'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'autoplay',
					'heading'     => esc_html__('Autoplay', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'autoplay_speed',
					'heading'     => esc_html__('Autoplay Speed', 'onschedule'),
					'description' => esc_html__('Enter autoplay speed interval in milliseconds. Default value is 3500', 'onschedule'),
					'dependency'  => array('element' => 'autoplay', 'value' => array('yes'))
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'autoplay'	     => 'yes',
			'autoplay_speed' => '3500'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$params['content']= $content;
		
		$html = '';
		
		$params['slider_data'] = $this->getSliderData($params);

		$params['desktop_images'] = $this->getDesktopImages($params);
		$params['mobile_images']  = $this->getMobileImages($params);
		
		$params['slider_links'] = $this->getLinks($params);
		$params['slider_targets'] = $this->getTargets($params);
		
		$html .= onschedule_edge_get_shortcode_module_template_part('templates/frame-slider-template', 'frame-slider', '', $params);

		return $html;
	}
	
	private function getSliderData($params) {
		$data = array();
		
		if(!empty($params['autoplay'])) {
			$data['data-autoplay'] = $params['autoplay'];
		}
		
		if(!empty($params['autoplay_speed'])) {
			$data['data-autoplay-speed'] = $params['autoplay_speed'];
		}
		
		return implode(' ', $data);
	}

	private function getDesktopImages($params) {
		$desktop_images_array = array();

		preg_match_all('/fs_desktop_image="([^\"]+)"/i', $params['content'], $desktop_images_matches, PREG_OFFSET_CAPTURE);

		if (isset($desktop_images_matches[0])) {
			$desktop_images = $desktop_images_matches[0];
		}

		foreach($desktop_images as $desktop_image) {
			preg_match('/fs_desktop_image="([^\"]+)"/i', $desktop_image[0], $slide_desktop_images_matches, PREG_OFFSET_CAPTURE);
			$desktop_images_array[] = $slide_desktop_images_matches[1][0];
		}

		return $desktop_images_array;
	}
	
	private function getMobileImages($params) {
		$mobile_images_array = array();

		preg_match_all('/fs_mobile_image="([^\"]+)"/i', $params['content'], $mobile_images_matches, PREG_OFFSET_CAPTURE);

		if (isset($mobile_images_matches[0])) {
			$mobile_images = $mobile_images_matches[0];
		}

		foreach($mobile_images as $mobile_image) {
			preg_match('/fs_mobile_image="([^\"]+)"/i', $mobile_image[0], $slide_mobile_images_matches, PREG_OFFSET_CAPTURE);
			$mobile_images_array[] = $slide_mobile_images_matches[1][0];
		}

		return $mobile_images_array;
	}

	private function getLinks($params) {
		$fs_links = array();

		preg_match_all('/fs_link="([^\"]+)"/i', $params['content'], $links_matches, PREG_OFFSET_CAPTURE);

		if (isset($links_matches[0])) {
			$links = $links_matches[0];
		}

		foreach($links as $link) {
			preg_match('/fs_link="([^\"]+)"/i', $link[0], $slide_links_matches, PREG_OFFSET_CAPTURE);
			$fs_links[] = $slide_links_matches[1][0];
		}
		
		return $fs_links;
	}

	private function getTargets($params) {
		$fs_targets = array();

		preg_match_all('/fs_target="([^\"]+)"/i', $params['content'], $targets_matches, PREG_OFFSET_CAPTURE);

		if (isset($targets_matches[0])) {
			$targets = $targets_matches[0];
		}

		foreach($targets as $target) {
			preg_match('/fs_target="([^\"]+)"/i', $target[0], $slide_targets_matches, PREG_OFFSET_CAPTURE);
			$fs_targets[] = $slide_targets_matches[1][0];
		}
		
		return $fs_targets;
	}
}
