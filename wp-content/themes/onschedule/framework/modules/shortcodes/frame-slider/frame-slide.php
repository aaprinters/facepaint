<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\FrameSlide;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class FrameSlide implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'edgtf_frame_slide';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Edge Frame Slide', 'onschedule'),
					'base' => $this->base,
					'as_child' => array('only' => 'edgtf_frame_slider'),
					'content_element' => true,
					'category' => esc_html__('by EDGE', 'onschedule'),
					'icon' => 'icon-wpb-frame-slide extended-custom-icon',
					'params' => array(
						array(
							'type' => 'attach_image',
							'param_name' => 'fs_desktop_image',
							'heading' => esc_html__('Desktop Image', 'onschedule'),
							'description' => esc_html__('Image should be sized in 16:9 aspect ratio.', 'onschedule'),
						),
						array(
							'type' => 'attach_image',
							'param_name' => 'fs_mobile_image',
							'heading' => esc_html__('Mobile Image', 'onschedule'),
							'description' => esc_html__('Image should be sized in 9:16 aspect ratio.', 'onschedule'),
						),
						array(
							'type' => 'textfield',
							'param_name' => 'fs_link',
							'heading' => esc_html__('Link', 'onschedule')
						),
						array(
							'type' => 'dropdown',
							'param_name' => 'fs_target',
							'heading' => esc_html__('Link Target', 'onschedule'),
							'value' => array(
								esc_html__('Same Window', 'onschedule') => '_self',
								esc_html__('New Window', 'onschedule') => '_blank'
							),
							'save_always' => true
						)
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'fs_desktop_image'	=> '',
			'fs_mobile_image'	=> '',
			'fs_link'		    => '',
			'fs_target'			=> '_self'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$params['content']   = $content;
		$params['fs_target'] = !empty($params['fs_target']) ? $params['fs_target'] : $args['fs_target'];
		
		$html = onschedule_edge_get_shortcode_module_template_part('templates/frame-slide-template', 'frame-slider', '', $params);

		return $html;
	}
}
