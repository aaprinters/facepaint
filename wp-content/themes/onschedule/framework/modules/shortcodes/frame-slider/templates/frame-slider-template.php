<div class="edgtf-frame-slider-holder" <?php echo onschedule_edge_get_inline_attrs($slider_data); ?>>
	<div class="edgtf-fs-images-holder">
		<div class="edgtf-fs-mobile-holder edgtf-fs-inner-slider-holder">
			<div class="edgtf-fs-inner-slider-holder2">
				<img src="<?php echo EDGE_ASSETS_ROOT .'/img/frame-slider-phone.png' ?>" class="edgtf-fs-phone-frame"  alt="<?php esc_attr_e('phone','onschedule');?>"/>
				<div class="edgtf-fs-mobile-slider edgtf-fs-inner-slider">
					<div class="edgtf-fs-mobile-images  edgtf-frame-slider-element">
						<?php
						$k = 0;
						foreach($mobile_images as $mobile_image){ ?>
							<?php if(array_key_exists($k, $slider_links)){ ?>
								<div class="edgtf-fs-mobile-image"><a href="<?php echo esc_url($slider_links[$k]); ?>" target="<?php echo esc_attr($slider_targets[$k]); ?>"><?php echo wp_get_attachment_image($mobile_image,'full'); ?></a></div>
							<?php } else { ?>
								<div class="edgtf-fs-mobile-image"><?php echo wp_get_attachment_image($mobile_image,'full'); ?></div>
							<?php } ?>
							<?php $k++; } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="edgtf-fs-desktop-holder edgtf-fs-inner-slider-holder">
			<div class="edgtf-fs-inner-slider-holder2">
				<img src="<?php echo EDGE_ASSETS_ROOT .'/img/frame-slider-monitor.png' ?>" class="edgtf-fs-desktop-frame" alt="<?php esc_attr_e('monitor','onschedule');?>"/>
				<div class="edgtf-fs-desktop-slider edgtf-fs-inner-slider">
					<div class="edgtf-fs-desktop-images edgtf-frame-slider-element">
						<?php
						$i = 0;
						foreach($desktop_images as $desktop_image){ ?>
							<?php if(array_key_exists($i, $slider_links)){ ?>
								<div class="edgtf-fs-desktop-image"><a href="<?php echo esc_url($slider_links[$i]); ?>" target="<?php echo esc_attr($slider_targets[$i]); ?>"><?php echo wp_get_attachment_image($desktop_image,'full'); ?></a></div>
							<?php } else { ?>
								<div class="edgtf-fs-desktop-image"><?php echo wp_get_attachment_image($desktop_image,'full'); ?></div>
							<?php } ?>
							<?php $i++; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>