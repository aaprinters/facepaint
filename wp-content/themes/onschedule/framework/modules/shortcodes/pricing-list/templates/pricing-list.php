<div class="edgtf-pricing-list-item clearfix">
	<div class="edgtf-pli-content">
		<div class="edgtf-pli-top-holder">
			<div class="edgtf-pli-title-holder">
				<h5 itemprop="name" class="edgtf-pli-title entry-title" <?php echo onschedule_edge_get_inline_style($title_styles); ?>>
					<?php if(!empty($link)): ?><a itemprop="url" target="<?php echo esc_attr($target); ?>" href="<?php echo esc_url($link); ?>"><?php endif; ?>
						<?php echo esc_html($title); ?>
					<?php if(!empty($link)): ?></a><?php endif; ?>
				</h5>	
			</div>
			<div class="edgtf-pli-dots"></div>
			<?php if(!empty($price)) : ?>
				<div class="edgtf-pli-price-holder">
					<h5 class="edgtf-pli-price" <?php echo onschedule_edge_get_inline_style($price_styles); ?>><?php echo esc_html($price); ?></h5>
				</div>
			<?php endif; ?>
		</div>
		<div class="edgtf-pli-bottom-holder clearfix" <?php echo onschedule_edge_get_inline_style($desc_styles); ?>>
			<?php echo esc_html($description); ?>
		</div>
	</div>
</div>	