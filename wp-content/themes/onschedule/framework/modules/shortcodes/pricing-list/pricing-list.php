<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\PricingList;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingList implements ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_pricing_list';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map(
			array(
				'name' => esc_html__('Edge Pricing list', 'onschedule'),
				'base' => $this->base,
				'as_parent' => array('only' => 'edgtf_pricing_list_item'),
				'content_element' => true,
				'category' => esc_html__('by EDGE', 'onschedule'),
				'icon' => 'icon-wpb-pricing-list extended-custom-icon',
				'show_settings_on_create' => true,
				'js_view' => 'VcColumnView',
				'params' => array(
					array(
						'type'        => 'textfield',
						'param_name'  => 'title',
						'heading'     => esc_html__('Title', 'onschedule'),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_position',
						'heading'     => esc_html__('Title Position', 'onschedule'),
						'value'       => array(
							esc_html__('Left', 'onschedule')   => '',
							esc_html__('Center', 'onschedule') => 'center'
						),
						'save_always' => true
					)
				)
			)
		);
	}

	public function render($atts, $content = null) {
		$args = array(
			'title'               => '',
			'title_position'      => ''
		);
		
		$params = shortcode_atts($args, $atts);

		extract($params);

		$holderClass = 'edgtf-pl-title-holder-'.$params['title_position'];
		
		$html = '<div class="edgtf-pricing-list clearfix">';
			$html .= '<div class="edgtf-pl-wrapper">';
				if( !empty($params['title']) ) {
					$html .= '<h3 class="edgtf-pl-title '.esc_attr($holderClass).'">'.esc_html($title).'</h3>';
				}
				$html .= do_shortcode($content);
			$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}