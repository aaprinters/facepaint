<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\PricingListItem;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingListItem implements ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_pricing_list_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Pricing List Item', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-pricing-list-item extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'allowed_container_element' => 'vc_row',
			'as_child' => array('only' => 'edgtf_pricing_list'),
			'params' => array(
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__('Title', 'onschedule'),
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'description',
					'heading'     => esc_html__('Description', 'onschedule'),
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'price',
					'heading'    => esc_html__('Price', 'onschedule')
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'link',
					'heading'    => esc_html__('Title Link', 'onschedule'),
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'target',
					'heading'     => esc_html__('Link Target', 'onschedule'),
					'value'       => array(
						esc_html__('Same Window', 'onschedule')  => '_self',
						esc_html__('New Window', 'onschedule') => '_blank'
					)
				),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'color_title',
					'heading'    => esc_html__('Title Color', 'onschedule'),
					'group'      => 'Style'
				),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'color_description',
					'heading'    => esc_html__('Description Color', 'onschedule'),
					'group'      => 'Style'
				),
				array(
					'type'       => 'colorpicker',
					'param_name' => 'color_price',
					'heading'    => esc_html__('Price Color', 'onschedule'),
					'group'      => 'Style'
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'title'             => '',
			'description'       => '',
			'price'             => '',
			'link'              => '',
			'target'            => '_self',
			'color_title'       => '',
			'color_description' => '',
			'color_price'       => ''
		);
		
		$params = shortcode_atts($args, $atts);

		$params['title_styles'] = $this->getPricingListItemTitleStyles($params);
		$params['desc_styles']  = $this->getPricingListItemDescStyles($params);
		$params['price_styles'] = $this->getPricingListItemPriceStyles($params);

		extract($params);

		$html = onschedule_edge_get_shortcode_module_template_part('templates/pricing-list', 'pricing-list', '', $params);
		
		return $html;
	}

	private function getPricingListItemTitleStyles($params) {
		$styles = array();

		if(!empty($params['color_title'])) {
			$styles[] = 'color: '.$params['color_title'];
		}

		return implode( ';', $styles );
	}

	private function getPricingListItemDescStyles($params) {
		$styles = array();

		if(!empty($params['color_description'])) {
			$styles[] = 'color: '.$params['color_description'];
		}

		return implode( ';', $styles );
	}

	private function getPricingListItemPriceStyles($params) {
		$styles = array();

		if(!empty($params['color_price'])) {
			$styles[] = 'color: '.$params['color_price'];
		}

		return implode( ';', $styles );
	}
}