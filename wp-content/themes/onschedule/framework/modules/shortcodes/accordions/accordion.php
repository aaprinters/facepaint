<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Accordion;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
* class Accordions
*/
class Accordion implements ShortcodeInterface{
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'edgtf_accordion';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return	$this->base;
	}

	public function vcMap() {
		vc_map( array(
			'name' =>  esc_html__('Edge Accordion', 'onschedule'),
			'base' => $this->base,
			'as_parent' => array('only' => 'edgtf_accordion_tab'),
			'content_element' => true,
			'category' => esc_html__('by EDGE', 'onschedule'),
			'icon' => 'icon-wpb-accordion extended-custom-icon',
			'show_settings_on_create' => true,
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type'       => 'textfield',
					'param_name' => 'el_class',
					'heading'    => esc_html__('Custom CSS Class', 'onschedule')
				),
                array(
					'type'       => 'dropdown',
					'param_name' => 'style',
					'heading'    => esc_html__('Style', 'onschedule'),
					'value'      => array(
						esc_html__('Accordion', 'onschedule') => 'accordion',
						esc_html__('Toggle', 'onschedule') => 'toggle'
					)
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'layout',
					'heading'    => esc_html__('Layout', 'onschedule'),
					'value'      => array(
						esc_html__('Boxed', 'onschedule') => 'boxed',
						esc_html__('Simple', 'onschedule') => 'simple'
					)
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'background_skin',
					'heading'    => esc_html__('Background Skin', 'onschedule'),
					'value' => array(
						esc_html__('Default', 'onschedule') => '',
						esc_html__('White', 'onschedule') => 'white'
					),
					'dependency'  => array('element' => 'layout', 'value' => array('boxed'))
				)
			)
		) );
	}
	
	public function render($atts, $content = null) {
		$default_atts=(array(
			'el_class'        => '',
			'title'           => '',
			'style'           => 'accordion',
			'layout'          => 'boxed',
			'background_skin' => ''
		));
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);

		$params['acc_class'] = $this->getAccordionClasses($params);
		$params['content'] = $content;

		$output = '';

		$output .= onschedule_edge_get_shortcode_module_template_part('templates/accordion-holder-template','accordions', '', $params);

		return $output;
	}

	/**
	   * Generates accordion classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getAccordionClasses($params){

		$acc_class = 'edgtf-ac-default';

		switch($params['style']) {
            case 'toggle':
                $acc_class .= ' edgtf-toggle';
                break;
            default:
                $acc_class .= ' edgtf-accordion';
                break;
        }
		
		if (!empty($params['layout'])) {
			$acc_class .= ' edgtf-ac-'.esc_attr($params['layout']);
		}
		
		if (!empty($params['background_skin'])) {
			$acc_class .= ' edgtf-'.esc_attr($params['background_skin']).'-skin';
		}

		if (!empty($params['el_class'])) {
			$acc_class .= ' '.esc_attr($params['el_class']);
		}

        return $acc_class;
	}
}
