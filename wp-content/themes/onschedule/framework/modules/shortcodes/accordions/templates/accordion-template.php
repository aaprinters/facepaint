<<?php echo esc_attr($title_tag); ?> class="edgtf-title-holder">
    <span class="edgtf-accordion-mark">
		<span class="edgtf_icon_plus ion-plus"></span>
		<span class="edgtf_icon_minus ion-minus"></span>
	</span>
	<span class="edgtf-tab-title"><?php echo esc_html($title); ?></span>
</<?php echo esc_attr($title_tag); ?>>
<div class="edgtf-accordion-content">
	<div class="edgtf-accordion-content-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>