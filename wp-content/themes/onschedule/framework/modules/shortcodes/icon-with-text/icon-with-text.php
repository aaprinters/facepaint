<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\IconWithText;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class IconWithText
 * @package OnScheduleEdgeNamespace\Modules\Shortcodes\IconWithText
 */
class IconWithText implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     *
     */
    public function __construct() {
        $this->base = 'edgtf_icon_with_text';

        add_action('vc_before_init', array($this, 'vcMap'));
    }
	
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name' => esc_html__('Edge Icon With Text', 'onschedule'),
            'base' => $this->base,
            'icon' => 'icon-wpb-icon-with-text extended-custom-icon',
            'category' => esc_html__('by EDGE', 'onschedule'),
            'allowed_container_element' => 'vc_row',
            'params' => array_merge(
                array(
	                array(
		                'type'       => 'dropdown',
		                'param_name' => 'type',
		                'heading'    => esc_html__('Type', 'onschedule'),
		                'value'      => array(
			                esc_html__('Icon Left From Text', 'onschedule')  => 'icon-left',
			                esc_html__('Icon Left From Title', 'onschedule') => 'icon-left-from-title',
			                esc_html__('Icon Top', 'onschedule')             => 'icon-top'
		                )
	                )
                ),
                onschedule_edge_icon_collections()->getVCParamsArray(),
                array(
                    array(
                        'type'       => 'attach_image',
                        'param_name' => 'custom_icon',
                        'heading'    => esc_html__('Custom Icon', 'onschedule')
                    ),
                    array(
                        'type'       => 'dropdown',
                        'param_name' => 'icon_type',
                        'heading'    => esc_html__('Icon Type', 'onschedule'),
                        'value'      => array(
						    esc_html__('Normal', 'onschedule') => 'edgtf-normal',
						    esc_html__('Circle', 'onschedule') => 'edgtf-circle',
						    esc_html__('Square', 'onschedule') => 'edgtf-square'
                        ),
                        'group'       => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'dropdown',
                        'param_name' => 'icon_size',
                        'heading'    => esc_html__('Icon Size', 'onschedule'),
                        'value'      => array(
	                        esc_html__('Medium', 'onschedule')     => 'edgtf-icon-medium',
	                        esc_html__('Tiny', 'onschedule')       => 'edgtf-icon-tiny',
						    esc_html__('Small', 'onschedule')      => 'edgtf-icon-small',
						    esc_html__('Large', 'onschedule')      => 'edgtf-icon-large',
						    esc_html__('Very Large', 'onschedule') => 'edgtf-icon-huge'
                        ),
                        'group'       => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'textfield',
                        'param_name' => 'custom_icon_size',
                        'heading'    => esc_html__('Custom Icon Size (px)', 'onschedule'),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'textfield',
                        'param_name' => 'shape_size',
                        'heading'    => esc_html__('Shape Size (px)', 'onschedule'),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_color',
                        'heading'    => esc_html__('Icon Color', 'onschedule'),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_hover_color',
                        'heading'    => esc_html__('Icon Hover Color', 'onschedule'),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_background_color',
                        'heading'    => esc_html__('Icon Background Color', 'onschedule'),
                        'dependency' => array('element' => 'icon_type', 'value' => array('edgtf-square', 'edgtf-circle')),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_hover_background_color',
                        'heading'    => esc_html__('Icon Hover Background Color', 'onschedule'),
                        'dependency' => array('element' => 'icon_type', 'value' => array('edgtf-square', 'edgtf-circle')),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_border_color',
                        'heading'    => esc_html__('Icon Border Color', 'onschedule'),
                        'dependency' => array('element' => 'icon_type', 'value' => array('edgtf-square', 'edgtf-circle')),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'icon_border_hover_color',
                        'heading'    => esc_html__('Icon Border Hover Color', 'onschedule'),
                        'dependency' => array('element' => 'icon_type', 'value' => array('edgtf-square', 'edgtf-circle')),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
                    array(
                        'type'       => 'textfield',
                        'param_name' => 'icon_border_width',
                        'heading'    => esc_html__('Border Width (px)', 'onschedule'),
                        'dependency' => array('element' => 'icon_type', 'value' => array('edgtf-square', 'edgtf-circle')),
                        'group'      => esc_html__('Icon Settings', 'onschedule')
                    ),
	                array(
		                'type'        => 'dropdown',
		                'param_name'  => 'icon_animation',
		                'heading'     => esc_html__('Icon Animation', 'onschedule'),
		                'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false)),
		                'group'       => esc_html__('Icon Settings', 'onschedule')
	                ),
	                array(
		                'type'       => 'textfield',
		                'param_name' => 'icon_animation_delay',
		                'heading'    => esc_html__('Icon Animation Delay (ms)', 'onschedule'),
		                'dependency' => array('element' => 'icon_animation', 'value' => array('yes')),
		                'group'      => esc_html__('Icon Settings', 'onschedule')
	                ),
                    array(
                        'type'       => 'textfield',
                        'param_name' => 'title',
                        'heading'    => esc_html__('Title', 'onschedule')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'param_name'  => 'title_tag',
                        'heading'     => esc_html__('Title Tag', 'onschedule'),
                        'value'       => array_flip(onschedule_edge_get_title_tag(true, array('p' => 'p', 'div' => 'div'))),
						'save_always' => true,
                        'dependency'  => array('element' => 'title', 'not_empty' => true),
                        'group'       => esc_html__('Text Settings', 'onschedule')
                    ),
	                array(
		                'type'       => 'colorpicker',
		                'param_name' => 'title_color',
		                'heading'    => esc_html__('Title Color', 'onschedule'),
		                'dependency' => array('element' => 'title', 'not_empty' => true),
		                'group'      => esc_html__('Text Settings', 'onschedule')
	                ),
	                array(
		                'type'       => 'textfield',
		                'param_name' => 'title_top_margin',
		                'heading'    => esc_html__('Title Top Margin (px)', 'onschedule'),
		                'dependency' => array('element' => 'title', 'not_empty' => true),
		                'group'      => esc_html__('Text Settings', 'onschedule')
	                ),
                    array(
                        'type'       => 'textarea',
                        'param_name' => 'text',
                        'heading'    => esc_html__('Text', 'onschedule')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'param_name' => 'text_color',
                        'heading'    => esc_html__('Text Color', 'onschedule'),
                        'dependency' => array('element' => 'text', 'not_empty' => true),
                        'group'      => esc_html__('Text Settings', 'onschedule')
                    ),
	                array(
		                'type'       => 'textfield',
		                'param_name' => 'text_top_margin',
		                'heading'    => esc_html__('Text Top Margin (px)', 'onschedule'),
		                'dependency' => array('element' => 'text', 'not_empty' => true),
		                'group'      => esc_html__('Text Settings', 'onschedule')
	                ),
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'link',
                        'heading'     => esc_html__('Link', 'onschedule'),
                        'description' => esc_html__('Set link around icon and title', 'onschedule')
                    ),
                    array(
                        'type'       => 'dropdown',
                        'param_name' => 'target',
                        'heading'    => esc_html__('Target', 'onschedule'),
                        'value'      => array(
						    esc_html__('Same Window', 'onschedule')  => '_self',
						    esc_html__('New Window', 'onschedule') => '_blank'
                        ),
                        'dependency'  => array('element' => 'link', 'not_empty' => true),
                    ),
	                array(
		                'type'        => 'textfield',
		                'param_name'  => 'text_padding',
		                'heading'     => esc_html__('Text Padding (px)', 'onschedule'),
		                'description' => esc_html__('Set left or top padding dependence of type for your text holder. Default value is 13 for left type and 25 for top icon with text type', 'onschedule'),
		                'dependency'  => array('element' => 'type', 'value' => array('icon-left', 'icon-top')),
		                'group'       => esc_html__('Text Settings', 'onschedule')
	                )
                )
            )
        ));
    }

    /**
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'type'                        => 'icon-left',
            'custom_icon'                 => '',
            'icon_type'                   => 'edgtf-normal',
            'icon_size'                   => 'edgtf-icon-medium',
            'custom_icon_size'            => '',
            'shape_size'                  => '',
            'icon_color'                  => '',
            'icon_hover_color'            => '',
            'icon_background_color'       => '',
            'icon_hover_background_color' => '',
            'icon_border_color'           => '',
            'icon_border_hover_color'     => '',
            'icon_border_width'           => '',
            'icon_animation'              => '',
            'icon_animation_delay'        => '',
            'title'                       => '',
            'title_tag'                   => 'h4',
            'title_color'                 => '',
	        'title_top_margin'            => '',
            'text'                        => '',
            'text_color'                  => '',
	        'text_top_margin'             => '',
            'link'                        => '',
            'target'                      => '_self',
            'text_padding'                => ''
        );

        $default_atts = array_merge($default_atts, onschedule_edge_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);
	
	    $params['type'] = !empty($params['type']) ? $params['type'] : $default_atts['type'];

        $params['icon_parameters'] = $this->getIconParameters($params);
        $params['holder_classes']  = $this->getHolderClasses($params);
	    $params['content_styles']  = $this->getContentStyles($params);
	    $params['title_styles']    = $this->getTitleStyles($params);
	    $params['title_tag']       = !empty($params['title_tag']) ? $params['title_tag'] : $default_atts['title_tag'];
        $params['text_styles']     = $this->getTextStyles($params);
	    $params['target']          = !empty($params['target']) ? $params['target'] : $default_atts['target'];
	    
		return onschedule_edge_get_shortcode_module_template_part('templates/iwt', 'icon-with-text', $params['type'], $params);
    }

    /**
     * Returns parameters for icon shortcode as a string
     *
     * @param $params
     *
     * @return array
     */
    private function getIconParameters($params) {
        $params_array = array();

        if(empty($params['custom_icon'])) {
            $iconPackName = onschedule_edge_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

            $params_array['icon_pack']   = $params['icon_pack'];
            $params_array[$iconPackName] = $params[$iconPackName];

            if(!empty($params['icon_size'])) {
                $params_array['size'] = $params['icon_size'];
            }

            if(!empty($params['custom_icon_size'])) {
                $params_array['custom_size'] = onschedule_edge_filter_px($params['custom_icon_size']).'px';
            }

            if(!empty($params['icon_type'])) {
                $params_array['type'] = $params['icon_type'];
            }
	
	        if(!empty($params['shape_size'])) {
		        $params_array['shape_size'] = onschedule_edge_filter_px($params['shape_size']).'px';
	        }

            if(!empty($params['icon_border_color'])) {
                $params_array['border_color'] = $params['icon_border_color'];
            }

            if(!empty($params['icon_border_hover_color'])) {
                $params_array['hover_border_color'] = $params['icon_border_hover_color'];
            }

            if($params['icon_border_width'] !== '') {
                $params_array['border_width'] = onschedule_edge_filter_px($params['icon_border_width']).'px';
            }

            if(!empty($params['icon_background_color'])) {
                $params_array['background_color'] = $params['icon_background_color'];
            }

            if(!empty($params['icon_hover_background_color'])) {
                $params_array['hover_background_color'] = $params['icon_hover_background_color'];
            }

            $params_array['icon_color'] = $params['icon_color'];

            if(!empty($params['icon_hover_color'])) {
                $params_array['hover_icon_color'] = $params['icon_hover_color'];
            }

            $params_array['icon_animation']       = $params['icon_animation'];
            $params_array['icon_animation_delay'] = $params['icon_animation_delay'];
        }

        return $params_array;
    }

    /**
     * Returns array of holder classes
     *
     * @param $params
     *
     * @return array
     */
    private function getHolderClasses($params) {
        $classes = array('edgtf-iwt', 'clearfix');

	    if(!empty($params['type'])) {
		    $classes[] = 'edgtf-iwt-'.$params['type'];
	    }

        if(!empty($params['icon_size'])) {
            $classes[] = 'edgtf-iwt-'.str_replace('edgtf-', '', $params['icon_size']);
        }

        return $classes;
    }
	
	/**
	 * Returns inline content styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getContentStyles($params) {
		$styles = array();
		
		if($params['text_padding'] !== '' && $params['type'] === 'icon-left') {
			$styles[] = 'padding-left: '.onschedule_edge_filter_px($params['text_padding']).'px';
		}
		
		if($params['text_padding'] !== '' && $params['type'] === 'icon-top') {
			$styles[] = 'padding-top: '.onschedule_edge_filter_px($params['text_padding']).'px';
		}
		
		return implode(';', $styles);
	}

	/**
     * Returns inline title styles
     *
     * @param $params
     *
     * @return string
     */
    private function getTitleStyles($params) {
        $styles = array();

        if(!empty($params['title_color'])) {
            $styles[] = 'color: '.$params['title_color'];
        }
	
	    if(!empty($params['title_top_margin'])) {
		    $styles[] = 'margin-top: '.onschedule_edge_filter_px($params['title_top_margin']).'px';
	    }

        return implode(';', $styles);
    }

	/**
     * Returns inline text styles
     *
     * @param $params
     *
     * @return string
     */
    private function getTextStyles($params) {
        $styles = array();

        if(!empty($params['text_color'])) {
            $styles[] = 'color: '.$params['text_color'];
        }
	    
	    if(!empty($params['text_top_margin'])) {
		    $styles[] = 'margin-top: '.onschedule_edge_filter_px($params['text_top_margin']).'px';
	    }

        return implode(';', $styles);
    }
}