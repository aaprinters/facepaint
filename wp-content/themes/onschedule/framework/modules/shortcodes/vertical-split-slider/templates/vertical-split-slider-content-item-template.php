<div class="edgtf-vss-ms-section" <?php echo onschedule_edge_get_inline_attrs($content_data); ?> <?php onschedule_edge_inline_style($content_style);?>>
	<?php echo do_shortcode($content); ?>
</div>