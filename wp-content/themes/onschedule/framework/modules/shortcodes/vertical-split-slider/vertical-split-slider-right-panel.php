<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\VerticalSplitSliderRightPanel;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class VerticalSplitSliderRightPanel implements ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'edgtf_vertical_split_slider_right_panel';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(
			array(
				'name' => esc_html__('Edge Right Sliding Panel', 'onschedule'),
				'base' => $this->base,
				'as_parent'	=> array('only' => 'edgtf_vertical_split_slider_content_item'),
				'as_child'	=> array('only' => 'edgtf_vertical_split_slider'),
				'content_element' => true,
				'category' => esc_html__('by EDGE', 'onschedule'),
				'icon' => 'icon-wpb-vertical-split-slider-right-panel extended-custom-icon',
				'show_settings_on_create' => false,
				'js_view' => 'VcColumnView'
			)
		);
	}

	public function render($atts, $content = null) {
		$args = array();

		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '<div class="edgtf-vss-ms-right">';
		$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;
	}
}
