<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Charts;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class Charts implements ShortcodeInterface {

    private $base;

	/**
	 * Charts constructor.
	 */
    function __construct() {
        $this->base = 'edgtf_charts';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

	/**
	 * Returns base for shortcode
	 * @return string
	 */
    public function getBase() {
        return $this->base;
    }

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
    public function vcMap() {

        $no_of_datasets = 3;
        $no_of_points = 12;

        /* points tab begin */
        // first point group w/o dependency
        $points_array[] = array(
            'type'        => 'dropdown',
	        'param_name'  => 'points_1',
	        'heading'     => esc_html__('Use Point Group 1', 'onschedule'),
	        'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
            'save_always' => true,
            'group'       => esc_html__('Points', 'onschedule'),
        );

        $points_array[] = array(
            'type'       => 'colorpicker',
	        'param_name' => 'point_1_color',
	        'heading'    => esc_html__('Point 1 Color', 'onschedule'),
            'group'      => esc_html__('Points', 'onschedule'),
        );

        $points_array[] = array(
            'type'       => 'textfield',
	        'param_name' => 'point_1_label',
	        'heading'    => esc_html__('Point 1 Label', 'onschedule'),
            'group'      => esc_html__('Points', 'onschedule'),
        );

        // from second to twelfth w/ dependency
        for ($i = 2; $i <= $no_of_points; $i++) {
            $points_array[] = array(
                'type'        => 'dropdown',
	            'param_name'  => 'points_' . $i,
	            'heading'     => esc_html__('Use Point Group ', 'onschedule') . $i,
	            'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, false)),
                'save_always' => true,
	            'dependency'  => array('element' => 'points_' . ($i - 1), 'value' => 'yes'),
	            'group'       => esc_html__('Points', 'onschedule')
            );

            $points_array[] = array(
                'type'       => 'colorpicker',
	            'param_name' => 'point_' . $i . '_color',
	            'heading'    => esc_html__('Point ', 'onschedule') . $i . esc_html__(' Color', 'onschedule'),
                'dependency' => array('element' => 'points_' . $i, 'value' => 'yes'),
	            'group'      => esc_html__('Points', 'onschedule')
            );

            $points_array[] = array(
                'type'       => 'textfield',
	            'param_name' => 'point_' . $i . '_label',
	            'heading'    => esc_html__('Point ', 'onschedule') . $i . esc_html__(' Label', 'onschedule'),
                'dependency' => array('element' => 'points_' . $i, 'value' => 'yes'),
	            'group'      => esc_html__('Points', 'onschedule')
            );
        }
        /* points tab end */

        /* dataset tabs begin */
        for ($i = 1; $i <= $no_of_datasets; $i++) { // no of datasets

            $dataset_array[] = array(
                'type'       => 'textfield',
	            'param_name' => 'dataset_' . $i . '_label',
	            'heading'    => esc_html__('Dataset ', 'onschedule') . $i . esc_html__(' Label', 'onschedule'),
                'group'      => esc_html__('Dataset ', 'onschedule') . $i,
            );

            $dataset_array[] = array(
                'type'       => 'colorpicker',
	            'param_name' => 'dataset_' . $i . '_color',
	            'heading'    => esc_html__('Dataset ', 'onschedule') . $i . esc_html__(' Color', 'onschedule'),
                'dependency' => array('element' => 'color_scheme', 'value' => 'dataset'),
	            'group'      => esc_html__('Dataset ', 'onschedule') . $i
            );

            for ($j = 1; $j <= $no_of_points; $j++) { // no of points in dataset
                $dataset_array[] = array(
                    'type'       => 'textfield',
	                'param_name' => 'point_' . $i . '_' . $j,
	                'heading'    => esc_html__('Point ', 'onschedule') . $j . esc_html__(' Value', 'onschedule'),
                    'dependency' => array('element' => 'points_' . $j, 'value' => 'yes'),
                    'group'      => esc_html__('Dataset ', 'onschedule') . $i
                );
            }
        }
        /* dataset tabs end */

        vc_map(array(
            'name' => esc_html__('Edge Charts', 'onschedule'),
            'base' => $this->base,
            'icon' => 'icon-wpb-charts extended-custom-icon',
            'category' => esc_html__('by EDGE','onschedule'),
            'allowed_container_element' => 'vc_row',
            'params' => array_merge(
                array(
                    array(
                        'type'       => 'dropdown',
	                    'param_name' => 'type',
	                    'heading'    => esc_html__('Type', 'onschedule'),
                        'value'      => array(
                            esc_html__('Line', 'onschedule')            => 'line',
                            esc_html__('Vertical Bars', 'onschedule')   => 'bar',
                            esc_html__('Horizontal Bars', 'onschedule') => 'horizontalBar',
                            esc_html__('Radar', 'onschedule')           => 'radar',
                            esc_html__('Polar', 'onschedule')           => 'polarArea',
                            esc_html__('Pie', 'onschedule')             => 'pie',
                            esc_html__('Doughnut', 'onschedule')        => 'doughnut'
                        ),
                        'save_always' => true
                    ),
	                array(
		                'type'       => 'dropdown',
		                'param_name' => 'legend_display',
		                'heading'    => esc_html__('Legend', 'onschedule'),
		                'value'      => array(
			                esc_html__('Show', 'onschedule')  => 'show',
			                esc_html__('Hide', 'onschedule')  => 'hide'
		                ),
		                'save_always' => true
	                ),
                    array(
                        'type'       => 'dropdown',
	                    'param_name' => 'legend_position',
	                    'std'        => 'right',
	                    'heading'    => esc_html__('Legend Position', 'onschedule'),
                        'value'      => array(
                            esc_html__('Top', 'onschedule')    => 'top',
                            esc_html__('Right', 'onschedule')  => 'right',
                            esc_html__('Bottom', 'onschedule') => 'bottom',
                            esc_html__('Left', 'onschedule')   => 'left'
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'       => 'dropdown',
	                    'param_name' => 'color_scheme',
	                    'heading'    => esc_html__('Color Scheme', 'onschedule'),
                        'value'      => array(
                            esc_html__('One Color per Dataset', 'onschedule')     => 'dataset',
                            esc_html__('One Color per Point Group', 'onschedule') => 'point'
                        ),
                        'save_always' => true
                    ),
                ),
                $points_array,
                $dataset_array
            )
        ));
    }

    public function render($atts, $content = null) {
        $args = array(
            'type'            => '',
            'color_scheme'    => '',
	        'legend_display'  => 'show',
            'legend_position' => '',
        );

        $no_of_datasets = 3;
        $no_of_points = 12;

        $point_fields = array();
        $dataset_fields = array();

        for ($i = 1; $i <= $no_of_points; $i++) {
            $point_fields['point_' . $i . '_color'] = '';
            $point_fields['point_' . $i . '_label'] = '';
        }

        for ($i = 1; $i <= $no_of_datasets; $i++) {
            $dataset_fields['dataset_' . $i . '_color'] = '';
            $dataset_fields['dataset_' . $i . '_label'] = '';

            for ($j = 1; $j <= $no_of_points; $j++) {
                $dataset_fields['point_' . $i . '_' . $j] = '';
            }
        }

        $args = array_merge($args, $point_fields, $dataset_fields);

        $params = shortcode_atts($args, $atts);

        // extract params for use in method
        extract($params);

        for ($i = 1; $i <= $no_of_datasets; $i++) {
            $params['dataset_' . $i . '_values'] = $this->getDatasetValues($params, $i, $no_of_points);
        }

        $params['no_of_used_datasets'] = $this->getNoOfUsedDatasets($params, $no_of_datasets);
	    $params['point_group_labels'] = $this->getPointGroupLabels($params, $no_of_points);
	    $params['point_group_colors'] = $this->getPointGroupColors($params, $no_of_points);

	    $params['attributes'] = array(
		    'data-type'                 => $params['type'],
		    'data-no_of_used_datasets'  => $params['no_of_used_datasets'],
			'data-color_scheme'         => $params['color_scheme'],
		    'data-legend_display'       => $params['legend_display'],
		    'data-legend_position'      => $params['legend_position'],
		    'data-point_group_labels'   => $params['point_group_labels'],
		    'data-point_group_colors'   => $params['point_group_colors'],
	    );

        for ($i = 1; $i <= $params['no_of_used_datasets']; $i++) {
	        $params['attributes']['data-dataset_' . $i] = $params['dataset_' . $i . '_values'];
        }

        for ($i = 1; $i <= $params['no_of_used_datasets']; $i++) {
	        $params['attributes']['data-dataset_' . $i . '_label'] = $params['dataset_' . $i . '_label'];
	        $params['attributes']['data-dataset_' . $i . '_color'] = $params['dataset_' . $i . '_color'];
        }

        $html = onschedule_edge_get_shortcode_module_template_part('templates/charts', 'charts', '', $params);

        return $html;
    }

    /*
     * convert dataset values from shortcode into usable string
     *
     * @params $params - mixed, shortcode params
     * @params $dataset - integer, current dataset, since function is being called from loop
     * @params $no_of_points - integer, total number of points
     *
     * @return string
     */
    private function getDatasetValues($params, $dataset, $no_of_points) {
        $dataset_values = array();

        for ($i = 1; $i <= $no_of_points; $i++) {
            if ($params['point_' . $dataset . '_' . $i] !== '') {
	            $dataset_values[] = $params['point_' . $dataset . '_' . $i];
            }
        }

        $dataset_values = implode(',', $dataset_values);

        return $dataset_values;
    }

    /*
     * create data attribute for inline print in html
     *
     * @params $title - string, name of the data attribute
     * @params $raw_attribute - string, value of the data attribute
     *
     * @return string
     */
    private function getDataAttribute($title, $raw_attribute) {
        $data_attribute = 'data-' . $title . '="' . $raw_attribute . '"';

        return $data_attribute;
    }

    /*
     * determine how many datasets are being used in shortcode
     *
     * @params $params - mixed, shortcode params
     * @params $no_of_datasets - integer, total number of datasets available in shortcode interface
     *
     * @return integer
     */
    private function getNoOfUsedDatasets($params, $no_of_datasets) {
        for ($i = $no_of_datasets; $i >= 1; $i--) {
            if ($params['dataset_' . $i . '_values'] !== '') {
                return $i;
            }
        }
    }

    private function getPointGroupLabels($params, $no_of_labels) {
        $point_group_labels = array();

        for ($i = 1; $i <= $no_of_labels; $i++) {
            if ($params['point_' . $i . '_label'] !== '') {
	            $point_group_labels[] = $params['point_' . $i . '_label'];
            }
        }

        $point_group_labels = implode(',', $point_group_labels);

        return $point_group_labels;
    }

    private function getPointGroupColors($params, $no_of_labels) {
        $point_group_colors = array();

        for ($i = 1; $i <= $no_of_labels; $i++) {
            if ($params['point_' . $i . '_color'] !== '') {
	            $point_group_colors[] = $params['point_' . $i . '_color'];
            }
        }

        $point_group_colors = implode(',', $point_group_colors);

        return $point_group_colors;
    }
}