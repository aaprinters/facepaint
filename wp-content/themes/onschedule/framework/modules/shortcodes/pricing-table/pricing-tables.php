<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\PricingTables;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTables implements ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_pricing_tables';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map(
			array(
				'name' => esc_html__('Edge Pricing Tables', 'onschedule'),
				'base' => $this->base,
				'as_parent' => array('only' => 'edgtf_pricing_table'),
				'content_element' => true,
				'category' => esc_html__('by EDGE', 'onschedule'),
				'icon' => 'icon-wpb-pricing-tables extended-custom-icon',
				'show_settings_on_create' => true,
				'js_view' => 'VcColumnView',
				'params' => array(
					array(
						'type'       => 'dropdown',
						'param_name' => 'columns',
						'heading'    => esc_html__('Number of Columns', 'onschedule'),
						'value' => array(
							esc_html__('One', 'onschedule') => 'edgtf-one-column',
							esc_html__('Two', 'onschedule') => 'edgtf-two-columns',
							esc_html__('Three', 'onschedule') => 'edgtf-three-columns',
							esc_html__('Four', 'onschedule') => 'edgtf-four-columns',
							esc_html__('Five', 'onschedule') => 'edgtf-five-columns'
						),
						'save_always' => true
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'space_between_columns',
						'heading'    => esc_html__('Space Between Columns', 'onschedule'),
						'value'      => array(
							esc_html__('Normal', 'onschedule') => 'normal',
							esc_html__('Small', 'onschedule') => 'small',
							esc_html__('Tiny', 'onschedule') => 'tiny',
							esc_html__('No Space', 'onschedule') => 'no'
						),
						'save_always' => true
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'appear_effect',
						'heading'    => esc_html__('Appear Effect', 'onschedule'),
						'value'      => array(
							esc_html__('Yes', 'onschedule') => 'yes',
							esc_html__('No', 'onschedule') => 'no',
						),
						'save_always' => true
					)
				)
			)
		);
	}

	public function render($atts, $content = null) {
		$args = array(
			'columns'         	    => 'edgtf-two-columns',
			'space_between_columns' => 'normal',
			'appear_effect' => 'yes'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$holder_class = '';
		
		if (!empty($columns)) {
			$holder_class .= ' '.$columns;
		}

		if (!empty($space_between_columns)) {
			$holder_class .= ' edgtf-pt-'.$space_between_columns.'-space';
		}
		
		if (($appear_effect) == 'yes') {
			$holder_class .= ' edgtf-pt-appear-effect';
		}

		$html = '<div class="edgtf-pricing-tables clearfix '.esc_attr($holder_class).'">';
			$html .= '<div class="edgtf-pt-wrapper">';
				$html .= do_shortcode($content);
			$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}