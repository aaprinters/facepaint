<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\PricingTable;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTable implements ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_pricing_table';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Pricing Table', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-pricing-table extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'allowed_container_element' => 'vc_row',
			'as_child' => array('only' => 'edgtf_pricing_tables'),
			'params' => array(
				array(
					'type'       => 'dropdown',
					'param_name' => 'type',
					'heading'    => esc_html__('Type', 'onschedule'),
					'value' => array(
						esc_html__('Standard', 'onschedule') => 'standard',
						esc_html__('Simple', 'onschedule') => 'simple'
					),
					'save_always' => true
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__('Title', 'onschedule'),
					'value'       => esc_html__('Basic Plan', 'onschedule'),
					'save_always' => true
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'price',
					'heading'    => esc_html__('Price', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'currency',
					'heading'     => esc_html__('Currency', 'onschedule'),
					'description' => esc_html__('Default mark is $', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'price_period',
					'heading'     => esc_html__('Price Period', 'onschedule'),
					'description' => esc_html__('Default label is monthly', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'button_text',
					'heading'     => esc_html__('Button Text', 'onschedule'),
					'value'       => esc_html__('BUY NOW', 'onschedule'),
					'save_always' => true
				),
				array(
					'type'       => 'textfield',
					'param_name' => 'link',
					'heading'    => esc_html__('Button Link', 'onschedule'),
					'dependency' => array('element' => 'button_text',  'not_empty' => true)
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'button_style',
					'heading'    => esc_html__('Button Style', 'onschedule'),
					'value'      => array(
						esc_html__('Default', 'onschedule') => '',
						esc_html__('Dark', 'onschedule') => 'dark'
					),
					'dependency' => array('element' => 'button_text',  'not_empty' => true)
				),
				array(
					'type'       => 'textarea_html',
					'param_name' => 'content',
					'heading'    => esc_html__('Content', 'onschedule'),
					'value'      => '<li>content content content</li><li>content content content</li><li>content content content</li>'
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'type'         => 'standard',
			'title'        => '',
			'price'        => '100',
			'currency'     => '$',
			'price_period' => 'monthly',
			'button_text'  => '',
			'link'         => '',
			'button_style' => ''
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '';
		
		$params['content']= preg_replace('#^<\/p>|<p>$#', '', $content); // delete p tag before and after content
		
		$params['type'] = !empty($params['type']) ? $params['type'] : $args['type'];
		$params['holder_classes'] = $this->getHolderClasses($params);
		
		$html .= onschedule_edge_get_shortcode_module_template_part('templates/pricing-table-'.$params['type'], 'pricing-table', '', $params);
		
		return $html;
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holderClasses = array();
		
		if(!empty($params['type'])){
			$holderClasses[] = 'edgtf-pt-'.$params['type'];
		}
		
		if(!empty($params['button_style'])){
			$holderClasses[] = 'edgtf-pt-button-'.$params['button_style'];
		}
		
		return implode(' ', $holderClasses);
	}
}