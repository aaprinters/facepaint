<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Team;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Team
 */
class Team implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'edgtf_team';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap()	{
		$team_social_icons_array = array();
		for ($x = 1; $x<6; $x++) {
			$teamIconCollections = onschedule_edge_icon_collections()->getCollectionsWithSocialIcons();
			foreach($teamIconCollections as $collection_key => $collection) {

				$team_social_icons_array[] = array(
					'type' => 'dropdown',
					'heading' => esc_html__('Social Icon ', 'onschedule').$x,
					'param_name' => 'team_social_'.$collection->param.'_'.$x,
					'value' => $collection->getSocialIconsArrayVC(),
					'dependency' => Array('element' => 'team_social_icon_pack', 'value' => array($collection_key))
				);
			}

			$team_social_icons_array[] = array(
				'type' => 'textfield',
				'heading' => esc_html__('Social Icon ', 'onschedule').$x.esc_html__(' Link', 'onschedule'),
				'param_name' => 'team_social_icon_'.$x.'_link',
				'dependency' => array('element' => 'team_social_icon_pack', 'value' => onschedule_edge_icon_collections()->getIconCollectionsKeys())
			);

			$team_social_icons_array[] = array(
				'type' => 'dropdown',
				'heading' => esc_html__('Social Icon ', 'onschedule').$x.esc_html__(' Target', 'onschedule'),
				'param_name' => 'team_social_icon_'.$x.'_target',
				'value' => array(
					esc_html__('Same Window', 'onschedule') => '_self',
					esc_html__('New Window', 'onschedule') => '_blank'
				),
				'dependency' => Array('element' => 'team_social_icon_'.$x.'_link', 'not_empty' => true)
			);
		}

		vc_map( array(
			'name' => esc_html__('Edge Team', 'onschedule'),
			'base' => $this->base,
			'category' => esc_html__('by EDGE', 'onschedule'),
			'icon' => 'icon-wpb-team extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params' => array_merge(
				array(
					array(
						'type'       => 'dropdown',
						'param_name' => 'type',
						'heading'    => esc_html__('Type', 'onschedule'),
						'value' => array(
							esc_html__('Info Below Image', 'onschedule') => 'info-below-image',
							esc_html__('Info On Image Hover', 'onschedule') => 'info-on-image'
						),
						'save_always' => true
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'shape',
						'heading'    => esc_html__('Image Shape', 'onschedule'),
						'value' => array(
							esc_html__('Square', 'onschedule') => 'square',
							esc_html__('Circle', 'onschedule') => 'circle'
						),
						'save_always' => true
					),
					array(
						'type'       => 'attach_image',
						'param_name' => 'team_image',
						'heading'    => esc_html__('Image', 'onschedule')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'team_name',
						'heading'    => esc_html__('Name', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'team_name_tag',
						'heading'     => esc_html__('Name Tag', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_title_tag(true)),
						'save_always' => true,
						'dependency'  => array('element' => 'team_name', 'not_empty' => true)
					),
					array(
	                    'type'       => 'colorpicker',
	                    'param_name' => 'team_name_color',
	                    'heading'    => esc_html__('Name Color', 'onschedule'),
	                    'dependency' => array('element' => 'team_name', 'not_empty' => true)
	                ),
					array(
						'type'       => 'textfield',
						'param_name' => 'team_position',
						'heading'    => esc_html__('Position', 'onschedule')
					),
					array(
	                    'type'       => 'colorpicker',
	                    'param_name' => 'team_position_color',
	                    'heading'    => esc_html__('Position Color', 'onschedule'),
	                    'dependency' => array('element' => 'team_position', 'not_empty' => true)
	                ),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'team_social_icon_pack',
						'heading'     => esc_html__('Social Icon Pack', 'onschedule'),
						'value'       => array_merge(array('' => ''),onschedule_edge_icon_collections()->getIconCollectionsVCExclude('linea_icons')),
						'save_always' => true
					)
				),
				$team_social_icons_array
			)
		) );
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'type'                  => 'info-below-image',
			'shape'                 => 'square',
			'team_image'            => '',
			'team_name'             => '',
			'team_name_tag'         => 'h6',
			'team_name_color'       => '',
			'team_position'         => '',
			'team_position_color'   => '',
			'team_social_icon_pack' => ''
		);

		$team_social_icons_form_fields = array();
		$number_of_social_icons = 5;

		for ($x = 1; $x <= $number_of_social_icons; $x++) {

			foreach (onschedule_edge_icon_collections()->iconCollections as $collection_key => $collection) {
				$team_social_icons_form_fields['team_social_' . $collection->param . '_' . $x] = '';
			}

			$team_social_icons_form_fields['team_social_icon_'.$x.'_link'] = '';
			$team_social_icons_form_fields['team_social_icon_'.$x.'_target'] = '';
		}

		$args = array_merge($args, $team_social_icons_form_fields);

		$params = shortcode_atts($args, $atts);

		$params['number_of_social_icons'] = 5;
		
		$params['type'] = !empty($params['type']) ? $params['type'] : $args['type'];
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['team_name_tag'] = !empty($params['team_name_tag']) ? $params['team_name_tag'] : $args['team_name_tag'];
		$params['team_social_icons'] = $this->getTeamSocialIcons($params);
		$params['team_name_styles'] = $this->getTeamNameStyles($params);
		$params['team_position_styles'] = $this->getTeamPositionStyles($params);

		//Get HTML from template based on type of team
		$html = onschedule_edge_get_shortcode_module_template_part('templates/'.$params['type'], 'team', '', $params);

		return $html;
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holderClasses = array();
		
		if(!empty($params['type'])){
			$holderClasses[] = 'edgtf-team-'.$params['type'];
		}

		if(!empty($params['shape'])){
			$holderClasses[] = 'edgtf-team-'.$params['shape'];
		}
		
		return implode(' ', $holderClasses);
	}

	private function getTeamSocialIcons($params) {
		extract($params);
		$social_icons = array();

		if ($team_social_icon_pack !== '') {

			$icon_pack = onschedule_edge_icon_collections()->getIconCollection($team_social_icon_pack);
			$team_social_icon_type_label = 'team_social_' . $icon_pack->param;
			$team_social_icon_param_label = $icon_pack->param;

			for ( $i = 1; $i <= $params['number_of_social_icons']; $i++ ) {

				$team_social_icon = ${$team_social_icon_type_label . '_' . $i};
				$team_social_link = ${'team_social_icon_' . $i . '_link'};
				$team_social_target = ${'team_social_icon_' . $i . '_target'};

				if ($team_social_icon !== '') {

					$team_icon_params = array();
					$team_icon_params['icon_pack'] = $team_social_icon_pack;
					$team_icon_params[$team_social_icon_param_label] =   $team_social_icon;
					$team_icon_params['link'] = ($team_social_link !== '') ? $team_social_link : '';
					$team_icon_params['target'] = ($team_social_target !== '') ? $team_social_target : '';

					$social_icons[] = onschedule_edge_execute_shortcode('edgtf_icon', $team_icon_params);
				}
			}
		}

		return $social_icons;
	}

	private function getTeamNameStyles($params) {
		$styles = array();

        if (!empty($params['team_name_color'])) {
	        $styles[] = 'color: '.$params['team_name_color'];
        }

        return implode(';', $styles);
    }

    private function getTeamPositionStyles($params) {
	    $styles = array();

        if (!empty($params['team_position_color'])) {
	        $styles[] = 'color: '.$params['team_position_color'];
        }

        return implode(';', $styles);
    }
}