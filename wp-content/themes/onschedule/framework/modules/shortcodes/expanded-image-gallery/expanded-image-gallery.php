<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ExpandedImageGallery;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class ExpandedImageGallery implements ShortcodeInterface{

	private $base;

	/**
	 * Image Gallery constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_expanded_image_gallery';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Expanded Image Gallery', 'onschedule'),
			'base'                      => $this->getBase(),
			'category'                  => esc_html__('by EDGE', 'onschedule'),
			'icon' 						=> 'icon-wpb-expanded-image-gallery extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'		  => 'attach_images',
					'param_name'  => 'images',
					'heading'	  => esc_html__('Images', 'onschedule'),
					'description' => esc_html__('Select images from media library', 'onschedule')
				),
				array(
					'type'		  => 'textfield',
					'param_name'  => 'image_height',
					'heading'	  => esc_html__('Visible Image Height (px)', 'onschedule')
				),
				array(
					'type'		  => 'textfield',
					'param_name'  => 'expand_image_speed',
					'heading'	  => esc_html__('Expanding Images Speed (ms)', 'onschedule'),
					'description' => esc_html__('Default value is 500', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'images_shadow',
					'heading'     => esc_html__('Enable Shadow Around Images', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false))
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'number_of_columns',
					'heading'    => esc_html__('Number of Columns', 'onschedule'),
					'value' => array(
						esc_html__('Two', 'onschedule') => '2',
						esc_html__('Three', 'onschedule') => '3',
						esc_html__('Four', 'onschedule') => '4',
						esc_html__('Five', 'onschedule') => '5'
					),
					'save_always' => true
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'space_between_columns',
					'heading'    => esc_html__('Space Between Columns', 'onschedule'),
					'value'      => array(
						esc_html__('Large', 'onschedule') => 'large',
						esc_html__('Normal', 'onschedule') => 'normal',
						esc_html__('Small', 'onschedule') => 'small',
						esc_html__('Tiny', 'onschedule') => 'tiny',
						esc_html__('No Space', 'onschedule') => 'no'
					),
					'save_always' => true
				),
                array(
                    'type'        => 'textarea',
                    'param_name'  => 'custom_links',
                    'heading'     => esc_html__('Custom Links', 'onschedule'),
                    'description' => esc_html__('Delimit links by comma', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'custom_link_target',
                    'heading'     => esc_html__('Custom Link Target', 'onschedule'),
                    'value'       => array(
						esc_html__('Same Window', 'onschedule')  => '_self',
						esc_html__('New Window', 'onschedule') => '_blank'
                    ),
                    'save_always' => true
                ),
                array(
                	'type'       => 'dropdown',
                	'param_name' => 'appear_effect',
                	'heading'    => esc_html__('Appear Effect', 'onschedule'),
                	'value' => array(
                		esc_html__('Yes', 'onschedule') => 'yes',
                		esc_html__('No', 'onschedule') => 'no',
                	),
                	'save_always' => true,
                	'group' => esc_html__('Behavior', 'onschedule')
                ),
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'images'			    => '',
			'image_height'		    => '',
			'expand_image_speed'    => '500',
			'images_shadow'         => 'yes',
			'number_of_columns'		=> '3',
			'space_between_columns' => 'normal',
			'custom_links'		    => '',
			'custom_link_target'    => '_self',
			'appear_effect' 		=> 'yes'
		);

		$params = shortcode_atts($args, $atts);
		
		$params['gallery_classes'] = $this->getGalleryClasses($params);
		$params['gallery_data']    = $this->getGalleryData($params);
		$params['images']          = $this->getGalleryImages($params);
		$params['links']           = $this->getGalleryLinks($params);
		$params['target']          = !empty($params['custom_link_target']) ? $params['custom_link_target'] : $args['custom_link_target'];

		$html = onschedule_edge_get_shortcode_module_template_part('templates/expanded-image-gallery', 'expanded-image-gallery', '', $params);

		return $html;
	}
	
	/**
	 * Generates gallery classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getGalleryClasses($params) {
		$holderClasses = '';
		
		$holderClasses .= !empty($params['number_of_columns']) ? ' edgtf-eig-columns-' . $params['number_of_columns'] : ' edgtf-eig-columns-3';
		$holderClasses .= !empty($params['space_between_columns']) ? ' edgtf-eig-' . $params['space_between_columns'] . '-space' : ' edgtf-eig-normal-space';
		$holderClasses .= ($params['appear_effect'] === 'yes') ? ' edgtf-eig-appear-effect' : '';
		$holderClasses .= $params['images_shadow'] === 'no' ? ' edgtf-eig-no-shadow' : '';
		
		return $holderClasses;
	}
	
	/**
	 * Generates gallery data
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getGalleryData($params) {
		$holderData = array();
		
		$holderData['data-image-height'] = !empty($params['image_height']) ? onschedule_edge_filter_px($params['image_height']) : '';
		$holderData['data-image-speed'] = !empty($params['expand_image_speed']) ? $params['expand_image_speed'] : '500';
		
		return $holderData;
	}

	/**
	 * Return images for gallery
	 *
	 * @param $params
	 * @return array
	 */
	private function getGalleryImages($params) {
		$image_ids = array();
		$images = array();
		$i = 0;

		if ($params['images'] !== '') {
			$image_ids = explode(',', $params['images']);
		}

		foreach ($image_ids as $id) {

			$image['image_id'] = $id;
			$image_original = wp_get_attachment_image_src($id, 'full');
			$image['url'] = $image_original[0];
			$image['title'] = get_the_title($id);
			$image['alt'] = get_post_meta( $id, '_wp_attachment_image_alt', true);

			$images[$i] = $image;
			$i++;
		}

		return $images;
	}

    /**
     * Return links for gallery
     *
     * @param $params
     * @return array
     */
    private function getGalleryLinks($params) {
        $custom_links = array();

        if (!empty($params['custom_links'])) {
            $custom_links = array_map('trim', explode(',', $params['custom_links']));
	    }

        return $custom_links;
    }
}