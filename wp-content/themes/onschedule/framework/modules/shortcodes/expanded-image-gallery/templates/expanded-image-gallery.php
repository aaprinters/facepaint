<?php $i = 0; ?>
<div class="edgtf-expanded-image-gallery">
	<div class="edgtf-eig-filter">
		<a class="edgtf-eig-standard-view edgtf-eig-active" href="#">
			<span class="edgtf-eig-icon"></span>
			<span class="edgtf-eig-label">
				<span class="edgtf-eig-label-top"><?php esc_html_e('STANDARD', 'onschedule'); ?></span>
				<span class="edgtf-eig-label-bottom"><?php esc_html_e('PREVIEW', 'onschedule'); ?></span>
			</span>
		</a>
		<a class="edgtf-eig-open-view" href="#">
			<span class="edgtf-eig-icon"></span>
			<span class="edgtf-eig-label">
				<span class="edgtf-eig-label-top"><?php esc_html_e('OPEN', 'onschedule'); ?></span>
				<span class="edgtf-eig-label-bottom"><?php esc_html_e('PREVIEW', 'onschedule'); ?></span>
			</span>
		</a>
	</div>
	<div class="edgtf-eig-grid <?php echo esc_attr($gallery_classes); ?>" <?php echo onschedule_edge_get_inline_attrs($gallery_data); ?>>
		<?php foreach ($images as $image) { ?>
			<div class="edgtf-eig-image">
				<div class="edgtf-eig-image-inner">
					<?php if(!empty($links[$i])){ ?>
		                <a itemprop="url" class="edgtf-eig-link" href="<?php echo esc_url($links[$i]) ?>" title="<?php echo esc_attr($image['title']); ?>" target="<?php echo esc_attr($target); ?>">
		            <?php } ?>
						<?php echo wp_get_attachment_image($image['image_id'], 'full'); ?>
		            <?php if(!empty($links[$i])){ ?>
						</a>
					<?php } ?>
				</div>
				<?php if(!empty($image['alt'])) { ?>
					<span class="edgtf-eig-image-label">
						<?php if(!empty($links[$i])){ ?>
							<a itemprop="url" class="edgtf-eig-image-label-link" href="<?php echo esc_url($links[$i]) ?>" title="<?php echo esc_attr($image['title']); ?>" target="<?php echo esc_attr($target); ?>">
						<?php } ?>
							<?php echo esc_attr($image['alt']); ?>
						<?php if(!empty($links[$i])){ ?>
							</a>
						<?php } ?>
					</span>
				<?php } ?>
			</div>
			<?php $i++; ?>
		<?php } ?>
	</div>
</div>