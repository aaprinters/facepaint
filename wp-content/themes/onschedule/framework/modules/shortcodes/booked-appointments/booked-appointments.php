<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\BookedAppointments;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class BookedAppointments implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;
	
	public function __construct() {
		$this->base = 'edgtf_booked_appointments';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name'                    => esc_html__( 'Edge Booked User Appointments', 'onschedule' ),
			'base'                    => $this->base,
			'category'                => esc_html__( 'by EDGE', 'onschedule' ),
			'icon'                    => 'icon-wpb-booked-user-appointments extended-custom-icon',
			'params'                  => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'skin',
					'heading'     => esc_html__('Skin', 'onschedule'),
					'value'       => array(
						esc_html__('Default', 'onschedule') => '',
						esc_html__('Custom', 'onschedule') => 'custom'
					),
					'save_always' => true
				),
			)
		) );
	}
	
	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {
		$args = array(
			'skin' => ''
		);

		extract(shortcode_atts($args, $atts));

		$holder_classes = '';
		if(!empty($skin)) {
			$holder_classes .= ' edgtf-booked-'.esc_attr($skin).'-skin';
		}
		
		$html = '<div class="edgtf-booked-user-appointments-holder '.esc_attr($holder_classes).'">' . do_shortcode( '[booked-appointments]' ) . '</div>';
		
		return $html;
	}
}