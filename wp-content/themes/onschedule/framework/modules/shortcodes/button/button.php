<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Button;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Button that represents button shortcode
 * @package OnScheduleEdgeNamespace\Modules\Shortcodes\Button
 */
class Button implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     * Sets base attribute and registers shortcode with Visual Composer
     */
    public function __construct() {
        $this->base = 'edgtf_button';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base attribute
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     */
    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Edge Button', 'onschedule'),
            'base'                      => $this->base,
            'admin_enqueue_css' => array(onschedule_edge_get_skin_uri().'/assets/css/edgtf-vc-extend.css'),
            'category'                  => esc_html__('by EDGE', 'onschedule'),
            'icon'                      => 'icon-wpb-button extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array_merge(
                array(
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'custom_class',
                        'heading'     => esc_html__('Custom CSS Class', 'onschedule')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'param_name'  => 'type',
                        'heading'     => esc_html__('Type', 'onschedule'),
                        'value'       => array(
						    esc_html__('Solid', 'onschedule')   => 'solid',
						    esc_html__('Outline', 'onschedule') => 'outline',
						    esc_html__('Simple', 'onschedule')  => 'simple'
                        ),
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'param_name'  => 'size',
                        'heading'     => esc_html__('Size', 'onschedule'),
                        'value'       => array(
						    esc_html__('Default', 'onschedule') => '',
						    esc_html__('Small', 'onschedule')   => 'small',
						    esc_html__('Medium', 'onschedule')  => 'medium',
						    esc_html__('Large', 'onschedule')   => 'large',
						    esc_html__('Huge', 'onschedule')    => 'huge'
                        ),
                        'dependency'  => array('element' => 'type', 'value' => array('solid', 'outline'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'text',
                        'heading'     => esc_html__('Text', 'onschedule'),
                        'value'       => esc_html__('Button Text', 'onschedule'),
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'link',
                        'heading'     => esc_html__('Link', 'onschedule')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'param_name'  => 'target',
                        'heading'     => esc_html__('Link Target', 'onschedule'),
                        'value'       => array(
						    esc_html__('Same Window', 'onschedule')  => '_self',
						    esc_html__('New Window', 'onschedule') => '_blank'
                        )
                    )
                ),
                onschedule_edge_icon_collections()->getVCParamsArray(array(), '', true),
                array(
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'color',
                        'heading'     => esc_html__('Color', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'hover_color',
                        'heading'     => esc_html__('Hover Color', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'background_color',
                        'heading'     => esc_html__('Background Color', 'onschedule'),
                        'dependency'  => array('element' => 'type', 'value' => array('solid')),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'hover_background_color',
                        'heading'     => esc_html__('Hover Background Color', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'border_color',
                        'heading'     => esc_html__('Border Color', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'param_name'  => 'hover_border_color',
                        'heading'     => esc_html__('Hover Border Color', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'font_size',
                        'heading'     => esc_html__('Font Size (px)', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'param_name'  => 'font_weight',
                        'heading'     => esc_html__('Font Weight', 'onschedule'),
                        'value'       => array_flip(onschedule_edge_get_font_weight_array(true)),
                        'save_always' => true,
                        'group'       => esc_html__('Design Options', 'onschedule')
                    ),
                    array(
                        'type'        => 'textfield',
                        'param_name'  => 'margin',
                        'heading'     => esc_html__('Margin', 'onschedule'),
                        'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'onschedule'),
                        'group'       => esc_html__('Design Options', 'onschedule')
                    )
                )
            )
        ));
    }

    /**
     * Renders HTML for button shortcode
     *
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'size'                   => '',
            'type'                   => 'solid',
            'text'                   => '',
            'link'                   => '',
            'target'                 => '',
            'color'                  => '',
            'hover_color'            => '',
            'background_color'       => '',
            'hover_background_color' => '',
            'border_color'           => '',
            'hover_border_color'     => '',
            'font_size'              => '',
            'font_weight'            => '',
            'margin'                 => '',
            'custom_class'           => '',
            'html_type'              => 'anchor',
            'input_name'             => '',
            'custom_attrs'           => array()
        );

        $default_atts = array_merge($default_atts, onschedule_edge_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        if($params['html_type'] !== 'input') {
            $iconPackName   = onschedule_edge_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
            $params['icon'] = $iconPackName ? $params[$iconPackName] : '';
        }

        $params['size'] = !empty($params['size']) ? $params['size'] : 'medium';
        $params['type'] = !empty($params['type']) ? $params['type'] : 'solid';


        $params['link']   = !empty($params['link']) ? $params['link'] : '#';
        $params['target'] = !empty($params['target']) ? $params['target'] : '_self';

        //prepare params for template
        $params['button_classes']      = $this->getButtonClasses($params);
        $params['button_custom_attrs'] = !empty($params['custom_attrs']) ? $params['custom_attrs'] : array();
        $params['button_styles']       = $this->getButtonStyles($params);
        $params['button_data']         = $this->getButtonDataAttr($params);

        return onschedule_edge_get_shortcode_module_template_part('templates/'.$params['html_type'], 'button', '', $params);
    }

    /**
     * Returns array of button styles
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonStyles($params) {
        $styles = array();

        if(!empty($params['color'])) {
            $styles[] = 'color: '.$params['color'];
        }

        if(!empty($params['background_color']) && $params['type'] !== 'outline') {
            $styles[] = 'background-color: '.$params['background_color'];
        }

        if(!empty($params['border_color'])) {
            $styles[] = 'border-color: '.$params['border_color'];
        }

        if(!empty($params['font_size'])) {
            $styles[] = 'font-size: '.onschedule_edge_filter_px($params['font_size']).'px';
        }

        if(!empty($params['font_weight']) && $params['font_weight'] !== '') {
            $styles[] = 'font-weight: '.$params['font_weight'];
        }

        if(!empty($params['margin'])) {
            $styles[] = 'margin: '.$params['margin'];
        }

        return $styles;
    }

    /**
     *
     * Returns array of button data attr
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonDataAttr($params) {
        $data = array();

        if(!empty($params['hover_color'])) {
            $data['data-hover-color'] = $params['hover_color'];
        }

        if(!empty($params['hover_background_color'])) {
            $data['data-hover-bg-color'] = $params['hover_background_color'];
        }

        if(!empty($params['hover_border_color'])) {
            $data['data-hover-border-color'] = $params['hover_border_color'];
        }

        return $data;
    }

    /**
     * Returns array of HTML classes for button
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonClasses($params) {
        $buttonClasses = array(
            'edgtf-btn',
            'edgtf-btn-'.$params['size'],
            'edgtf-btn-'.$params['type']
        );

        if(!empty($params['hover_background_color'])) {
            $buttonClasses[] = 'edgtf-btn-custom-hover-bg';
        }

        if(!empty($params['hover_border_color'])) {
            $buttonClasses[] = 'edgtf-btn-custom-border-hover';
        }

        if(!empty($params['hover_color'])) {
            $buttonClasses[] = 'edgtf-btn-custom-hover-color';
        }

        if(!empty($params['icon'])) {
            $buttonClasses[] = 'edgtf-btn-icon';
        }

        if(!empty($params['custom_class'])) {
            $buttonClasses[] = esc_attr($params['custom_class']);
        }

        return $buttonClasses;
    }
}