<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\InfoCards;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class InfoCards implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_info_cards';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Info Cards', 'onschedule'),
			'description' => esc_html__('For best performance use odd number of items inside this holder', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-info-cards extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'as_parent' => array('only' => 'edgtf_info_cards_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type'       => 'colorpicker',
					'param_name' => 'background_color',
					'heading'    => esc_html__('Holder Background Color', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'top_bottom_padding',
					'heading'     => esc_html__('Holder Top/Bottom Padding', 'onschedule'),
					'description' => esc_html__('Enter top/bottom padding for holder. You can use px or % for value (example 20px)', 'onschedule')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'left_right_padding',
					'heading'     => esc_html__('Inner Holder Left/Right Padding', 'onschedule'),
					'description' => esc_html__('Enter left/right padding for inner holder. You can use px or % for value (example 10%)', 'onschedule')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'enable_appear_animation',
					'heading'     => esc_html__('Enable Appear Item Animation', 'onschedule'),
					'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true))
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'background_color'        => '',
			'top_bottom_padding'      => '',
			'left_right_padding'      => '',
			'enable_appear_animation' => 'yes'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$holder_classes       = $this->getHolderClasses($params);
		$holder_styles        = $this->getHolderStyles($params);
		
		$overlay_styles       = $this->getOverlayStyles($params);
		$overlay_inner_styles = $this->getOverlayInnerStyles($params);

		$html = '';

		$html .= '<div class="edgtf-info-cards-holder '.esc_attr($holder_classes).'">';
			$html .= '<div class="edgtf-info-cards-wrapper edgtf-grid" '.onschedule_edge_get_inline_style($holder_styles).'>';
				$html .= '<div class="edgtf-info-cards-inner">';
					$html .= do_shortcode($content);
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="edgtf-ic-overlay" '.onschedule_edge_get_inline_style($overlay_styles).'><div class="edgtf-ic-overlay-inner" '.onschedule_edge_get_inline_style($overlay_inner_styles).'></div></div>';
		$html .= '</div>';

		return $html;
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params) {
		$classes = array();
		$classes[] = $params['enable_appear_animation'] === 'yes' ? 'edgtf-ic-animation' : '';
		
		return implode(' ', $classes);
	}
	
	/**
	 * Returns inline holder styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderStyles($params) {
		$styles = array();
		
		if($params['left_right_padding'] !== '') {
			$styles[] = 'padding-left: '.$params['left_right_padding'];
			$styles[] = 'padding-right: '.$params['left_right_padding'];
		}
		
		return implode(';', $styles);
	}
	
	/**
	 * Returns inline overlay styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getOverlayStyles($params) {
		$styles = array();
		
		if($params['top_bottom_padding'] !== '') {
			$styles[] = 'padding-top: '.$params['top_bottom_padding'];
			$styles[] = 'padding-bottom: '.$params['top_bottom_padding'];
		}
		
		return implode(';', $styles);
	}
	
	/**
	 * Returns inline overlay inner styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getOverlayInnerStyles($params) {
		$styles = array();
		
		if(!empty($params['background_color'])) {
			$styles[] = 'background-color: '.$params['background_color'];
		}
		
		return implode(';', $styles);
	}
}
