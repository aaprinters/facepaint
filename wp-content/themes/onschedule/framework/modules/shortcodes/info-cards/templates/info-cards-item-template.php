<div class="edgtf-ic-item <?php echo esc_attr($item_classes); ?>">
	<div class="edgtf-ic-item-inner" <?php echo onschedule_edge_get_inline_style($item_styles); ?>>
		<div class="edgtf-ic-item-content">
			<?php echo do_shortcode($content); ?>
		</div>
	</div>
</div>