<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\InfoCardsItem;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

class InfoCardsItem implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'edgtf_info_cards_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Edge Info Cards Item', 'onschedule'),
					'base' => $this->base,
					'as_child' => array('only' => 'edgtf_info_cards'),
					'category' => esc_html__('by EDGE', 'onschedule'),
					'icon' => 'icon-wpb-info-cards-item extended-custom-icon',
					'params' => array(
						array(
							'type'       => 'colorpicker',
							'param_name' => 'background_color',
							'heading'    => esc_html__('Card Background Color', 'onschedule')
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'enable_card_shadow',
							'heading'     => esc_html__('Enable Card Box Shadow', 'onschedule'),
							'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
							'save_always' => true
						),
						array(
							'type'		  => 'textarea_html',
							'param_name'  => 'content',
							'heading'	  => esc_html__('Content', 'onschedule')
						)
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'background_color'   => '',
			'enable_card_shadow' => 'yes'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$params['content']      = preg_replace('#^<\/p>|<p>$#', '', $content);
		$params['item_classes'] = $this->getItemClasses($params);
		$params['item_styles']  = $this->getItemStyles($params);

		$html = onschedule_edge_get_shortcode_module_template_part('templates/info-cards-item-template', 'info-cards', '', $params);

		return $html;
	}
	
	/**
	 * Generates Cards Item classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getItemClasses($params) {
		$classes = array();
		$classes[] = $params['enable_card_shadow'] === 'yes' ? 'edgtf-ic-has-shadow' : '';
		
		return implode(' ', $classes);
	}
	
	/**
	 * Return Cards Item style
	 *
	 * @param $params
	 * @return array
	 */
	private function getItemStyles($params) {
		$styles = array();

		if ($params['background_color'] !== '') {
			$styles[] = 'background-color: '.$params['background_color'];
		}

		return implode(';', $styles);
	}
}
