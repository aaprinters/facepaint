<div class="edgtf-clients-carousel-holder <?php echo esc_attr($holder_classes); ?>" <?php echo onschedule_edge_get_inline_attrs($carousel_data); ?>>
	<div class="edgtf-cc-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>