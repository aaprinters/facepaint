<div class="edgtf-footer-bottom-holder">
	<div class="edgtf-footer-bottom-inner <?php echo esc_attr($footer_bottom_grid_class); ?>">
		<div class="edgtf-columns-wrapper <?php echo esc_attr($footer_bottom_classes); ?>">
			<div class="edgtf-columns-inner">
				<?php for($i = 1; $i <= $footer_bottom_columns; $i++) { ?>
					<div class="edgtf-column-content edgtf-column-content<?php echo esc_attr($i); ?>">
						<?php
							if(is_active_sidebar('footer_bottom_column_'.$i)) {
								dynamic_sidebar('footer_bottom_column_'.$i);
							}
						?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>