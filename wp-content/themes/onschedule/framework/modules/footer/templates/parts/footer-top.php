<div class="edgtf-footer-top-holder">
	<div class="edgtf-footer-top-inner <?php echo esc_attr($footer_top_grid_class); ?>">
		<div class="edgtf-columns-wrapper <?php echo esc_attr($footer_top_classes); ?>">
			<div class="edgtf-columns-inner">
				<?php for($i = 1; $i <= $footer_top_columns; $i++) { ?>
					<div class="edgtf-column-content edgtf-column-content<?php echo esc_attr($i); ?>">
						<?php
							if(is_active_sidebar('footer_top_column_'.$i)) {
								dynamic_sidebar('footer_top_column_'.$i);
							}
						?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>