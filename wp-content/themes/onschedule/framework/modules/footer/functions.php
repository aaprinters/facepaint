<?php

if (!function_exists('onschedule_edge_footer_top_classes')) {
	/**
	 * Return classes for footer top
	 *
	 * @return string
	 */
	function onschedule_edge_footer_top_classes() {

		$footer_top_classes = array();

		$grid_class = onschedule_edge_options()->getOptionValue('footer_in_grid') == 'yes' ? 'edgtf-grid' : 'edgtf-full-width';

		//footer aligment
		$footer_top_alignment = onschedule_edge_options()->getOptionValue('footer_top_columns_alignment');
		$footer_top_classes[] = $footer_top_alignment !== '' ? 'edgtf-footer-top-alignment-'.esc_attr($footer_top_alignment) : '';

		$footer_top_columns =  onschedule_edge_options()->getOptionValue('footer_top_columns');

		switch($footer_top_columns){
			case '1':
				$footer_top_classes[] = 'edgtf-content-columns-100';
				break;
			case '2':
				$footer_top_classes[] = 'edgtf-content-columns-50-50';
				break;
			case '3':
				$footer_top_classes[] = 'edgtf-content-columns-three';
				break;
			case '4':
				$footer_top_classes[] = 'edgtf-content-columns-four';
				break;
		}

		$footer_top_classes['three-cols-layout'] = '';
		if($footer_top_columns === '3' ){
			$footer_top_classes['three-cols-layout']  = 'edgtf-'.onschedule_edge_options()->getOptionValue('footer_top_three_columns_layout');
		}
		
		//set spacing between columns
		$footer_top_classes[] = 'edgtf-columns-normal-space';

		$footer_top_classes_array = apply_filters('onschedule_edge_filter_footer_top_classes', $footer_top_classes);

		$classes = implode(' ', $footer_top_classes_array);

		$return_array = array(
			'grid_class' => $grid_class,
			'classes' => $classes
		);

		return $return_array;
	}
}

if (!function_exists('onschedule_edge_footer_bottom_classes')) {
	/**
	 * Return classes for footer bottom
	 *
	 * @return string
	 */
	function onschedule_edge_footer_bottom_classes() {

		$footer_bottom_classes = array();

		$grid_class = onschedule_edge_options()->getOptionValue('footer_in_grid') == 'yes' ? 'edgtf-grid' : 'edgtf-full-width';

		$footer_bottom_columns =  onschedule_edge_options()->getOptionValue('footer_bottom_columns');

		switch($footer_bottom_columns){
			case '1':
				$footer_bottom_classes[] = 'edgtf-content-columns-100';
				break;
			case '2':
				$footer_bottom_classes[] = 'edgtf-content-columns-50-50';
				break;
			case '3':
				$footer_bottom_classes[] = 'edgtf-content-columns-three';
				break;
		}

		$footer_bottom_classes[] = 'edgtf-columns-normal-space';

		$footer_bottom_classes_array = apply_filters('onschedule_edge_filter_footer_bottom_classes', $footer_bottom_classes);

		$classes = implode(' ', $footer_bottom_classes_array);

		$return_array = array(
			'grid_class' => $grid_class,
			'classes' => $classes
		);

		return $return_array;
	}
}