<?php

if (!function_exists('onschedule_edge_register_footer_sidebar')) {

    function onschedule_edge_register_footer_sidebar() {

        register_sidebar(array(
            'name' => esc_html__('Footer Top Column 1', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the first column of top footer area', 'onschedule'),
            'id' => 'footer_top_column_1',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-column-1 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top Column 2', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the second column of top footer area', 'onschedule'),
            'id' => 'footer_top_column_2',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-column-2 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top Column 3', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the third column of top footer area', 'onschedule'),
            'id' => 'footer_top_column_3',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-column-3 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top Column 4', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the fourth column of top footer area', 'onschedule'),
            'id' => 'footer_top_column_4',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-column-4 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom Left Column', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the left column of bottom footer area', 'onschedule'),
            'id' => 'footer_bottom_column_1',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-bottom-column-1 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom Middle Column', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the middle column of bottom footer area', 'onschedule'),
            'id' => 'footer_bottom_column_2',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-bottom-column-2 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom Right Column', 'onschedule'),
            'description'   => esc_html__('Widgets added here will appear in the right column of bottom footer area', 'onschedule'),
            'id' => 'footer_bottom_column_3',
            'before_widget' => '<div id="%1$s" class="widget edgtf-footer-bottom-column-3 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="edgtf-widget-title">',
            'after_title' => '</h5>'
        ));
    }

    add_action('widgets_init', 'onschedule_edge_register_footer_sidebar');
}

if (!function_exists('onschedule_edge_get_footer')) {
    /**
     * Loads footer HTML
     */
    function onschedule_edge_get_footer() {
        $parameters = array();
	    $page_id = onschedule_edge_get_page_id();
	
	    if(get_post_meta($page_id, 'edgtf_disable_footer_meta', true) == 'yes'){
		    $parameters['display_footer'] = false;
	    } else {
		    $parameters['display_footer'] = true;
	    }

        $parameters['display_footer_top'] = onschedule_edge_show_footer_top();
        $parameters['display_footer_bottom'] = onschedule_edge_show_footer_bottom();
        onschedule_edge_get_module_template_part('templates/footer', 'footer', '', $parameters);
    }
}

if(!function_exists('onschedule_edge_show_footer_top')){
    /**
     * Check footer top showing
     * Function check value from options and checks if footer columns are empty.
     * return bool
     */
    function onschedule_edge_show_footer_top(){
        $footer_top_flag = false;
	    
        //check value from options and meta field on current page
        $option_flag = (onschedule_edge_get_meta_field_intersect('show_footer_top') === 'yes') ? true : false;

        //check footer columns.If they are empty, disable footer top
        $columns_flag = false;
	    for($i = 1; $i <= 4; $i++){
	    	$footer_columns_id = 'footer_top_column_'.$i;
	    	if(is_active_sidebar($footer_columns_id)) {
			    $columns_flag = true;
			    break;
		    }
	    }
       
        if($option_flag && $columns_flag){
            $footer_top_flag = true;
        }

        return $footer_top_flag;
    }
}

if(!function_exists('onschedule_edge_show_footer_bottom')){
    /**
     * Check footer bottom showing
     * Function check value from options and checks if footer columns are empty.
     * return bool
     */
    function onschedule_edge_show_footer_bottom(){
        $footer_bottom_flag = false;
	    
        //check value from options and meta field on current page
        $option_flag = (onschedule_edge_get_meta_field_intersect('show_footer_bottom') === 'yes') ? true : false;

        //check footer columns.If they are empty, disable footer bottom
	    $columns_flag = false;
	    for($i = 1; $i <= 3; $i++){
		    $footer_columns_id = 'footer_bottom_column_'.$i;
		    if(is_active_sidebar($footer_columns_id)) {
			    $columns_flag = true;
			    break;
		    }
	    }
	    
        if($option_flag && $columns_flag){
	        $footer_bottom_flag = true;
        }

        return $footer_bottom_flag;
    }
}

if (!function_exists('onschedule_edge_get_footer_top')) {
    /**
     * Return footer top HTML
     */
    function onschedule_edge_get_footer_top() {
        $parameters = array();

        $parameters['footer_top_columns'] = onschedule_edge_options()->getOptionValue('footer_top_columns');
        //get footer top classes
        $footer_classes = onschedule_edge_footer_top_classes();
        //get footer top grid/full width class
        $parameters['footer_top_grid_class'] = $footer_classes['grid_class'];
        //get footer top other classes
        $parameters['footer_top_classes'] = $footer_classes['classes'];

        onschedule_edge_get_module_template_part('templates/parts/footer-top', 'footer', '', $parameters);
    }
}

if (!function_exists('onschedule_edge_get_footer_bottom')) {
    /**
     * Return footer bottom HTML
     */
    function onschedule_edge_get_footer_bottom() {
        $parameters = array();

        $parameters['footer_bottom_columns'] = onschedule_edge_options()->getOptionValue('footer_bottom_columns');

        //get footer top classes
        $footer_classes = onschedule_edge_footer_bottom_classes();
        //get footer top grid/full width class
        $parameters['footer_bottom_grid_class'] = $footer_classes['grid_class'];
        //get footer top other classes
        $parameters['footer_bottom_classes'] = $footer_classes['classes'];

        onschedule_edge_get_module_template_part('templates/parts/footer-bottom', 'footer', '', $parameters);
    }
}