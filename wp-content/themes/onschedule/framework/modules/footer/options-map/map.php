<?php

if ( ! function_exists('onschedule_edge_footer_options_map') ) {
	/**
	 * Add footer options
	 */
	function onschedule_edge_footer_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_footer_page',
				'title' => esc_html__('Footer', 'onschedule'),
				'icon' => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = onschedule_edge_add_admin_panel(
			array(
				'title' => esc_html__('Footer', 'onschedule'),
				'name' => 'footer',
				'page' => '_footer_page'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'footer_in_grid',
				'default_value' => 'yes',
				'label' => esc_html__('Footer in Grid', 'onschedule'),
				'description' => esc_html__('Enabling this option will place Footer content in grid', 'onschedule'),
				'parent' => $footer_panel,
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_top',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Top', 'onschedule'),
				'description' => esc_html__('Enabling this option will show Footer Top area', 'onschedule'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_show_footer_top_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_top_container = onschedule_edge_add_admin_container(
			array(
				'name' => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns',
				'default_value' => '4',
				'label' => esc_html__('Footer Top Columns', 'onschedule'),
				'description' => esc_html__('Choose number of columns for Footer Top area', 'onschedule'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4'
				),
				'parent' => $show_footer_top_container,
				'args' => array(
					'dependence' => true,
					'show' => array(
						'1' => '',
						'2' => '',
						'3' => '#edgtf_footer_top_three_columns_container',
						'4' => '',
					),
					'hide' => array(
						'1' => '#edgtf_footer_top_three_columns_container',
						'2' => '#edgtf_footer_top_three_columns_container',
						'3' => '',
						'4' => '#edgtf_footer_top_three_columns_container',
					)
				)
			)
		);

		$footer_top_three_columns_container = onschedule_edge_add_admin_container(array(
			'name' => 'footer_top_three_columns_container',
			'parent' => $show_footer_top_container,
			'hidden_property' => 'footer_top_columns',
			'hidden_values' => array('1','2','4')
		));

		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_three_columns_layout',
				'default_value' => '33-33-33',
				'label' => esc_html__('Footer Top Three Columns Layout', 'onschedule'),
				'description' => esc_html__('Choose Three Columns Layout', 'onschedule'),
				'options' => array(
					'33-33-33' => esc_html__('33%-33%-33%', 'onschedule'),
					'25-25-50' => esc_html__('25%-25%-50%', 'onschedule'),
					'50-25-25' => esc_html__('50%-25%-25%', 'onschedule')
				),
				'parent' => $footer_top_three_columns_container
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns_alignment',
				'default_value' => 'left',
				'label' => esc_html__('Footer Top Columns Alignment', 'onschedule'),
				'description' => esc_html__('Text Alignment in Footer Columns', 'onschedule'),
				'options' => array(
					'left' => esc_html__('Left', 'onschedule'),
					'center' => esc_html__('Center', 'onschedule'),
					'right' => esc_html__('Right', 'onschedule')
				),
				'parent' => $show_footer_top_container,
			)
		);

		onschedule_edge_add_admin_field(array(
			'name' => 'footer_top_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'onschedule'),
			'description' => esc_html__('Set background color for top footer area', 'onschedule'),
			'parent' => $show_footer_top_container
		));

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_bottom',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Bottom', 'onschedule'),
				'description' => esc_html__('Enabling this option will show Footer Bottom area', 'onschedule'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_show_footer_bottom_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_bottom_container = onschedule_edge_add_admin_container(
			array(
				'name' => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_bottom_columns',
				'default_value' => '4',
				'label' => esc_html__('Footer Bottom Columns', 'onschedule'),
				'description' => esc_html__('Choose number of columns for Footer Bottom area', 'onschedule'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent' => $show_footer_bottom_container,
			)
		);

		onschedule_edge_add_admin_field(array(
			'name' => 'footer_bottom_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'onschedule'),
			'description' => esc_html__('Set background color for bottom footer area', 'onschedule'),
			'parent' => $show_footer_bottom_container
		));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_footer_options_map', 11);
}