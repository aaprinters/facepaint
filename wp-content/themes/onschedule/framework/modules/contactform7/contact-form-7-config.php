<?php

if ( ! function_exists('onschedule_edge_contact_form_map') ) {
	/**
	 * Map Contact Form 7 shortcode
	 * Hooks on vc_after_init action
	 */
	function onschedule_edge_contact_form_map() {
		vc_add_param('contact-form-7', array(
			'type' => 'dropdown',
			'heading' => esc_html__('Style', 'onschedule'),
			'param_name' => 'html_class',
			'value' => array(
				esc_html__('Default', 'onschedule') => 'default',
				esc_html__('Custom Style 1', 'onschedule') => 'cf7_custom_style_1',
				esc_html__('Custom Style 2', 'onschedule') => 'cf7_custom_style_2',
				esc_html__('Custom Style 3', 'onschedule') => 'cf7_custom_style_3'
			),
			'description' => esc_html__('You can style each form element individually in Edge Options > Contact Form 7', 'onschedule')
		));
	}
	
	add_action('vc_after_init', 'onschedule_edge_contact_form_map');
}