<?php
include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/woocommerce-functions.php';

if (onschedule_edge_is_woocommerce_installed()) {
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/options-map/map.php';
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/woocommerce-template-hooks.php';
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/woocommerce-config.php';
	
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/shortcodes/product-info/product-info.php';
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/shortcodes/product-list/product-list.php';
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/shortcodes/product-list-carousel/product-list-carousel.php';
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/shortcodes/product-list-simple/product-list-simple.php';
	
	include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/woocommerce/widgets/woocommerce-dropdown-cart.php';
}