<?php

if ( ! function_exists('onschedule_edge_woocommerce_options_map') ) {

	/**
	 * Add Woocommerce options page
	 */
	function onschedule_edge_woocommerce_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_woocommerce_page',
				'title' => esc_html__('Woocommerce', 'onschedule'),
				'icon' => 'fa fa-shopping-cart'
			)
		);

		/**
		 * Product List Settings
		 */
		$panel_product_list = onschedule_edge_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_product_list',
				'title' => esc_html__('Product List', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        	=> 'edgtf_woo_product_list_columns',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Product List Columns', 'onschedule'),
			'default_value'	=> 'edgtf-woocommerce-columns-4',
			'description' 	=> esc_html__('Choose number of columns for main product list page', 'onschedule'),
			'options'		=> array(
				'edgtf-woocommerce-columns-3' => esc_html__('3 Columns', 'onschedule'),
				'edgtf-woocommerce-columns-4' => esc_html__('4 Columns', 'onschedule')
			),
			'parent'      	=> $panel_product_list,
		));
		
		onschedule_edge_add_admin_field(array(
			'name'        	=> 'edgtf_woo_product_list_columns_space',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Space Between Products', 'onschedule'),
			'default_value'	=> 'edgtf-woo-normal-space',
			'description' 	=> esc_html__('Select space between products for product listing and related products on single product', 'onschedule'),
			'options'		=> array(
				'edgtf-woo-normal-space' => esc_html__('Normal', 'onschedule'),
				'edgtf-woo-small-space'  => esc_html__('Small', 'onschedule'),
				'edgtf-woo-no-space'     => esc_html__('No Space', 'onschedule')
			),
			'parent'      	=> $panel_product_list,
		));
		
		onschedule_edge_add_admin_field(array(
			'name'        	=> 'edgtf_woo_product_list_info_position',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Product Info Position', 'onschedule'),
			'default_value'	=> 'info_below_image',
			'description' 	=> esc_html__('Select product info position for product listing and related products on single product', 'onschedule'),
			'options'		=> array(
				'info_below_image'    => esc_html__('Info Below Image', 'onschedule'),
				'info_on_image_hover' => esc_html__('Info On Image Hover', 'onschedule')
			),
			'parent'      	=> $panel_product_list,
		));

		onschedule_edge_add_admin_field(array(
			'name'        	=> 'edgtf_woo_products_per_page',
			'type'        	=> 'text',
			'label'       	=> esc_html__('Number of products per page', 'onschedule'),
			'default_value'	=> '',
			'description' 	=> esc_html__('Set number of products on shop page', 'onschedule'),
			'parent'      	=> $panel_product_list,
			'args' 			=> array(
				'col_width' => 3
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        	=> 'edgtf_products_list_title_tag',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Products Title Tag', 'onschedule'),
			'default_value'	=> 'h6',
			'description' 	=> '',
			'options'       => onschedule_edge_get_title_tag(),
			'parent'      	=> $panel_product_list,
		));

		/**
		 * Single Product Settings
		 */
		$panel_single_product = onschedule_edge_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_single_product',
				'title' => esc_html__('Single Product', 'onschedule')
			)
		);

			onschedule_edge_add_admin_field(array(
				'name' => 'woo_single_header_background_color',
				'type' => 'color',
				'label' => esc_html__('Header Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for header area', 'onschedule'),
				'parent' => $panel_single_product
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'woo_single_header_background_transparency',
				'type' => 'text',
				'label' => esc_html__('Header Background Transparency', 'onschedule'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
				'parent' => $panel_single_product,
				'args' 			=> array(
					'col_width' => 3
				)
			));

			onschedule_edge_add_admin_field(array(
					'name' => 'woo_single_header_border_color',
					'type' => 'color',
					'label' => esc_html__('Header Border Color', 'onschedule'),
					'description' => esc_html__('Set border bottom color for header area. This option doesn\'t work for Vertical header layout', 'onschedule'),
					'default_value' => '',
					'parent' => $panel_single_product
				)
			);

			onschedule_edge_add_admin_field(array(
				'name'          => 'woo_enable_single_thumb_featured_switch',
				'type'          => 'yesno',
				'label'         => esc_html__('Switch Featured Image on Thumbnail Click', 'onschedule'),
				'description'   => esc_html__('Enabling this option will switch featured image with thumbnail image on thumbnail click', 'onschedule'),
				'default_value' => 'yes',
				'parent'        => $panel_single_product
			));
			
			onschedule_edge_add_admin_field(array(
				'name'          => 'woo_set_thumb_images_position',
				'type'          => 'select',
				'label'         => esc_html__('Set Thumbnail Images Position', 'onschedule'),
				'default_value' => 'below-image',
				'options'		=> array(
					'below-image'  => esc_html__('Below Featured Image', 'onschedule'),
					'on-left-side' => esc_html__('On The Left Side Of Featured Image', 'onschedule')
				),
				'parent'        => $panel_single_product
			));

			onschedule_edge_add_admin_field(array(
				'name'        	=> 'edgtf_single_product_title_tag',
				'type'        	=> 'select',
				'label'       	=> esc_html__('Single Product Title Tag', 'onschedule'),
				'default_value'	=> 'h2',
				'description' 	=> '',
				'options'       => onschedule_edge_get_title_tag(),
				'parent'      	=> $panel_single_product,
			));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_woocommerce_options_map', 21);
}