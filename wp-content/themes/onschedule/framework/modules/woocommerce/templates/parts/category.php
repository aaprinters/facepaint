<?php

if($display_category === 'yes') {
	$product      = wc_get_product( get_the_ID() );
	$product_categories   = ! empty( $product ) ? wc_get_product_category_list( $product->get_id(), ', ' ) : '';
	
	if (!empty($product_categories)) { ?>
		<p class="edgtf-<?php echo esc_attr($class_name); ?>-category"><?php echo onschedule_edge_get_module_part($product_categories); ?></p>
	<?php } ?>
<?php } ?>