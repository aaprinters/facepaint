<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\ProductListCarousel;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class ProductListCarousel
 */
class ProductListCarousel implements ShortcodeInterface {
	/**
	* @var string
	*/
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_product_list_carousel';
		
		add_action('vc_before_init', array($this,'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Product List - Carousel', 'onschedule'),
			'base' => $this->base,
			'icon' => 'icon-wpb-product-list-carousel extended-custom-icon',
			'category' => esc_html__('by EDGE', 'onschedule'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
					array(
						'type'       => 'dropdown',
						'param_name' => 'type',
						'heading'    => esc_html__('Type', 'onschedule'),
						'value'      => array(
							esc_html__('Standard', 'onschedule') => 'standard',
							esc_html__('Simple', 'onschedule') => 'simple'
						)
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'number_of_posts',
						'heading'    => esc_html__('Number of Products', 'onschedule')
					),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__('Number of Visible Items', 'onschedule'),
                        'param_name' => 'number_of_visible_items',
                        'value'      => array(
							esc_html__('One', 'onschedule') => '1',
							esc_html__('Two', 'onschedule') => '2',
							esc_html__('Three', 'onschedule') => '3',
							esc_html__('Four', 'onschedule') => '4',
							esc_html__('Five', 'onschedule') => '5'
                        ),
                        'save_always' => true,
						'dependency'  => array('element' => 'type', 'value' => array('standard'))
                    ),
                    array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Enable Carousel Autoplay', 'onschedule'),
						'param_name'  => 'carousel_autoplay',
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true))
					),
                    array(
						'type'        => 'textfield',
						'param_name'  => 'carousel_autoplay_timeout',
						'heading'     => esc_html__('Slide Duration (ms)', 'onschedule'),
						'description' => esc_html__('Autoplay interval timeout. Default value is 5000', 'onschedule'),
						'dependency'  => array('element' => 'carousel_autoplay', 'value' => array('yes'))
					),
                    array(
						'type'        => 'dropdown',
						'param_name'  => 'carousel_loop',
						'heading'     => esc_html__('Enable Carousel Loop', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true))
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'carousel_speed',
						'heading'     => esc_html__('Animation Speed (ms)', 'onschedule'),
						'description' => esc_html__('Carousel Speed interval. Default value is 650', 'onschedule'),
					),
                    array(
						'type'       => 'dropdown',
						'param_name' => 'space_between_items',
						'heading'    => esc_html__('Space Between Items', 'onschedule'),
						'value'      => array(
							esc_html__('Normal', 'onschedule') => 'normal',
							esc_html__('Small', 'onschedule') => 'small',
							esc_html__('Tiny', 'onschedule') => 'tiny',
							esc_html__('No Space', 'onschedule') => 'no'
						),
						'dependency'  => array('element' => 'type', 'value' => array('standard'))
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'order_by',
						'heading'    => esc_html__('Order By', 'onschedule'),
						'value'      => array(
							esc_html__('Title', 'onschedule') => 'title',
							esc_html__('Date', 'onschedule') => 'date',
							esc_html__('Random', 'onschedule') => 'rand',
							esc_html__('Post Name', 'onschedule') => 'name',
							esc_html__('ID', 'onschedule') => 'id',
							esc_html__('Menu Order', 'onschedule') => 'menu_order'
						)
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'order',
						'heading'    => esc_html__('Order', 'onschedule'),
						'value' => array(
							esc_html__('ASC', 'onschedule') => 'ASC',
							esc_html__('DESC', 'onschedule') => 'DESC'
						)
					),
					array(
	                    'type'       => 'dropdown',
	                    'param_name' => 'taxonomy_to_display',
	                    'heading'    => esc_html__('Choose Sorting Taxonomy', 'onschedule'),
	                    'value'      => array(
							esc_html__('Category', 'onschedule') => 'category',
							esc_html__('Tag', 'onschedule') => 'tag',
							esc_html__('Id', 'onschedule') => 'id'
	                    ),
	                    'description' => esc_html__('If you would like to display only certain products, this is where you can select the criteria by which you would like to choose which products to display', 'onschedule')
	                ),
	                array(
	                    'type'        => 'textfield',
	                    'param_name'  => 'taxonomy_values',
	                    'heading'     => esc_html__('Enter Taxonomy Values', 'onschedule'),
	                    'description' => esc_html__('Separate values (category slugs, tags, or post IDs) with a comma', 'onschedule')
	                ),
	                array(
						'type'       => 'dropdown',
						'heading'    => esc_html__('Image Proportions', 'onschedule'),
						'param_name' => 'image_size',
						'value'      => array(
							esc_html__('Default', 'onschedule') => '',
							esc_html__('Original', 'onschedule') => 'full',
							esc_html__('Square', 'onschedule') => 'square',
							esc_html__('Landscape', 'onschedule') => 'landscape',
							esc_html__('Portrait', 'onschedule') => 'portrait',
							esc_html__('Medium', 'onschedule') => 'medium',
							esc_html__('Large', 'onschedule') => 'large'
						)
					),
	                array(
						'type'        => 'dropdown',
						'param_name'  => 'carousel_navigation',
						'heading'     => esc_html__('Enable Carousel Navigation', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true))
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'carousel_navigation_skin',
						'heading'    => esc_html__('Carousel Navigation Skin', 'onschedule'),
						'value'      => array(
							esc_html__('Default', 'onschedule')  => 'default',
							esc_html__('Light', 'onschedule') => 'light'
						),
						'dependency'  => array('element' => 'carousel_navigation', 'value' => array('yes'))
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'carousel_pagination',
						'heading'     => esc_html__('Enable Carousel Pagination', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false))
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'carousel_pagination_skin',
						'heading'    => esc_html__('Carousel Pagination Skin', 'onschedule'),
						'value'      => array(
							esc_html__('Default', 'onschedule')  => 'default',
							esc_html__('Light', 'onschedule') => 'light'
						),
						'dependency'  => array('element' => 'carousel_pagination', 'value' => array('yes'))
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'carousel_pagination_pos',
						'heading'    => esc_html__('Carousel Pagination Position', 'onschedule'),
						'value'      => array(
							esc_html__('Below Carousel', 'onschedule')  => 'bellow-slider',
							esc_html__('Inside Carousel', 'onschedule') => 'inside-slider'
						),
						'dependency'  => array('element' => 'carousel_pagination', 'value' => array('yes'))
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_title',
						'heading'     => esc_html__('Display Title', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_tag',
						'heading'     => esc_html__('Title Tag', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_title_tag(true)),
						'dependency'  => array('element' => 'display_title', 'value' => array('yes')),
						'group'	      => esc_html__('Product Info Style', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_transform',
						'heading'     => esc_html__('Title Text Transform', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_text_transform_array(true)),
						'dependency'  => array('element' => 'display_title', 'value' => array('yes')),
						'group'	      => esc_html__('Product Info Style', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_category',
						'heading'     => esc_html__('Display Category', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_excerpt',
						'heading'     => esc_html__('Display Excerpt', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'excerpt_length',
						'heading'     => esc_html__('Excerpt Length', 'onschedule'),
						'description' => esc_html__('Number of characters', 'onschedule'),
						'dependency'  => array('element' => 'display_excerpt', 'value' => array('yes')),
						'group'	      => esc_html__('Product Info Style', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_rating',
						'heading'     => esc_html__('Display Rating', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_price',
						'heading'     => esc_html__('Display Price', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'display_button',
						'heading'     => esc_html__('Display Button', 'onschedule'),
						'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
						'group'	      => esc_html__('Product Info', 'onschedule')
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'button_skin',
						'heading'    => esc_html__('Button Skin', 'onschedule'),
						'value'      => array(
							esc_html__('Default', 'onschedule')  => 'default',
							esc_html__('Light', 'onschedule') => 'light',
							esc_html__('Dark', 'onschedule') => 'dark'
						),
						'dependency'  => array('element' => 'display_button', 'value' => array('yes')),
						'group'	      => esc_html__('Product Info Style', 'onschedule')
					),
					array(
						'type'       => 'colorpicker',
						'param_name' => 'shader_background_color',
						'heading'    => esc_html__('Shader Background Color', 'onschedule'),
						'group'	     => esc_html__('Product Info Style', 'onschedule')
					)
				)
		) );

	}
	public function render($atts, $content = null) {
		$default_atts = array(
			'type'						=> 'standard',
            'number_of_posts' 		 	=> '8',
            'number_of_visible_items' 	=> '4',
            'carousel_autoplay'	 	  	=> 'yes',
            'carousel_autoplay_timeout' => '5000',
            'carousel_loop'	 		 	=> 'yes',
            'carousel_speed' 		 	=> '650',
            'space_between_items'	  	=> 'normal',
            'carousel_navigation'	 	=> 'yes',
			'carousel_navigation_skin'  => 'default',
            'carousel_pagination'	 	=> 'no',
			'carousel_pagination_skin'  => 'default',
			'carousel_pagination_pos'   => 'bellow-slider',
            'order_by' 				  	=> 'date',
            'order' 				  	=> 'ASC',
            'taxonomy_to_display' 	  	=> 'category',
            'taxonomy_values' 		 	=> '',
            'image_size'			  	=> 'full',
            'display_title' 		  	=> 'yes',
            'title_tag'				  	=> 'h4',
            'title_transform'		  	=> '',
			'display_category'          => 'no',
			'display_excerpt'			=> 'no',
			'excerpt_length' 		    => '20',
			'display_rating' 		  	=> 'yes',
			'display_price' 		  	=> 'yes',
            'display_button' 		  	=> 'yes',
			'button_skin'               => 'default',
			'shader_background_color' 	=> ''
        );
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['holder_data'] = $this->getProductListCarouselDataAttributes($params);
		$params['class_name'] = 'plc';
		
		$params['type'] = !empty($params['type']) ? $params['type'] : $default_atts['type'];
		
		$params['title_tag'] = !empty($params['title_tag']) ? $params['title_tag'] : $default_atts['title_tag'];
		$params['title_styles'] = $this->getTitleStyles($params);

		$params['shader_styles'] = $this->getShaderStyles($params);

		$queryArray = $this->generateProductQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$params['query_result'] = $query_result;

		$html = onschedule_edge_get_woo_shortcode_module_template_part('templates/product-list-'.$params['type'], 'product-list-carousel', '', $params);
		
		return $html;
	}

	/**
	   * Generates holder classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getHolderClasses($params){
		$holderClasses = '';

		$carouselType = !empty($params['type']) ? 'edgtf-'.$params['type'].'-layout' : 'edgtf-standard-layout';
		
		$columnsSpace = !empty($params['space_between_items']) ? 'edgtf-'.$params['space_between_items'].'-space' : 'edgtf-normal-space';
		
		$carouselClasses = $this->getCarouselClasses($params);
		
		$holderClasses .= ' '. $carouselType . ' '. $columnsSpace . ' ' . $carouselClasses;
		
		return $holderClasses;
	}
	
	/**
	 * Generates carousel classes for product list holder
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCarouselClasses($params){
		$carouselClasses = '';
		
		if(!empty($params['carousel_navigation_skin'])) {
			$carouselClasses .= ' edgtf-plc-nav-'.$params['carousel_navigation_skin'].'-skin';
		}
		
		if(!empty($params['carousel_pagination_pos'])) {
			$carouselClasses .= ' edgtf-plc-pag-'.$params['carousel_pagination_pos'];
		}
		
		if($params['carousel_pagination'] === 'yes' && $params['carousel_pagination_pos'] === 'bellow-slider') {
			$carouselClasses .= ' edgtf-plc-pag-enabled';
		}
		
		if(!empty($params['carousel_pagination_skin'])) {
			$carouselClasses .= ' edgtf-plc-pag-'.$params['carousel_pagination_skin'].'-skin';
		}
		
		return $carouselClasses;
	}
	
    /**
     * Return all data that product list carousel needs
     *
     * @param $params
     * @return array
     */
    private function getProductListCarouselDataAttributes($params) {
        $data = array();

        if(!empty($params['number_of_visible_items']) && $params['type'] !== 'simple'){
            $data['data-number-of-visible-items'] = $params['number_of_visible_items'];
        } else if($params['type'] === 'simple'){
            $data['data-number-of-visible-items'] = 1;
        }
        if(!empty($params['carousel_autoplay'])) {
            $data['data-autoplay'] = $params['carousel_autoplay'];
        }
        if(!empty($params['carousel_autoplay_timeout'])) {
            $data['data-autoplay-timeout'] = $params['carousel_autoplay_timeout'];
        }
        if(!empty($params['carousel_loop'])) {
            $data['data-loop'] = $params['carousel_loop'];
        }
        if(!empty($params['carousel_speed'])) {
            $data['data-speed'] = $params['carousel_speed'];
        }
        if(!empty($params['carousel_navigation'])) {
            $data['data-navigation'] = $params['carousel_navigation'];
        }
        if(!empty($params['carousel_pagination'])) {
            $data['data-pagination'] = $params['carousel_pagination'];
        }

        return $data;
    }

	/**
	   * Generates query array
	   *
	   * @param $params
	   *
	   * @return array
	*/
	public function generateProductQueryArray($params){
		$queryArray = array(
			'post_status' => 'publish',
			'post_type' => 'product',
			'ignore_sticky_posts' => 1,
			'posts_per_page' => $params['number_of_posts'],
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'meta_query' => WC()->query->get_meta_query()
		);

        if ($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'category') {
            $queryArray['product_cat'] = $params['taxonomy_values'];
        }

        if ($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'tag') {
            $queryArray['product_tag'] = $params['taxonomy_values'];
        }

        if ($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'id') {
            $idArray = $params['taxonomy_values'];
            $ids = explode(',', $idArray);
            $queryArray['post__in'] = $ids;
        }

        return $queryArray;
	}
	
	/**
	 * Return Style for Title
	 *
	 * @param $params
	 * @return string
	 */
	public function getTitleStyles($params) {
		$styles = array();
		
		if (!empty($params['title_transform'])) {
			$styles[] = 'text-transform: '.$params['title_transform'];
		}
		
		return implode(';', $styles);
	}
	
	/**
	 * Return Style for Shader
	 *
	 * @param $params
	 * @return string
	 */
	public function getShaderStyles($params) {
		$styles = array();
		
		if (!empty($params['shader_background_color'])) {
			$styles[] = 'background-color: '.$params['shader_background_color'];
		}
		
		return implode(';', $styles);
	}
}