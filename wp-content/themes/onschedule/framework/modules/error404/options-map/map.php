<?php

if ( ! function_exists('onschedule_edge_error_404_options_map') ) {

	function onschedule_edge_error_404_options_map() {

		onschedule_edge_add_admin_page(array(
			'slug' => '__404_error_page',
			'title' => esc_html__('404 Error Page', 'onschedule'),
			'icon' => 'fa fa-exclamation-triangle'
		));

		$panel_404_header = onschedule_edge_add_admin_panel(array(
			'page' => '__404_error_page',
			'name'	=> 'panel_404_header',
			'title'	=> esc_html__('Header', 'onschedule')
		));

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_header,
				'type' => 'color',
				'name' => '404_menu_area_background_color_header',
				'label' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for header area', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_header,
				'type' => 'text',
				'name' => '404_menu_area_background_transparency_header',
				'default_value' => '',
				'label' => esc_html__('Background Transparency', 'onschedule'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_header,
				'type' => 'color',
				'name' => '404_menu_area_border_color_header',
				'default_value' => '',
				'label' => esc_html__('Border Color', 'onschedule'),
				'description' => esc_html__('Choose a border bottom color for header area', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_header,
				'type' => 'select',
				'name' => '404_header_style',
				'default_value' => '',
				'label' => esc_html__('Header Skin', 'onschedule'),
				'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'onschedule'),
				'options' => array(
					'' => esc_html__('Default', 'onschedule'),
					'light-header' => esc_html__('Light', 'onschedule'),
					'dark-header' => esc_html__('Dark', 'onschedule')
				)
			)
		);

		$panel_404_options = onschedule_edge_add_admin_panel(array(
			'page' => '__404_error_page',
			'name'	=> 'panel_404_options',
			'title'	=> esc_html__('404 Page Options', 'onschedule')
		));

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_options,
				'type' => 'color',
				'name' => '404_page_background_color',
				'label' => esc_html__('Background Color', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_options,
				'type' => 'image',
				'name' => '404_page_background_image',
				'label' => esc_html__('Background Image', 'onschedule'),
				'description' => esc_html__('Choose a background image for 404 page', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_options,
				'type' => 'image',
				'name' => '404_page_background_pattern_image',
				'label' => esc_html__('Pattern Background Image', 'onschedule'),
				'description' => esc_html__('Choose a pattern image for 404 page', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_404_options,
				'type' => 'image',
				'name' => '404_page_title_image',
				'label' => esc_html__('Title Image', 'onschedule'),
				'description' => esc_html__('Choose a background image for displaying above 404 page Title', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_title',
			'default_value' => '',
			'label' => esc_html__('Title', 'onschedule'),
			'description' => esc_html__('Enter title for 404 page. Default label is "404".', 'onschedule')
		));

		$first_level_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_404_options,
				'name' => 'first_level_group',
				'title' => esc_html__('Title Style', 'onschedule'),
				'description' => esc_html__('Define styles for 404 page title', 'onschedule')
			)
		);

		$first_level_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row1'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => '404_title_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'fontsimple',
				'name' => '404_title_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'textsimple',
				'name' => '404_title_font_size',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'textsimple',
				'name' => '404_title_line_height',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$first_level_row2 = onschedule_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row2',
				'next' => true
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_title_font_style',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'onschedule'),
				'options' => onschedule_edge_get_font_style_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_title_font_weight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'onschedule'),
				'options' => onschedule_edge_get_font_weight_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'textsimple',
				'name' => '404_title_letter_spacing',
				'default_value' => '',
				'label' => esc_html__('Letter Spacing', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_title_text_transform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'onschedule'),
				'options' => onschedule_edge_get_text_transform_array()
			)
		);

		onschedule_edge_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_subtitle',
			'default_value' => '',
			'label' => esc_html__('Subtitle', 'onschedule'),
			'description' => esc_html__('Enter subtitle for 404 page. Default label is "PAGE NOT FOUND".', 'onschedule')
		));

		$second_level_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_404_options,
				'name' => 'second_level_group',
				'title' => esc_html__('Subtitle Style', 'onschedule'),
				'description' => esc_html__('Define styles for 404 page subtitle', 'onschedule')
			)
		);

		$second_level_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row1'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'colorsimple',
				'name' => '404_subtitle_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'fontsimple',
				'name' => '404_subtitle_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'textsimple',
				'name' => '404_subtitle_font_size',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'textsimple',
				'name' => '404_subtitle_line_height',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_row2 = onschedule_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row2',
				'next' => true
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_subtitle_font_style',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'onschedule'),
				'options' => onschedule_edge_get_font_style_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_subtitle_font_weight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'onschedule'),
				'options' => onschedule_edge_get_font_weight_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => '404_subtitle_letter_spacing',
				'default_value' => '',
				'label' => esc_html__('Letter Spacing', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_subtitle_text_transform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'onschedule'),
				'options' => onschedule_edge_get_text_transform_array()
			)
		);

		onschedule_edge_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_text',
			'default_value' => '',
			'label' => esc_html__('Text', 'onschedule'),
			'description' => esc_html__('Enter text for 404 page.', 'onschedule')
		));

		$third_level_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_404_options,
				'name' => '$third_level_group',
				'title' => esc_html__('Text Style', 'onschedule'),
				'description' => esc_html__('Define styles for 404 page text', 'onschedule')
			)
		);

		$third_level_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => '$third_level_row1'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'colorsimple',
				'name' => '404_text_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'fontsimple',
				'name' => '404_text_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'onschedule'),
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'textsimple',
				'name' => '404_text_font_size',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'textsimple',
				'name' => '404_text_line_height',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$third_level_row2 = onschedule_edge_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => '$third_level_row2',
				'next' => true
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_text_font_style',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'onschedule'),
				'options' => onschedule_edge_get_font_style_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_text_font_weight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'onschedule'),
				'options' => onschedule_edge_get_font_weight_array()
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'textsimple',
				'name' => '404_text_letter_spacing',
				'default_value' => '',
				'label' => esc_html__('Letter Spacing', 'onschedule'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'selectblanksimple',
				'name' => '404_text_text_transform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'onschedule'),
				'options' => onschedule_edge_get_text_transform_array()
			)
		);

		onschedule_edge_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_back_to_home',
			'label' => esc_html__('Back to Home Button Label', 'onschedule'),
			'description' => esc_html__('Enter label for "BACK TO HOME" button', 'onschedule')
		));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_error_404_options_map', 19);
}