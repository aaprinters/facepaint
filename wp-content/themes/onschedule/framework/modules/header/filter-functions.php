<?php

if(!function_exists('onschedule_edge_header_class')) {
    /**
     * Function that adds class to header based on theme options
     * @param array array of classes from main filter
     * @return array array of classes with added header class
     */
    function onschedule_edge_header_class($classes) {
        $header_type = onschedule_edge_get_meta_field_intersect('header_type', onschedule_edge_get_page_id());

        $classes[] = 'edgtf-'.$header_type;

        return $classes;
    }

    add_filter('body_class', 'onschedule_edge_header_class');
}

if(!function_exists('onschedule_edge_header_behaviour_class')) {
    /**
     * Function that adds behaviour class to header based on theme options
     * @param array array of classes from main filter
     * @return array array of classes with added behaviour class
     */
    function onschedule_edge_header_behaviour_class($classes) {

        $classes[] = 'edgtf-'.onschedule_edge_options()->getOptionValue('header_behaviour');

        return $classes;
    }

    add_filter('body_class', 'onschedule_edge_header_behaviour_class');
}

if(!function_exists('onschedule_edge_mobile_header_class')) {
    function onschedule_edge_mobile_header_class($classes) {
        $classes[] = 'edgtf-default-mobile-header';

        $classes[] = 'edgtf-sticky-up-mobile-header';

        return $classes;
    }

    add_filter('body_class', 'onschedule_edge_mobile_header_class');
}

if(!function_exists('onschedule_edge_menu_dropdown_appearance')) {
    /**
     * Function that adds menu dropdown appearance class to body tag
     * @param array array of classes from main filter
     * @return array array of classes with added menu dropdown appearance class
     */
    function onschedule_edge_menu_dropdown_appearance($classes) {

        if(onschedule_edge_options()->getOptionValue('menu_dropdown_appearance') !== 'default'){
            $classes[] = 'edgtf-'.onschedule_edge_options()->getOptionValue('menu_dropdown_appearance');
        }

        return $classes;
    }

    add_filter('body_class', 'onschedule_edge_menu_dropdown_appearance');
}

if (!function_exists('onschedule_edge_header_skin_class')) {

    function onschedule_edge_header_skin_class( $classes ) {

        $id = onschedule_edge_get_page_id();

		if(($meta_temp = get_post_meta($id, 'edgtf_header_style_meta', true)) !== ''){
			$classes[] = 'edgtf-' . $meta_temp;
		}
        else if ( is_404() && onschedule_edge_options()->getOptionValue('404_header_style') !== '' ) {
            $classes[] = 'edgtf-' . onschedule_edge_options()->getOptionValue('404_header_style');
        }
        else if ( onschedule_edge_options()->getOptionValue('header_style') !== '' ) {
            $classes[] = 'edgtf-' . onschedule_edge_options()->getOptionValue('header_style');
        }

        return $classes;
    }

    add_filter('body_class', 'onschedule_edge_header_skin_class');
}

if(!function_exists('onschedule_edge_header_global_js_var')) {
    function onschedule_edge_header_global_js_var($global_variables) {

        $global_variables['edgtfTopBarHeight'] = onschedule_edge_get_top_bar_height();
        $global_variables['edgtfStickyHeaderHeight'] = onschedule_edge_get_sticky_header_height();
        $global_variables['edgtfStickyHeaderTransparencyHeight'] = onschedule_edge_get_sticky_header_height_of_complete_transparency();
        $global_variables['edgtfStickyScrollAmount'] = onschedule_edge_get_sticky_scroll_amount();

        return $global_variables;
    }

    add_filter('onschedule_edge_filter_js_global_variables', 'onschedule_edge_header_global_js_var');
}

if(!function_exists('onschedule_edge_header_per_page_js_var')) {
    function onschedule_edge_header_per_page_js_var($perPageVars) {

        $perPageVars['edgtfStickyScrollAmount'] = onschedule_edge_get_sticky_scroll_amount_per_page();

        return $perPageVars;
    }

    add_filter('onschedule_edge_filter_per_page_js_vars', 'onschedule_edge_header_per_page_js_var');
}