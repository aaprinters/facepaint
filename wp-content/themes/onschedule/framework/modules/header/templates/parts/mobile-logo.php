<?php
	$attachment_meta = onschedule_edge_get_attachment_meta_from_url($logo_image);
	$hwstring = !empty($attachment_meta) ? image_hwstring( $attachment_meta['width'], $attachment_meta['height'] ) : '';
?>

<?php do_action('onschedule_edge_action_before_mobile_logo'); ?>

<div class="edgtf-mobile-logo-wrapper">
    <a itemprop="url" href="<?php echo esc_url(home_url('/')); ?>" <?php onschedule_edge_inline_style($logo_styles); ?>>
        <img itemprop="image" src="<?php echo esc_url($logo_image); ?>" <?php echo onschedule_edge_get_module_part($hwstring); ?> alt="<?php esc_attr_e('mobile logo','onschedule'); ?>"/>
    </a>
</div>

<?php do_action('onschedule_edge_action_after_mobile_logo'); ?>