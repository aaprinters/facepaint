<?php do_action('onschedule_edge_action_before_page_header'); ?>

<header class="edgtf-page-header">
    <div class="edgtf-menu-area">
		<?php do_action( 'onschedule_edge_action_after_header_menu_area_html_open' )?>
        <?php if($header_in_grid) : ?>
            <div class="edgtf-grid">
        <?php endif; ?>
        <div class="edgtf-vertical-align-containers">
            <div class="edgtf-position-left">
                <div class="edgtf-position-left-inner">
                    <?php if(!$hide_logo) {
                        onschedule_edge_get_logo();
                    } ?>
                </div>
            </div>
            <div class="edgtf-position-right">
                <div class="edgtf-position-right-inner">
                    <?php onschedule_edge_get_header_widget_area(); ?>
                </div>
            </div>
        </div>
        <?php if($header_in_grid) : ?>
            </div>
        <?php endif; ?>
    </div>
    <?php do_action('onschedule_edge_action_end_of_page_header_html'); ?>
</header>

<?php do_action('onschedule_edge_action_after_page_header'); ?>