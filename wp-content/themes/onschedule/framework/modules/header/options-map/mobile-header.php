<?php

if ( ! function_exists('onschedule_edge_mobile_header_options_map') ) {

	function onschedule_edge_mobile_header_options_map() {

		onschedule_edge_add_admin_page(array(
			'slug'  => '_mobile_header',
			'title' => esc_html__('Mobile Header', 'onschedule'),
			'icon'  => 'fa fa-mobile'
		));

		$panel_mobile_header = onschedule_edge_add_admin_panel(array(
			'title' => esc_html__('Mobile Header', 'onschedule'),
			'name'  => 'panel_mobile_header',
			'page'  => '_mobile_header'
		));
		
		$mobile_header_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_mobile_header,
				'name' => 'mobile_header_group',
				'title' => esc_html__('Mobile Header Styles', 'onschedule')
			)
		);
		
		$mobile_header_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $mobile_header_group,
				'name' => 'mobile_header_row1'
			)
		);

			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_header_height',
				'type'        => 'textsimple',
				'label'       => esc_html__('Height', 'onschedule'),
				'parent'      => $mobile_header_row1,
				'args'        => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			));
	
			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_header_background_color',
				'type'        => 'colorsimple',
				'label'       => esc_html__('Background Color', 'onschedule'),
				'parent'      => $mobile_header_row1
			));
	
			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_header_border_bottom_color',
				'type'        => 'colorsimple',
				'label'       => esc_html__('Border Bottom Color', 'onschedule'),
				'parent'      => $mobile_header_row1
			));
		
		$mobile_menu_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_mobile_header,
				'name' => 'mobile_menu_group',
				'title' => esc_html__('Mobile Menu Styles', 'onschedule')
			)
		);
		
		$mobile_menu_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $mobile_menu_group,
				'name' => 'mobile_menu_row1'
			)
		);

			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_menu_background_color',
				'type'        => 'colorsimple',
				'label'       => esc_html__('Background Color', 'onschedule'),
				'parent'      => $mobile_menu_row1
			));
	
			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_menu_border_bottom_color',
				'type'        => 'colorsimple',
				'label'       => esc_html__('Border Bottom Color', 'onschedule'),
				'parent'      => $mobile_menu_row1
			));
	
			onschedule_edge_add_admin_field(array(
				'name'        => 'mobile_menu_separator_color',
				'type'        => 'colorsimple',
				'label'       => esc_html__('Menu Item Separator Color', 'onschedule'),
				'parent'      => $mobile_menu_row1
			));
		
		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_logo_height',
			'type'        => 'text',
			'label'       => esc_html__('Logo Height For Mobile Header', 'onschedule'),
			'description' => esc_html__('Define logo height for screen size smaller than 1024px', 'onschedule'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));
		
		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_logo_height_phones',
			'type'        => 'text',
			'label'       => esc_html__('Logo Height For Mobile Devices', 'onschedule'),
			'description' => esc_html__('Define logo height for screen size smaller than 480px', 'onschedule'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		onschedule_edge_add_admin_section_title(array(
			'parent' => $panel_mobile_header,
			'name'   => 'mobile_header_fonts_title',
			'title'  => esc_html__('Typography', 'onschedule')
		));

		$first_level_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_mobile_header,
				'name' => 'first_level_group',
				'title' => esc_html__('1st Level Menu', 'onschedule'),
				'description' => esc_html__('Define styles for 1st level in Mobile Menu Navigation', 'onschedule')
			)
		);

		$first_level_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row1'
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_text_color',
			'type'        => 'colorsimple',
			'label'       => esc_html__('Text Color', 'onschedule'),
			'parent'      => $first_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_text_hover_color',
			'type'        => 'colorsimple',
			'label'       => esc_html__('Hover/Active Text Color', 'onschedule'),
			'parent'      => $first_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_google_fonts',
			'type'        => 'fontsimple',
			'label'       => esc_html__('Font Family', 'onschedule'),
			'parent'      => $first_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_font_size',
			'type'        => 'textsimple',
			'label'       => esc_html__('Font Size', 'onschedule'),
			'parent'      => $first_level_row1,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		$first_level_row2 = onschedule_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row2'
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_line_height',
			'type'        => 'textsimple',
			'label'       => esc_html__('Line Height', 'onschedule'),
			'parent'      => $first_level_row2,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_text_transform',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Text Transform', 'onschedule'),
			'parent'      => $first_level_row2,
			'options'     => onschedule_edge_get_text_transform_array()
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_font_style',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Font Style', 'onschedule'),
			'parent'      => $first_level_row2,
			'options'     => onschedule_edge_get_font_style_array()
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_font_weight',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Font Weight', 'onschedule'),
			'parent'      => $first_level_row2,
			'options'     => onschedule_edge_get_font_weight_array()
		));
		
		$first_level_row3 = onschedule_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row3'
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'mobile_letter_spacing',
				'label' => esc_html__('Letter Spacing', 'onschedule'),
				'default_value' => '',
				'parent' => $first_level_row3,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $panel_mobile_header,
				'name' => 'second_level_group',
				'title' => esc_html__('Dropdown Menu', 'onschedule'),
				'description' => esc_html__('Define styles for drop down menu items in Mobile Menu Navigation', 'onschedule')
			)
		);

		$second_level_row1 = onschedule_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row1'
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_text_color',
			'type'        => 'colorsimple',
			'label'       => esc_html__('Text Color', 'onschedule'),
			'parent'      => $second_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_text_hover_color',
			'type'        => 'colorsimple',
			'label'       => esc_html__('Hover/Active Text Color', 'onschedule'),
			'parent'      => $second_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_google_fonts',
			'type'        => 'fontsimple',
			'label'       => esc_html__('Font Family', 'onschedule'),
			'parent'      => $second_level_row1
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_font_size',
			'type'        => 'textsimple',
			'label'       => esc_html__('Font Size', 'onschedule'),
			'parent'      => $second_level_row1,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		$second_level_row2 = onschedule_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row2'
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_line_height',
			'type'        => 'textsimple',
			'label'       => esc_html__('Line Height', 'onschedule'),
			'parent'      => $second_level_row2,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_text_transform',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Text Transform', 'onschedule'),
			'parent'      => $second_level_row2,
			'options'     => onschedule_edge_get_text_transform_array()
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_font_style',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Font Style', 'onschedule'),
			'parent'      => $second_level_row2,
			'options'     => onschedule_edge_get_font_style_array()
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_dropdown_font_weight',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Font Weight', 'onschedule'),
			'parent'      => $second_level_row2,
			'options'     => onschedule_edge_get_font_weight_array()
		));
		
		$second_level_row3 = onschedule_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row3'
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'mobile_dropdown_letter_spacing',
				'label' => esc_html__('Letter Spacing', 'onschedule'),
				'default_value' => '',
				'parent' => $second_level_row3,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_section_title(array(
			'name' => 'mobile_opener_panel',
			'parent' => $panel_mobile_header,
			'title' => esc_html__('Mobile Menu Opener', 'onschedule')
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_menu_title',
			'type'        => 'text',
			'label'       => esc_html__('Mobile Navigation Title', 'onschedule'),
			'description' => esc_html__('Enter title for mobile menu navigation', 'onschedule'),
			'parent'      => $panel_mobile_header,
			'args' => array(
				'col_width' => 3
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_icon_color',
			'type'        => 'color',
			'label'       => esc_html__('Mobile Navigation Icon Color', 'onschedule'),
			'parent'      => $panel_mobile_header
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'mobile_icon_hover_color',
			'type'        => 'color',
			'label'       => esc_html__('Mobile Navigation Icon Hover Color', 'onschedule'),
			'parent'      => $panel_mobile_header
		));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_mobile_header_options_map', 5);
}