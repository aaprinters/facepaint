<?php

if ( ! function_exists('onschedule_edge_logo_options_map') ) {

	function onschedule_edge_logo_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_logo_page',
				'title' => esc_html__('Logo', 'onschedule'),
				'icon' => 'fa fa-coffee'
			)
		);

		$panel_logo = onschedule_edge_add_admin_panel(
			array(
				'page' => '_logo_page',
				'name' => 'panel_logo',
				'title' => esc_html__('Logo', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_logo,
				'type' => 'yesno',
				'name' => 'hide_logo',
				'default_value' => 'no',
				'label' => esc_html__('Hide Logo', 'onschedule'),
				'description' => esc_html__('Enabling this option will hide logo image', 'onschedule'),
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "#edgtf_hide_logo_container",
					"dependence_show_on_yes" => ""
				)
			)
		);

		$hide_logo_container = onschedule_edge_add_admin_container(
			array(
				'parent' => $panel_logo,
				'name' => 'hide_logo_container',
				'hidden_property' => 'hide_logo',
				'hidden_value' => 'yes'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'name' => 'logo_image',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Default', 'onschedule'),
				'parent' => $hide_logo_container
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'name' => 'logo_image_dark',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Dark', 'onschedule'),
				'parent' => $hide_logo_container
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'name' => 'logo_image_light',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Light', 'onschedule'),
				'parent' => $hide_logo_container
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'name' => 'logo_image_sticky',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Sticky', 'onschedule'),
				'parent' => $hide_logo_container
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'name' => 'logo_image_mobile',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Mobile', 'onschedule'),
				'parent' => $hide_logo_container
			)
		);
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_logo_options_map', 2);
}