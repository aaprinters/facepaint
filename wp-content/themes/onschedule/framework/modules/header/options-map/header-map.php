<?php

if ( ! function_exists('onschedule_edge_header_options_map') ) {

	function onschedule_edge_header_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_header_page',
				'title' => esc_html__('Header', 'onschedule'),
				'icon' => 'fa fa-header'
			)
		);

		$panel_header = onschedule_edge_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header',
				'title' => esc_html__('Header', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'radiogroup',
				'name' => 'header_type',
				'default_value' => 'header-standard',
				'label' => esc_html__('Choose Header Type', 'onschedule'),
				'description' => esc_html__('Select the type of header you would like to use', 'onschedule'),
				'options' => array(
					'header-standard' => array(
						'image' => EDGE_FRAMEWORK_ADMIN_ASSETS_ROOT.'/img/header-standard.png'
					),
					'header-boxed' => array(
						'image' => EDGE_FRAMEWORK_ADMIN_ASSETS_ROOT.'/img/header-box.png'
					),
                    'header-full-screen' => array(
                        'image' => EDGE_FRAMEWORK_ADMIN_ASSETS_ROOT.'/img/header-full-screen.png'
                    )
				),
				'args' => array(
					'use_images' => true,
					'hide_labels' => true,
					'dependence' => true,
					'show' => array(
						'header-standard' => '#edgtf_panel_header_standard,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header,#edgtf_panel_main_menu',
						'header-boxed' => '#edgtf_panel_header_boxed,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header,#edgtf_panel_main_menu',
						'header-full-screen' => '#edgtf_panel_header_full_screen'
					),
					'hide' => array(
						'header-standard' => '#edgtf_panel_header_boxed,#edgtf_panel_header_full_screen',
						'header-boxed' => '#edgtf_panel_header_standard,#edgtf_panel_header_full_screen',
						'header-full-screen' => '#edgtf_panel_header_standard,#edgtf_panel_header_boxed,#edgtf_panel_main_menu,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header',
					)
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_behaviour',
				'default_value' => 'fixed-on-scroll',
				'label' => esc_html__('Choose Header Behaviour', 'onschedule'),
				'description' => esc_html__('Select the behaviour of header when you scroll down to page', 'onschedule'),
				'options' => array(
					'sticky-header-on-scroll-up' => esc_html__('Sticky on scroll up', 'onschedule'),
					'sticky-header-on-scroll-down-up' => esc_html__('Sticky on scroll up/down', 'onschedule'),
					'fixed-on-scroll' => esc_html__('Fixed on scroll', 'onschedule')
				),
				'args' => array(
					'dependence' => true,
					'show' => array(
						'sticky-header-on-scroll-up' => '#edgtf_panel_sticky_header',
						'sticky-header-on-scroll-down-up' => '#edgtf_panel_sticky_header',
						'fixed-on-scroll' => '#edgtf_panel_fixed_header'
					),
					'hide' => array(
						'sticky-header-on-scroll-up' => '#edgtf_panel_fixed_header',
						'sticky-header-on-scroll-down-up' => '#edgtf_panel_fixed_header',
						'fixed-on-scroll' => '#edgtf_panel_sticky_header'
					)
				),
                'hidden_property' => 'header_type',
                'hidden_values' => array(
	                'header-full-screen'
                )
			)
		);

		/***************** Top Header Layout **********************/

			onschedule_edge_add_admin_field(
				array(
					'name' => 'top_bar',
					'type' => 'yesno',
					'default_value' => 'no',
					'label' => esc_html__('Top Bar', 'onschedule'),
					'description' => esc_html__('Enabling this option will show top bar area', 'onschedule'),
					'parent' => $panel_header,
					'args' => array(
						"dependence" => true,
						"dependence_hide_on_yes" => "",
						"dependence_show_on_yes" => "#edgtf_top_bar_container"
					)
				)
			);

			$top_bar_container = onschedule_edge_add_admin_container(array(
				'name' => 'top_bar_container',
				'parent' => $panel_header,
				'hidden_property' => 'top_bar',
				'hidden_value' => 'no'
			));

			onschedule_edge_add_admin_field(
				array(
					'name' => 'top_bar_in_grid',
					'type' => 'yesno',
					'default_value' => 'no',
					'label' => esc_html__('Enable Grid Layout', 'onschedule'),
					'description' => esc_html__('Set top bar content to be in grid', 'onschedule'),
					'parent' => $top_bar_container
				)
			);

			onschedule_edge_add_admin_field(array(
				'name' => 'top_bar_background_color',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for header area', 'onschedule'),
				'parent' => $top_bar_container
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'top_bar_background_transparency',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'onschedule'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
				'parent' => $top_bar_container,
				'args' => array('col_width' => 3)
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'top_bar_height',
				'type' => 'text',
				'label' => esc_html__('Top Bar Height', 'onschedule'),
				'description' => esc_html__('Enter top bar height (Default is 42px)', 'onschedule'),
				'parent' => $top_bar_container,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			));

		/***************** Top Header Layout **********************/
		
		/***************** Header Skin Options ********************/
		
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header,
					'type' => 'select',
					'name' => 'header_style',
					'default_value' => '',
					'label' => esc_html__('Header Skin', 'onschedule'),
					'description' => esc_html__('Choose a predefined header style for header elements (logo, main menu, side menu opener...)', 'onschedule'),
					'options' => array(
						'' => esc_html__('Default', 'onschedule'),
						'light-header' => esc_html__('Light', 'onschedule'),
						'dark-header' => esc_html__('Dark', 'onschedule')
					)
				)
			);
		
		/***************** Header Skin Options ********************/
		
		/***************** Header Style **********************/
			
			onschedule_edge_add_admin_section_title(
				array(
					'parent' => $panel_header,
					'name' => 'header_area_style',
					'title' => esc_html__('Header Area Style', 'onschedule')
				)
			);
		
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header,
					'type' => 'yesno',
					'name' => 'enable_grid_layout_header',
					'default_value' => 'no',
					'label' => esc_html__('Enable Grid Layout', 'onschedule'),
					'description' => esc_html__('Enabling this option you will set header area to be in grid', 'onschedule'),
				)
			);
			
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header,
					'type' => 'color',
					'name' => 'header_area_background_color',
					'default_value' => '',
					'label' => esc_html__('Background Color', 'onschedule'),
					'description' => esc_html__('Choose a background color for header area', 'onschedule'),
				)
			);
			
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header,
					'type' => 'text',
					'name' => 'header_area_background_transparency',
					'default_value' => '',
					'label' => esc_html__('Background Transparency', 'onschedule'),
					'description' => esc_html__('Choose a transparency for the header area background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
					'args' => array(
						'col_width' => 3
					)
				)
			);
			
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header,
					'type' => 'color',
					'name' => 'header_area_border_color',
					'default_value' => '',
					'label' => esc_html__('Border Color', 'onschedule'),
					'description' => esc_html__('Set border bottom color for header area. This option doesn\'t work for Vertical header layout', 'onschedule')
				)
			);
		
		/***************** Header Style **********************/

		/***************** Standard Header Layout *****************/

			$panel_header_standard = onschedule_edge_add_admin_panel(
				array(
					'page' => '_header_page',
					'name' => 'panel_header_standard',
					'title' => esc_html__('Header Standard', 'onschedule'),
					'hidden_property' => 'header_type',
					'hidden_value' => '',
					'hidden_values' => array(
						'header-boxed',
	                    'header-full-screen'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header_standard,
					'type' => 'select',
					'name' => 'set_menu_area_position',
					'default_value' => 'right',
					'label' => esc_html__('Choose Menu Area Position', 'onschedule'),
					'description' => esc_html__('Select menu area position in your header', 'onschedule'),
					'options' => array(
						'right' => esc_html__('Right', 'onschedule'),
						'left' => esc_html__('Left', 'onschedule'),
						'center' => esc_html__('Center', 'onschedule')
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header_standard,
					'type' => 'text',
					'name' => 'menu_area_height_header_standard',
					'default_value' => '',
					'label' => esc_html__('Height', 'onschedule'),
					'description' => esc_html__('Enter Header Height (default is 76px)', 'onschedule'),
					'args' => array(
						'col_width' => 3,
						'suffix' => 'px'
					)
				)
			);

		/***************** Standard Header Layout *****************/
		
		/***************** boxed Header Layout *****************/
		
			$panel_header_boxed = onschedule_edge_add_admin_panel(
				array(
					'page' => '_header_page',
					'name' => 'panel_header_boxed',
					'title' => esc_html__('Header Boxed', 'onschedule'),
					'hidden_property' => 'header_type',
					'hidden_value' => '',
					'hidden_values' => array(
						'header-standard',
						'header-full-screen'
					)
				)
			);
			
			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header_boxed,
					'type' => 'text',
					'name' => 'menu_area_height_header_boxed',
					'default_value' => '',
					'label' => esc_html__('Height', 'onschedule'),
					'description' => esc_html__('Enter Header Height (default is 76px)', 'onschedule'),
					'args' => array(
						'col_width' => 3,
						'suffix' => 'px'
					)
				)
			);
		
		/***************** boxed Header Layout *****************/

		/***************** Full Screen Header Layout *******************/

			$panel_header_full_screen = onschedule_edge_add_admin_panel(
				array(
					'page' => '_header_page',
					'name' => 'panel_header_full_screen',
					'title' => esc_html__('Header Full Screen', 'onschedule'),
					'hidden_property' => 'header_type',
					'hidden_value' => '',
					'hidden_values' => array(
	                    'header-standard',
						'header-boxed'
					)
				)
			);

			onschedule_edge_add_admin_section_title(
				array(
					'parent' => $panel_header_full_screen,
					'name' => 'header_full_screen_title',
					'title' => esc_html__('Header Full Screen', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_header_full_screen,
					'type' => 'text',
					'name' => 'menu_area_height_header_full_screen',
					'default_value' => '',
					'label' => esc_html__('Height', 'onschedule'),
					'description' => esc_html__('Enter Header Height (default is 76px)', 'onschedule'),
					'args' => array(
						'col_width' => 3,
						'suffix' => 'px'
					)
				)
			);

		/***************** Full Screen Header Layout *******************/

        /***************** Sticky Header Layout *******************/

			$panel_sticky_header = onschedule_edge_add_admin_panel(
				array(
					'title' => esc_html__('Sticky Header', 'onschedule'),
					'name' => 'panel_sticky_header',
					'page' => '_header_page',
					'hidden_property' => 'header_behaviour',
					'hidden_value' => '',
					'hidden_values' => array(
	                    'fixed-on-scroll'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'name' => 'scroll_amount_for_sticky',
					'type' => 'text',
					'label' => esc_html__('Scroll Amount for Sticky', 'onschedule'),
					'description' => esc_html__('Enter scroll amount for Sticky Menu to appear (deafult is header height). This value can be overriden on a page level basis', 'onschedule'),
					'parent' => $panel_sticky_header,
					'args' => array(
						'col_width' => 2,
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'name' => 'sticky_header_in_grid',
					'type' => 'yesno',
					'default_value' => 'no',
					'label' => esc_html__('Sticky Header in Grid', 'onschedule'),
					'description' => esc_html__('Enabling this option will put sticky header in grid', 'onschedule'),
					'parent' => $panel_sticky_header,
				)
			);

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_header_background_color',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for header area', 'onschedule'),
				'parent' => $panel_sticky_header
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_header_transparency',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'onschedule'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
				'parent' => $panel_sticky_header,
				'args' => array(
					'col_width' => 1
				)
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_header_border_color',
				'type' => 'color',
				'label' => esc_html__('Border Color', 'onschedule'),
				'description' => esc_html__('Set border bottom color for header area', 'onschedule'),
				'parent' => $panel_sticky_header
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_header_height',
				'type' => 'text',
				'label' => esc_html__('Sticky Header Height', 'onschedule'),
				'description' => esc_html__('Enter height for sticky header (default is 56px)', 'onschedule'),
				'parent' => $panel_sticky_header,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			));

			$group_sticky_header_menu = onschedule_edge_add_admin_group(array(
				'title' => esc_html__('Sticky Header Menu', 'onschedule'),
				'name' => 'group_sticky_header_menu',
				'parent' => $panel_sticky_header,
				'description' => esc_html__('Define styles for sticky menu items', 'onschedule')
			));

			$row1_sticky_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row1',
				'parent' => $group_sticky_header_menu
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_color',
				'type' => 'colorsimple',
				'label' => esc_html__('Text Color', 'onschedule'),
				'description' => '',
				'parent' => $row1_sticky_header_menu
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'sticky_hovercolor',
				'type' => 'colorsimple',
				'label' => esc_html__(esc_html__('Hover/Active Color', 'onschedule'), 'onschedule'),
				'description' => '',
				'parent' => $row1_sticky_header_menu
			));

			$row2_sticky_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row2',
				'parent' => $group_sticky_header_menu
			));

			onschedule_edge_add_admin_field(
				array(
					'name' => 'sticky_google_fonts',
					'type' => 'fontsimple',
					'label' => esc_html__('Font Family', 'onschedule'),
					'default_value' => '-1',
					'parent' => $row2_sticky_header_menu,
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'sticky_font_size',
					'label' => esc_html__('Font Size', 'onschedule'),
					'default_value' => '',
					'parent' => $row2_sticky_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'sticky_line_height',
					'label' => esc_html__('Line Height', 'onschedule'),
					'default_value' => '',
					'parent' => $row2_sticky_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'sticky_text_transform',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'default_value' => '',
					'options' => onschedule_edge_get_text_transform_array(),
					'parent' => $row2_sticky_header_menu
				)
			);

			$row3_sticky_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row3',
				'parent' => $group_sticky_header_menu
			));

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'sticky_font_style',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array(),
					'parent' => $row3_sticky_header_menu
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'sticky_font_weight',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array(),
					'parent' => $row3_sticky_header_menu
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'sticky_letter_spacing',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'default_value' => '',
					'parent' => $row3_sticky_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

		/***************** Sticky Header Layout *******************/
			
		/***************** Fixed Header Layout ********************/	

			$panel_fixed_header = onschedule_edge_add_admin_panel(
				array(
					'title' => esc_html__('Fixed Header', 'onschedule'),
					'name' => 'panel_fixed_header',
					'page' => '_header_page',
					'hidden_property' => 'header_behaviour',
					'hidden_value' => '',
					'hidden_values' => array(
						'sticky-header-on-scroll-up',
						'sticky-header-on-scroll-down-up'
					)
				)
			);

			onschedule_edge_add_admin_field(array(
				'name' => 'fixed_header_background_color',
				'type' => 'color',
				'default_value' => '',
				'label' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for header area', 'onschedule'),
				'parent' => $panel_fixed_header
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'fixed_header_transparency',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'onschedule'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
				'parent' => $panel_fixed_header,
				'args' => array(
					'col_width' => 1
				)
			));

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_fixed_header,
					'type' => 'color',
					'name' => 'fixed_header_border_bottom_color',
					'default_value' => '',
					'label' => esc_html__('Border Color', 'onschedule'),
					'description' => esc_html__('Set border bottom color for header area', 'onschedule'),
				)
			);

			$group_fixed_header_menu = onschedule_edge_add_admin_group(array(
				'title' => esc_html__('Fixed Header Menu', 'onschedule'),
				'name' => 'group_fixed_header_menu',
				'parent' => $panel_fixed_header,
				'description' => esc_html__('Define styles for fixed menu items', 'onschedule')
			));

			$row1_fixed_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row1',
				'parent' => $group_fixed_header_menu
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'fixed_color',
				'type' => 'colorsimple',
				'label' => esc_html__('Text Color', 'onschedule'),
				'description' => '',
				'parent' => $row1_fixed_header_menu
			));

			onschedule_edge_add_admin_field(array(
				'name' => 'fixed_hovercolor',
				'type' => 'colorsimple',
				'label' => esc_html__(esc_html__('Hover/Active Color', 'onschedule'), 'onschedule'),
				'description' => '',
				'parent' => $row1_fixed_header_menu
			));

			$row2_fixed_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row2',
				'parent' => $group_fixed_header_menu
			));

			onschedule_edge_add_admin_field(
				array(
					'name' => 'fixed_google_fonts',
					'type' => 'fontsimple',
					'label' => esc_html__('Font Family', 'onschedule'),
					'default_value' => '-1',
					'parent' => $row2_fixed_header_menu,
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'fixed_font_size',
					'label' => esc_html__('Font Size', 'onschedule'),
					'default_value' => '',
					'parent' => $row2_fixed_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'fixed_line_height',
					'label' => esc_html__('Line Height', 'onschedule'),
					'default_value' => '',
					'parent' => $row2_fixed_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'fixed_text_transform',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'default_value' => '',
					'options' => onschedule_edge_get_text_transform_array(),
					'parent' => $row2_fixed_header_menu
				)
			);

			$row3_fixed_header_menu = onschedule_edge_add_admin_row(array(
				'name' => 'row3',
				'parent' => $group_fixed_header_menu
			));

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'fixed_font_style',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array(),
					'parent' => $row3_fixed_header_menu
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'selectblanksimple',
					'name' => 'fixed_font_weight',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array(),
					'parent' => $row3_fixed_header_menu
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'type' => 'textsimple',
					'name' => 'fixed_letter_spacing',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'default_value' => '',
					'parent' => $row3_fixed_header_menu,
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

		/***************** Fixed Header Layout ********************/	

		/******************* Main Menu Layout *********************/

			$panel_main_menu = onschedule_edge_add_admin_panel(
				array(
					'title' => esc_html__('Main Menu', 'onschedule'),
					'name' => 'panel_main_menu',
					'page' => '_header_page',
					'hidden_property' => 'header_type',
	                'hidden_values' => array(
	                	'header-full-screen'
	            	)
				)
			);

			onschedule_edge_add_admin_section_title(
				array(
					'parent' => $panel_main_menu,
					'name' => 'main_menu_area_title',
					'title' => esc_html__('Main Menu General Settings', 'onschedule')
				)
			);

			$drop_down_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'drop_down_group',
					'title' => esc_html__('Main Dropdown Menu', 'onschedule'),
					'description' => esc_html__('Choose a color and transparency for the main menu background (0 = fully transparent, 1 = opaque)', 'onschedule')
				)
			);

			$drop_down_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $drop_down_group,
					'name' => 'drop_down_row1',
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $drop_down_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_background_color',
					'default_value' => '',
					'label' => esc_html__('Background Color', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $drop_down_row1,
					'type' => 'textsimple',
					'name' => 'dropdown_background_transparency',
					'default_value' => '',
					'label' => esc_html__('Background Transparency', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_main_menu,
					'type' => 'select',
					'name' => 'menu_dropdown_appearance',
					'default_value' => 'dropdown-animate-height',
					'label' => esc_html__('Main Dropdown Menu Appearance', 'onschedule'),
					'description' => esc_html__('Choose appearance for dropdown menu', 'onschedule'),
					'options' => array(
						'dropdown-default' => esc_html__('Default', 'onschedule'),
						'dropdown-animate-height' => esc_html__('Animate Height', 'onschedule')
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $panel_main_menu,
					'type' => 'text',
					'name' => 'dropdown_top_position',
					'default_value' => '',
					'label' => esc_html__('Dropdown Position', 'onschedule'),
					'description' => esc_html__('Enter value in percentage of entire header height', 'onschedule'),
					'args' => array(
						'col_width' => 3,
						'suffix' => '%'
					)
				)
			);

			$first_level_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'first_level_group',
					'title' => esc_html__('1st Level Menu', 'onschedule'),
					'description' => esc_html__('Define styles for 1st level in Top Navigation Menu', 'onschedule')
				)
			);

			$first_level_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row1'
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row1,
					'type' => 'colorsimple',
					'name' => 'menu_color',
					'default_value' => '',
					'label' => esc_html__('Text Color', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row1,
					'type' => 'colorsimple',
					'name' => 'menu_hovercolor',
					'default_value' => '',
					'label' => esc_html__('Hover Text Color', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row1,
					'type' => 'colorsimple',
					'name' => 'menu_activecolor',
					'default_value' => '',
					'label' => esc_html__('Active Text Color', 'onschedule'),
				)
			);

			$first_level_row3 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row3',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row3,
					'type' => 'colorsimple',
					'name' => 'menu_light_hovercolor',
					'default_value' => '',
					'label' => esc_html__('Light Menu Hover Text Color', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row3,
					'type' => 'colorsimple',
					'name' => 'menu_light_activecolor',
					'default_value' => '',
					'label' => esc_html__('Light Menu Active Text Color', 'onschedule'),
				)
			);

			$first_level_row4 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row4',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row4,
					'type' => 'colorsimple',
					'name' => 'menu_dark_hovercolor',
					'default_value' => '',
					'label' => esc_html__('Dark Menu Hover Text Color', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row4,
					'type' => 'colorsimple',
					'name' => 'menu_dark_activecolor',
					'default_value' => '',
					'label' => esc_html__('Dark Menu Active Text Color', 'onschedule'),
				)
			);

			$first_level_row5 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row5',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row5,
					'type' => 'fontsimple',
					'name' => 'menu_google_fonts',
					'default_value' => '-1',
					'label' => esc_html__('Font Family', 'onschedule'),
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row5,
					'type' => 'textsimple',
					'name' => 'menu_font_size',
					'default_value' => '',
					'label' => esc_html__('Font Size', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row5,
					'type' => 'textsimple',
					'name' => 'menu_line_height',
					'default_value' => '',
					'label' => esc_html__('Line Height', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$first_level_row6 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row6',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row6,
					'type' => 'selectblanksimple',
					'name' => 'menu_font_style',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row6,
					'type' => 'selectblanksimple',
					'name' => 'menu_font_weight',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row6,
					'type' => 'textsimple',
					'name' => 'menu_letter_spacing',
					'default_value' => '',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row6,
					'type' => 'selectblanksimple',
					'name' => 'menu_text_transform',
					'default_value' => '',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'options' => onschedule_edge_get_text_transform_array()
				)
			);

			$first_level_row7 = onschedule_edge_add_admin_row(
				array(
					'parent' => $first_level_group,
					'name' => 'first_level_row7',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row7,
					'type' => 'textsimple',
					'name' => 'menu_padding_left_right',
					'default_value' => '',
					'label' => esc_html__('Padding Left/Right', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $first_level_row7,
					'type' => 'textsimple',
					'name' => 'menu_margin_left_right',
					'default_value' => '',
					'label' => esc_html__('Margin Left/Right', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$second_level_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'second_level_group',
					'title' => esc_html__('2nd Level Menu', 'onschedule'),
					'description' => esc_html__('Define styles for 2nd level in Top Navigation Menu', 'onschedule')
				)
			);

			$second_level_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_group,
					'name' => 'second_level_row1'
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_color',
					'default_value' => '',
					'label' => esc_html__('Text Color', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_hovercolor',
					'default_value' => '',
					'label' => esc_html__('Hover/Active Color', 'onschedule')
				)
			);

			$second_level_row2 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_group,
					'name' => 'second_level_row2',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row2,
					'type' => 'fontsimple',
					'name' => 'dropdown_google_fonts',
					'default_value' => '-1',
					'label' => esc_html__('Font Family', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_font_size',
					'default_value' => '',
					'label' => esc_html__('Font Size', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_line_height',
					'default_value' => '',
					'label' => esc_html__('Line Height', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$second_level_row3 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_group,
					'name' => 'second_level_row3',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_font_style',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_font_weight',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row3,
					'type' => 'textsimple',
					'name' => 'dropdown_letter_spacing',
					'default_value' => '',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_text_transform',
					'default_value' => '',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'options' => onschedule_edge_get_text_transform_array()
				)
			);

			$second_level_wide_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'second_level_wide_group',
					'title' => esc_html__('2nd Level Wide Menu', 'onschedule'),
					'description' => esc_html__('Define styles for 2nd level in Wide Menu', 'onschedule')
				)
			);

			$second_level_wide_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_wide_group,
					'name' => 'second_level_wide_row1'
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_wide_color',
					'default_value' => '',
					'label' => esc_html__('Text Color', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_wide_hovercolor',
					'default_value' => '',
					'label' => esc_html__('Hover/Active Color', 'onschedule')
				)
			);

			$second_level_wide_row2 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_wide_group,
					'name' => 'second_level_wide_row2',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row2,
					'type' => 'fontsimple',
					'name' => 'dropdown_wide_google_fonts',
					'default_value' => '-1',
					'label' => esc_html__('Font Family', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_font_size',
					'default_value' => '',
					'label' => esc_html__('Font Size', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_line_height',
					'default_value' => '',
					'label' => esc_html__('Line Height', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$second_level_wide_row3 = onschedule_edge_add_admin_row(
				array(
					'parent' => $second_level_wide_group,
					'name' => 'second_level_wide_row3',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_font_style',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_font_weight',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row3,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_letter_spacing',
					'default_value' => '',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $second_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_text_transform',
					'default_value' => '',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'options' => onschedule_edge_get_text_transform_array()
				)
			);

			$third_level_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'third_level_group',
					'title' => esc_html__('3nd Level Menu', 'onschedule'),
					'description' => esc_html__('Define styles for 3nd level in Top Navigation Menu', 'onschedule')
				)
			);

			$third_level_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_group,
					'name' => 'third_level_row1'
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_color_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Text Color', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_hovercolor_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Hover/Active Color', 'onschedule')
				)
			);

			$third_level_row2 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_group,
					'name' => 'third_level_row2',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row2,
					'type' => 'fontsimple',
					'name' => 'dropdown_google_fonts_thirdlvl',
					'default_value' => '-1',
					'label' => esc_html__('Font Family', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_font_size_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Size', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_line_height_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Line Height', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$third_level_row3 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_group,
					'name' => 'third_level_row3',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_font_style_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_font_weight_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row3,
					'type' => 'textsimple',
					'name' => 'dropdown_letter_spacing_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_text_transform_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'options' => onschedule_edge_get_text_transform_array()
				)
			);

			$third_level_wide_group = onschedule_edge_add_admin_group(
				array(
					'parent' => $panel_main_menu,
					'name' => 'third_level_wide_group',
					'title' => esc_html__('3rd Level Wide Menu', 'onschedule'),
					'description' => esc_html__('Define styles for 3rd level in Wide Menu', 'onschedule')
				)
			);

			$third_level_wide_row1 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_wide_group,
					'name' => 'third_level_wide_row1'
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_wide_color_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Text Color', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row1,
					'type' => 'colorsimple',
					'name' => 'dropdown_wide_hovercolor_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Hover/Active Color', 'onschedule')
				)
			);

			$third_level_wide_row2 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_wide_group,
					'name' => 'third_level_wide_row2',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row2,
					'type' => 'fontsimple',
					'name' => 'dropdown_wide_google_fonts_thirdlvl',
					'default_value' => '-1',
					'label' => esc_html__('Font Family', 'onschedule')
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_font_size_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Size', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row2,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_line_height_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Line Height', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			$third_level_wide_row3 = onschedule_edge_add_admin_row(
				array(
					'parent' => $third_level_wide_group,
					'name' => 'third_level_wide_row3',
					'next' => true
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_font_style_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Style', 'onschedule'),
					'options' => onschedule_edge_get_font_style_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_font_weight_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Font Weight', 'onschedule'),
					'options' => onschedule_edge_get_font_weight_array()
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row3,
					'type' => 'textsimple',
					'name' => 'dropdown_wide_letter_spacing_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Letter Spacing', 'onschedule'),
					'args' => array(
						'suffix' => 'px'
					)
				)
			);

			onschedule_edge_add_admin_field(
				array(
					'parent' => $third_level_wide_row3,
					'type' => 'selectblanksimple',
					'name' => 'dropdown_wide_text_transform_thirdlvl',
					'default_value' => '',
					'label' => esc_html__('Text Transform', 'onschedule'),
					'options' => onschedule_edge_get_text_transform_array()
				)
			);

        /******************* Main Menu Layout *********************/

	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_header_options_map', 4);
}