<?php

//top header bar
add_action('onschedule_edge_action_before_page_header', 'onschedule_edge_get_header_top');

//mobile header
add_action('onschedule_edge_action_after_page_header', 'onschedule_edge_get_mobile_header');