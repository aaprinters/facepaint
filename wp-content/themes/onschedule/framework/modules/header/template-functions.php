<?php

use OnScheduleEdgeNamespace\Modules\Header\Lib\HeaderFactory;

if(!function_exists('onschedule_edge_get_header')) {
    /**
     * Loads header HTML based on header type option. Sets all necessary parameters for header
     * and defines onschedule_edge_filter_header_type_parameters filter
     */
    function onschedule_edge_get_header() {
        //will be read from options
        $header_type = onschedule_edge_get_meta_field_intersect('header_type');

        $header_in_grid = onschedule_edge_get_meta_field_intersect('enable_grid_layout_header');

	    $set_menu_area_position = onschedule_edge_get_meta_field_intersect('set_menu_area_position');
	    
	    $header_behavior = onschedule_edge_options()->getOptionValue('header_behaviour');
	
	    if(HeaderFactory::getInstance()->validHeaderObject()) {
            $parameters = array(
                'hide_logo' => onschedule_edge_options()->getOptionValue('hide_logo') == 'yes' ? true : false,
                'header_in_grid' => $header_in_grid == 'yes' ? true : false,
	            'standard_menu_area_class' => !empty($set_menu_area_position) ? 'edgtf-menu-'.$set_menu_area_position : 'edgtf-menu-right',
                'set_menu_area_position' => $set_menu_area_position,
                'show_sticky' => in_array($header_behavior, array(
                    'sticky-header-on-scroll-up',
                    'sticky-header-on-scroll-down-up'
                )) ? true : false,
                'show_fixed_wrapper' => in_array($header_behavior, array('fixed-on-scroll')) ? true : false
            );

            $parameters = apply_filters('onschedule_edge_filter_header_type_parameters', $parameters, $header_type);

            HeaderFactory::getInstance()->getHeaderObject()->loadTemplate($parameters);
        }
    }
}

if(!function_exists('onschedule_edge_get_header_top')) {
    /**
     * Loads header top HTML and sets parameters for it
     */
    function onschedule_edge_get_header_top() {

        $params = array(
            'show_header_top' => onschedule_edge_get_meta_field_intersect('top_bar') === 'yes' ? true : false,
            'top_bar_in_grid' => onschedule_edge_get_meta_field_intersect('top_bar_in_grid') === 'yes' ? true : false
        );

        $params = apply_filters('onschedule_edge_filter_header_top_params', $params);

        onschedule_edge_get_module_template_part('templates/parts/header-top', 'header', '', $params);
    }
}

if(!function_exists('onschedule_edge_get_logo')) {
    /**
     * Loads logo HTML
     *
     * @param $slug
     */
    function onschedule_edge_get_logo($slug = '') {

        $slug = $slug !== '' ? $slug : onschedule_edge_options()->getOptionValue('header_type');

        if ($slug == 'sticky'){
            $logo_image = onschedule_edge_options()->getOptionValue('logo_image_sticky');
        } else {
            $logo_image = onschedule_edge_get_meta_field_intersect('logo_image', onschedule_edge_get_page_id());
        }

        $logo_image_dark = onschedule_edge_get_meta_field_intersect('logo_image_dark', onschedule_edge_get_page_id());
        $logo_image_light = onschedule_edge_get_meta_field_intersect('logo_image_light', onschedule_edge_get_page_id());


        //get logo image dimensions and set style attribute for image link.
        $logo_dimensions = onschedule_edge_get_image_dimensions($logo_image);

        $logo_height = '';
        $logo_styles = '';
        if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
            $logo_height = $logo_dimensions['height'];
            $logo_styles = 'height: '.intval($logo_height / 2).'px;'; //divided with 2 because of retina screens
        }

        $params = array(
            'logo_image'  => $logo_image,
            'logo_image_dark' => $logo_image_dark,
            'logo_image_light' => $logo_image_light,
            'logo_styles' => $logo_styles
        );

        onschedule_edge_get_module_template_part('templates/parts/logo', 'header', $slug, $params);
    }
}

if(!function_exists('onschedule_edge_get_main_menu')) {
    /**
     * Loads main menu HTML
     *
     * @param string $additional_class addition class to pass to template
     */
    function onschedule_edge_get_main_menu($additional_class = 'edgtf-default-nav') {
        onschedule_edge_get_module_template_part('templates/parts/navigation', 'header', '', array('additional_class' => $additional_class));
    }
}

if(!function_exists('onschedule_edge_get_header_widget_area')) {
	/**
	 * Loads header widgets area HTML
	 */
	function onschedule_edge_get_header_widget_area() {
		$page_id = onschedule_edge_get_page_id();
		if(onschedule_edge_is_woocommerce_installed() && onschedule_edge_is_woocommerce_shop()) {
			//get shop page id from options table
			$shop_id = get_option('woocommerce_shop_page_id');
			
			if(!empty($shop_id)) {
				$page_id = $shop_id;
			} else {
				$page_id = '-1';
			}
		}
		
        if(get_post_meta($page_id, 'edgtf_disable_header_widget_area_meta', 'true') !== 'yes') {
            if(is_active_sidebar('edgtf-header-widget-area') && get_post_meta($page_id, 'edgtf_custom_header_sidebar_meta', true) === '') {
                dynamic_sidebar('edgtf-header-widget-area');
            } else if (get_post_meta($page_id, 'edgtf_custom_header_sidebar_meta', true) !== '') {
                $sidebar = get_post_meta($page_id, 'edgtf_custom_header_sidebar_meta', true);
                if (is_active_sidebar($sidebar)) {
                    dynamic_sidebar($sidebar);
                }
            }
        }
	}
}

if(!function_exists('onschedule_edge_get_sticky_menu')) {
	/**
	 * Loads sticky menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function onschedule_edge_get_sticky_menu($additional_class = 'edgtf-default-nav') {
		onschedule_edge_get_module_template_part('templates/parts/sticky-navigation', 'header', '', array('additional_class' => $additional_class));
	}
}

if(!function_exists('onschedule_edge_get_sticky_header')) {
    /**
     * Loads sticky header behavior HTML
     */
    function onschedule_edge_get_sticky_header() {
        $parameters = array(
            'hide_logo'             => onschedule_edge_options()->getOptionValue('hide_logo') == 'yes' ? true : false,
            'sticky_header_in_grid' => onschedule_edge_options()->getOptionValue('sticky_header_in_grid') == 'yes' ? true : false
        );

        onschedule_edge_get_module_template_part('templates/behaviors/sticky-header', 'header', '', $parameters);
    }
}

if(!function_exists('onschedule_edge_get_mobile_header')) {
    /**
     * Loads mobile header HTML only if responsiveness is enabled
     */
    function onschedule_edge_get_mobile_header() {
        if(onschedule_edge_is_responsive_on()) {
            $mobile_menu_title = onschedule_edge_options()->getOptionValue('mobile_menu_title');

            $has_navigation = false;
            if(has_nav_menu('main-navigation') || has_nav_menu('mobile-navigation')) {
                $has_navigation = true;
            }

            $parameters = array(
                'show_logo'              => onschedule_edge_options()->getOptionValue('hide_logo') == 'yes' ? false : true,
                'show_navigation_opener' => $has_navigation,
                'mobile_menu_title'      => $mobile_menu_title
            );

            onschedule_edge_get_module_template_part('templates/types/mobile-header', 'header', '', $parameters);
        }
    }
}

if(!function_exists('onschedule_edge_get_mobile_logo')) {
    /**
     * Loads mobile logo HTML. It checks if mobile logo image is set and uses that, else takes normal logo image
     *
     * @param string $slug
     */
    function onschedule_edge_get_mobile_logo($slug = '') {
        $slug = $slug !== '' ? $slug : onschedule_edge_options()->getOptionValue('header_type');

        //check if mobile logo has been set and use that, else use normal logo
	    $custom_logo_image = get_post_meta(onschedule_edge_get_page_id(), 'edgtf_logo_image_meta');
        if(onschedule_edge_options()->getOptionValue('logo_image_mobile') !== '' && empty($custom_logo_image)) {
            $logo_image = onschedule_edge_options()->getOptionValue('logo_image_mobile');
        } else {
	        $logo_image = onschedule_edge_get_meta_field_intersect('logo_image', onschedule_edge_get_page_id());
        }

        //get logo image dimensions and set style attribute for image link.
        $logo_dimensions = onschedule_edge_get_image_dimensions($logo_image);

        $logo_height = '';
        $logo_styles = '';
        if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
            $logo_height = $logo_dimensions['height'];
            $logo_styles = 'height: '.intval($logo_height / 2).'px'; //divided with 2 because of retina screens
        }

        //set parameters for logo
        $parameters = array(
            'logo_image'      => $logo_image,
            'logo_dimensions' => $logo_dimensions,
            'logo_height'     => $logo_height,
            'logo_styles'     => $logo_styles
        );

        onschedule_edge_get_module_template_part('templates/parts/mobile-logo', 'header', $slug, $parameters);
    }
}

if(!function_exists('onschedule_edge_get_mobile_nav')) {
    /**
     * Loads mobile navigation HTML
     */
    function onschedule_edge_get_mobile_nav() {

        onschedule_edge_get_module_template_part('templates/parts/mobile-navigation', 'header', '');
    }
}

if( !function_exists('onschedule_edge_header_area_style') ) {
	/**
	 * Function that return styles for header area
	 */
	function onschedule_edge_header_area_style($style) {
		$id = onschedule_edge_get_page_id();
		$class_id = onschedule_edge_get_page_id();
		$is_product = false;

		if(onschedule_edge_is_woocommerce_installed() && is_product()) {
			$class_id = get_the_ID();
			$is_product = true;
		}
		
		$class_prefix = onschedule_edge_get_unique_page_class($class_id);
		
		$current_styles = '';
		
		$header_styles = array();
		$header_selector = array(
			$class_prefix . ' .edgtf-page-header'
		);
		
		$menu_styles = array();
		$menu_selector = array(
			$class_prefix . ' .edgtf-page-header .edgtf-menu-area'
		);
		
		$menu_elegant_styles   = array();
		$menu_elegant_selector = array(
			$class_prefix . ' .edgtf-header-boxed .edgtf-page-header .edgtf-menu-area'
		);
		
		$header_background_color                 = '';
		$header_border_color                     = '';
		
		$background_color = get_post_meta($id, 'edgtf_header_area_background_color_meta', true);
		$background_transparency = get_post_meta($id, 'edgtf_header_area_background_transparency_meta', true);
		$border_bottom_color = get_post_meta($id, 'edgtf_header_area_border_color_meta', true);

		if( $is_product ) {
			$background_color = onschedule_edge_options()->getOptionValue('woo_single_header_background_color');
			$background_transparency = onschedule_edge_options()->getOptionValue('woo_single_header_background_transparency');
			$border_bottom_color = onschedule_edge_options()->getOptionValue('woo_single_header_border_color');
		}
		
		if(!empty($background_color)) {
			$header_background_color = $background_color;
				
			if($background_transparency !== '') {
				$header_background_transparency = $background_transparency;
				
				$header_background_color = onschedule_edge_rgba_color($header_background_color, $header_background_transparency);
			}
		}
		
		if(!empty($border_bottom_color)) {
			$header_border_color = $border_bottom_color;
		}
		
		if(!empty($header_background_color)) {
			$header_styles['background-color'] = $header_background_color;
			$menu_elegant_styles['background-color'] = $header_background_color;
			$current_styles .= onschedule_edge_dynamic_css($header_selector, $header_styles);
		}
		
		if(!empty($header_border_color)) {
			$menu_styles['border-color'] = $header_border_color;
			$current_styles .= onschedule_edge_dynamic_css($menu_selector, $menu_styles);
		}
		
		if(!empty($menu_elegant_styles)) {
			$current_styles .= onschedule_edge_dynamic_css($menu_elegant_selector, $menu_elegant_styles);
		}
		
		$current_style = $current_styles . $style;
		
		return $current_style;
	}
	
	add_filter('onschedule_edge_filter_add_page_custom_style', 'onschedule_edge_header_area_style');
}