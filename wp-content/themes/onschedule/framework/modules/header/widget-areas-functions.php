<?php

if(!function_exists('onschedule_edge_register_top_header_areas')) {
    /**
     * Registers widget areas for top header bar when it is enabled
     */
    function onschedule_edge_register_top_header_areas() {

        register_sidebar(array(
            'name'          => esc_html__('Top Bar Left Column', 'onschedule'),
            'id'            => 'edgtf-top-bar-left',
            'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-top-bar-widget">',
            'after_widget'  => '</div>',
            'description'   => esc_html__('Widgets added here will appear on the left side in top bar header', 'onschedule')
        ));

        register_sidebar(array(
            'name'          => esc_html__('Top Bar Right Column', 'onschedule'),
            'id'            => 'edgtf-top-bar-right',
            'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-top-bar-widget">',
            'after_widget'  => '</div>',
            'description'   => esc_html__('Widgets added here will appear on the right side in top bar header', 'onschedule')
        ));
    }

    add_action('widgets_init', 'onschedule_edge_register_top_header_areas');
}

if(!function_exists('onschedule_edge_header_widget_areas')) {
    /**
     * Registers widget areas for header types
     */
    function onschedule_edge_header_standard_widget_areas() {
    	if ( onschedule_edge_core_plugin_installed() ) {
			register_sidebar(array(
				'name' => esc_html__('Header Widget Area', 'onschedule'),
				'id' => 'edgtf-header-widget-area',
				'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-header-widget-area">',
				'after_widget' => '</div>',
				'description' => esc_html__('Widgets added here will appear on the right hand side from the main menu', 'onschedule')
			));
		}
    }

    add_action('widgets_init', 'onschedule_edge_header_standard_widget_areas');
}

if(!function_exists('onschedule_edge_register_mobile_header_areas')) {
    /**
     * Registers widget areas for mobile header
     */
    function onschedule_edge_register_mobile_header_areas() {
        if(onschedule_edge_is_responsive_on() && onschedule_edge_core_plugin_installed() ) {
            register_sidebar(array(
                'name'          => esc_html__('Mobile Header Widget Area', 'onschedule'),
                'id'            => 'edgtf-right-from-mobile-logo',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-right-from-mobile-logo">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the mobile logo on mobile header', 'onschedule')
            ));
        }
    }

    add_action('widgets_init', 'onschedule_edge_register_mobile_header_areas');
}

if(!function_exists('onschedule_edge_register_sticky_header_areas')) {
    /**
     * Registers widget area for sticky header
     */
    function onschedule_edge_register_sticky_header_areas() {
        if(in_array(onschedule_edge_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            register_sidebar(array(
                'name'          => esc_html__('Sticky Header Widget Area', 'onschedule'),
                'id'            => 'edgtf-sticky-right',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-sticky-right">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the sticky menu', 'onschedule')
            ));
        }
    }

    add_action('widgets_init', 'onschedule_edge_register_sticky_header_areas');
}