<?php

/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
if(function_exists('vc_set_as_theme')) {
	vc_set_as_theme(true);
}

/**
 * Change path for overridden templates
 */
if(function_exists('vc_set_shortcodes_templates_dir')) {
	$dir = EDGE_ROOT_DIR . '/vc-templates';
	vc_set_shortcodes_templates_dir( $dir );
}

if ( ! function_exists('onschedule_edge_configure_visual_composer_frontend_editor') ) {
	/**
	 * Configuration for Visual Composer FrontEnd Editor
	 * Hooks on vc_after_init action
	 */
	function onschedule_edge_configure_visual_composer_frontend_editor() {
		/**
		 * Remove frontend editor
		 */
		if(function_exists('vc_disable_frontend')){
			vc_disable_frontend();
		}
	}

	add_action('vc_after_init', 'onschedule_edge_configure_visual_composer_frontend_editor');
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_Edgtf_Accordion extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Accordion_Tab extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Animation_Holder extends WPBakeryShortCodesContainer {}
	if(onschedule_edge_is_booked_calendar_installed()) {
		class WPBakeryShortCode_Edgtf_Booked_Slider extends WPBakeryShortCodesContainer {}
	}
	class WPBakeryShortCode_Edgtf_Clients_Carousel extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Elements_Holder extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Elements_Holder_Item extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Frame_Slider extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Image_Slider extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Info_Cards extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Item_Showcase extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Parallax extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Pricing_List extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Pricing_Tables extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Tabs extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Tab extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Vertical_Split_Slider extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Vertical_Split_Slider_Left_Panel extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Vertical_Split_Slider_Right_Panel extends WPBakeryShortCodesContainer {}
	class WPBakeryShortCode_Edgtf_Vertical_Split_Slider_Content_Item extends WPBakeryShortCodesContainer {}
}

if (!function_exists('onschedule_edge_vc_row_map')) {
	/**
	 * Map VC Row shortcode
	 * Hooks on vc_after_init action
	 */
	function onschedule_edge_vc_row_map() {

		vc_add_param('vc_row', array(
			'type' => 'dropdown',
			'param_name' => 'row_content_width',
			'heading' => esc_html__('Edge Row Content Width', 'onschedule'),
			'value' => array(
				esc_html__('Full Width', 'onschedule') => 'full-width',
				esc_html__('In Grid', 'onschedule') => 'grid'
			)
		));

		vc_add_param('vc_row', array(
			'type' => 'textfield',
			'param_name' => 'anchor',
			'heading' => esc_html__('Edge Anchor ID', 'onschedule'),
			'description' => esc_html__('For example "home"', 'onschedule')
		));
		
		vc_add_param('vc_row', array(
			'type' => 'textfield',
			'param_name' => 'anchor_icon',
			'heading' => esc_html__('Edge Anchor Link Icon', 'onschedule'),
			'description' => esc_html__('Fill anchor ID where you want to scroll content on icon click. For example "home"', 'onschedule')
		));
		
		vc_add_param('vc_row', array(
			'type' => 'dropdown',
			'param_name' => 'anchor_icon_skin',
			'heading' => esc_html__('Edge Anchor Icon Skin', 'onschedule'),
			'value' => array(
				esc_html__('Default', 'onschedule') => '',
				esc_html__('Light', 'onschedule') => 'edgtf-light'
			)
		));

		vc_add_param('vc_row', array(
			'type' => 'dropdown',
			'param_name' => 'content_text_aligment',
			'heading' => esc_html__('Edge Content Aligment', 'onschedule'),
			'value' => array(
				esc_html__('Default', 'onschedule') => '',
				esc_html__('Left', 'onschedule') => 'left',
				esc_html__('Center', 'onschedule') => 'center',
				esc_html__('Right', 'onschedule') => 'right'
			)
		));

		/*** Row Inner ***/

		vc_add_param('vc_row_inner', array(
			'type' => 'dropdown',
			'param_name' => 'row_content_width',
			'heading' => esc_html__('Edge Row Content Width', 'onschedule'),
			'value' => array(
				esc_html__('Full Width', 'onschedule') => 'full-width',
				esc_html__('In Grid', 'onschedule') => 'grid'
			)
		));

		vc_add_param('vc_row_inner', array(
			'type' => 'dropdown',
			'param_name' => 'content_text_aligment',
			'heading' => esc_html__('Edge Content Aligment', 'onschedule'),
			'value' => array(
				esc_html__('Default', 'onschedule') => '',
				esc_html__('Left', 'onschedule') => 'left',
				esc_html__('Center', 'onschedule') => 'center',
				esc_html__('Right', 'onschedule') => 'right'
			)
		));
	}

	add_action('vc_after_init', 'onschedule_edge_vc_row_map');
}