<?php

if (!function_exists('onschedule_edge_get_booked_calendars')) {
	/**
	 * Get Booked calendars
	 *
	 * @return array
	 */
	function onschedule_edge_get_booked_calendars() {
		global $wpdb;
		
		if(onschedule_edge_is_wpml_installed()) {
			$lang = ICL_LANGUAGE_CODE;
			
			$sql = "SELECT t.term_id AS id, t.name AS name 
				    FROM {$wpdb->prefix}terms t
				    INNER JOIN {$wpdb->prefix}term_taxonomy tt ON tt.term_id = t.term_id
				    INNER JOIN {$wpdb->prefix}icl_translations icl_t ON icl_t.element_id = t.term_id
				    WHERE icl_t.element_type = 'tax_booked_custom_calendars'
				    AND icl_t.language_code='$lang'
				    ORDER BY name ASC";
		} else {
			$sql = "SELECT t.term_id AS id, t.name AS name 
				    FROM {$wpdb->prefix}terms t
				    INNER JOIN {$wpdb->prefix}term_taxonomy tt ON tt.term_id = t.term_id
				    WHERE tt.taxonomy = 'booked_custom_calendars'
				    ORDER BY name ASC";
		}
		
		$calendars = array();
		$calendars_terms = $wpdb->get_results($sql);
		
		if(!empty($calendars_terms)) {
			$calendars = $calendars_terms;
		}
		
		return $calendars;
	}
}

if(!function_exists('onschedule_edge_get_booked_calendar_array')) {
	/**
	 * Get value array for visual composer shortcodes
	 *
	 * @return array
	 */
	function onschedule_edge_get_booked_calendar_array($enable_default = true) {
		$select_options = array();
		
		if($enable_default) {
			$select_options[''] = esc_html__('Default', 'onschedule');
		}
		
		$calendars = onschedule_edge_get_booked_calendars();
		if(!empty($calendars)) {
			foreach($calendars as $calendar) {
				$select_options[$calendar->id] = $calendar->name;
			}
		}
		
		return $select_options;
	}
}

if(!function_exists('onschedule_edge_get_booked_calendar_skins')) {
	/**
	 * Get value array for visual composer shortcodes
	 *
	 * @return array
	 */
	function onschedule_edge_get_booked_calendar_skins() {
		$skins = array(
			'default'             => esc_html__('Default', 'onschedule'),
			'light'               => esc_html__('Light', 'onschedule'),
			'dark'                => esc_html__('Dark', 'onschedule'),
			'dark-transparent-1'  => esc_html__('Dark Transparent 1', 'onschedule'),
			'dark-transparent-2'  => esc_html__('Dark Transparent 2', 'onschedule'),
			'yellow'              => esc_html__('Yellow', 'onschedule'),
			'teal'                => esc_html__('Teal', 'onschedule'),
			'green'               => esc_html__('Green', 'onschedule'),
			'blue'                => esc_html__('Blue', 'onschedule'),
			'red'                 => esc_html__('Red', 'onschedule'),
		);

		return $skins;
	}
}