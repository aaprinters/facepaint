<?php

if( !function_exists('onschedule_edge_load_search') ) {
    function onschedule_edge_load_search() {

        $search_type = 'fullscreen';
        $search_type = onschedule_edge_options()->getOptionValue('search_type');

        if ( onschedule_edge_active_widget( false, false, 'edgtf_search_opener' ) ) {
            include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/search/types/' . $search_type . '.php';
        }
    }

    add_action('init', 'onschedule_edge_load_search');
}