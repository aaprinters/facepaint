<?php

if ( ! function_exists('onschedule_edge_search_options_map') ) {

	function onschedule_edge_search_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_search_page',
				'title' => esc_html__('Search', 'onschedule'),
				'icon' => 'fa fa-search'
			)
		);

		$search_page_panel = onschedule_edge_add_admin_panel(
			array(
				'title' => esc_html__('Search Page', 'onschedule'),
				'name' => 'search_template',
				'page' => '_search_page'
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'search_page_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout', 'onschedule'),
            'description' 	=> esc_html__("Choose a sidebar layout for search page", 'onschedule'),
            'default_value' => 'no-sidebar',
            'options'       => array(
                'no-sidebar'        => esc_html__('No Sidebar', 'onschedule'),
                'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
                'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
                'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
                'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
            ),
			'parent'      => $search_page_panel
		));

        $onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
        if(count($onschedule_custom_sidebars) > 0) {
            onschedule_edge_add_admin_field(array(
                'name' => 'search_custom_sidebar_area',
                'type' => 'selectblank',
                'label' => esc_html__('Sidebar to Display', 'onschedule'),
                'description' => esc_html__('Choose a sidebar to display on search page. Default sidebar is "Sidebar"', 'onschedule'),
                'parent' => $search_page_panel,
                'options' => $onschedule_custom_sidebars
            ));
        }

		$search_panel = onschedule_edge_add_admin_panel(
			array(
				'title' => esc_html__('Search', 'onschedule'),
				'name' => 'search',
				'page' => '_search_page'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_type',
				'default_value'	=> 'fullscreen',
				'label' 		=> esc_html__('Select Search Type', 'onschedule'),
				'description' 	=> esc_html__("Choose a type of Select search bar (Note: Slide From Header Bottom search type doesn't work with Vertical Header)", 'onschedule'),
				'options' 		=> array(
					'fullscreen' => esc_html__('Fullscreen Search', 'onschedule'),
                    'covers-header' => esc_html__('Search Covers Header', 'onschedule')
				)
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_icon_pack',
				'default_value'	=> 'ion_icons',
				'label'			=> esc_html__('Search Icon Pack', 'onschedule'),
				'description'	=> esc_html__('Choose icon pack for search icon', 'onschedule'),
				'options'		=> onschedule_edge_icon_collections()->getIconCollectionsExclude(array('linea_icons'))
			)
		);

        onschedule_edge_add_admin_field(
            array(
                'parent'		=> $search_panel,
                'type'			=> 'yesno',
                'name'			=> 'search_in_grid',
                'default_value'	=> 'yes',
                'label'			=> esc_html__('Enable Grid Layout', 'onschedule'),
                'description'	=> esc_html__('Set search area to be in grid. (Applied for Search covers header and Slide from Window Top types.', 'onschedule'),
            )
        );
		
		onschedule_edge_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'initial_header_icon_title',
				'title'		=> esc_html__('Initial Search Icon in Header', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'text',
				'name'			=> 'header_search_icon_size',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Size', 'onschedule'),
				'description'	=> esc_html__('Set size for icon', 'onschedule'),
				'args'			=> array(
					'col_width' => 3,
					'suffix'	=> 'px'
				)
			)
		);
		
		$search_icon_color_group = onschedule_edge_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Colors', 'onschedule'),
				'description' => esc_html__('Define color style for icon', 'onschedule'),
				'name'		=> 'search_icon_color_group'
			)
		);
		
		$search_icon_color_row = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row'
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'	=> $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_color',
				'label'		=> esc_html__('Color', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_hover_color',
				'label'		=> esc_html__('Hover Color', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'enable_search_icon_text',
				'default_value'	=> 'no',
				'label'			=> esc_html__('Enable Search Icon Text', 'onschedule'),
				'description'	=> esc_html__("Enable this option to show 'Search' text next to search icon in header", 'onschedule'),
				'args'			=> array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_enable_search_icon_text_container'
				)
			)
		);
		
		$enable_search_icon_text_container = onschedule_edge_add_admin_container(
			array(
				'parent'			=> $search_panel,
				'name'				=> 'enable_search_icon_text_container',
				'hidden_property'	=> 'enable_search_icon_text',
				'hidden_value'		=> 'no'
			)
		);
		
		$enable_search_icon_text_group = onschedule_edge_add_admin_group(
			array(
				'parent'	=> $enable_search_icon_text_container,
				'title'		=> esc_html__('Search Icon Text', 'onschedule'),
				'name'		=> 'enable_search_icon_text_group',
				'description'	=> esc_html__('Define style for search icon text', 'onschedule')
			)
		);
		
		$enable_search_icon_text_row = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row'
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color',
				'label'			=> esc_html__('Text Color', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color_hover',
				'label'			=> esc_html__('Text Hover Color', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_font_size',
				'label'			=> esc_html__('Font Size', 'onschedule'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_line_height',
				'label'			=> esc_html__('Line Height', 'onschedule'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
		
		$enable_search_icon_text_row2 = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row2',
				'next'		=> true
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_text_transform',
				'label'			=> esc_html__('Text Transform', 'onschedule'),
				'default_value'	=> '',
				'options'		=> onschedule_edge_get_text_transform_array()
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_icon_text_google_fonts',
				'label'			=> esc_html__('Font Family', 'onschedule'),
				'default_value'	=> '-1',
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_font_style',
				'label'			=> esc_html__('Font Style', 'onschedule'),
				'default_value'	=> '',
				'options'		=> onschedule_edge_get_font_style_array(),
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_font_weight',
				'label'			=> esc_html__('Font Weight', 'onschedule'),
				'default_value'	=> '',
				'options'		=> onschedule_edge_get_font_weight_array(),
			)
		);
		
		$enable_search_icon_text_row3 = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row3',
				'next'		=> true
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row3,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_letter_spacing',
				'label'			=> esc_html__('Letter Spacing', 'onschedule'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
	}

	add_action('onschedule_edge_action_options_map', 'onschedule_edge_search_options_map', 16);
}