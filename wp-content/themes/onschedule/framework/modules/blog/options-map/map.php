<?php

if ( ! function_exists('onschedule_edge_blog_options_map') ) {

	function onschedule_edge_blog_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_blog_page',
				'title' => esc_html__('Blog', 'onschedule'),
				'icon' => 'fa fa-files-o'
			)
		);

		/**
		 * Blog Lists
		 */
		$panel_blog_lists = onschedule_edge_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_lists',
				'title' => esc_html__('Blog Lists', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'blog_list_type',
			'type'        => 'select',
			'label'       => esc_html__('Blog Layout for Archive Pages', 'onschedule'),
			'description' => esc_html__('Choose a default blog layout for archived blog post lists', 'onschedule'),
			'default_value' => 'standard',
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'masonry'   => esc_html__('Blog: Masonry', 'onschedule'),
                'standard'  => esc_html__('Blog: Standard', 'onschedule')
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'archive_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout for Archive Pages', 'onschedule'),
			'description' => esc_html__('Choose a sidebar layout for archived blog post lists', 'onschedule'),
			'default_value' => '',
			'parent'      => $panel_blog_lists,
			'options'     => array(
				''		            => esc_html__('Default', 'onschedule'),
				'no-sidebar'		=> esc_html__('No Sidebar', 'onschedule'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
			)
		));
		
		$onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
		if(count($onschedule_custom_sidebars) > 0) {
			onschedule_edge_add_admin_field(array(
				'name' => 'archive_custom_sidebar_area',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display for Archive Pages', 'onschedule'),
				'description' => esc_html__('Choose a sidebar to display on archived blog post lists. Default sidebar is "Sidebar Page"', 'onschedule'),
				'parent' => $panel_blog_lists,
				'options' => onschedule_edge_get_custom_sidebars()
			));
		}

        onschedule_edge_add_admin_field(array(
            'name'        => 'blog_masonry_layout',
            'type'        => 'select',
            'label'       => esc_html__('Masonry - Layout', 'onschedule'),
            'default_value' => 'in-grid',
            'description' => esc_html__('Set masonry layout. Default is in grid.', 'onschedule'),
            'parent'      => $panel_blog_lists,
            'options'     => array(
                'in-grid'    => esc_html__('In Grid', 'onschedule'),
                'full-width' => esc_html__('Full Width', 'onschedule')
            )
        ));
		
		onschedule_edge_add_admin_field(array(
			'name'        => 'blog_masonry_number_of_columns',
			'type'        => 'select',
			'label'       => esc_html__('Masonry - Number of Columns', 'onschedule'),
			'default_value' => 'four',
			'description' => esc_html__('Set number of columns for your masonry blog lists. Default value is 4 columns', 'onschedule'),
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'two'   => esc_html__('2 Columns', 'onschedule'),
				'three' => esc_html__('3 Columns', 'onschedule'),
				'four'  => esc_html__('4 Columns', 'onschedule'),
				'five'  => esc_html__('5 Columns', 'onschedule')
			)
		));
		
		onschedule_edge_add_admin_field(array(
			'name'        => 'blog_masonry_space_between_items',
			'type'        => 'select',
			'label'       => esc_html__('Masonry - Space Between Items', 'onschedule'),
			'default_value' => 'normal',
			'description' => esc_html__('Set space size between posts for your masonry blog lists. Default value is normal', 'onschedule'),
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'normal'  => esc_html__('Normal', 'onschedule'),
				'small'   => esc_html__('Small', 'onschedule'),
				'tiny'    => esc_html__('Tiny', 'onschedule'),
				'no'      => esc_html__('No Space', 'onschedule')
			)
		));

        onschedule_edge_add_admin_field(array(
            'name'        => 'blog_list_featured_image_proportion',
            'type'        => 'select',
            'label'       => esc_html__('Featured Image Proportion', 'onschedule'),
            'default_value' => 'fixed',
            'description' => esc_html__('Choose type of proportions you want to use for featured images on blog lists.', 'onschedule'),
            'parent'      => $panel_blog_lists,
            'options'     => array(
                'fixed'    => esc_html__('Fixed', 'onschedule'),
                'original' => esc_html__('Original', 'onschedule')
            )
        ));

		onschedule_edge_add_admin_field(array(
			'name'        => 'blog_pagination_type',
			'type'        => 'select',
			'label'       => esc_html__('Pagination Type', 'onschedule'),
			'description' => esc_html__('Choose a pagination layout for Blog Lists', 'onschedule'),
			'parent'      => $panel_blog_lists,
			'default_value' => 'standard',
			'options'     => array(
				'standard'		  => esc_html__('Standard', 'onschedule'),
				'load-more'		  => esc_html__('Load More', 'onschedule'),
				'infinite-scroll' => esc_html__('Infinite Scroll', 'onschedule'),
				'no-pagination'   => esc_html__('No Pagination', 'onschedule')
			)
		));
	
		onschedule_edge_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'number_of_chars',
				'default_value' => '40',
				'label' => esc_html__('Number of Words in Excerpt', 'onschedule'),
				'description' => esc_html__('Enter a number of words in excerpt (article summary). Default value is 40', 'onschedule'),
				'parent' => $panel_blog_lists,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		/**
		 * Blog Single
		 */
		$panel_blog_single = onschedule_edge_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_single',
				'title' => esc_html__('Blog Single', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'        => 'blog_single_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout', 'onschedule'),
			'description' => esc_html__('Choose a sidebar layout for Blog Single pages', 'onschedule'),
			'default_value'	=> '',
			'parent'      => $panel_blog_single,
			'options'     => array(
				''		            => esc_html__('Default', 'onschedule'),
				'no-sidebar'		=> esc_html__('No Sidebar', 'onschedule'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
			)
		));

		if(count($onschedule_custom_sidebars) > 0) {
			onschedule_edge_add_admin_field(array(
				'name' => 'blog_single_custom_sidebar_area',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'onschedule'),
				'description' => esc_html__('Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"', 'onschedule'),
				'parent' => $panel_blog_single,
				'options' => onschedule_edge_get_custom_sidebars()
			));
		}
		
		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'show_title_area_blog',
				'default_value' => '',
				'label'       => esc_html__('Show Title Area', 'onschedule'),
				'description' => esc_html__('Enabling this option will show title area on single post pages', 'onschedule'),
				'parent'      => $panel_blog_single,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'yes' => esc_html__('Yes', 'onschedule'),
                    'no' => esc_html__('No', 'onschedule')
                ),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'          => 'blog_single_title_in_title_area',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Post Title in Title Area', 'onschedule'),
			'description'   => esc_html__('Enabling this option will show post title in title area on single post pages', 'onschedule'),
			'parent'        => $panel_blog_single,
			'default_value' => 'no'
		));

		onschedule_edge_add_admin_field(array(
			'name'			=> 'blog_single_related_posts',
			'type'			=> 'yesno',
			'label'			=> esc_html__('Show Related Posts', 'onschedule'),
			'description'   => esc_html__('Enabling this option will show related posts on single post pages', 'onschedule'),
			'parent'        => $panel_blog_single,
			'default_value' => 'yes'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'blog_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments Form', 'onschedule'),
			'description'   => esc_html__('Enabling this option will show comments form on single post pages', 'onschedule'),
			'parent'        => $panel_blog_single,
			'default_value' => 'yes'
		));

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_navigation',
				'default_value' => 'no',
				'label' => esc_html__('Enable Prev/Next Single Post Navigation Links', 'onschedule'),
				'description' => esc_html__('Enable navigation links through the blog posts (left and right arrows will appear)', 'onschedule'),
				'parent' => $panel_blog_single,
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_edgtf_blog_single_navigation_container'
				)
			)
		);

		$blog_single_navigation_container = onschedule_edge_add_admin_container(
			array(
				'name' => 'edgtf_blog_single_navigation_container',
				'hidden_property' => 'blog_single_navigation',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_navigation_through_same_category',
				'default_value' => 'no',
				'label'       => esc_html__('Enable Navigation Only in Current Category', 'onschedule'),
				'description' => esc_html__('Limit your navigation only through current category', 'onschedule'),
				'parent'      => $blog_single_navigation_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_author_info',
				'default_value' => 'yes',
				'label' => esc_html__('Show Author Info Box', 'onschedule'),
				'description' => esc_html__('Enabling this option will display author name and descriptions on single post pages', 'onschedule'),
				'parent' => $panel_blog_single,
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_edgtf_blog_single_author_info_container'
				)
			)
		);

		$blog_single_author_info_container = onschedule_edge_add_admin_container(
			array(
				'name' => 'edgtf_blog_single_author_info_container',
				'hidden_property' => 'blog_author_info',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_author_info_email',
				'default_value' => 'no',
				'label'       => esc_html__('Show Author Email', 'onschedule'),
				'description' => esc_html__('Enabling this option will show author email', 'onschedule'),
				'parent'      => $blog_single_author_info_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_author_social',
				'default_value' => 'yes',
				'label'       => esc_html__('Show Author Social Icons', 'onschedule'),
				'description' => esc_html__('Enabling this option will show author social icons on single post pages', 'onschedule'),
				'parent'      => $blog_single_author_info_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_blog_options_map', 13);
}