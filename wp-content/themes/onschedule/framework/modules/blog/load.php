<?php

include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/blog/options-map/map.php';
include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/blog/blog-functions.php';

/* Loading shortcodes */
include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/blog/shortcodes/blog-list/blog-list.php';
include_once EDGE_FRAMEWORK_MODULES_ROOT_DIR.'/blog/shortcodes/blog-slider/blog-slider.php';