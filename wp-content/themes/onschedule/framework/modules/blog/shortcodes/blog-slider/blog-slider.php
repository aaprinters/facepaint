<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\BlogSlider;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class BlogList
 */
class BlogSlider implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'edgtf_blog_slider';

        add_action('vc_before_init', array($this,'vcMap'));

        //Category filter
        add_filter( 'vc_autocomplete_edgtf_blog_slider_category_callback', array( &$this, 'blogListCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

        //Category render
        add_filter( 'vc_autocomplete_edgtf_blog_slider_category_render', array( &$this, 'blogListCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array

    }

    public function getBase() {
        return $this->base;
    }
    
    public function vcMap() {
        vc_map( array(
            'name' => esc_html__('Edge Blog Slider', 'onschedule'),
            'base' => $this->base,
            'icon' => 'icon-wpb-blog-slider extended-custom-icon',
            'category' => esc_html__('by EDGE', 'onschedule'),
            'allowed_container_element' => 'vc_row',
            'params' => array(
                array(
                    'type'       => 'textfield',
                    'param_name' => 'number_of_posts',
                    'heading'    => esc_html__('Number of Posts', 'onschedule'),
                ),
                array(
                    'type'       => 'dropdown',
                    'param_name' => 'order_by',
                    'heading'    => esc_html__('Order By', 'onschedule'),
                    'value'      => array(
	                    esc_html__('Title', 'onschedule') => 'title',
	                    esc_html__('Date', 'onschedule') => 'date',
	                    esc_html__('Random', 'onschedule') => 'rand',
	                    esc_html__('Post Name', 'onschedule') => 'name'
                    )
                ),
                array(
                    'type'       => 'dropdown',
                    'param_name' => 'order',
                    'heading'    => esc_html__('Order', 'onschedule'),
                    'value'      => array(
                        esc_html__('ASC', 'onschedule') => 'ASC',
                        esc_html__('DESC', 'onschedule') => 'DESC'
                    )
                ),
                array(
                    'type'        => 'autocomplete',
                    'param_name'  => 'category',
                    'heading'     => esc_html__('Category', 'onschedule'),
                    'description' => esc_html__('Enter one category slug (leave empty for showing all categories)', 'onschedule')
                ),
                array(
                    'type'       => 'dropdown',
                    'param_name' => 'image_size',
                    'heading'    => esc_html__('Image Size', 'onschedule'),
                    'value'      => array(
                        esc_html__('Original', 'onschedule') => 'full',
                        esc_html__('Square', 'onschedule') => 'square',
                        esc_html__('Landscape', 'onschedule') => 'landscape',
                        esc_html__('Portrait', 'onschedule') => 'portrait',
                        esc_html__('Medium', 'onschedule') => 'medium',
                        esc_html__('Large', 'onschedule') => 'large'
                    )
                ),
                array(
                    'type'       => 'dropdown',
                    'param_name' => 'title_tag',
                    'heading'    => esc_html__('Title Tag', 'onschedule'),
                    'value'      => array_flip(onschedule_edge_get_title_tag(true)),
                    'group'      => esc_html__('Post Info', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'post_info_author',
                    'heading'     => esc_html__('Enable Post Info Author', 'onschedule'),
                    'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
                    'dependency'  => Array('element' => 'post_info_section', 'value' => array('yes')),
                    'group'       => esc_html__('Post Info', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'post_info_date',
                    'heading'     => esc_html__('Enable Post Info Date', 'onschedule'),
                    'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
                    'dependency'  => Array('element' => 'post_info_section', 'value' => array('yes')),
                    'group'       => esc_html__('Post Info', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'post_info_category',
                    'heading'     => esc_html__('Enable Post Info Category', 'onschedule'),
                    'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false, true)),
                    'dependency'  => Array('element' => 'post_info_section', 'value' => array('yes')),
                    'group'       => esc_html__('Post Info', 'onschedule')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'post_info_comments',
                    'heading'     => esc_html__('Enable Post Info Comments', 'onschedule'),
                    'value'       => array_flip(onschedule_edge_get_yes_no_select_array(false)),
                    'dependency'  => Array('element' => 'post_info_section', 'value' => array('yes')),
                    'group'       => esc_html__('Post Info', 'onschedule')
                )
            )
        ) );
    }
    
    public function render($atts, $content = null) {
        $default_atts = array(
            'number_of_posts'    => '-1',
            'order_by'           => 'title',
            'order'              => 'ASC',
            'category'           => '',
            'image_size'         => 'full',
            'title_tag'          => 'h2',
            'post_info_author'   => 'yes',
            'post_info_date'     => 'yes',
            'post_info_category' => 'yes',
            'post_info_comments' => 'yes'
        );

        $params = shortcode_atts($default_atts, $atts);
        extract($params);

        $queryArray = $this->generateBlogQueryArray($params);
        $query_result = new \WP_Query($queryArray);
        $params['query_result'] = $query_result;

        $params['thumb_image_size'] = $this->generateImageSize($params);

        ob_start();

        onschedule_edge_get_module_template_part('shortcodes/blog-slider/holder', 'blog', '', $params);

        $html = ob_get_contents();

        ob_end_clean();

        return $html;
    }

    /**
     * Generates query array
     *
     * @param $params
     *
     * @return array
     */
    public function generateBlogQueryArray($params){
        $queryArray = array(
            'post_status'    => 'publish',
            'post_type'      => 'post',
            'orderby'        => $params['order_by'],
            'order'          => $params['order'],
            'posts_per_page' => $params['number_of_posts'],
            'post__not_in'   => get_option('sticky_posts')
        );

        if(!empty($params['category'])){
            $queryArray['category_name'] = $params['category'];
        }

        return $queryArray;
    }

    /**
     * Generates image size option
     *
     * @param $params
     *
     * @return string
     */
    private function generateImageSize($params){
        $thumb_size = '';
        $image_size = $params['image_size'];

        switch ($image_size) {
            case 'landscape':
                $thumb_size = 'onschedule_edge_landscape';
                break;
            case 'portrait':
                $thumb_size = 'onschedule_edge_portrait';
                break;
            case 'square':
                $thumb_size = 'onschedule_edge_square';
                break;
            case 'medium':
                $thumb_size = 'medium';
                break;
            case 'large':
                $thumb_size = 'large';
                break;
            case 'full':
                $thumb_size = 'full';
                break;
        }

        return $thumb_size;
    }

    /**
     * Filter categories
     *
     * @param $query
     *
     * @return array
     */
    public function blogListCategoryAutocompleteSuggester( $query ) {
        global $wpdb;
        $post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

        $results = array();
        if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
            foreach ( $post_meta_infos as $value ) {
                $data          = array();
                $data['value'] = $value['slug'];
                $data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'onschedule' ) . ': ' . $value['category_title'] : '' );
                $results[]     = $data;
            }
        }

        return $results;
    }

    /**
     * Find categories by slug
     * @since 4.4
     *
     * @param $query
     *
     * @return bool|array
     */
    public function blogListCategoryAutocompleteRender( $query ) {
        $query = trim( $query['value'] ); // get value from requested
        if ( ! empty( $query ) ) {
            // get category
            $category = get_term_by( 'slug', $query, 'category' );
            if ( is_object( $category ) ) {

                $category_slug = $category->slug;
                $category_title = $category->name;

                $category_title_display = '';
                if ( ! empty( $category_title ) ) {
                    $category_title_display = esc_html__( 'Category', 'onschedule' ) . ': ' . $category_title;
                }

                $data          = array();
                $data['value'] = $category_slug;
                $data['label'] = $category_title_display;

                return ! empty( $data ) ? $data : false;
            }

            return false;
        }

        return false;
    }
}
