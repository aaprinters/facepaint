<li class="edgtf-bl-item clearfix">
	<div class="edgtf-bli-inner">
        <?php if( $featured_image === 'yes' ) {
	        onschedule_edge_get_module_template_part('templates/parts/image', 'blog', '', $params);
        } ?>
        <div class="edgtf-bli-content">
	        <?php if($post_info_section == 'yes' && $post_info_date == 'yes') { ?>
		        <div class="edgtf-bli-info-top">
			        <?php onschedule_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $params); ?>
		        </div>
	        <?php } ?>
	        <?php onschedule_edge_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
	        <?php if($post_info_section == 'yes') { ?>
		        <div class="edgtf-bli-info-middle">
			        <?php
			        if ($post_info_category == 'yes') {
				        onschedule_edge_get_module_template_part('templates/parts/post-info/category', 'blog', '', $params);
			        }
			        if ($post_info_author == 'yes') {
				        onschedule_edge_get_module_template_part('templates/parts/post-info/author', 'blog', '', $params);
			        }
			        if ($post_info_comments == 'yes') {
				        onschedule_edge_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $params);
			        }
			        if ($post_info_share == 'yes') {
				        onschedule_edge_get_module_template_part('templates/parts/post-info/share', 'blog', '', $params);
			        }
			        ?>
		        </div>
	        <?php } ?>
            <div class="edgtf-bli-excerpt">
                <?php onschedule_edge_get_module_template_part('templates/parts/excerpt', 'blog', 'masonry', $params); ?>
            </div>
        </div>
	</div>
</li>