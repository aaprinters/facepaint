<li class="edgtf-bl-item clearfix">
	<div class="edgtf-bli-inner">
        <?php onschedule_edge_get_module_template_part('templates/parts/image', 'blog', '', $params); ?>
		
		<div class="edgtf-bli-content">
            <?php onschedule_edge_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
			<?php onschedule_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $params); ?>
		</div>
	</div>
</li>