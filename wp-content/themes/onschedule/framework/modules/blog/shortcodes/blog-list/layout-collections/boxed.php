<li class="edgtf-bl-item clearfix">
	<div class="edgtf-bli-inner">
        <?php onschedule_edge_get_module_template_part('templates/parts/image', 'blog', '', $params); ?>
        <div class="edgtf-bli-content">
	        <div class="edgtf-bli-outer">
		        <div class="edgtf-bli-wrapper">
			        <?php if($post_info_section == 'yes' && $post_info_date == 'yes') { ?>
				        <div class="edgtf-bli-info-top">
					        <?php onschedule_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $params); ?>
				        </div>
			        <?php } ?>
			        <?php onschedule_edge_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
			        <?php if($post_info_section == 'yes') { ?>
				        <div class="edgtf-bli-info-middle">
					        <?php
					        if ($post_info_category == 'yes') {
						        onschedule_edge_get_module_template_part('templates/parts/post-info/category', 'blog', '', $params);
					        }
					        if ($post_info_author == 'yes') {
						        onschedule_edge_get_module_template_part('templates/parts/post-info/author', 'blog', '', $params);
					        }
					        if ($post_info_comments == 'yes') {
						        onschedule_edge_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $params);
					        }
					        if ($post_info_share == 'yes') {
						        onschedule_edge_get_module_template_part('templates/parts/post-info/share', 'blog', '', $params);
					        }
					        ?>
				        </div>
			        <?php } ?>
		            <?php
		                $params['boxed_button_type'] = 'solid';
		                $params['boxed_button_size'] = 'small';
		                onschedule_edge_get_module_template_part('templates/parts/post-info/read-more', 'blog', '', $params); ?>
		        </div>
	        </div>
        </div>
		<?php if(onschedule_edge_blog_item_has_link()) { ?>
			<a itemprop="url" class="edgtf-bli-link" href="<?php echo get_the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
		<?php } ?>
	</div>
</li>