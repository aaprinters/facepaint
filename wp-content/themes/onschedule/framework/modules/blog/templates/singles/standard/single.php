<?php

onschedule_edge_get_single_post_format_html($blog_single_type);

onschedule_edge_get_module_template_part('templates/parts/single/author-info', 'blog');

onschedule_edge_get_module_template_part('templates/parts/single/single-navigation', 'blog');

onschedule_edge_get_module_template_part('templates/parts/single/related-posts', 'blog', '', $single_info_params);

onschedule_edge_get_module_template_part('templates/parts/single/comments', 'blog');