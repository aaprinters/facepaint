<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<?php onschedule_edge_get_module_template_part( 'templates/parts/post-type/quote', 'blog', 'single', $part_params ); ?>
		<div class="edgtf-post-text">
			<div class="edgtf-post-text-inner">
				<div class="edgtf-post-info-top">
					<?php onschedule_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $part_params); ?>
				</div>
				<div class="edgtf-post-text-main">
					<?php onschedule_edge_get_module_template_part('templates/parts/title', 'blog', '', $part_params); ?>
					<div class="edgtf-post-info-middle">
						<?php onschedule_edge_get_module_template_part('templates/parts/post-info/category', 'blog', '', $part_params); ?>
						<?php onschedule_edge_get_module_template_part('templates/parts/post-info/author', 'blog', '', $part_params); ?>
						<?php onschedule_edge_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $part_params); ?>
						<?php onschedule_edge_get_module_template_part('templates/parts/post-info/tags', 'blog', '', $part_params); ?>
					</div>
					<?php the_content(); ?>
					<?php do_action('onschedule_edge_single_link_pages'); ?>
				</div>
				<div class="edgtf-post-info-bottom">
					<?php onschedule_edge_get_module_template_part('templates/parts/post-info/share', 'blog', '', $part_params); ?>
				</div>
			</div>
		</div>
	</div>
</article>