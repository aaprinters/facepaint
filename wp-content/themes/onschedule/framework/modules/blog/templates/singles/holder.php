<div class="edgtf-columns-wrapper <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-columns-inner">
		<div class="edgtf-column-content edgtf-column-content1">
			<div class="edgtf-blog-holder edgtf-blog-single">
				<?php onschedule_edge_get_blog_single_type($blog_single_type); ?>
			</div>
		</div>
		<?php if($sidebar_layout !== 'no-sidebar') { ?>
			<div class="edgtf-column-content edgtf-column-content2">
				<?php get_sidebar(); ?>
			</div>
		<?php } ?>
	</div>
</div>