<div class="edgtf-post-info-comments-holder">
    <a itemprop="url" class="edgtf-post-info-comments" href="<?php comments_link(); ?>" target="_self">
        <?php comments_number('0 ' . esc_html__('Comments','onschedule'), '1 '.esc_html__('Comment','onschedule'), '% '.esc_html__('Comments','onschedule') ); ?>
    </a>
</div>