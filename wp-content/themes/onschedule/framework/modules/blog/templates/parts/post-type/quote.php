<?php
$title_tag = isset($quote_tag) ? $quote_tag : 'h4';

$post_quote_meta = get_post_meta(get_the_ID(), "edgtf_post_quote_text_meta", true );

$post_title = !empty($post_quote_meta) ? $post_quote_meta : get_the_title();
$post_author = get_post_meta(get_the_ID(), "edgtf_post_quote_author_meta", true );
?>

<div class="edgtf-post-quote-holder">
    <span class="edgtf-quote-icon">
        <i class="edgtf-icon-simple-line-icon icon-bubbles"></i>
    </span>
    <<?php echo esc_attr($title_tag);?> itemprop="name" class="edgtf-quote-title edgtf-post-title">
		<?php echo esc_html($post_title); ?>
    </<?php echo esc_attr($title_tag);?>>
	<?php if(!empty($post_author)) { ?>
		<span class="edgtf-quote-author">
            <?php echo esc_html($post_author); ?>
        </span>
	<?php } ?>
	<?php if(onschedule_edge_blog_item_has_link()) { ?>
		<a itemprop="url" class="edgtf-post-quote-link" href="<?php echo get_the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
	<?php } ?>
</div>