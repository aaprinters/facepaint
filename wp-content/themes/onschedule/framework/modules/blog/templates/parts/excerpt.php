<?php
$excerpt_length = isset($params['excerpt_length']) ? $params['excerpt_length'] : '';
$excerpt = onschedule_edge_excerpt($excerpt_length);
?>
<?php if($excerpt !== '') { ?>
    <div class="edgtf-post-excerpt-holder">
        <p itemprop="description" class="edgtf-post-excerpt"><?php echo onschedule_edge_get_module_part($excerpt); ?></p>
    </div>
<?php } ?>
