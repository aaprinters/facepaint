<?php
$title_tag = isset($link_tag) ? $link_tag : 'h4';
$post_link_meta = get_post_meta(get_the_ID(), "edgtf_post_link_link_meta", true );
?>

<div class="edgtf-post-link-holder">
	<span class="edgtf-link-icon">
        <i class="edgtf-icon-simple-line-icon icon-link"></i>
    </span>
    <<?php echo esc_attr($title_tag);?> itemprop="name" class="edgtf-link-title edgtf-post-title">
        <?php echo get_the_title(); ?>
    </<?php echo esc_attr($title_tag);?>>
	<?php if(isset($post_link_meta) && $post_link_meta != '') { ?>
		<a itemprop="url" class="edgtf-post-link-link" href="<?php echo esc_url($post_link_meta); ?>" title="<?php the_title_attribute(); ?>" target="_blank"></a>
	<?php } ?>
</div>