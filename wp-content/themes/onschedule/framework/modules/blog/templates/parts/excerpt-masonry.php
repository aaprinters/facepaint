<?php
$excerpt_length = isset($params['excerpt_length']) ? $params['excerpt_length'] : '';
$excerpt = onschedule_edge_excerpt($excerpt_length);
?>
<?php if($excerpt !== '') { ?>
    <div class="edgtf-post-excerpt-holder">
        <p itemprop="description" class="edgtf-post-excerpt">
	        <?php echo onschedule_edge_get_module_part($excerpt); ?>
	        <?php if(onschedule_edge_core_plugin_installed()) {
		        $button_type = isset($boxed_button_type) && !empty($boxed_button_type) ? $boxed_button_type : 'simple';
		        $button_size = isset($boxed_button_size) && !empty($boxed_button_size) ? $boxed_button_size : 'medium';
		        echo onschedule_edge_get_button_html(
			        apply_filters(
				        'onschedule_edge_filter_blog_masonry_template_read_more_button',
				        array(
					        'type' => $button_type,
					        'size' => $button_size,
					        'link' => get_the_permalink(),
					        'text' => esc_html__('READ MORE', 'onschedule'),
					        'custom_class' => 'edgtf-blog-list-button'
				        )
			        )
		        );
	        } ?>
        </p>
    </div>
<?php } ?>