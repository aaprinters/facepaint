<div class="edgtf-post-read-more-button">
<?php
    if(onschedule_edge_core_plugin_installed()) {
        $button_type = isset($boxed_button_type) && !empty($boxed_button_type) ? $boxed_button_type : 'simple';
	    $button_size = isset($boxed_button_size) && !empty($boxed_button_size) ? $boxed_button_size : 'medium';
        echo onschedule_edge_get_button_html(
            apply_filters(
                'onschedule_edge_filter_blog_template_read_more_button',
                array(
                    'type' => $button_type,
                    'size' => $button_size,
                    'link' => get_the_permalink(),
                    'text' => esc_html__('READ MORE', 'onschedule'),
                    'custom_class' => 'edgtf-blog-list-button'
                )
            )
        );
    } else { ?>
        <a itemprop="url" href="<?php echo esc_url(get_the_permalink()); ?>" target="_self" class="edgtf-btn edgtf-btn-medium edgtf-btn-simple edgtf-blog-list-button">
            <span class="edgtf-btn-text"><?php echo esc_html__('READ MORE', 'onschedule'); ?></span>
            <i class="edgtf-icon-ion-icon ion-android-arrow-forward"></i>
        </a>
<?php } ?>
</div>
