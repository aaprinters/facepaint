<?php
$blog_single_navigation = onschedule_edge_options()->getOptionValue('blog_single_navigation') === 'no' ? false : true;
$blog_navigation_through_same_category = onschedule_edge_options()->getOptionValue('blog_navigation_through_same_category') === 'no' ? false : true;
?>
<?php if($blog_single_navigation){ ?>
	<div class="edgtf-blog-single-navigation">
		<?php
		/* Single navigation section - SETTING PARAMS */
		$post_navigation = array(
			'prev' => array(
				'mark' => '<span class="edgtf-blog-single-nav-mark icon-arrows-left"></span><span class="edgtf-blog-single-nav-label">'.esc_html__('Previous', 'onschedule').'</span>'
			),
			'next' => array(
				'mark' => '<span class="edgtf-blog-single-nav-label">'.esc_html__('Next', 'onschedule').'</span><span class="edgtf-blog-single-nav-mark icon-arrows-right"></span>'
			)
		);
		
		if(get_previous_post() !== ""){
			if($blog_navigation_through_same_category){
				if(get_previous_post(true) !== ""){
					$post_navigation['prev']['post'] = get_previous_post(true);
				}
			} else {
				if(get_previous_post() !== ""){
					$post_navigation['prev']['post'] = get_previous_post();
				}
			}
		}
		if(get_next_post() != ""){
			if($blog_navigation_through_same_category){
				if(get_next_post(true) !== ""){
					$post_navigation['next']['post'] = get_next_post(true);
				}
			} else {
				if(get_next_post() !== ""){
					$post_navigation['next']['post'] = get_next_post();
				}
			}
		}
		
		/* Single navigation section - RENDERING */
		if (isset($post_navigation['prev']['post']) || isset($post_navigation['next']['post'])) {
			foreach (array('prev', 'next') as $nav_type) {
				$post_id = isset( $post_navigation[$nav_type]['post'] ) ? $post_navigation[$nav_type]['post']->ID : null;
				$post_thumb = get_the_post_thumbnail($post_id);
				?>
				<div class="edgtf-blog-single-<?php echo onschedule_edge_get_module_part($nav_type); ?>">
					<?php if (isset($post_navigation[$nav_type]['post'])) { ?>
						<?php if($nav_type === 'prev' && !empty($post_thumb)) { ?>
							<div class="edgtf-blog-single-nav-image">
								<?php echo get_the_post_thumbnail($post_id, array(109, 0)); ?>
							</div>
						<?php } ?>
						<a itemprop="url" class="edgtf-blog-single-icon" href="<?php echo esc_url(get_permalink($post_navigation[$nav_type]['post']->ID)); ?>">
							<?php echo onschedule_edge_get_module_part($post_navigation[$nav_type]['mark']); ?>
						</a>
						<?php if($nav_type === 'next' && !empty($post_thumb)) { ?>
							<div class="edgtf-blog-single-nav-image">
								<?php echo get_the_post_thumbnail($post_id, array(109, 0)); ?>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				<?php
			}
		}
		?>
	</div>
<?php } ?>
