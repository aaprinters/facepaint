<?php
$share_type = isset($share_type) ? $share_type : 'list';
?>
<?php if(onschedule_edge_options()->getOptionValue('enable_social_share') === 'yes' && onschedule_edge_options()->getOptionValue('enable_social_share_on_post') === 'yes') { ?>
    <div class="edgtf-blog-share">
	    <span class="edgtf-blog-share-label"><?php echo esc_html__('Share', 'onschedule'); ?></span>
        <?php echo onschedule_edge_get_social_share_html(array('type' => $share_type)); ?>
    </div>
<?php } ?>