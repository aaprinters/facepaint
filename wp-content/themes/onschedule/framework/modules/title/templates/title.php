<?php do_action('onschedule_edge_action_before_page_title'); 
$title_background_image_src1 = "http://dev.kumailenterprises.com/wp-content/uploads/2020/05/title_bg1.png"; ?>

<div class="edgtf-title <?php echo onschedule_edge_title_classes(); ?>" style="background-color: black;height: 249px;background-position-y: 48%;background-size: 100% auto;background-image: url(<?php echo $title_background_image_src1 ?>); <?php echo esc_attr($title_background_color); //echo esc_attr($title_background_image); ?>" data-height="<?php echo esc_attr(intval(preg_replace('/[^0-9]+/', '', $title_height), 10));?>" <?php echo esc_attr($title_background_image_width);
 ?>>
    <?php if(!empty($title_background_image_src)) { ?>
        <div class="edgtf-title-image">
            <img itemprop="image" src="<?php echo esc_url($title_background_image_src); ?>" alt="<?php esc_attr_e('Title Image', 'onschedule'); ?>" />
        </div>
    <?php } ?>
    <div class="edgtf-title-holder" <?php onschedule_edge_inline_style($title_holder_height); ?>>
        <div class="edgtf-container clearfix">
            <div class="edgtf-container-inner">
                <div class="edgtf-title-subtitle-holder" style="<?php echo esc_attr($title_subtitle_holder_padding); ?>">
                    <div class="edgtf-title-subtitle-holder-inner">
                        <?php switch ($type){
                            case 'standard': ?>
                                
                                <div class="title_text" style="display: none;">

                                <?php if(onschedule_edge_get_title_text() !== '') { ?>
                                    <<?php echo esc_attr($title_tag); ?> class="edgtf-page-title entry-title" <?php onschedule_edge_inline_style($title_color); ?>><span><?php onschedule_edge_title_text(); ?></span></<?php echo esc_attr($title_tag); ?>>
                                <?php } ?>
                                <?php if($has_subtitle){ ?>
                                    <span class="edgtf-subtitle" <?php onschedule_edge_inline_style($subtitle_styles); ?>><span><?php onschedule_edge_subtitle_text(); ?></span></span>
                                <?php } ?>
                                <?php if($enable_breadcrumbs){ ?>
                                    <div class="edgtf-breadcrumbs-holder"> <?php //onschedule_edge_custom_breadcrumbs(); ?></div>
                                <?php } ?>

                                </div>

                                


                            <?php break;
                            case 'breadcrumb': ?>
                                <div class="edgtf-breadcrumbs-holder"> <?php //onschedule_edge_custom_breadcrumbs(); ?></div>
                            <?php break;
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php do_action('onschedule_edge_action_after_page_title'); ?>
