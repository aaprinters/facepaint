<div class="edgtf-columns-wrapper edgtf-content-columns-66-33 edgtf-content-sidebar-right edgtf-content-has-sidebar edgtf-columns-normal-space">
	<div class="edgtf-columns-inner">
		<div class="edgtf-column-content edgtf-column-content1">
			<div class="edgtf-ps-image-holder">
				<div class="edgtf-ps-image-inner edgtf-owl-slider">
					<?php
					$media = onschedule_edge_get_portfolio_single_media();
					
					if(is_array($media) && count($media)) : ?>
						<?php foreach($media as $single_media) : ?>
							<div class="edgtf-ps-image">
								<?php onschedule_edge_portfolio_get_media_html($single_media); ?>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="edgtf-column-content edgtf-column-content2">
			<div class="edgtf-ps-info-holder">
				<?php
				//get portfolio content section
				onschedule_edge_get_module_template_part('templates/single/parts/content', 'portfolio', $item_layout);
				
				//get portfolio custom fields section
				onschedule_edge_get_module_template_part('templates/single/parts/custom-fields', 'portfolio', $item_layout);
				
				//get portfolio categories section
				onschedule_edge_get_module_template_part('templates/single/parts/categories', 'portfolio', $item_layout);
				
				//get portfolio date section
				onschedule_edge_get_module_template_part('templates/single/parts/date', 'portfolio', $item_layout);
				
				//get portfolio tags section
				onschedule_edge_get_module_template_part('templates/single/parts/tags', 'portfolio', $item_layout);
				
				//get portfolio share section
				onschedule_edge_get_module_template_part('templates/single/parts/social', 'portfolio', $item_layout);
				?>
			</div>
		</div>
	</div>
</div>