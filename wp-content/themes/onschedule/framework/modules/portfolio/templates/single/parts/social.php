<?php if(onschedule_edge_options()->getOptionValue('enable_social_share') == 'yes' && onschedule_edge_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
    <div class="edgtf-ps-info-item edgtf-ps-social-share">
        <?php echo onschedule_edge_get_social_share_html() ?>
    </div>
<?php endif; ?>