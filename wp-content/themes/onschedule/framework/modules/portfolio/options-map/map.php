<?php

if ( ! function_exists('onschedule_edge_portfolio_options_map') ) {
	function onschedule_edge_portfolio_options_map() {

		onschedule_edge_add_admin_page(array(
			'slug'  => '_portfolio',
			'title' => esc_html__('Portfolio', 'onschedule'),
			'icon'  => 'fa fa-camera-retro'
		));

		$panel_archive = onschedule_edge_add_admin_panel(array(
			'title' => esc_html__('Portfolio Archive', 'onschedule'),
			'name'  => 'panel_portfolio_archive',
			'page'  => '_portfolio'
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_archive_number_of_items',
			'type'        => 'text',
			'label'       => esc_html__('Number of Items', 'onschedule'),
			'description' => esc_html__('Set number of items for your portfolio list on archive pages. Default value is 12', 'onschedule'),
			'parent'      => $panel_archive,
			'args'        => array(
				'col_width' => 3
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_archive_number_of_columns',
			'type'        => 'select',
			'label'       => esc_html__('Number of Columns', 'onschedule'),
			'default_value' => '4',
			'description' => esc_html__('Set number of columns for your portfolio list on archive pages. Default value is 4 columns', 'onschedule'),
			'parent'      => $panel_archive,
			'options'     => array(
				'2' => esc_html__('2 Columns', 'onschedule'),
				'3' => esc_html__('3 Columns', 'onschedule'),
				'4' => esc_html__('4 Columns', 'onschedule'),
				'5' => esc_html__('5 Columns', 'onschedule')
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_archive_space_between_items',
			'type'        => 'select',
			'label'       => esc_html__('Space Between Items', 'onschedule'),
			'default_value' => 'normal',
			'description' => esc_html__('Set space size between portfolio items for your portfolio list on archive pages. Default value is normal', 'onschedule'),
			'parent'      => $panel_archive,
			'options'     => array(
				'normal'    => esc_html__('Normal', 'onschedule'),
				'small'     => esc_html__('Small', 'onschedule'),
				'tiny'      => esc_html__('Tiny', 'onschedule'),
				'no'        => esc_html__('No Space', 'onschedule')
			)
		));

		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_archive_image_size',
			'type'        => 'select',
			'label'       => esc_html__('Image Proportions', 'onschedule'),
			'default_value' => 'landscape',
			'description' => esc_html__('Set image proportions for your portfolio list on archive pages. Default value is landscape', 'onschedule'),
			'parent'      => $panel_archive,
			'options'     => array(
				'full'      => esc_html__('Original', 'onschedule'),
				'landscape' => esc_html__('Landscape', 'onschedule'),
				'portrait'  => esc_html__('Portrait', 'onschedule'),
				'square'    => esc_html__('Square', 'onschedule')
			)
		));
		
		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_archive_item_layout',
			'type'        => 'select',
			'label'       => esc_html__('Item Style', 'onschedule'),
			'default_value' => 'standard-shader',
			'description' => esc_html__('Set item style for your portfolio list on archive pages. Default value is Standard - Shader', 'onschedule'),
			'parent'      => $panel_archive,
			'options'     => array(
				'standard-shader' => esc_html__('Standard - Shader', 'onschedule'),
				'gallery-overlay' => esc_html__('Gallery - Overlay', 'onschedule')
			)
		));

		$panel = onschedule_edge_add_admin_panel(array(
			'title' => esc_html__('Portfolio Single', 'onschedule'),
			'name'  => 'panel_portfolio_single',
			'page'  => '_portfolio'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_template',
			'type'          => 'select',
			'label'         => esc_html__('Portfolio Type', 'onschedule'),
			'default_value'	=> 'small-images',
			'description'   => esc_html__('Choose a default type for Single Project pages', 'onschedule'),
			'parent'        => $panel,
			'options'       => array(
				'images'            => esc_html__('Portfolio Images', 'onschedule'),
				'small-images'      => esc_html__('Portfolio Small Images', 'onschedule'),
				'slider'            => esc_html__('Portfolio Slider', 'onschedule'),
				'small-slider'      => esc_html__('Portfolio Small Slider', 'onschedule'),
				'gallery'           => esc_html__('Portfolio Gallery', 'onschedule'),
				'small-gallery'     => esc_html__('Portfolio Small Gallery', 'onschedule'),
				'masonry'           => esc_html__('Portfolio Masonry', 'onschedule'),
				'small-masonry'     => esc_html__('Portfolio Small Masonry', 'onschedule'),
				'custom'            => esc_html__('Portfolio Custom', 'onschedule'),
				'full-width-custom' => esc_html__('Portfolio Full Width Custom', 'onschedule')
			),
			'args' => array(
				'dependence' => true,
				'show' => array(
					'images'            => '',
					'small-images'      => '',
					'slider'            => '',
					'small-slider'      => '',
					'gallery'           => '#edgtf_portfolio_gallery_container',
					'small-gallery'     => '#edgtf_portfolio_gallery_container',
					'masonry'           => '#edgtf_portfolio_masonry_container',
					'small-masonry'     => '#edgtf_portfolio_masonry_container',
					'custom'            => '',
					'full-width-custom' => ''
				),
				'hide' => array(
					'images'            => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container',
					'small-images'      => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container',
					'slider'            => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container',
					'small-slider'      => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container',
					'gallery'           => '#edgtf_portfolio_masonry_container',
					'small-gallery'     => '#edgtf_portfolio_masonry_container',
					'masonry'           => '#edgtf_portfolio_gallery_container',
					'small-masonry'     => '#edgtf_portfolio_gallery_container',
					'custom'            => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container',
					'full-width-custom' => '#edgtf_portfolio_gallery_container, #edgtf_portfolio_masonry_container'
				)
			)
		));
		
		/***************** Gallery Layout *****************/
		
		$portfolio_gallery_container = onschedule_edge_add_admin_container(array(
			'parent'          => $panel,
			'name'            => 'portfolio_gallery_container',
			'hidden_property' => 'portfolio_single_template',
			'hidden_values' => array(
				'images',
				'small-images',
				'slider',
				'small-slider',
				'masonry',
				'small-masonry',
				'custom',
				'full-width-custom'
			)
		));
		
			onschedule_edge_add_admin_field(array(
				'name'        => 'portfolio_single_gallery_columns_number',
				'type'        => 'select',
				'label'       => esc_html__('Number of Columns', 'onschedule'),
				'default_value' => 'three',
				'description' => esc_html__('Set number of columns for portfolio gallery type', 'onschedule'),
				'parent'      => $portfolio_gallery_container,
				'options'     => array(
					'two'   => esc_html__('2 Columns', 'onschedule'),
					'three' => esc_html__('3 Columns', 'onschedule'),
					'four'  => esc_html__('4 Columns', 'onschedule')
				)
			));
		
			onschedule_edge_add_admin_field(array(
				'name'        => 'portfolio_single_gallery_space_between_items',
				'type'        => 'select',
				'label'       => esc_html__('Space Between Items', 'onschedule'),
				'default_value' => 'normal',
				'description' => esc_html__('Set space size between columns for portfolio gallery type', 'onschedule'),
				'parent'      => $portfolio_gallery_container,
				'options'     => array(
					'normal'    => esc_html__('Normal', 'onschedule'),
					'small'     => esc_html__('Small', 'onschedule'),
					'tiny'      => esc_html__('Tiny', 'onschedule'),
					'no'        => esc_html__('No Space', 'onschedule')
				)
			));
		
		/***************** Gallery Layout *****************/
		
		/***************** Masonry Layout *****************/
		
		$portfolio_masonry_container = onschedule_edge_add_admin_container(array(
			'parent'          => $panel,
			'name'            => 'portfolio_masonry_container',
			'hidden_property' => 'portfolio_single_template',
			'hidden_values' => array(
				'images',
				'small-images',
				'slider',
				'small-slider',
				'gallery',
				'small-gallery',
				'custom',
				'full-width-custom'
			)
		));
		
			onschedule_edge_add_admin_field(array(
				'name'        => 'portfolio_single_masonry_columns_number',
				'type'        => 'select',
				'label'       => esc_html__('Number of Columns', 'onschedule'),
				'default_value' => 'three',
				'description' => esc_html__('Set number of columns for portfolio masonry type', 'onschedule'),
				'parent'      => $portfolio_masonry_container,
				'options'     => array(
					'two'   => esc_html__('2 Columns', 'onschedule'),
					'three' => esc_html__('3 Columns', 'onschedule'),
					'four'  => esc_html__('4 Columns', 'onschedule')
				)
			));
			
			onschedule_edge_add_admin_field(array(
				'name'        => 'portfolio_single_masonry_space_between_items',
				'type'        => 'select',
				'label'       => esc_html__('Space Between Items', 'onschedule'),
				'default_value' => 'normal',
				'description' => esc_html__('Set space size between columns for portfolio masonry type', 'onschedule'),
				'parent'      => $portfolio_masonry_container,
				'options'     => array(
					'normal'    => esc_html__('Normal', 'onschedule'),
					'small'     => esc_html__('Small', 'onschedule'),
					'tiny'      => esc_html__('Tiny', 'onschedule'),
					'no'        => esc_html__('No Space', 'onschedule')
				)
			));
		
		/***************** Masonry Layout *****************/
		
		onschedule_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'show_title_area_portfolio_single',
				'default_value' => '',
				'label'       => esc_html__('Show Title Area', 'onschedule'),
				'description' => esc_html__('Enabling this option will show title area on single projects', 'onschedule'),
				'parent'      => $panel,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'yes' => esc_html__('Yes', 'onschedule'),
                    'no' => esc_html__('No', 'onschedule')
                ),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_images',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Lightbox for Images', 'onschedule'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for projects with images', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_videos',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Lightbox for Videos', 'onschedule'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for YouTube/Vimeo projects', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_enable_categories',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Categories', 'onschedule'),
			'description'   => esc_html__('Enabling this option will enable category meta description on single projects', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_hide_date',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Date', 'onschedule'),
			'description'   => esc_html__('Enabling this option will enable date meta on single projects', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));
		
		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_sticky_sidebar',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Sticky Side Text', 'onschedule'),
			'description'   => esc_html__('Enabling this option will make side text sticky on Single Project pages. This option works only for Full Width Images, Small Images, Small Gallery and Small Masonry portfolio types', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'onschedule'),
			'description'   => esc_html__('Enabling this option will show comments on your page', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'portfolio_single_hide_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Pagination', 'onschedule'),
			'description'   => esc_html__('Enabling this option will turn off portfolio pagination functionality', 'onschedule'),
			'parent'        => $panel,
			'default_value' => 'no',
			'args' => array(
				'dependence' => true,
				'dependence_hide_on_yes' => '#edgtf_navigate_same_category_container'
			)
		));

			$container_navigate_category = onschedule_edge_add_admin_container(array(
				'name'            => 'navigate_same_category_container',
				'parent'          => $panel,
				'hidden_property' => 'portfolio_single_hide_pagination',
				'hidden_value'    => 'yes'
			));
	
				onschedule_edge_add_admin_field(array(
					'name'            => 'portfolio_single_nav_same_category',
					'type'            => 'yesno',
					'label'           => esc_html__('Enable Pagination Through Same Category', 'onschedule'),
					'description'     => esc_html__('Enabling this option will make portfolio pagination sort through current category', 'onschedule'),
					'parent'          => $container_navigate_category,
					'default_value'   => 'no'
				));

		onschedule_edge_add_admin_field(array(
			'name'        => 'portfolio_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Portfolio Single Slug', 'onschedule'),
			'description' => esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)', 'onschedule'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_portfolio_options_map', 14);
}