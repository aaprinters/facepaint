<?php

if ( ! function_exists('onschedule_edge_sidearea_options_map') ) {

	function onschedule_edge_sidearea_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_side_area_page',
				'title' => esc_html__('Side Area', 'onschedule'),
				'icon' => 'fa fa-indent'
			)
		);

		$side_area_panel = onschedule_edge_add_admin_panel(
			array(
				'title' => esc_html__('Side Area', 'onschedule'),
				'name' => 'side_area',
				'page' => '_side_area_page'
			)
		);

		$side_area_icon_style_group = onschedule_edge_add_admin_group(
			array(
				'parent' => $side_area_panel,
				'name' => 'side_area_icon_style_group',
				'title' => esc_html__('Side Area Icon Style', 'onschedule'),
				'description' => esc_html__('Define styles for Side Area icon', 'onschedule')
			)
		);

		$side_area_icon_style_row1 = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row1'
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_color',
				'label' => esc_html__('Color', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_hover_color',
				'label' => esc_html__('Hover Color', 'onschedule')
			)
		);

		$side_area_icon_style_row2 = onschedule_edge_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row2',
				'next'		=> true
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_color',
				'label' => esc_html__('Close Icon Color', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_hover_color',
				'label' => esc_html__('Close Icon Hover Color', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_width',
				'default_value' => '',
				'label' => esc_html__('Side Area Width', 'onschedule'),
				'description' => esc_html__('Enter a width for Side Area', 'onschedule'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'color',
				'name' => 'side_area_background_color',
				'label' => esc_html__('Background Color', 'onschedule'),
				'description' => esc_html__('Choose a background color for Side Area', 'onschedule')
			)
		);
		
		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_padding',
				'label' => esc_html__('Padding', 'onschedule'),
				'description' => esc_html__('Define padding for Side Area in format top right bottom left', 'onschedule'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		onschedule_edge_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'selectblank',
				'name' => 'side_area_aligment',
				'default_value' => '',
				'label' => esc_html__('Text Alignment', 'onschedule'),
				'description' => esc_html__('Choose text alignment for side area', 'onschedule'),
				'options' => array(
					'' => esc_html__('Default', 'onschedule'),
					'left' => esc_html__('Left', 'onschedule'),
					'center' => esc_html__('Center', 'onschedule'),
					'right' => esc_html__('Right', 'onschedule')
				)
			)
		);
	}

	add_action('onschedule_edge_action_options_map', 'onschedule_edge_sidearea_options_map', 15);
}