<section class="edgtf-side-menu">
	<div class="edgtf-close-side-menu-holder">
		<a class="edgtf-close-side-menu" href="#" target="_self">
			<span class="ion-android-close"></span>
		</a>
	</div>
	<?php if(is_active_sidebar('sidearea')) {
		dynamic_sidebar('sidearea');
	} ?>
</section>