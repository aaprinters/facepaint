<?php

if ( ! function_exists('onschedule_edge_sidebar_options_map') ) {

	function onschedule_edge_sidebar_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug' => '_sidebar_page',
				'title' => esc_html__('Sidebar Area', 'onschedule'),
				'icon' => 'fa fa-indent'
			)
		);

		$sidebar_panel = onschedule_edge_add_admin_panel(
			array(
				'title' => esc_html__('Sidebar Area', 'onschedule'),
				'name' => 'sidebar',
				'page' => '_sidebar_page'
			)
		);
		
		onschedule_edge_add_admin_field(array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__('Sidebar Layout', 'onschedule'),
			'description'   => esc_html__('Choose a sidebar layout for pages', 'onschedule'),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
			'options'       => array(
				'no-sidebar'        => esc_html__('No Sidebar', 'onschedule'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
			)
		));
		
		$onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
		if(count($onschedule_custom_sidebars) > 0) {
			onschedule_edge_add_admin_field(array(
				'name' => 'custom_sidebar_area',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'onschedule'),
				'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'onschedule'),
				'parent' => $sidebar_panel,
				'options' => $onschedule_custom_sidebars
			));
		}
	}

	add_action('onschedule_edge_action_options_map', 'onschedule_edge_sidebar_options_map', 9);
}