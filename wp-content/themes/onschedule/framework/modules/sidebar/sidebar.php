<?php

if (!function_exists('onschedule_edge_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function onschedule_edge_register_sidebars() {

        register_sidebar(array(
            'name' => esc_html__('Sidebar', 'onschedule'),
            'id' => 'sidebar',
            'description' => esc_html__('Default Sidebar', 'onschedule'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="edgtf-widget-title">',
            'after_title' => '</h4>'
        ));
    }

    add_action('widgets_init', 'onschedule_edge_register_sidebars', 1);
}

if (!function_exists('onschedule_edge_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates OnScheduleEdgeClassSidebar object
     */
    function onschedule_edge_add_support_custom_sidebar() {
        add_theme_support('OnScheduleEdgeClassSidebar');
        if (get_theme_support('OnScheduleEdgeClassSidebar')) new OnScheduleEdgeClassSidebar();
    }

    add_action('after_setup_theme', 'onschedule_edge_add_support_custom_sidebar');
}