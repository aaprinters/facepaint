<?php

$edgt_blog_categories = array();
$categories = get_categories();
foreach($categories as $category) {
    $edgt_blog_categories[$category->term_id] = $category->name;
}

$yesnoarray = array(
    'yes' => esc_html__('Yes', 'onschedule'),
    'no' => esc_html__('No', 'onschedule')
);

$blog_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('page'),
        'title' => esc_html__('Blog', 'onschedule'),
        'name' => 'blog_meta'
    )
);

    onschedule_edge_create_meta_box_field(
        array(
            'name'        => 'edgtf_blog_category_meta',
            'type'        => 'selectblank',
            'label'       => esc_html__('Blog Category', 'onschedule'),
            'description' => esc_html__('Choose category of posts to display (leave empty to display all categories)', 'onschedule'),
            'parent'      => $blog_meta_box,
            'options'     => $edgt_blog_categories
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name'        => 'edgtf_show_posts_per_page_meta',
            'type'        => 'text',
            'label'       => esc_html__('Number of Posts', 'onschedule'),
            'description' => esc_html__('Enter the number of posts to display', 'onschedule'),
            'parent'      => $blog_meta_box,
            'options'     => $edgt_blog_categories,
            'args'        => array("col_width" => 3)
        )
    );

    onschedule_edge_create_meta_box_field(array(
        'name'        => 'edgtf_blog_masonry_layout_meta',
        'type'        => 'select',
        'label'       => esc_html__('Masonry - Layout', 'onschedule'),
        'description' => esc_html__('Set masonry layout. Default is in grid.', 'onschedule'),
        'parent'      => $blog_meta_box,
        'options'     => array(
            ''      => esc_html__('Default', 'onschedule'),
            'in-grid'   => esc_html__('In Grid', 'onschedule'),
            'full-width' => esc_html__('Full Width', 'onschedule')
        )
    ));

	onschedule_edge_create_meta_box_field(array(
		'name'        => 'edgtf_blog_masonry_number_of_columns_meta',
		'type'        => 'select',
		'label'       => esc_html__('Masonry - Number of Columns', 'onschedule'),
		'description' => esc_html__('Set number of columns for your masonry blog lists', 'onschedule'),
		'parent'      => $blog_meta_box,
		'options'     => array(
			''      => esc_html__('Default', 'onschedule'),
			'two'   => esc_html__('2 Columns', 'onschedule'),
			'three' => esc_html__('3 Columns', 'onschedule'),
			'four'  => esc_html__('4 Columns', 'onschedule'),
			'five'  => esc_html__('5 Columns', 'onschedule')
		)
	));

	onschedule_edge_create_meta_box_field(array(
		'name'        => 'edgtf_blog_masonry_space_between_items_meta',
		'type'        => 'select',
		'label'       => esc_html__('Masonry - Space Between Items', 'onschedule'),
		'description' => esc_html__('Set space size between posts for your masonry blog lists', 'onschedule'),
		'parent'      => $blog_meta_box,
		'options'     => array(
			''        => esc_html__('Default', 'onschedule'),
			'normal'  => esc_html__('Normal', 'onschedule'),
			'small'   => esc_html__('Small', 'onschedule'),
			'tiny'    => esc_html__('Tiny', 'onschedule'),
			'no'      => esc_html__('No Space', 'onschedule')
		)
	));

    onschedule_edge_create_meta_box_field(array(
        'name'        => 'edgtf_blog_list_featured_image_proportion_meta',
        'type'        => 'select',
        'label'       => esc_html__('Featured Image Proportion', 'onschedule'),
        'description' => esc_html__('Choose type of proportions you want to use for featured images on blog lists.', 'onschedule'),
        'parent'      => $blog_meta_box,
        'default_value' => '',
        'options'     => array(
            ''		   => esc_html__('Default', 'onschedule'),
            'fixed'    => esc_html__('Fixed', 'onschedule'),
            'original' => esc_html__('Original', 'onschedule')
        )
    ));

	onschedule_edge_create_meta_box_field(array(
		'name'        => 'edgtf_blog_pagination_type_meta',
		'type'        => 'select',
		'label'       => esc_html__('Pagination Type', 'onschedule'),
		'description' => esc_html__('Choose a pagination layout for Blog Lists', 'onschedule'),
		'parent'      => $blog_meta_box,
		'default_value' => '',
		'options'     => array(
			''                => esc_html__('Default', 'onschedule'),
			'standard'		  => esc_html__('Standard', 'onschedule'),
			'load-more'		  => esc_html__('Load More', 'onschedule'),
			'infinite-scroll' => esc_html__('Infinite Scroll', 'onschedule'),
			'no-pagination'   => esc_html__('No Pagination', 'onschedule')
		)
	));

    onschedule_edge_create_meta_box_field(
        array(
            'type' => 'text',
            'name' => 'edgtf_number_of_chars_meta',
            'default_value' => '',
            'label' => esc_html__('Number of Words in Excerpt', 'onschedule'),
            'description' => esc_html__('Enter a number of words in excerpt (article summary). Default value is 40', 'onschedule'),
            'parent' => $blog_meta_box,
            'args' => array(
                'col_width' => 3
            )
        )
    );
	

