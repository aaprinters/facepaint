<?php

$testimonial_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('testimonials'),
        'title' => esc_html__('Testimonial', 'onschedule'),
        'name' => 'testimonial_meta'
    )
);

    onschedule_edge_create_meta_box_field(
        array(
            'name'        	=> 'edgtf_testimonial_title',
            'type'        	=> 'text',
            'label'       	=> esc_html__('Title', 'onschedule'),
            'description' 	=> esc_html__('Enter testimonial title', 'onschedule'),
            'parent'      	=> $testimonial_meta_box,
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name'        	=> 'edgtf_testimonial_text',
            'type'        	=> 'text',
            'label'       	=> esc_html__('Text', 'onschedule'),
            'description' 	=> esc_html__('Enter testimonial text', 'onschedule'),
            'parent'      	=> $testimonial_meta_box,
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name'        	=> 'edgtf_testimonial_author',
            'type'        	=> 'text',
            'label'       	=> esc_html__('Author', 'onschedule'),
            'description' 	=> esc_html__('Enter author name', 'onschedule'),
            'parent'      	=> $testimonial_meta_box,
        )
    );

	onschedule_edge_create_meta_box_field(
		array(
			'name'        	=> 'edgtf_testimonial_author_position',
			'type'        	=> 'text',
			'label'       	=> esc_html__('Author Job Position', 'onschedule'),
			'description' 	=> esc_html__('Enter author job position', 'onschedule'),
			'parent'      	=> $testimonial_meta_box,
		)
	);