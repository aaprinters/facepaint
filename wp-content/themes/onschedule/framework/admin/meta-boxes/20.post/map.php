<?php

/*** Post Settings ***/

if(!function_exists('onschedule_edge_map_post')) {
    function onschedule_edge_map_post() {

        $post_meta_box = onschedule_edge_create_meta_box(
            array(
                'scope' => array('post'),
                'title' => esc_html__('Post', 'onschedule'),
                'name' => 'post-meta'
            )
        );
	
	    onschedule_edge_create_meta_box_field(array(
		    'name'        => 'edgtf_blog_single_sidebar_layout_meta',
		    'type'        => 'select',
		    'label'       => esc_html__('Sidebar Layout', 'onschedule'),
		    'description' => esc_html__('Choose a sidebar layout for Blog single page', 'onschedule'),
		    'default_value'	=> '',
		    'parent'      => $post_meta_box,
		    'options'     => array(
			    ''		            => esc_html__('Default', 'onschedule'),
			    'no-sidebar'		=> esc_html__('No Sidebar', 'onschedule'),
			    'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
			    'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
			    'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
			    'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
		    )
	    ));
	
	    $onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
	    if(count($onschedule_custom_sidebars) > 0) {
		    onschedule_edge_create_meta_box_field(array(
			    'name' => 'edgtf_blog_single_custom_sidebar_area_meta',
			    'type' => 'selectblank',
			    'label' => esc_html__('Sidebar to Display', 'onschedule'),
			    'description' => esc_html__('Choose a sidebar to display on Blog single page. Default sidebar is "Sidebar"', 'onschedule'),
			    'parent' => $post_meta_box,
			    'options' => onschedule_edge_get_custom_sidebars()
		    ));
	    }

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_blog_list_featured_image_meta',
                'type' => 'image',
                'label' => esc_html__('Blog List Image', 'onschedule'),
                'description' => esc_html__('Choose an Image for displaying in blog list. If not uploaded, featured image will be shown.', 'onschedule'),
                'parent' => $post_meta_box
            )
        );

        onschedule_edge_create_meta_box_field(array(
            'name' => 'edgtf_blog_masonry_gallery_fixed_dimensions_meta',
            'type' => 'select',
            'label' => esc_html__('Dimensions for Fixed Proportion', 'onschedule'),
            'description' => esc_html__('Choose image layout when it appears in Masonry lists in fixed proportion', 'onschedule'),
            'default_value' => 'default',
            'parent' => $post_meta_box,
            'options' => array(
                'default' => esc_html__('Default', 'onschedule'),
                'large-width' => esc_html__('Large Width', 'onschedule'),
                'large-height' => esc_html__('Large Height', 'onschedule'),
                'large-width-height' => esc_html__('Large Width/Height', 'onschedule')
            )
        ));

        onschedule_edge_create_meta_box_field(array(
            'name' => 'edgtf_blog_masonry_gallery_original_dimensions_meta',
            'type' => 'select',
            'label' => esc_html__('Dimensions for Original Proportion', 'onschedule'),
            'description' => esc_html__('Choose image layout when it appears in Masonry lists in original proportion', 'onschedule'),
            'default_value' => 'default',
            'parent' => $post_meta_box,
            'options' => array(
                'default' => esc_html__('Default', 'onschedule'),
                'large-width' => esc_html__('Large Width', 'onschedule')
            )
        ));

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_show_title_area_blog_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Show Title Area', 'onschedule'),
                'description' => esc_html__('Enabling this option will show title area on your single post page', 'onschedule'),
                'parent'      => $post_meta_box,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'yes' => esc_html__('Yes', 'onschedule'),
                    'no' => esc_html__('No', 'onschedule')
                )
            )
        );
    }
    
    add_action('onschedule_edge_action_meta_boxes_map', 'onschedule_edge_map_post');
}
