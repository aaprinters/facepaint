<?php

$quote_post_format_meta_box = onschedule_edge_create_meta_box(
	array(
		'scope' =>	array('post'),
		'title' => esc_html__('Quote Post Format', 'onschedule'),
		'name' 	=> 'post_format_quote_meta'
	)
);

onschedule_edge_create_meta_box_field(
	array(
		'name'        => 'edgtf_post_quote_text_meta',
		'type'        => 'text',
		'label'       => esc_html__('Quote Text', 'onschedule'),
		'description' => esc_html__('Enter Quote text', 'onschedule'),
		'parent'      => $quote_post_format_meta_box,

	)
);

onschedule_edge_create_meta_box_field(
	array(
		'name'        => 'edgtf_post_quote_author_meta',
		'type'        => 'text',
		'label'       => esc_html__('Quote Author', 'onschedule'),
		'description' => esc_html__('Enter Quote author', 'onschedule'),
		'parent'      => $quote_post_format_meta_box,
	)
);