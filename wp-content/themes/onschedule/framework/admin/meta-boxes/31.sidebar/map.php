<?php

$onschedule_sidebar_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item'),
        'title' => esc_html__('Sidebar', 'onschedule'),
        'name' => 'sidebar_meta'
    )
);

    onschedule_edge_create_meta_box_field(
        array(
            'name'        => 'edgtf_sidebar_layout_meta',
            'type'        => 'select',
            'label'       => esc_html__('Layout', 'onschedule'),
            'description' => esc_html__('Choose the sidebar layout', 'onschedule'),
            'parent'      => $onschedule_sidebar_meta_box,
            'options'     => array(
				''			        => esc_html__('Default', 'onschedule'),
				'no-sidebar'		=> esc_html__('No Sidebar', 'onschedule'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'onschedule'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'onschedule'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'onschedule'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'onschedule')
			)
        )
    );

	$onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
	if(count($onschedule_custom_sidebars) > 0) {
	    onschedule_edge_create_meta_box_field(array(
	        'name' => 'edgtf_custom_sidebar_area_meta',
	        'type' => 'selectblank',
	        'label' => esc_html__('Choose Widget Area in Sidebar', 'onschedule'),
	        'description' => esc_html__('Choose Custom Widget area to display in Sidebar"', 'onschedule'),
	        'parent' => $onschedule_sidebar_meta_box,
	        'options' => $onschedule_custom_sidebars
	    ));
	}