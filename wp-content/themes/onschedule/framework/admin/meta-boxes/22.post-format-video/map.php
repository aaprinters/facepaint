<?php

$video_post_format_meta_box = onschedule_edge_create_meta_box(
	array(
		'scope' =>	array('post'),
		'title' => esc_html__('Video Post Format', 'onschedule'),
		'name' 	=> 'post_format_video_meta'
	)
);

onschedule_edge_create_meta_box_field(
	array(
		'name'        => 'edgtf_video_type_meta',
		'type'        => 'select',
		'label'       => esc_html__('Video Type', 'onschedule'),
		'description' => esc_html__('Choose video type', 'onschedule'),
		'parent'      => $video_post_format_meta_box,
		'default_value' => 'social_networks',
		'options'     => array(
			'social_networks' => esc_html__('Video Service', 'onschedule'),
			'self' => esc_html__('Self Hosted', 'onschedule')
		),
		'args' => array(
			'dependence' => true,
			'hide' => array(
				'social_networks' => '#edgtf_edgtf_video_self_hosted_container',
				'self' => '#edgtf_edgtf_video_embedded_container'
			),
			'show' => array(
				'social_networks' => '#edgtf_edgtf_video_embedded_container',
				'self' => '#edgtf_edgtf_video_self_hosted_container')
		)
	)
);

$edgtf_video_embedded_container = onschedule_edge_add_admin_container(
	array(
		'parent' => $video_post_format_meta_box,
		'name' => 'edgtf_video_embedded_container',
		'hidden_property' => 'edgtf_video_type_meta',
		'hidden_value' => 'self'
	)
);

$edgtf_video_self_hosted_container = onschedule_edge_add_admin_container(
	array(
		'parent' => $video_post_format_meta_box,
		'name' => 'edgtf_video_self_hosted_container',
		'hidden_property' => 'edgtf_video_type_meta',
		'hidden_value' => 'social_networks'
	)
);

onschedule_edge_create_meta_box_field(
	array(
		'name'        => 'edgtf_post_video_link_meta',
		'type'        => 'text',
		'label'       => esc_html__('Video URL', 'onschedule'),
		'description' => esc_html__('Enter Video URL', 'onschedule'),
		'parent'      => $edgtf_video_embedded_container,
	)
);

onschedule_edge_create_meta_box_field(
	array(
		'name'        => 'edgtf_post_video_custom_meta',
		'type'        => 'text',
		'label'       => esc_html__('Video MP4', 'onschedule'),
		'description' => esc_html__('Enter video URL for MP4 format', 'onschedule'),
		'parent'      => $edgtf_video_self_hosted_container,
	)
);