<?php

$edgt_pages = array();
$pages = get_pages(); 
foreach($pages as $page) {
	$edgt_pages[$page->ID] = $page->post_title;
}

//Portfolio Images

$edgtPortfolioImages = new OnScheduleEdgeClassMetaBox('portfolio-item', esc_html__('Portfolio Images (multiple upload)', 'onschedule'), '', '', 'portfolio_images');
$onschedule_edge_global_Framework->edgtMetaBoxes->addMetaBox('portfolio_images',$edgtPortfolioImages);

	$edgtf_portfolio_image_gallery = new OnScheduleEdgeClassMultipleImages('edgtf-portfolio-image-gallery', esc_html__('Portfolio Images', 'onschedule'), esc_html__('Choose your portfolio images', 'onschedule'));
	$edgtPortfolioImages->addChild('edgtf-portfolio-image-gallery', $edgtf_portfolio_image_gallery);

//Portfolio Images/Videos 2

$edgtPortfolioImagesVideos2 = new OnScheduleEdgeClassMetaBox('portfolio-item', esc_html__('Portfolio Images/Videos (single upload)', 'onschedule'));
$onschedule_edge_global_Framework->edgtMetaBoxes->addMetaBox('portfolio_images_videos2', $edgtPortfolioImagesVideos2);

	$edgtf_portfolio_images_videos2 = new OnScheduleEdgeClassImagesVideosFramework('', '');
	$edgtPortfolioImagesVideos2->addChild('edgt_portfolio_images_videos2', $edgtf_portfolio_images_videos2);

//Portfolio Additional Sidebar Items

$edgtAdditionalSidebarItems = onschedule_edge_create_meta_box(
    array(
        'scope' => array('portfolio-item'),
        'title' => esc_html__('Additional Portfolio Sidebar Items', 'onschedule'),
        'name' => 'portfolio_properties'
    )
);

	$edgt_portfolio_properties = onschedule_edge_add_options_framework(
	    array(
	        'label' => esc_html__('Portfolio Properties', 'onschedule'),
	        'name' => 'edgt_portfolio_properties',
	        'parent' => $edgtAdditionalSidebarItems
	    )
	);