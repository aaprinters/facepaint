<?php

$title_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post', 'restaurant-menu-item'),
        'title' => esc_html__('Title', 'onschedule'),
        'name' => 'title_meta'
    )
);

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_show_title_area_meta',
            'type' => 'select',
            'default_value' => '',
            'label' => esc_html__('Show Title Area', 'onschedule'),
            'description' => esc_html__('Disabling this option will turn off page title area', 'onschedule'),
            'parent' => $title_meta_box,
            'options' => onschedule_edge_get_yes_no_select_array(),
            'args' => array(
                "dependence" => true,
                "hide" => array(
                    "" => "",
                    "no" => "#edgtf_edgtf_show_title_area_meta_container",
                    "yes" => ""
                ),
                "show" => array(
                    "" => "#edgtf_edgtf_show_title_area_meta_container",
                    "no" => "",
                    "yes" => "#edgtf_edgtf_show_title_area_meta_container"
                )
            )
        )
    );

    $show_title_area_meta_container = onschedule_edge_add_admin_container(
        array(
            'parent' => $title_meta_box,
            'name' => 'edgtf_show_title_area_meta_container',
            'hidden_property' => 'edgtf_show_title_area_meta',
            'hidden_value' => 'no'
        )
    );

		onschedule_edge_create_meta_box_field(
			array(
				'name' => 'edgtf_title_in_grid_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Title Area in Grid', 'onschedule'),
				'description' => esc_html__('Choose wheter for title content to be in grid', 'onschedule'),
				'parent' => $show_title_area_meta_container,
				'options' => onschedule_edge_get_yes_no_select_array()
			)
		);

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_type_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Title Area Type', 'onschedule'),
                'description' => esc_html__('Choose title type', 'onschedule'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'standard' => esc_html__('Standard', 'onschedule'),
                    'breadcrumb' => esc_html__('Breadcrumb', 'onschedule')
                ),
                'args' => array(
                    "dependence" => true,
                    "hide" => array(
                        "standard" => "",
                        "breadcrumb" => "#edgtf_edgtf_title_area_type_meta_container"
                    ),
                    "show" => array(
                        "" => "#edgtf_edgtf_title_area_type_meta_container",
                        "standard" => "#edgtf_edgtf_title_area_type_meta_container",
                        "breadcrumb" => ""
                    )
                )
            )
        );

        $title_area_type_meta_container = onschedule_edge_add_admin_container(
            array(
                'parent' => $show_title_area_meta_container,
                'name' => 'edgtf_title_area_type_meta_container',
                'hidden_property' => 'edgtf_title_area_type_meta',
                'hidden_value' => 'breadcrumb'
            )
        );

            onschedule_edge_create_meta_box_field(
                array(
                    'name' => 'edgtf_title_area_enable_breadcrumbs_meta',
                    'type' => 'select',
                    'default_value' => '',
                    'label' => esc_html__('Enable Breadcrumbs', 'onschedule'),
                    'description' => esc_html__('This option will display Breadcrumbs in Title Area', 'onschedule'),
                    'parent' => $title_area_type_meta_container,
                    'options' => onschedule_edge_get_yes_no_select_array()
                )
            );

            

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_vertical_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Vertical Alignment', 'onschedule'),
                'description' => esc_html__('Specify title vertical alignment', 'onschedule'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'header_bottom' => esc_html__('From Bottom of Header', 'onschedule'),
                    'window_top' => esc_html__('From Window Top', 'onschedule')
                )
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_content_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Horizontal Alignment', 'onschedule'),
                'description' => esc_html__('Specify title horizontal alignment', 'onschedule'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'left' => esc_html__('Left', 'onschedule'),
                    'center' => esc_html__('Center', 'onschedule'),
                    'right' => esc_html__('Right', 'onschedule')
                )
            )
        );

		onschedule_edge_create_meta_box_field(
			array(
				'name' => 'edgtf_title_area_title_tag_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Title Tag', 'onschedule'),
				'parent' => $title_area_type_meta_container,
				'options' => onschedule_edge_get_title_tag(true, array('span' => esc_html__('Custom Heading', 'onschedule')))
			)
		);

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_text_color_meta',
                'type' => 'color',
                'label' => esc_html__('Title Color', 'onschedule'),
                'description' => esc_html__('Choose a color for title text', 'onschedule'),
                'parent' => $show_title_area_meta_container
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_background_color_meta',
                'type' => 'color',
                'label' => esc_html__('Background Color', 'onschedule'),
                'description' => esc_html__('Choose a background color for title area', 'onschedule'),
                'parent' => $show_title_area_meta_container
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_hide_background_image_meta',
                'type' => 'yesno',
                'default_value' => 'no',
                'label' => esc_html__('Hide Background Image', 'onschedule'),
                'description' => esc_html__('Enable this option to hide background image in title area', 'onschedule'),
                'parent' => $show_title_area_meta_container,
                'args' => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "#edgtf_edgtf_hide_background_image_meta_container",
                    "dependence_show_on_yes" => ""
                )
            )
        );

        $hide_background_image_meta_container = onschedule_edge_add_admin_container(
            array(
                'parent' => $show_title_area_meta_container,
                'name' => 'edgtf_hide_background_image_meta_container',
                'hidden_property' => 'edgtf_hide_background_image_meta',
                'hidden_value' => 'yes'
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_background_image_meta',
                'type' => 'image',
                'label' => esc_html__('Background Image', 'onschedule'),
                'description' => esc_html__('Choose an Image for title area', 'onschedule'),
                'parent' => $hide_background_image_meta_container
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_title_area_background_image_responsive_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Background Responsive Image', 'onschedule'),
                'description' => esc_html__('Enabling this option will make Title background image responsive', 'onschedule'),
                'parent' => $hide_background_image_meta_container,
                'options' => onschedule_edge_get_yes_no_select_array(),
                'args' => array(
                    "dependence" => true,
                    "hide" => array(
                        "" => "",
                        "no" => "",
                        "yes" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta"
                    ),
                    "show" => array(
                        "" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta",
                        "no" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta",
                        "yes" => ""
                    )
                )
            )
        );

        $title_area_background_image_responsive_meta_container = onschedule_edge_add_admin_container(
            array(
                'parent' => $hide_background_image_meta_container,
                'name' => 'edgtf_title_area_background_image_responsive_meta_container',
                'hidden_property' => 'edgtf_title_area_background_image_responsive_meta',
                'hidden_value' => 'yes'
            )
        );

            onschedule_edge_create_meta_box_field(
                array(
                    'name' => 'edgtf_title_area_background_image_parallax_meta',
                    'type' => 'select',
                    'default_value' => '',
                    'label' => esc_html__('Background Image in Parallax', 'onschedule'),
                    'description' => esc_html__('Enabling this option will make Title background image parallax', 'onschedule'),
                    'parent' => $title_area_background_image_responsive_meta_container,
                    'options' => array(
                        '' => esc_html__('Default', 'onschedule'),
                        'no' => esc_html__('No', 'onschedule'),
                        'yes' => esc_html__('Yes', 'onschedule'),
                        'yes_zoom' => esc_html__('Yes, with zoom out', 'onschedule')
                    )
                )
            );

        onschedule_edge_create_meta_box_field(array(
            'name' => 'edgtf_title_area_height_meta',
            'type' => 'text',
            'label' => esc_html__('Height', 'onschedule'),
            'description' => esc_html__('Set a height for Title Area', 'onschedule'),
            'parent' => $show_title_area_meta_container,
            'args' => array(
                'col_width' => 2,
                'suffix' => 'px'
            )
        ));

        onschedule_edge_create_meta_box_field(array(
            'name' => 'edgtf_title_area_subtitle_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => esc_html__('Subtitle Text', 'onschedule'),
            'description' => esc_html__('Enter your subtitle text', 'onschedule'),
            'parent' => $show_title_area_meta_container,
            'args' => array(
                'col_width' => 6
            )
        ));

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_subtitle_color_meta',
                'type' => 'color',
                'label' => esc_html__('Subtitle Color', 'onschedule'),
                'description' => esc_html__('Choose a color for subtitle text', 'onschedule'),
                'parent' => $show_title_area_meta_container
            )
        );

		onschedule_edge_create_meta_box_field(array(
			'name' => 'edgtf_subtitle_side_padding_meta',
			'type' => 'text',
			'label' => esc_html__('Subtitle Side Padding', 'onschedule'),
			'description' => esc_html__('Set left/right padding for subtitle area', 'onschedule'),
			'parent' => $show_title_area_meta_container,
			'args' => array(
				'col_width' => 2,
				'suffix' => '%'
			)
		));