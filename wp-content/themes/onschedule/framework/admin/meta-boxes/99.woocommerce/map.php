<?php

if(onschedule_edge_is_woocommerce_installed()){

    $woocommerce_meta_box = onschedule_edge_create_meta_box(
        array(
            'scope' => array('product'),
            'title' => esc_html__('Product Meta', 'onschedule'),
            'name' => 'woo_product_meta'
        )
    );
		
		onschedule_edge_create_meta_box_field(array(
			'name'        => 'edgtf_product_featured_image_size',
			'type'        => 'select',
			'label'       => esc_html__('Dimensions for Product List Shortcode', 'onschedule'),
			'description' => esc_html__('Choose image layout when it appears in Edge Product List - Masonry layout shortcode', 'onschedule'),
			'parent'      => $woocommerce_meta_box,
			'options'     => array(
				'edgtf-woo-image-normal-width' => esc_html__('Default', 'onschedule'),
				'edgtf-woo-image-large-width'  => esc_html__('Large Width', 'onschedule')
			)
		));
		
		onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_show_title_area_woo_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Show Title Area', 'onschedule'),
                'description' => esc_html__('Disabling this option will turn off page title area', 'onschedule'),
                'parent' => $woocommerce_meta_box,
                'options' => onschedule_edge_get_yes_no_select_array()
            )
        );

        onschedule_edge_create_meta_box_field(
            array(
                'name'        => 'edgtf_disable_page_content_top_padding_meta',
                'type'        => 'select',
                'label'       => esc_html__('Disable Content Top Padding', 'onschedule'),
                'description' => esc_html__('Enabling this option will disable content top padding', 'onschedule'),
                'parent'      => $woocommerce_meta_box,
                'options' => onschedule_edge_get_yes_no_select_array()
            )
        ); 
}

