<?php

$audio_post_format_meta_box = onschedule_edge_create_meta_box(
	array(
		'scope' =>	array('post'),
		'title' => esc_html__('Audio Post Format', 'onschedule'),
		'name' 	=> 'post_format_audio_meta'
	)
);

onschedule_edge_create_meta_box_field(
    array(
        'name'        => 'edgtf_audio_type_meta',
        'type'        => 'select',
        'label'       => esc_html__('Audio Type', 'onschedule'),
        'description' => esc_html__('Choose audio type', 'onschedule'),
        'parent'      => $audio_post_format_meta_box,
        'default_value' => 'social_networks',
        'options'     => array(
            'social_networks' => esc_html__('Audio Service', 'onschedule'),
            'self' => esc_html__('Self Hosted', 'onschedule')
        ),
        'args' => array(
            'dependence' => true,
            'hide' => array(
                'social_networks' => '#edgtf_edgtf_audio_self_hosted_container',
                'self' => '#edgtf_edgtf_audio_embedded_container'
            ),
            'show' => array(
                'social_networks' => '#edgtf_edgtf_audio_embedded_container',
                'self' => '#edgtf_edgtf_audio_self_hosted_container')
        )
    )
);

$edgtf_audio_embedded_container = onschedule_edge_add_admin_container(
    array(
        'parent' => $audio_post_format_meta_box,
        'name' => 'edgtf_audio_embedded_container',
        'hidden_property' => 'edgtf_audio_type_meta',
        'hidden_value' => 'self'
    )
);

$edgtf_audio_self_hosted_container = onschedule_edge_add_admin_container(
    array(
        'parent' => $audio_post_format_meta_box,
        'name' => 'edgtf_audio_self_hosted_container',
        'hidden_property' => 'edgtf_audio_type_meta',
        'hidden_value' => 'social_networks'
    )
);

onschedule_edge_create_meta_box_field(
    array(
        'name'        => 'edgtf_post_audio_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Audio URL', 'onschedule'),
        'description' => esc_html__('Enter audio URL', 'onschedule'),
        'parent'      => $edgtf_audio_embedded_container,
    )
);

onschedule_edge_create_meta_box_field(
    array(
        'name'        => 'edgtf_post_audio_custom_meta',
        'type'        => 'text',
        'label'       => esc_html__('Audio Link', 'onschedule'),
        'description' => esc_html__('Enter audio link', 'onschedule'),
        'parent'      => $edgtf_audio_self_hosted_container,
    )
);