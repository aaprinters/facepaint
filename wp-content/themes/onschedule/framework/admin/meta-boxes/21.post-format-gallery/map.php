<?php

$gallery_post_format_meta_box = onschedule_edge_create_meta_box(
	array(
		'scope' =>	array('post'),
		'title' => esc_html__('Gallery Post Format', 'onschedule'),
		'name' 	=> 'post_format_gallery_meta'
	)
);

onschedule_edge_add_multiple_images_field(
	array(
		'name'        => 'edgtf_post_gallery_images_meta',
		'label'       => esc_html__('Gallery Images', 'onschedule'),
		'description' => esc_html__('Choose your gallery images', 'onschedule'),
		'parent'      => $gallery_post_format_meta_box,
	)
);
