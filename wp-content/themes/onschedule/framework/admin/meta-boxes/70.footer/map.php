<?php

$footer_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post', 'restaurant-menu-item'),
        'title' => esc_html__('Footer', 'onschedule'),
        'name' => 'footer_meta'
    )
);

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_disable_footer_meta',
            'type' => 'yesno',
            'default_value' => 'no',
            'label' => esc_html__('Disable Footer for this Page', 'onschedule'),
            'description' => esc_html__('Enabling this option will hide footer on this page', 'onschedule'),
            'parent' => $footer_meta_box
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_show_footer_top_meta',
            'type' => 'select',
            'default_value' => '',
            'label' => esc_html__('Show Footer Top', 'onschedule'),
            'description' => esc_html__('Enabling this option will show Footer Top area', 'onschedule'),
            'parent' => $footer_meta_box,
            'options' => onschedule_edge_get_yes_no_select_array()
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_show_footer_bottom_meta',
            'type' => 'select',
            'default_value' => '',
            'label' => esc_html__('Show Footer Bottom', 'onschedule'),
            'description' => esc_html__('Enabling this option will show Footer Bottom area', 'onschedule'),
            'parent' => $footer_meta_box,
            'options' => onschedule_edge_get_yes_no_select_array()
        )
    );