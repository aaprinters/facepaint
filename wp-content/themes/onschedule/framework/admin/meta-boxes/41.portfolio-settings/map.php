<?php

if(!function_exists('onschedule_edge_map_portfolio_settings')) {
    function onschedule_edge_map_portfolio_settings() {
        $meta_box = onschedule_edge_create_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Settings', 'onschedule'),
            'name'  => 'portfolio_settings_meta_box'
        ));

        onschedule_edge_create_meta_box_field(array(
            'name'        => 'edgtf_portfolio_single_template_meta',
            'type'        => 'select',
            'label'       => esc_html__('Portfolio Type', 'onschedule'),
            'description' => esc_html__('Choose a default type for Single Project pages', 'onschedule'),
            'parent'      => $meta_box,
            'options'     => array(
                ''                  => esc_html__('Default', 'onschedule'),
                'images'            => esc_html__('Portfolio Images', 'onschedule'),
                'small-images'      => esc_html__('Portfolio Small Images', 'onschedule'),
                'slider'            => esc_html__('Portfolio Slider', 'onschedule'),
                'small-slider'      => esc_html__('Portfolio Small Slider', 'onschedule'),
                'gallery'           => esc_html__('Portfolio Gallery', 'onschedule'),
                'small-gallery'     => esc_html__('Portfolio Small Gallery', 'onschedule'),
                'masonry'           => esc_html__('Portfolio Masonry', 'onschedule'),
                'small-masonry'     => esc_html__('Portfolio Small Masonry', 'onschedule'),
                'custom'            => esc_html__('Portfolio Custom', 'onschedule'),
                'full-width-custom' => esc_html__('Portfolio Full Width Custom', 'onschedule')
            ),
            'args' => array(
	            'dependence' => true,
	            'show' => array(
		            ''                  => '',
		            'images'            => '',
		            'small-images'      => '',
		            'slider'            => '',
		            'small-slider'      => '',
		            'gallery'           => '#edgtf_edgtf_gallery_type_meta_container',
		            'small-gallery'     => '#edgtf_edgtf_gallery_type_meta_container',
		            'masonry'           => '#edgtf_edgtf_masonry_type_meta_container',
		            'small-masonry'     => '#edgtf_edgtf_masonry_type_meta_container',
		            'custom'            => '',
		            'full-width-custom' => ''
	            ),
	            'hide' => array(
		            ''                  => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'images'            => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'small-images'      => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'slider'            => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'small-slider'      => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'gallery'           => '#edgtf_edgtf_masonry_type_meta_container',
		            'small-gallery'     => '#edgtf_edgtf_masonry_type_meta_container',
		            'masonry'           => '#edgtf_edgtf_gallery_type_meta_container',
		            'small-masonry'     => '#edgtf_edgtf_gallery_type_meta_container',
		            'custom'            => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container',
		            'full-width-custom' => '#edgtf_edgtf_gallery_type_meta_container, #edgtf_edgtf_masonry_type_meta_container'
	            )
            )
        ));
	
	    /***************** Gallery Layout *****************/
	
	    $gallery_type_meta_container = onschedule_edge_add_admin_container(
		    array(
			    'parent' => $meta_box,
			    'name' => 'edgtf_gallery_type_meta_container',
			    'hidden_property' => 'edgtf_portfolio_single_template_meta',
			    'hidden_values' => array(
				    'images',
				    'small-images',
				    'slider',
				    'small-slider',
				    'masonry',
				    'small-masonry',
				    'custom',
				    'full-width-custom'
			    )
		    )
	    );
	
	        onschedule_edge_create_meta_box_field(array(
			    'name'        => 'edgtf_portfolio_single_gallery_columns_number_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Number of Columns', 'onschedule'),
			    'default_value' => '',
			    'description' => esc_html__('Set number of columns for portfolio gallery type', 'onschedule'),
			    'parent'      => $gallery_type_meta_container,
			    'options'     => array(
				    ''      => esc_html__('Default', 'onschedule'),
				    'two'   => esc_html__('2 Columns', 'onschedule'),
				    'three' => esc_html__('3 Columns', 'onschedule'),
				    'four'  => esc_html__('4 Columns', 'onschedule')
			    )
		    ));
	
	        onschedule_edge_create_meta_box_field(array(
			    'name'        => 'edgtf_portfolio_single_gallery_space_between_items_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Space Between Items', 'onschedule'),
			    'default_value' => '',
			    'description' => esc_html__('Set space size between columns for portfolio gallery type', 'onschedule'),
			    'parent'      => $gallery_type_meta_container,
			    'options'     => array(
				    ''          => esc_html__('Default', 'onschedule'),
				    'normal'    => esc_html__('Normal', 'onschedule'),
				    'small'     => esc_html__('Small', 'onschedule'),
				    'tiny'      => esc_html__('Tiny', 'onschedule'),
				    'no'        => esc_html__('No Space', 'onschedule')
			    )
		    ));
	
	    /***************** Gallery Layout *****************/
	
	    /***************** Masonry Layout *****************/
	
	    $masonry_type_meta_container = onschedule_edge_add_admin_container(
		    array(
			    'parent' => $meta_box,
			    'name' => 'edgtf_masonry_type_meta_container',
			    'hidden_property' => 'edgtf_portfolio_single_template_meta',
			    'hidden_values' => array(
				    'images',
				    'small-images',
				    'slider',
				    'small-slider',
				    'gallery',
				    'small-gallery',
				    'custom',
				    'full-width-custom'
			    )
		    )
	    );
	
		    onschedule_edge_create_meta_box_field(array(
			    'name'        => 'edgtf_portfolio_single_masonry_columns_number_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Number of Columns', 'onschedule'),
			    'default_value' => '',
			    'description' => esc_html__('Set number of columns for portfolio masonry type', 'onschedule'),
			    'parent'      => $masonry_type_meta_container,
			    'options'     => array(
				    ''      => esc_html__('Default', 'onschedule'),
				    'two'   => esc_html__('2 Columns', 'onschedule'),
				    'three' => esc_html__('3 Columns', 'onschedule'),
				    'four'  => esc_html__('4 Columns', 'onschedule')
			    )
		    ));
		
		    onschedule_edge_create_meta_box_field(array(
			    'name'        => 'edgtf_portfolio_single_masonry_space_between_items_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Space Between Items', 'onschedule'),
			    'default_value' => '',
			    'description' => esc_html__('Set space size between columns for portfolio masonry type', 'onschedule'),
			    'parent'      => $masonry_type_meta_container,
			    'options'     => array(
				    ''          => esc_html__('Default', 'onschedule'),
				    'normal'    => esc_html__('Normal', 'onschedule'),
				    'small'     => esc_html__('Small', 'onschedule'),
				    'tiny'      => esc_html__('Tiny', 'onschedule'),
				    'no'        => esc_html__('No Space', 'onschedule')
			    )
		    ));
	
	    /***************** Masonry Layout *****************/

        onschedule_edge_create_meta_box_field(
            array(
                'name' => 'edgtf_show_title_area_portfolio_single_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Show Title Area', 'onschedule'),
                'description' => esc_html__('Enabling this option will show title area on your single portfolio page', 'onschedule'),
                'parent'      => $meta_box,
                'options' => array(
                    '' => esc_html__('Default', 'onschedule'),
                    'yes' => esc_html__('Yes', 'onschedule'),
                    'no' => esc_html__('No', 'onschedule')
                )
            )
        );

	    onschedule_edge_create_meta_box_field(array(
		    'name'        => 'portfolio_info_top_padding',
		    'type'        => 'text',
		    'label'       => esc_html__('Portfolio Info Top Padding', 'onschedule'),
		    'description' => esc_html__('Set top padding for portfolio info elements holder. This option works only for Portfolio Images, Slider, Gallery and Masonry portfolio types', 'onschedule'),
		    'parent'      => $meta_box,
		    'args'        => array(
			    'col_width' => 3,
			    'suffix' => 'px'
		    )
	    ));

        $all_pages = array();
        $pages     = get_pages();
        foreach($pages as $page) {
            $all_pages[$page->ID] = $page->post_title;
        }

        onschedule_edge_create_meta_box_field(array(
            'name'        => 'portfolio_single_back_to_link',
            'type'        => 'select',
            'label'       => esc_html__('"Back To" Link', 'onschedule'),
            'description' => esc_html__('Choose "Back To" page to link from portfolio Single Project page', 'onschedule'),
            'parent'      => $meta_box,
            'options'     => $all_pages
        ));

        onschedule_edge_create_meta_box_field(array(
            'name'        => 'portfolio_external_link',
            'type'        => 'text',
            'label'       => esc_html__('Portfolio External Link', 'onschedule'),
            'description' => esc_html__('Enter URL to link from Portfolio List page', 'onschedule'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));
	
	    onschedule_edge_create_meta_box_field(array(
		    'name' => 'edgtf_portfolio_masonry_fixed_dimensions_meta',
		    'type' => 'select',
		    'label' => esc_html__('Dimensions for Masonry - Image Fixed Proportion', 'onschedule'),
		    'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists whre image proportion is fixed', 'onschedule'),
		    'default_value' => 'default',
		    'parent' => $meta_box,
		    'options' => array(
			    'default' => esc_html__('Default', 'onschedule'),
			    'large-width' => esc_html__('Large Width', 'onschedule'),
			    'large-height' => esc_html__('Large Height', 'onschedule'),
			    'large-width-height' => esc_html__('Large Width/Height', 'onschedule')
		    )
	    ));
	
	    onschedule_edge_create_meta_box_field(array(
		    'name' => 'edgtf_portfolio_masonry_original_dimensions_meta',
		    'type' => 'select',
		    'label' => esc_html__('Dimensions for Masonry - Image Original Proportion', 'onschedule'),
		    'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists whre image proportion is original', 'onschedule'),
		    'default_value' => 'default',
		    'parent' => $meta_box,
		    'options' => array(
			    'default' => esc_html__('Default', 'onschedule'),
			    'large-width' => esc_html__('Large Width', 'onschedule')
		    )
	    ));
    }

    add_action('onschedule_edge_action_meta_boxes_map', 'onschedule_edge_map_portfolio_settings');
}