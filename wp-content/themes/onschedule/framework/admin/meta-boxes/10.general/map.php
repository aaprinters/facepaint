<?php

    $general_meta_box = onschedule_edge_create_meta_box(
        array(
            'scope' => array('page', 'portfolio-item', 'post', 'restaurant-menu-item'),
            'title' => esc_html__('General', 'onschedule'),
            'name' => 'general_meta'
        )
    );

	onschedule_edge_create_meta_box_field(
		array(
			'name'          => 'edgtf_smooth_page_transitions_meta',
			'type'          => 'select',
			'default_value' => '',
			'label'         => esc_html__( 'Smooth Page Transitions', 'onschedule' ),
			'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'onschedule' ),
			'parent'        => $general_meta_box,
			'options'     => onschedule_edge_get_yes_no_select_array(),
			'args'          => array(
				"dependence"             => true,
				"hide"       => array(
					""    => "#edgtf_page_transitions_container_meta",
					"no"  => "#edgtf_page_transitions_container_meta",
					"yes" => ""
				),
				"show"       => array(
					""    => "",
					"no"  => "",
					"yes" => "#edgtf_page_transitions_container_meta"
				)
			)
		)
	);
	
	$page_transitions_container_meta = onschedule_edge_add_admin_container(
		array(
			'parent'          => $general_meta_box,
			'name'            => 'page_transitions_container_meta',
			'hidden_property' => 'edgtf_smooth_page_transitions_meta',
			'hidden_values'   => array('','no')
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name'          => 'edgtf_page_transition_preloader_meta',
			'type'          => 'select',
			'default_value' => '',
			'label'         => esc_html__( 'Enable Preloading Animation', 'onschedule' ),
			'description'   => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'onschedule' ),
			'parent'        => $page_transitions_container_meta,
			'options'     => onschedule_edge_get_yes_no_select_array(),
			'args'          => array(
				"dependence"             => true,
				"hide"       => array(
					""    => "#edgtf_page_transition_preloader_container_meta",
					"no"  => "#edgtf_page_transition_preloader_container_meta",
					"yes" => ""
				),
				"show"       => array(
					""    => "",
					"no"  => "",
					"yes" => "#edgtf_page_transition_preloader_container_meta"
				)
			)
		)
	);
	
	$page_transition_preloader_container_meta = onschedule_edge_add_admin_container(
		array(
			'parent'          => $page_transitions_container_meta,
			'name'            => 'page_transition_preloader_container_meta',
			'hidden_property' => 'edgtf_page_transition_preloader_meta',
			'hidden_values'   => array('','no')
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name'   => 'edgtf_smooth_pt_bgnd_color_meta',
			'type'   => 'color',
			'label'  => esc_html__( 'Page Loader Background Color', 'onschedule' ),
			'parent' => $page_transition_preloader_container_meta
		)
	);
	
	$group_pt_spinner_animation_meta = onschedule_edge_add_admin_group(
		array(
			'name'        => 'group_pt_spinner_animation_meta',
			'title'       => esc_html__( 'Loader Style', 'onschedule' ),
			'description' => esc_html__( 'Define styles for loader spinner animation', 'onschedule' ),
			'parent'      => $page_transition_preloader_container_meta
		)
	);
	
	$row_pt_spinner_animation_meta = onschedule_edge_add_admin_row(
		array(
			'name'   => 'row_pt_spinner_animation_meta',
			'parent' => $group_pt_spinner_animation_meta
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'type'          => 'selectsimple',
			'name'          => 'edgtf_smooth_pt_spinner_type_meta',
			'default_value' => '',
			'label'         => esc_html__( 'Spinner Type', 'onschedule' ),
			'parent'        => $row_pt_spinner_animation_meta,
			'options'       => array(
				'color_spinner'         => esc_html__( 'Color Spinner', 'onschedule' ),
				'rotate_circles'        => esc_html__( 'Rotate Circles', 'onschedule' ),
				'pulse'                 => esc_html__( 'Pulse', 'onschedule' ),
				'double_pulse'          => esc_html__( 'Double Pulse', 'onschedule' ),
				'cube'                  => esc_html__( 'Cube', 'onschedule' ),
				'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'onschedule' ),
				'stripes'               => esc_html__( 'Stripes', 'onschedule' ),
				'wave'                  => esc_html__( 'Wave', 'onschedule' ),
				'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'onschedule' ),
				'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'onschedule' ),
				'atom'                  => esc_html__( 'Atom', 'onschedule' ),
				'clock'                 => esc_html__( 'Clock', 'onschedule' ),
				'mitosis'               => esc_html__( 'Mitosis', 'onschedule' ),
				'lines'                 => esc_html__( 'Lines', 'onschedule' ),
				'fussion'               => esc_html__( 'Fussion', 'onschedule' ),
				'wave_circles'          => esc_html__( 'Wave Circles', 'onschedule' ),
				'pulse_circles'         => esc_html__( 'Pulse Circles', 'onschedule' )
			)
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'type'          => 'colorsimple',
			'name'          => 'edgtf_smooth_pt_spinner_color_meta',
			'default_value' => '',
			'label'         => esc_html__( 'Spinner Color', 'onschedule' ),
			'parent'        => $row_pt_spinner_animation_meta
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name'          => 'edgtf_page_transition_fadeout_meta',
			'type'          => 'select',
			'default_value' => '',
			'label'         => esc_html__( 'Enable Fade Out Animation', 'onschedule' ),
			'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'onschedule' ),
			'options'     => onschedule_edge_get_yes_no_select_array(),
			'parent'        => $page_transitions_container_meta
		
		)
	);

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_page_background_color_meta',
            'type' => 'color',
            'label' => esc_html__('Page Background Color', 'onschedule'),
            'description' => esc_html__('Choose background color for page content', 'onschedule'),
            'parent' => $general_meta_box
        )
    );

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_page_main_color_meta',
			'type' => 'color',
			'label' => esc_html__('Page First Main Color', 'onschedule'),
			'description' => esc_html__('Choose first main color for page content', 'onschedule'),
			'parent' => $general_meta_box
		)
	);

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_page_slider_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => esc_html__('Slider Shortcode', 'onschedule'),
            'description' => esc_html__('Paste your slider shortcode here', 'onschedule'),
            'parent' => $general_meta_box
        )
    );

    $edgtf_content_padding_group = onschedule_edge_add_admin_group(array(
        'name' => 'content_padding_group',
        'title' => esc_html__('Content Style', 'onschedule'),
        'description' => esc_html__('Define styles for Content area', 'onschedule'),
        'parent' => $general_meta_box
    ));

    $edgtf_content_padding_row = onschedule_edge_add_admin_row(array(
        'name' => 'edgtf_content_padding_row',
        'next' => true,
        'parent' => $edgtf_content_padding_group
    ));

    onschedule_edge_create_meta_box_field(
        array(
            'name'          => 'edgtf_page_content_top_padding',
            'type'          => 'textsimple',
            'default_value' => '',
            'label'         => esc_html__('Content Top Padding', 'onschedule'),
            'parent'        => $edgtf_content_padding_row,
            'args'          => array(
                'suffix' => 'px'
            )
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name'        => 'edgtf_page_content_top_padding_mobile',
            'type'        => 'selectsimple',
            'label'       => esc_html__('Set this top padding for mobile header', 'onschedule'),
            'parent'      => $edgtf_content_padding_row,
            'options'     => onschedule_edge_get_yes_no_select_array(false)
        )
    );

    onschedule_edge_create_meta_box_field(
        array(
            'name'        => 'edgtf_page_comments_meta',
            'type'        => 'select',
            'label'       => esc_html__('Show Comments', 'onschedule'),
            'description' => esc_html__('Enabling this option will show comments on your page', 'onschedule'),
            'parent'      => $general_meta_box,
            'options'     => onschedule_edge_get_yes_no_select_array()
        )
    );