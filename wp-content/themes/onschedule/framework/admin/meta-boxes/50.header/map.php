<?php

$header_meta_box = onschedule_edge_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post', 'restaurant-menu-item'),
        'title' => esc_html__('Header', 'onschedule'),
        'name' => 'header_meta'
    )
);

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_header_type_meta',
			'type' => 'select',
			'default_value' => '',
			'label' => esc_html__('Choose Header Type', 'onschedule'),
			'description' => esc_html__('Select header type layout', 'onschedule'),
			'parent' => $header_meta_box,
			'options' => array(
				'' => 'Default',
				'header-standard' => esc_html__('Standard Header Layout', 'onschedule'),
				'header-boxed' => esc_html__('Boxed Header Layout', 'onschedule'),
				'header-full-screen' => esc_html__('Full Screen Header Layout', 'onschedule')
			),
			'args' => array(
				'dependence' => true,
				'hide' => array(
					'' => '#edgtf_edgtf_header_standard_type_meta_container',
					'header-standard' => '',
					'header-boxed' => '#edgtf_edgtf_header_standard_type_meta_container',
					'header-full-screen' => '#edgtf_edgtf_header_standard_type_meta_container'
				),
				'show' => array(
					'' => '',
					'header-standard' => '#edgtf_edgtf_header_standard_type_meta_container',
					'header-boxed' => '',
					'header-full-screen' => ''
				)
			)
		)
	);

    $header_standard_type_meta_container = onschedule_edge_add_admin_container(
        array(
            'parent' => $header_meta_box,
            'name' => 'edgtf_header_standard_type_meta_container',
            'hidden_property' => 'edgtf_header_type_meta',
            'hidden_values' => array(
	            'header-boxed',
                'header-full-screen'
            ),
        )
    );

		onschedule_edge_create_meta_box_field(
			array(
				'name' => 'edgtf_set_menu_area_position_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Choose Menu Area Position', 'onschedule'),
				'description' => esc_html__('Select menu area position in your header', 'onschedule'),
				'parent' => $header_standard_type_meta_container,
				'options' => array(
					'' => esc_html__('Default', 'onschedule'),
					'center' => esc_html__('Center', 'onschedule'),
					'left' => esc_html__('Left', 'onschedule'),
					'right' => esc_html__('Right', 'onschedule')
				)
			)
		);

	onschedule_edge_create_meta_box_field(
		array(
			'name'   => 'edgtf_logo_image_meta',
			'type'   => 'image',
			'label'  => esc_html__('Custom Logo Image', 'onschedule'),
			'parent' => $header_meta_box
		)
	);

	onschedule_edge_create_meta_box_field(
		array(
			'name'   => 'edgtf_logo_image_dark_meta',
			'type'   => 'image',
			'label'  => esc_html__('Custom Logo Image - Dark', 'onschedule'),
			'parent' => $header_meta_box
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name'   => 'edgtf_logo_image_light_meta',
			'type'   => 'image',
			'label'  => esc_html__('Custom Logo Image - Light', 'onschedule'),
			'parent' => $header_meta_box
		)
	);

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_disable_header_widget_area_meta',
			'type' => 'yesno',
			'default_value' => 'no',
			'label' => esc_html__('Disable Header Widget Area', 'onschedule'),
			'description' => esc_html__('Enabling this option will hide widget area from the right hand side of main menu', 'onschedule'),
			'parent' => $header_meta_box
		)
	);

	$onschedule_custom_sidebars = onschedule_edge_get_custom_sidebars();
	if(count($onschedule_custom_sidebars) > 0) {
		onschedule_edge_create_meta_box_field(array(
			'name' => 'edgtf_custom_header_sidebar_meta',
			'type' => 'selectblank',
			'label' => esc_html__('Choose Custom Widget Area in Header', 'onschedule'),
			'description' => esc_html__('Choose custom widget area to display in header area from the right hand side of main menu"', 'onschedule'),
			'parent' => $header_meta_box,
			'options' => $onschedule_custom_sidebars
		));
	}

    onschedule_edge_create_meta_box_field(
        array(
            'name' => 'edgtf_top_bar_meta',
            'type' => 'select',
            'default_value' => '',
            'label' => esc_html__('Header Top Bar', 'onschedule'),
            'description' => esc_html__('Enabling this option will show header top bar area', 'onschedule'),
            'parent' => $header_meta_box,
            'options' => onschedule_edge_get_yes_no_select_array()
        )
    );

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_header_style_meta',
			'type' => 'select',
			'default_value' => '',
			'label' => esc_html__('Header Skin', 'onschedule'),
			'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'onschedule'),
			'parent' => $header_meta_box,
			'options' => array(
				'' => esc_html__('Default', 'onschedule'),
				'light-header' => esc_html__('Light', 'onschedule'),
				'dark-header' => esc_html__('Dark', 'onschedule')
			)
		)
	);

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_enable_grid_layout_header_meta',
			'type' => 'select',
			'default_value' => '',
			'label' => esc_html__('Enable Grid Layout', 'onschedule'),
			'description' => esc_html__('Enabling this option you will set header area to be in grid', 'onschedule'),
			'parent' => $header_meta_box,
			'options' => onschedule_edge_get_yes_no_select_array()
		)
	);

	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_header_area_background_color_meta',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'onschedule'),
			'description' => esc_html__('Choose a background color for header area', 'onschedule'),
			'parent' => $header_meta_box
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_header_area_background_transparency_meta',
			'type' => 'text',
			'label' => esc_html__('Background Transparency', 'onschedule'),
			'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'onschedule'),
			'parent' => $header_meta_box,
			'args' => array(
				'col_width' => 2
			)
		)
	);
	
	onschedule_edge_create_meta_box_field(
		array(
			'name' => 'edgtf_header_area_border_color_meta',
			'type' => 'color',
			'label' => esc_html__('Border Color', 'onschedule'),
			'description' => esc_html__('Choose a border bottom color for header area. This option doesn\'t work for Vertical header layout', 'onschedule'),
			'parent' => $header_meta_box
		)
	);

	onschedule_edge_create_meta_box_field(
        array(
            'name'            => 'edgtf_scroll_amount_for_sticky_meta',
            'type'            => 'text',
            'label'           => esc_html__('Scroll amount for sticky header appearance', 'onschedule'),
            'description'     => esc_html__('Define scroll amount for sticky header appearance', 'onschedule'),
            'parent'          => $header_meta_box,
            'args'            => array(
                'col_width' => 2,
                'suffix'    => 'px'
            ),
            'hidden_property' => 'header_behaviour',
            'hidden_values'   => array("sticky-header-on-scroll-up", "fixed-on-scroll")
        )
    );