<?php

if ( ! function_exists('onschedule_edge_reset_options_map') ) {
	/**
	 * Reset options panel
	 */
	function onschedule_edge_reset_options_map() {

		onschedule_edge_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__('Reset', 'onschedule'),
				'icon'  => 'fa fa-retweet'
			)
		);

		$panel_reset = onschedule_edge_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__('Reset', 'onschedule')
			)
		);

		onschedule_edge_add_admin_field(array(
			'type'	=> 'yesno',
			'name'	=> 'reset_to_defaults',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Reset to Defaults', 'onschedule'),
			'description'	=> esc_html__('This option will reset all Select Options values to defaults', 'onschedule'),
			'parent'		=> $panel_reset
		));
	}

	add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_reset_options_map', 100 );
}