<?php

if ( ! function_exists('onschedule_edge_page_options_map') ) {

    function onschedule_edge_page_options_map() {

        onschedule_edge_add_admin_page(
            array(
                'slug'  => '_page_page',
                'title' => esc_html__('Page', 'onschedule'),
                'icon'  => 'fa fa-file-text-o'
            )
        );

        /***************** Page Layout - begin **********************/

            $panel_sidebar = onschedule_edge_add_admin_panel(
                array(
                    'page'  => '_page_page',
                    'name'  => 'panel_sidebar',
                    'title' => esc_html__('Page Style', 'onschedule')
                )
            );

            onschedule_edge_add_admin_field(array(
                'name'        => 'page_show_comments',
                'type'        => 'yesno',
                'label'       => esc_html__('Show Comments', 'onschedule'),
                'description' => esc_html__('Enabling this option will show comments on your page', 'onschedule'),
                'default_value' => 'yes',
                'parent'      => $panel_sidebar
            ));

        /***************** Page Layout - end **********************/

        /***************** Content Layout - begin **********************/

            $panel_content = onschedule_edge_add_admin_panel(
                array(
                    'page'  => '_page_page',
                    'name'  => 'panel_content',
                    'title' => esc_html__('Content Style', 'onschedule')
                )
            );

            onschedule_edge_add_admin_field(array(
                'type'          => 'text',
                'name'          => 'content_top_padding',
                'default_value' => '0',
                'label'         => esc_html__('Content Top Padding for Template in Full Width', 'onschedule'),
                'description'   => esc_html__('Enter top padding for content area for templates in full width. If you set this value then it\'s important to set also Content top padding for mobile header value', 'onschedule'),
                'args'          => array(
                    'suffix'    => 'px',
                    'col_width' => 3
                ),
                'parent'        => $panel_content
            ));

            onschedule_edge_add_admin_field(array(
                'type'          => 'text',
                'name'          => 'content_top_padding_in_grid',
                'default_value' => '40',
	            'label'         => esc_html__('Content Top Padding for Templates in Grid', 'onschedule'),
	            'description'   => esc_html__('Enter top padding for content area for Templates in grid. If you set this value then it\'s important to set also Content top padding for mobile header value', 'onschedule'),
                'args'          => array(
                    'suffix'    => 'px',
                    'col_width' => 3
                ),
                'parent'        => $panel_content
            ));

            onschedule_edge_add_admin_field(array(
                'type'          => 'text',
                'name'          => 'content_top_padding_mobile',
                'default_value' => '40',
	            'label'         => esc_html__('Content Top Padding for Mobile Header', 'onschedule'),
	            'description'   => esc_html__('Enter top padding for content area for Mobile Header', 'onschedule'),
                'args'          => array(
                    'suffix'    => 'px',
                    'col_width' => 3
                ),
                'parent'        => $panel_content
            ));

        /***************** Content Layout - end **********************/

    }

    add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_page_options_map', 8);

	if (!function_exists('onschedule_edge_page_main_color_style')) {
		/**
		 * Function that prints main color inline styles
		 */
		function onschedule_edge_page_main_color_style($style) {
			$id = onschedule_edge_get_page_id();
			$class_id = onschedule_edge_get_page_id();
			$class_prefix = onschedule_edge_get_unique_page_class($class_id);
			
			$color_selectors                = array();
			$color_imp_selectors            = array();
			$background_color_selectors     = array();
			$background_color_imp_selectors = array();
			$border_selectors               = array();
			$border_imp_selectors           = array();
			
			$color_booked_selectors                = array();
			$background_color_booked_imp_selectors = array();
			$border_color_booked_selectors         = array();
			
			$container_color_class      = array();
			$container_color_imp_class  = array();
			$container_bg_class         = array();
			$container_bg_imp_class     = array();
			$container_border_class     = array();
			$container_border_imp_class = array();

			$selectors = onschedule_edge_get_page_main_color_selectors();

			if( isset( $selectors['color'] ) ) {
				$color_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['color']);
			}

			if( isset( $selectors['color-imp'] ) ) {
				$color_imp_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['color-imp']);
			}

			if( isset( $selectors['background-color'] ) ) {
				$background_color_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['background-color']);
			}

			if( isset( $selectors['background-color-imp'] ) ) {
				$background_color_imp_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['background-color-imp']);
			}

			if( isset( $selectors['border'] ) ) {
				$border_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['border']);
			}
			
			if( isset( $selectors['border-imp'] ) ) {
				$border_imp_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['border-imp']);
			}
			
			if( isset( $selectors['color-b'] ) ) {
				$color_booked_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['color-b'], true);
			}
			if( isset( $selectors['background-color-b-imp'] ) ) {
				$background_color_booked_imp_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['background-color-b-imp'], true);
			}
			if( isset( $selectors['border-b'] ) ) {
				$border_color_booked_selectors = onschedule_edge_sort_page_inline_style($class_prefix, $selectors['border-b'], true);
			}

			$page_main_color = get_post_meta($id, "edgtf_page_main_color_meta", true);

			if ($page_main_color) {
				$container_color_class['color'] = $page_main_color;
				$container_color_imp_class['color'] = $page_main_color.'!important';
				$container_bg_class['background-color'] = $page_main_color;
				$container_bg_imp_class['background-color'] = $page_main_color.'!important';
				$container_border_class['border-color'] = $page_main_color;
				$container_border_imp_class['border-color'] = $page_main_color.'!important';
			}

			$current_style = onschedule_edge_dynamic_css($color_selectors, $container_color_class);
			$current_style .= onschedule_edge_dynamic_css($color_imp_selectors, $container_color_imp_class);
			$current_style .= onschedule_edge_dynamic_css($background_color_selectors, $container_bg_class);
			$current_style .= onschedule_edge_dynamic_css($background_color_imp_selectors, $container_bg_imp_class);
			$current_style .= onschedule_edge_dynamic_css($border_selectors, $container_border_class);
			$current_style .= onschedule_edge_dynamic_css($border_imp_selectors, $container_border_imp_class);
			
			$current_style .= onschedule_edge_dynamic_css($color_booked_selectors, $container_color_class);
			$current_style .= onschedule_edge_dynamic_css($background_color_booked_imp_selectors, $container_bg_imp_class);
			$current_style .= onschedule_edge_dynamic_css($border_color_booked_selectors, $container_border_class);

			$current_style = $current_style . $style;

			return $current_style;
		}

		add_filter('onschedule_edge_filter_add_page_custom_style', 'onschedule_edge_page_main_color_style');
	}

	if(!function_exists('onschedule_edge_get_page_main_color_selectors')) {
		/**
		 * Function that return page main color selectors
		 */
		function onschedule_edge_get_page_main_color_selectors() {
			$color_selector = array(
				'h1 a:hover',
				'h2 a:hover',
				'h3 a:hover',
				'h4 a:hover',
				'h5 a:hover',
				'h6 a:hover',
				'a:hover',
				'p a:hover',
				'.edgtf-comment-holder .edgtf-comment-text .replay',
				'.edgtf-comment-holder .edgtf-comment-text .comment-reply-link',
				'.edgtf-comment-holder .edgtf-comment-text .comment-edit-link',
				'.edgtf-comment-holder .edgtf-comment-text .edgtf-comment-date',
				'.edgtf-owl-slider .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-owl-slider .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-owl-slider .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-owl-slider .owl-nav .owl-next:hover .edgtf-next-icon',
				'.edgtf-main-menu ul li a:hover',
				'.edgtf-main-menu > ul > li.edgtf-active-item > a',
				'.edgtf-drop-down .second .inner ul li.current-menu-ancestor > a',
				'.edgtf-drop-down .second .inner ul li.current-menu-item > a',
				'.edgtf-drop-down .wide .second .inner > ul > li.current-menu-ancestor > a',
				'.edgtf-drop-down .wide .second .inner > ul > li.current-menu-item > a',
				'.edgtf-mobile-header .edgtf-mobile-nav ul li a:hover',
				'.edgtf-mobile-header .edgtf-mobile-nav ul li h5:hover',
				'.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor > a',
				'.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item > a',
				'.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid > ul > li.edgtf-active-item > a',
				'.edgtf-side-menu-button-opener.opened',
				'.edgtf-side-menu-button-opener:hover',
				'nav.edgtf-fullscreen-menu ul li a:hover',
				'nav.edgtf-fullscreen-menu ul li ul li.current-menu-ancestor > a',
				'nav.edgtf-fullscreen-menu ul li ul li.current-menu-item > a',
				'nav.edgtf-fullscreen-menu > ul > li.edgtf-active-item > a',
				'.edgtf-search-page-holder article.sticky .edgtf-post-title-area h3 a',
				'.edgtf-pl-standard-pagination ul li.edgtf-pl-pag-active a',
				'.edgtf-portfolio-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-portfolio-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-portfolio-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-portfolio-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
				'.edgtf-portfolio-single-holder .edgtf-ps-info-holder .edgtf-ps-info-item a:not(.edgtf-share-link):hover',
				'.edgtf-blog-holder article.sticky .edgtf-post-title a',
				'.edgtf-blog-holder article .edgtf-post-info-top a',
				'.edgtf-bl-standard-pagination ul li.edgtf-bl-pag-active a',
				'.edgtf-single-links-pages .edgtf-single-links-pages-inner > a:hover',
				'.edgtf-single-links-pages .edgtf-single-links-pages-inner > span:hover',
				'.edgtf-related-posts-holder .edgtf-related-post .edgtf-post-info a',
				'.edgtf-blog-list-holder .edgtf-bli-info-top a',
				'.edgtf-blog-list-holder.edgtf-bl-simple .edgtf-post-info-date',
				'.edgtf-blog-list-holder.edgtf-bl-minimal .edgtf-post-info-date',
				'.edgtf-blog-slider-holder .edgtf-blog-slider-item .edgtf-item-info-section > div a:hover',
				'.edgtf-blog-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-blog-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
				'.edgtf-btn.edgtf-btn-simple',
				'.edgtf-btn.edgtf-btn-outline',
				'.edgtf-image-gallery .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-image-gallery .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-image-gallery .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-image-gallery .owl-nav .owl-next:hover .edgtf-next-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-image-slider-holder .owl-nav .owl-next:hover .edgtf-next-icon',
				'.edgtf-social-share-holder.edgtf-dropdown .edgtf-social-share-dropdown-opener:hover',
				'.edgtf-team-holder .edgtf-team-social-holder .edgtf-team-icon .edgtf-icon-element:hover',
				'.edgtf-testimonials-holder .owl-nav .owl-prev:hover .edgtf-prev-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-prev:hover .edgtf-next-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-next:hover .edgtf-prev-icon',
				'.edgtf-testimonials-holder .owl-nav .owl-next:hover .edgtf-next-icon',
				'aside.edgtf-sidebar .widget.widget_pages ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_archive ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_categories ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_meta ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_recent_comments ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_recent_entries ul li a:hover',
				'aside.edgtf-sidebar .widget.widget_nav_menu ul li a:hover',
				'.wpb_widgetised_column .widget.widget_pages ul li a:hover',
				'.wpb_widgetised_column .widget.widget_archive ul li a:hover',
				'.wpb_widgetised_column .widget.widget_categories ul li a:hover',
				'.wpb_widgetised_column .widget.widget_meta ul li a:hover',
				'.wpb_widgetised_column .widget.widget_recent_comments ul li a:hover',
				'.wpb_widgetised_column .widget.widget_recent_entries ul li a:hover',
				'.wpb_widgetised_column .widget.widget_nav_menu ul li a:hover',
				'.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-twitter-icon',
				'.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-twitter-icon',
				'.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text a',
				'.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text span',
				'.edgtf-single-menu-item-holder .edgtf-smi-related-items .edgtf-smi-related-list .edgtf-smi-related-post-date',
				'.edgtf-working-hours-holder .edgtf-wh-title .edgtf-wh-title-accent-word',
				'.edgtf-blog-holder article .edgtf-post-info-bottom .edgtf-social-share-holder li a:hover',
				'.edgtf-blog-list-holder.edgtf-bl-boxed .edgtf-bl-item:hover .edgtf-bli-info-top a',
				'.edgtf-single-menu-item-holder .edgtf-smi-share-holder .edgtf-social-share-holder.edgtf-list li a:hover',
				'.edgtf-drop-down .second .inner ul li a .item_outer:before',
				'.edgtf-side-menu .widget.widget_nav_menu ul li a:hover',
				'.edgtf-top-bar .widget a:hover'
			);
			
			$woo_color_selector = array();
			if(onschedule_edge_is_woocommerce_installed()) {
				$woo_color_selector = array(
					'.edgtf-woocommerce-page .woocommerce-message > a:hover',
					'.edgtf-woocommerce-page .woocommerce-info > a:hover',
					'.edgtf-woocommerce-page .woocommerce-error > a:hover',
					'.edgtf-woocommerce-page .woocommerce-info .showcoupon:hover',
					'.woocommerce-pagination .page-numbers li a.current, .woocommerce-pagination .page-numbers li a:hover',
					'.woocommerce-pagination .page-numbers li span.current',
					'.woocommerce-pagination .page-numbers li span:hover',
					'.edgtf-woo-view-all-pagination a:before',
					'.edgtf-woocommerce-page .edgtf-content .variations .reset_variations',
					'.edgtf-woo-single-page .edgtf-single-product-summary .price',
					'.edgtf-woo-single-page .edgtf-single-product-summary .product_meta > span',
					'.edgtf-woo-single-page .edgtf-single-product-summary .product_meta > span a:hover',
					'.edgtf-woo-single-page .edgtf-single-product-summary .edgtf-woo-social-share-holder > span',
					'.edgtf-woo-single-page .edgtf-single-product-summary p.stock.out-of-stock',
					'.edgtf-woo-single-page .edgtf-single-product-summary p.stock.in-stock',
					'.edgtf-woo-single-page .woocommerce-tabs ul.tabs > li.active a',
					'.edgtf-woo-single-page .woocommerce-tabs table th',
					'.edgtf-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a:before',
					'.edgtf-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a.active:after',
					'ul.products > .product .edgtf-product-list-categories a:hover',
					'.edgtf-woocommerce-page table.cart thead tr th',
					'.edgtf-woocommerce-page table.cart tr.cart_item td.product-remove a:hover',
					'.edgtf-woocommerce-page .cart-empty',
					'.edgtf-woocommerce-page.woocommerce-order-received .woocommerce ul.order_details li strong',
					'.edgtf-woocommerce-page.woocommerce-account .woocommerce form.edit-account fieldset > legend',
					'.edgtf-woocommerce-page.woocommerce-account .vc_row .woocommerce form.login p label:not(.inline)',
					'.edgtf-woocommerce-page.edgtf-woocommerce-order-tracking .woocommerce > .track_order .form-row-first label',
					'.edgtf-woocommerce-page.edgtf-woocommerce-order-tracking .woocommerce > .track_order .form-row-last label',
					'.edgtf-content .woocommerce.add_to_cart_inline del',
					'.edgtf-content .woocommerce.add_to_cart_inline ins',
					'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin',
					'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin:hover',
					'.edgtf-pl-holder .edgtf-pli .edgtf-pli-category a:hover',
					'.edgtf-pl-holder .edgtf-pli .edgtf-pli-excerpt',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .button',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .added_to_cart',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-title',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-excerpt',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-rating',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-rating span',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-price',
					'.edgtf-pl-holder.edgtf-product-info-dark .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-category a',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-category a:hover',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-excerpt',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .button',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .added_to_cart',
					'.edgtf-plc-holder .owl-nav .edgtf-prev-icon',
					'.edgtf-plc-holder .owl-nav .edgtf-next-icon',
					'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li.active a',
					'div.woocommerce > .single-product .woocommerce-tabs table th',
					'div.woocommerce > .single-product .woocommerce-tabs #reviews .comment-respond .stars a:before',
					'div.woocommerce > .single-product .woocommerce-tabs #reviews .comment-respond .stars a.active:after',
					'.widget.woocommerce.widget_product_tag_cloud .tagcloud a',
					'.edgtf-light-header .edgtf-page-header > div:not(.edgtf-sticky-header):not(.fixed) .edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-count',
					'.edgtf-shopping-cart-dropdown .edgtf-item-info-holder .remove:hover',
					'.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-subtotal-holder .edgtf-total-amount',
					'.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-cart-description .edgtf-cart-description-inner span',
					'.woocommerce .star-rating',
					'.edgtf-shopping-cart-holder .edgtf-header-cart:hover'
				);
			}
			
			$color_selector = array_merge($color_selector, $woo_color_selector);

			$background_color_selector = array(
				'.edgtf-st-loader .pulse',
				'.edgtf-st-loader .double_pulse .double-bounce1',
				'.edgtf-st-loader .double_pulse .double-bounce2',
				'.edgtf-st-loader .cube',
				'.edgtf-st-loader .rotating_cubes .cube1',
				'.edgtf-st-loader .rotating_cubes .cube2',
				'.edgtf-st-loader .stripes > div',
				'.edgtf-st-loader .wave > div',
				'.edgtf-st-loader .two_rotating_circles .dot1',
				'.edgtf-st-loader .two_rotating_circles .dot2',
				'.edgtf-st-loader .five_rotating_circles .container1 > div',
				'.edgtf-st-loader .five_rotating_circles .container2 > div',
				'.edgtf-st-loader .five_rotating_circles .container3 > div',
				'.edgtf-st-loader .atom .ball-1:before',
				'.edgtf-st-loader .atom .ball-2:before',
				'.edgtf-st-loader .atom .ball-3:before',
				'.edgtf-st-loader .atom .ball-4:before',
				'.edgtf-st-loader .clock .ball:before',
				'.edgtf-st-loader .mitosis .ball',
				'.edgtf-st-loader .lines .line1',
				'.edgtf-st-loader .lines .line2',
				'.edgtf-st-loader .lines .line3',
				'.edgtf-st-loader .lines .line4',
				'.edgtf-st-loader .fussion .ball',
				'.edgtf-st-loader .fussion .ball-1',
				'.edgtf-st-loader .fussion .ball-2',
				'.edgtf-st-loader .fussion .ball-3',
				'.edgtf-st-loader .fussion .ball-4',
				'.edgtf-st-loader .wave_circles .ball',
				'.edgtf-st-loader .pulse_circles .ball',
				'#submit_comment',
				'.post-password-form input[type="submit"]',
				'input.wpcf7-form-control.wpcf7-submit',
				'#edgtf-back-to-top > span',
				'#ui-datepicker-div .ui-datepicker-today',
				'.edgtf-search-page-holder .edgtf-search-page-form .edgtf-form-holder button',
				'.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
				'.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls > .mejs-time-rail .mejs-time-total .mejs-time-current',
				'.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls > a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
				'.edgtf-blog-holder article.format-quote .edgtf-post-quote-holder',
				'.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-title-holder.ui-state-active',
				'.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-title-holder.ui-state-hover',
				'.edgtf-accordion-holder.edgtf-ac-simple .edgtf-title-holder:not(.ui-state-active) .edgtf-accordion-mark',
				'.edgtf-btn.edgtf-btn-solid',
				'.edgtf-icon-shortcode.edgtf-circle',
				'.edgtf-icon-shortcode.edgtf-square',
				'.edgtf-icon-shortcode.edgtf-dropcaps.edgtf-circle',
				'.edgtf-image-slider-holder .edgtf-is-slider-inner .edgtf-is-content > ul li:before',
				'.edgtf-progress-bar .edgtf-pb-content-holder .edgtf-pb-content',
				'.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-active a',
				'.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-hover a',
				'.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-active a',
				'.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-hover a',
				'.widget.widget_search button:hover',
				'.widget.widget_tag_cloud a:hover',
				'.edgtf-single-menu-item-holder .edgtf-smi-prep-info-top .edgtf-prep-info-icon',
				'.edgtf-single-menu-item-holder .edgtf-smi-prep-info-bottom .edgtf-prep-info-price-holder',
				'.edgtf-menu-list .edgtf-ml-title-holder .edgtf-ml-label-holder .edgtf-ml-label',
				'.edgtf-menu-grid .edgtf-mg-image-holder .edgtf-mg-price-holder .edgtf-mg-price',
				'.edgtf-menu-grid .edgtf-mg-image-holder .edgtf-mg-label-holder',
				'.edgtf-btn.edgtf-btn-solid:not(.edgtf-btn-custom-hover-bg)',
				'.edgtf-image-gallery .edgtf-ig-grid .edgtf-ig-image a:after',
				'.edgtf-booked-custom-skin #booked-profile-page input[type=submit].button-primary'
			);
			
			$woo_background_color_selector = array();
			if(onschedule_edge_is_woocommerce_installed()) {
				$woo_background_color_selector = array(
					'.woocommerce-page .edgtf-content a.button',
					'.woocommerce-page .edgtf-content a.added_to_cart',
					'.woocommerce-page .edgtf-content input[type="submit"]',
					'.woocommerce-page .edgtf-content button[type="submit"]',
					'.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
					'div.woocommerce a.button',
					'div.woocommerce a.added_to_cart',
					'div.woocommerce input[type="submit"]',
					'div.woocommerce button[type="submit"]',
					'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
					'.woocommerce .edgtf-onsale',
					'.woocommerce .edgtf-out-of-stock',
					'.edgtf-woo-single-page .woocommerce-tabs ul.tabs > li.active a:after',
					'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin:hover',
					'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-image .edgtf-pli-onsale',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-image .edgtf-pli-out-of-stock',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .button',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .added_to_cart',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .button:hover',
					'.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .added_to_cart:hover',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-image-outer .edgtf-plc-image .edgtf-plc-onsale',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-image-outer .edgtf-plc-image .edgtf-plc-out-of-stock',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .button:hover',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .added_to_cart:hover',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .button',
					'.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .added_to_cart',
					'.edgtf-plc-holder .owl-dots .owl-dot.active span',
					'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li.active a:after',
					'.widget.woocommerce.widget_product_search .woocommerce-product-search button',
					'.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-view-cart:hover',
					'.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-checkout'
				);
			}
			
			$background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);

			$color_important_selector = array(
				'.edgtf-fullscreen-menu-opener:hover',
				'.edgtf-fullscreen-menu-opener.edgtf-fm-opened',
				'.edgtf-blog-slider-holder .edgtf-blog-slider-item .edgtf-section-button-holder a:hover',
				'.edgtf-social-icon-widget-holder.edgtf-has-custom-hover:hover'
			);

			$background_color_important_selector = array(
				'.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-hover-bg):hover',
				'.edgtf-price-table.edgtf-pt-button-dark .edgtf-pt-inner .edgtf-btn.edgtf-btn-solid:hover',
				'#edgtf-back-to-top > span',
				'.edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-count',
				'.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range',
				'div.woocommerce button[type="submit"]:hover',
				'.woocommerce-page .edgtf-content button[type="submit"]:hover',
				'.woocommerce-page .edgtf-content a.button:hover',
				'.woocommerce-page .edgtf-content a.added_to_cart:hover',
				'.woocommerce-page .edgtf-content input[type="submit"]:hover',
				'.woocommerce-page .edgtf-content button[type="submit"]:hover',
				'.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
				'div.woocommerce a.button:hover',
				'div.woocommerce a.added_to_cart:hover',
				'div.woocommerce input[type="submit"]:hover',
				'div.woocommerce button[type="submit"]:hover',
				'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
				'.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
				'.edgtf-image-slider-holder .edgtf-is-slider-inner .edgtf-is-content > ul li:before',
				'.edgtf-booked-custom-skin.edgtf-booked-user-appointments-holder #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a',
				'.edgtf-booked-custom-skin.edgtf-booked-user-profile-holder #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a',
				'.edgtf-booked-custom-skin.edgtf-booked-user-profile-holder #booked-profile-page input[type=submit].button-primary',
				'.edgtf-booked-custom-skin.edgtf-booked-login-holder #booked-profile-page #profile-login input[type=submit].button-primary',
				'.edgtf-booked-custom-skin.edgtf-booked-login-holder #booked-profile-page #profile-forgot input[type=submit].button-primary'
			);

			$border_color_selector = array(
				'.edgtf-st-loader .pulse_circles .ball',
				'#edgtf-back-to-top > span',
				'.edgtf-drop-down .second .inner',
				'.edgtf-drop-down .narrow .second .inner ul li ul',
				'.edgtf-pl-load-more-holder .edgtf-pl-load-more a:hover',
				'.edgtf-btn.edgtf-btn-solid',
				'.edgtf-btn.edgtf-btn-outline',
				'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-white-skin:hover',
				'.edgtf-product-info .edgtf-pi-add-to-cart .edgtf-btn.edgtf-btn-solid.edgtf-dark-skin',
				'.edgtf-shopping-cart-dropdown',
				'.edgtf-color-spinner .edgtf-cs-line',
				'.edgtf-booked-calendar-holder.edgtf-custom-style table.booked-calendar td.today .date span'
			);
			
			$border_color_important_selector = array(
				'.edgtf-price-table.edgtf-pt-button-dark .edgtf-pt-inner .edgtf-btn.edgtf-btn-solid:hover',
				'.edgtf-booked-custom-skin #booked-profile-page input[type=submit].button-primary'
			);
			
			$color_booked = array(
				'.edgtf-booked-custom-style .booked-modal .bm-window a:not(.close)',
				'.edgtf-booked-custom-style .booked-modal .bm-window p i.fa',
				'.edgtf-booked-custom-style .booked-modal .bm-window p.appointment-title'
			);
			
			$background_color_booked_imp = array(
				'.edgtf-booked-custom-style .booked-modal input[type=submit].button-primary',
				'.edgtf-booked-custom-style .booked-modal button.cancel:hover',
				'.edgtf-booked-custom-style #ui-datepicker-div.booked_custom_date_picker .ui-datepicker-header',
				'.edgtf-booked-custom-style #ui-datepicker-div table.ui-datepicker-calendar tbody td #ui-datepicker-div.booked_custom_date_picker table.ui-datepicker-calendar tbody td a.ui-state-active:hover',
				'.edgtf-booked-custom-style #ui-datepicker-div table.ui-datepicker-calendar tbody td a.ui-state-active',
			);
			
			$border_color_booked = array(
				'.edgtf-booked-custom-style .booked-modal .bm-window'
			);

			$selectors = array(
				'color'                  => $color_selector,
				'color-imp'              => $color_important_selector,
				'background-color'       => $background_color_selector,
				'background-color-imp'   => $background_color_important_selector,
				'border'                 => $border_color_selector,
				'border-imp'             => $border_color_important_selector,
				'color-b'                => $color_booked,
				'background-color-b-imp' => $background_color_booked_imp,
				'border-b'               => $border_color_booked
			);

			return $selectors;
		}
	}
}