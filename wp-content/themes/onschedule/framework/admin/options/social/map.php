<?php

if (!function_exists('onschedule_edge_social_options_map')) {

    function onschedule_edge_social_options_map() {

        onschedule_edge_add_admin_page(
            array(
                'slug' => '_social_page',
                'title' => esc_html__('Social Networks', 'onschedule'),
                'icon' => 'fa fa-share-alt'
            )
        );

        /**
         * Enable Social Share
         */
        $panel_social_share = onschedule_edge_add_admin_panel(array(
            'page' => '_social_page',
            'name' => 'panel_social_share',
            'title' => esc_html__('Enable Social Share', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_social_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Social Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow social share on networks of your choice', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_panel_social_networks, #edgtf_panel_show_social_share_on'
            ),
            'parent' => $panel_social_share
        ));

        $panel_show_social_share_on = onschedule_edge_add_admin_panel(array(
            'page' => '_social_page',
            'name' => 'panel_show_social_share_on',
            'title' => esc_html__('Show Social Share On', 'onschedule'),
            'hidden_property' => 'enable_social_share',
            'hidden_value' => 'no'
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_social_share_on_post',
            'default_value' => 'no',
            'label' => esc_html__('Posts', 'onschedule'),
            'description' => esc_html__('Show Social Share on Blog Posts', 'onschedule'),
            'parent' => $panel_show_social_share_on
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_social_share_on_page',
            'default_value' => 'no',
            'label' => esc_html__('Pages', 'onschedule'),
            'description' => esc_html__('Show Social Share on Pages', 'onschedule'),
            'parent' => $panel_show_social_share_on
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_social_share_on_portfolio-item',
            'default_value' => 'no',
            'label' => esc_html__('Portfolio Item', 'onschedule'),
            'description' => esc_html__('Show Social Share for Portfolio Items', 'onschedule'),
            'parent' => $panel_show_social_share_on
        ));
	
	    if (onschedule_edge_restaurant_installed()) {
		    onschedule_edge_add_admin_field( array(
			    'type'          => 'yesno',
			    'name'          => 'enable_social_share_on_restaurant-menu-item',
			    'default_value' => 'no',
			    'label'         => esc_html__('Restaurant Menu Item', 'onschedule'),
			    'description'   => esc_html__('Show Social Share for Restaurant Menu Item', 'onschedule'),
			    'parent'        => $panel_show_social_share_on
		    ) );
	    }

        if (onschedule_edge_is_woocommerce_installed()) {
            onschedule_edge_add_admin_field(array(
                'type' => 'yesno',
                'name' => 'enable_social_share_on_product',
                'default_value' => 'no',
                'label' => esc_html__('Product', 'onschedule'),
                'description' => esc_html__('Show Social Share for Product Items', 'onschedule'),
                'parent' => $panel_show_social_share_on
            ));
        }

        /**
         * Social Share Networks
         */
        $panel_social_networks = onschedule_edge_add_admin_panel(array(
            'page' => '_social_page',
            'name' => 'panel_social_networks',
            'title' => esc_html__('Social Networks', 'onschedule'),
            'hidden_property' => 'enable_social_share',
            'hidden_value' => 'no'
        ));

        /**
         * Facebook
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'facebook_title',
            'title' => esc_html__('Share on Facebook', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_facebook_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via Facebook', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_facebook_share_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_facebook_share_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_facebook_share_container',
            'hidden_property' => 'enable_facebook_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'facebook_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_facebook_share_container
        ));

        /**
         * Twitter
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'twitter_title',
            'title' => esc_html__('Share on Twitter', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_twitter_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via Twitter', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_twitter_share_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_twitter_share_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_twitter_share_container',
            'hidden_property' => 'enable_twitter_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'twitter_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_twitter_share_container
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'text',
            'name' => 'twitter_via',
            'default_value' => '',
            'label' => esc_html__('Via', 'onschedule'),
            'parent' => $enable_twitter_share_container
        ));

        /**
         * Google Plus
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'google_plus_title',
            'title' => esc_html__('Share on Google Plus', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_google_plus_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via Google Plus', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_google_plus_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_google_plus_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_google_plus_container',
            'hidden_property' => 'enable_google_plus_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'google_plus_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_google_plus_container
        ));

        /**
         * Linked In
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'linkedin_title',
            'title' => esc_html__('Share on LinkedIn', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_linkedin_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via LinkedIn', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_linkedin_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_linkedin_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_linkedin_container',
            'hidden_property' => 'enable_linkedin_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'linkedin_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_linkedin_container
        ));

        /**
         * Tumblr
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'tumblr_title',
            'title' => esc_html__('Share on Tumblr', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_tumblr_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via Tumblr', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_tumblr_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_tumblr_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_tumblr_container',
            'hidden_property' => 'enable_tumblr_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'tumblr_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_tumblr_container
        ));

        /**
         * Pinterest
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'pinterest_title',
            'title' => esc_html__('Share on Pinterest', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_pinterest_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via Pinterest', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_pinterest_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_pinterest_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_pinterest_container',
            'hidden_property' => 'enable_pinterest_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'pinterest_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_pinterest_container
        ));

        /**
         * VK
         */
        onschedule_edge_add_admin_section_title(array(
            'parent' => $panel_social_networks,
            'name' => 'vk_title',
            'title' => esc_html__('Share on VK', 'onschedule')
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_vk_share',
            'default_value' => 'no',
            'label' => esc_html__('Enable Share', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow sharing via VK', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_vk_container'
            ),
            'parent' => $panel_social_networks
        ));

        $enable_vk_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_vk_container',
            'hidden_property' => 'enable_vk_share',
            'hidden_value' => 'no',
            'parent' => $panel_social_networks
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'vk_icon',
            'default_value' => '',
            'label' => esc_html__('Upload Icon', 'onschedule'),
            'parent' => $enable_vk_container
        ));

        if (defined('EDGEF_TWITTER_FEED_VERSION')) {
            $twitter_panel = onschedule_edge_add_admin_panel(array(
                'title' => esc_html__('Twitter', 'onschedule'),
                'name' => 'panel_twitter',
                'page' => '_social_page'
            ));

            onschedule_edge_add_admin_twitter_button(array(
                'name' => 'twitter_button',
                'parent' => $twitter_panel
            ));
        }

        if (defined('EDGEF_INSTAGRAM_FEED_VERSION')) {
            $instagram_panel = onschedule_edge_add_admin_panel(array(
                'title' => esc_html__('Instagram', 'onschedule'),
                'name' => 'panel_instagram',
                'page' => '_social_page'
            ));

            onschedule_edge_add_admin_instagram_button(array(
                'name' => 'instagram_button',
                'parent' => $instagram_panel
            ));
        }

        /**
         * Open Graph
         */
        $panel_open_graph = onschedule_edge_add_admin_panel(array(
            'page' => '_social_page',
            'name' => 'panel_open_graph',
            'title' => esc_html__('Open Graph', 'onschedule'),
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'yesno',
            'name' => 'enable_open_graph',
            'default_value' => 'no',
            'label' => esc_html__('Enable Open Graph', 'onschedule'),
            'description' => esc_html__('Enabling this option will allow usage of Open Graph protocol on your site', 'onschedule'),
            'args' => array(
                'dependence' => true,
                'dependence_hide_on_yes' => '',
                'dependence_show_on_yes' => '#edgtf_enable_open_graph_container'
            ),
            'parent' => $panel_open_graph
        ));

        $enable_open_graph_container = onschedule_edge_add_admin_container(array(
            'name' => 'enable_open_graph_container',
            'hidden_property' => 'enable_open_graph',
            'hidden_value' => 'no',
            'parent' => $panel_open_graph
        ));

        onschedule_edge_add_admin_field(array(
            'type' => 'image',
            'name' => 'open_graph_image',
            'default_value' => EDGE_ASSETS_ROOT . '/img/open_graph.jpg',
            'label' => esc_html__('Default Share Image', 'onschedule'),
            'parent' => $enable_open_graph_container,
            'description' => esc_html('Used when featured image is not set. Make sure that image is at least 1200 x 630 pixels, up to 8MB in size', 'onschedule'),
        ));

    }

    add_action('onschedule_edge_action_options_map', 'onschedule_edge_social_options_map', 18);
}