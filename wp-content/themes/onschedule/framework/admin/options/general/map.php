<?php

if ( ! function_exists('onschedule_edge_general_options_map') ) {
    /**
     * General options page
     */
    function onschedule_edge_general_options_map() {

        onschedule_edge_add_admin_page(
            array(
                'slug'  => '',
                'title' => esc_html__('General', 'onschedule'),
                'icon'  => 'fa fa-institution'
            )
        );

        $panel_design_style = onschedule_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_design_style',
                'title' => esc_html__('Design Style', 'onschedule')
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'google_fonts',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Google Font Family', 'onschedule'),
                'description'   => esc_html__('Choose a default Google font for your site', 'onschedule'),
                'parent' => $panel_design_style
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_fonts',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Additional Google Fonts', 'onschedule'),
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_additional_google_fonts_container"
                )
            )
        );

        $additional_google_fonts_container = onschedule_edge_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'additional_google_fonts_container',
                'hidden_property'   => 'additional_google_fonts',
                'hidden_value'      => 'no'
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font1',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'onschedule'),
                'description'   => esc_html__('Choose additional Google font for your site', 'onschedule'),
                'parent'        => $additional_google_fonts_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font2',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'onschedule'),
                'description'   => esc_html__('Choose additional Google font for your site', 'onschedule'),
                'parent'        => $additional_google_fonts_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font3',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'onschedule'),
                'description'   => esc_html__('Choose additional Google font for your site', 'onschedule'),
                'parent'        => $additional_google_fonts_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font4',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'onschedule'),
                'description'   => esc_html__('Choose additional Google font for your site', 'onschedule'),
                'parent'        => $additional_google_fonts_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font5',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'onschedule'),
                'description'   => esc_html__('Choose additional Google font for your site', 'onschedule'),
                'parent'        => $additional_google_fonts_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name' => 'google_font_weight',
                'type' => 'checkboxgroup',
                'default_value' => '',
                'label' => esc_html__('Google Fonts Style & Weight', 'onschedule'),
                'description' => esc_html__('Choose a default Google font weights for your site. Impact on page load time', 'onschedule'),
                'parent' => $panel_design_style,
                'options' => array(
                    '100'       => esc_html__('100 Thin', 'onschedule'),
                    '100italic' => esc_html__('100 Thin Italic', 'onschedule'),
                    '200'       => esc_html__('200 Extra-Light', 'onschedule'),
                    '200italic' => esc_html__('200 Extra-Light Italic', 'onschedule'),
                    '300'       => esc_html__('300 Light', 'onschedule'),
                    '300italic' => esc_html__('300 Light Italic', 'onschedule'),
                    '400'       => esc_html__('400 Regular', 'onschedule'),
                    '400italic' => esc_html__('400 Regular Italic', 'onschedule'),
                    '500'       => esc_html__('500 Medium', 'onschedule'),
                    '500italic' => esc_html__('500 Medium Italic', 'onschedule'),
                    '600'       => esc_html__('600 Semi-Bold', 'onschedule'),
                    '600italic' => esc_html__('600 Semi-Bold Italic', 'onschedule'),
                    '700'       => esc_html__('700 Bold', 'onschedule'),
                    '700italic' => esc_html__('700 Bold Italic', 'onschedule'),
                    '800'       => esc_html__('800 Extra-Bold', 'onschedule'),
                    '800italic' => esc_html__('800 Extra-Bold Italic', 'onschedule'),
                    '900'       => esc_html__('900 Ultra-Bold', 'onschedule'),
                    '900italic' => esc_html__('900 Ultra-Bold Italic', 'onschedule')
                )
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name' => 'google_font_subset',
                'type' => 'checkboxgroup',
                'default_value' => '',
                'label' => esc_html__('Google Fonts Subset', 'onschedule'),
                'description' => esc_html__('Choose a default Google font subsets for your site', 'onschedule'),
                'parent' => $panel_design_style,
                'options' => array(
                    'latin' => esc_html__('Latin', 'onschedule'),
                    'latin-ext' => esc_html__('Latin Extended', 'onschedule'),
                    'cyrillic' => esc_html__('Cyrillic', 'onschedule'),
                    'cyrillic-ext' => esc_html__('Cyrillic Extended', 'onschedule'),
                    'greek' => esc_html__('Greek', 'onschedule'),
                    'greek-ext' => esc_html__('Greek Extended', 'onschedule'),
                    'vietnamese' => esc_html__('Vietnamese', 'onschedule')
                )
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'first_color',
                'type'          => 'color',
                'label'         => esc_html__('First Main Color', 'onschedule'),
                'description'   => esc_html__('Choose the most dominant theme color. Default color is #00bbb3', 'onschedule'),
                'parent'        => $panel_design_style
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'page_background_color',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'onschedule'),
                'description'   => esc_html__('Choose the background color for page content. Default color is #ffffff', 'onschedule'),
                'parent'        => $panel_design_style
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'selection_color',
                'type'          => 'color',
                'label'         => esc_html__('Text Selection Color', 'onschedule'),
                'description'   => esc_html__('Choose the color users see when selecting text', 'onschedule'),
                'parent'        => $panel_design_style
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'boxed',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Boxed Layout', 'onschedule'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_boxed_container"
                )
            )
        );

        $boxed_container = onschedule_edge_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'boxed_container',
                'hidden_property'   => 'boxed',
                'hidden_value'      => 'no'
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'page_background_color_in_box',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'onschedule'),
                'description'   => esc_html__('Choose the page background color outside box', 'onschedule'),
                'parent'        => $boxed_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'boxed_background_image',
                'type'          => 'image',
                'label'         => esc_html__('Background Image', 'onschedule'),
                'description'   => esc_html__('Choose an image to be displayed in background', 'onschedule'),
                'parent'        => $boxed_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'boxed_pattern_background_image',
                'type'          => 'image',
                'label'         => esc_html__('Background Pattern', 'onschedule'),
                'description'   => esc_html__('Choose an image to be used as background pattern', 'onschedule'),
                'parent'        => $boxed_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'boxed_background_image_attachment',
                'type'          => 'select',
                'default_value' => 'fixed',
                'label'         => esc_html__('Background Image Attachment', 'onschedule'),
                'description'   => esc_html__('Choose background image attachment', 'onschedule'),
                'parent'        => $boxed_container,
                'options'       => array(
                    'fixed'     => esc_html__('Fixed', 'onschedule'),
                    'scroll'    => esc_html__('Scroll', 'onschedule')
                )
            )
        );
        
        onschedule_edge_add_admin_field(
            array(
                'name'          => 'paspartu',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Passepartout', 'onschedule'),
                'description'   => esc_html__('Enabling this option will display passepartout around site content', 'onschedule'),
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_paspartu_container"
                )
            )
        );

        $paspartu_container = onschedule_edge_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'paspartu_container',
                'hidden_property'   => 'paspartu',
                'hidden_value'      => 'no'
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'paspartu_color',
                'type'          => 'color',
                'label'         => esc_html__('Passepartout Color', 'onschedule'),
                'description'   => esc_html__('Choose passepartout color, default value is #ffffff', 'onschedule'),
                'parent'        => $paspartu_container
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name' => 'paspartu_width',
                'type' => 'text',
                'label' => esc_html__('Passepartout Size', 'onschedule'),
                'description' => esc_html__('Enter size amount for passepartout', 'onschedule'),
                'parent' => $paspartu_container,
                'args' => array(
                    'col_width' => 2,
                    'suffix' => '%'
                )
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'parent' => $paspartu_container,
                'type' => 'yesno',
                'default_value' => 'no',
                'name' => 'disable_top_paspartu',
                'label' => esc_html__('Disable Top Passepartout', 'onschedule')
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'initial_content_width',
                'type'          => 'select',
                'default_value' => 'edgtf-grid-1300',
                'label'         => esc_html__('Initial Width of Content', 'onschedule'),
                'description'   => esc_html__('Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'onschedule'),
                'parent'        => $panel_design_style,
                'options'       => array(
	                'edgtf-grid-1300' => esc_html__('1300px - default', 'onschedule'),
                    'edgtf-grid-1200' => esc_html__('1200px', 'onschedule'),
	                'edgtf-grid-1100' => esc_html__('1100px', 'onschedule'),
	                'edgtf-grid-1000' => esc_html__('1000px', 'onschedule'),
                    'edgtf-grid-800'  => esc_html__('800px', 'onschedule')
                )
            )
        );

        $panel_settings = onschedule_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_settings',
                'title' => esc_html__('Settings', 'onschedule')
            )
        );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'name'          => 'smooth_page_transitions',
			    'type'          => 'yesno',
			    'default_value' => 'no',
			    'label'         => esc_html__( 'Smooth Page Transitions', 'onschedule' ),
			    'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'onschedule' ),
			    'parent'        => $panel_settings,
			    'args'          => array(
				    "dependence"             => true,
				    "dependence_hide_on_yes" => "",
				    "dependence_show_on_yes" => "#edgtf_page_transitions_container"
			    )
		    )
	    );
	
	    $page_transitions_container = onschedule_edge_add_admin_container(
		    array(
			    'parent'          => $panel_settings,
			    'name'            => 'page_transitions_container',
			    'hidden_property' => 'smooth_page_transitions',
			    'hidden_value'    => 'no'
		    )
	    );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'name'          => 'page_transition_preloader',
			    'type'          => 'yesno',
			    'default_value' => 'no',
			    'label'         => esc_html__( 'Enable Preloading Animation', 'onschedule' ),
			    'description'   => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'onschedule' ),
			    'parent'        => $page_transitions_container,
			    'args'          => array(
				    "dependence"             => true,
				    "dependence_hide_on_yes" => "",
				    "dependence_show_on_yes" => "#edgtf_page_transition_preloader_container"
			    )
		    )
	    );
	
	    $page_transition_preloader_container = onschedule_edge_add_admin_container(
		    array(
			    'parent'          => $page_transitions_container,
			    'name'            => 'page_transition_preloader_container',
			    'hidden_property' => 'page_transition_preloader',
			    'hidden_value'    => 'no'
		    )
	    );
	
	
	    onschedule_edge_add_admin_field(
		    array(
			    'name'   => 'smooth_pt_bgnd_color',
			    'type'   => 'color',
			    'label'  => esc_html__( 'Page Loader Background Color', 'onschedule' ),
			    'parent' => $page_transition_preloader_container
		    )
	    );
	
	    $group_pt_spinner_animation = onschedule_edge_add_admin_group(
		    array(
			    'name'        => 'group_pt_spinner_animation',
			    'title'       => esc_html__( 'Loader Style', 'onschedule' ),
			    'description' => esc_html__( 'Define styles for loader spinner animation', 'onschedule' ),
			    'parent'      => $page_transition_preloader_container
		    )
	    );
	
	    $row_pt_spinner_animation = onschedule_edge_add_admin_row(
		    array(
			    'name'   => 'row_pt_spinner_animation',
			    'parent' => $group_pt_spinner_animation
		    )
	    );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'type'          => 'selectsimple',
			    'name'          => 'smooth_pt_spinner_type',
			    'default_value' => '',
			    'label'         => esc_html__( 'Spinner Type', 'onschedule' ),
			    'parent'        => $row_pt_spinner_animation,
			    'options'       => array(
				    'color_spinner'         => esc_html__( 'Color Spinner', 'onschedule' ),
			    	'rotate_circles'        => esc_html__( 'Rotate Circles', 'onschedule' ),
				    'pulse'                 => esc_html__( 'Pulse', 'onschedule' ),
				    'double_pulse'          => esc_html__( 'Double Pulse', 'onschedule' ),
				    'cube'                  => esc_html__( 'Cube', 'onschedule' ),
				    'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'onschedule' ),
				    'stripes'               => esc_html__( 'Stripes', 'onschedule' ),
				    'wave'                  => esc_html__( 'Wave', 'onschedule' ),
				    'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'onschedule' ),
				    'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'onschedule' ),
				    'atom'                  => esc_html__( 'Atom', 'onschedule' ),
				    'clock'                 => esc_html__( 'Clock', 'onschedule' ),
				    'mitosis'               => esc_html__( 'Mitosis', 'onschedule' ),
				    'lines'                 => esc_html__( 'Lines', 'onschedule' ),
				    'fussion'               => esc_html__( 'Fussion', 'onschedule' ),
				    'wave_circles'          => esc_html__( 'Wave Circles', 'onschedule' ),
				    'pulse_circles'         => esc_html__( 'Pulse Circles', 'onschedule' )
			    )
		    )
	    );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'type'          => 'colorsimple',
			    'name'          => 'smooth_pt_spinner_color',
			    'default_value' => '',
			    'label'         => esc_html__( 'Spinner Color', 'onschedule' ),
			    'parent'        => $row_pt_spinner_animation
		    )
	    );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'name'          => 'page_transition_fadeout',
			    'type'          => 'yesno',
			    'default_value' => 'no',
			    'label'         => esc_html__( 'Enable Fade Out Animation', 'onschedule' ),
			    'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'onschedule' ),
			    'parent'        => $page_transitions_container
		    )
	    );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'show_back_button',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Show "Back To Top Button"', 'onschedule'),
                'description'   => esc_html__('Enabling this option will display a Back to Top button on every page', 'onschedule'),
                'parent'        => $panel_settings
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'responsiveness',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Responsiveness', 'onschedule'),
                'description'   => esc_html__('Enabling this option will make all pages responsive', 'onschedule'),
                'parent'        => $panel_settings
            )
        );

        $panel_custom_code = onschedule_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_custom_code',
                'title' => esc_html__('Custom Code', 'onschedule')
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'custom_css',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom CSS', 'onschedule'),
                'description'   => esc_html__('Enter your custom CSS here', 'onschedule'),
                'parent'        => $panel_custom_code
            )
        );

        onschedule_edge_add_admin_field(
            array(
                'name'          => 'custom_js',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom JS', 'onschedule'),
                'description'   => esc_html__('Enter your custom Javascript here', 'onschedule'),
                'parent'        => $panel_custom_code
            )
        );
	
	    $panel_google_api = onschedule_edge_add_admin_panel(
		    array(
			    'page'  => '',
			    'name'  => 'panel_google_api',
			    'title' => esc_html__('Google API', 'onschedule')
		    )
	    );
	
	    onschedule_edge_add_admin_field(
		    array(
			    'name'        => 'google_maps_api_key',
			    'type'        => 'text',
			    'label'       => esc_html__('Google Maps Api Key', 'onschedule'),
			    'description' => esc_html__('Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our to our documentation.', 'onschedule'),
			    'parent'      => $panel_google_api
		    )
	    );
    }

    add_action( 'onschedule_edge_action_options_map', 'onschedule_edge_general_options_map', 1);
}