<?php if ( post_password_required() ) { ?>
		<p class="edgtf-no-password"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'onschedule' ); ?></p>
<?php } else { ?>
	<?php if ( have_comments() ) { ?>
		<div class="edgtf-comment-holder clearfix" id="comments">
			<div class="edgtf-comment-holder-inner">
				<div class="edgtf-comments-title">
					<h4><?php esc_html_e('Comments', 'onschedule' ); ?></h4>
				</div>
				<div class="edgtf-comments">
					<ul class="edgtf-comment-list">
						<?php wp_list_comments(array( 'callback' => 'onschedule_edge_comment')); ?>
					</ul>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<?php if ( ! comments_open() ) : ?>
			<p><?php esc_html_e('Sorry, the comment form is closed at this time.', 'onschedule'); ?></p>
		<?php endif; ?>	
	<?php } ?>
<?php } ?>
<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );

$consent  = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

$args = array(
	'id_form' => 'commentform',
	'id_submit' => 'submit_comment',
	'title_reply'=> esc_html__( 'Leave a comment','onschedule' ),
	'title_reply_before' => '<h3 id="reply-title" class="comment-reply-title">',
	'title_reply_after' => '</h3>',
	'title_reply_to' => esc_html__( 'Post a Reply to %s','onschedule' ),
	'cancel_reply_link' => esc_html__( 'cancel reply','onschedule' ),
	'label_submit' => esc_html__( 'POST COMMENT','onschedule' ),
	'comment_field' => '<textarea id="comment" placeholder="'.esc_attr__( 'Your comment','onschedule' ).'" name="comment" cols="45" rows="6" aria-required="true"></textarea>',
	'comment_notes_before' => '',
	'comment_notes_after' => '',
	'fields' => apply_filters( 'comment_form_default_fields', array(
		'author' => '<div class="edgtf-columns-wrapper edgtf-content-columns-50-50 edgtf-columns-tiny-space"><div class="edgtf-columns-inner"><div class="edgtf-column-content edgtf-column-content1"><input id="author" name="author" placeholder="'. esc_attr__( 'Name','onschedule' ) .'" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' /></div>',
		'email' => '<div class="edgtf-column-content edgtf-column-content2"><input id="email" name="email" placeholder="'. esc_attr__( 'Email','onschedule' ) .'" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' . $aria_req . ' /></div></div></div>',
		'cookies' => '<p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" ' . $consent . ' />' .
			'<label for="wp-comment-cookies-consent">' . esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', 'onschedule' ) . '</label></p>',
	) ) );
 ?>
<?php if(get_comment_pages_count() > 1){ ?>
	<div class="edgtf-comment-pager">
		<p><?php paginate_comments_links(); ?></p>
	</div>
<?php } ?>
<?php if(comments_open()) { ?>
	<div class="edgtf-comment-form">
		<div class="edgtf-comment-form-inner">
			<?php comment_form($args); ?>
		</div>
	</div>
<?php } ?>	