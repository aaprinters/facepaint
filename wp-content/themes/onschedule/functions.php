<?php
include_once get_template_directory() . '/theme-includes.php';

if (!function_exists('onschedule_edge_styles')) {
    /**
     * Function that includes theme's core styles
     */
    function onschedule_edge_styles() {

        //include theme's core styles
        wp_enqueue_style('onschedule-edge-style-default-style', EDGE_ROOT . '/style.css');
        wp_enqueue_style('onschedule-edge-style-modules', EDGE_ASSETS_ROOT . '/css/modules.min.css');

        onschedule_edge_icon_collections()->enqueueStyles();

        wp_enqueue_style('wp-mediaelement');

        //is woocommerce installed?
        if (onschedule_edge_is_woocommerce_installed()) {
            if (onschedule_edge_load_woo_assets()) {

                //include theme's woocommerce styles
                wp_enqueue_style('onschedule-edge-style-woo', EDGE_ASSETS_ROOT . '/css/woocommerce.min.css');
            }
        }

        //define files afer which style dynamic needs to be included. It should be included last so it can override other files
        $style_dynamic_deps_array = array();
        if (onschedule_edge_load_woo_assets()) {
            $style_dynamic_deps_array = array('onschedule-edge-style-woo', 'onschedule-edge-style-woo-responsive');
        }

        if (file_exists(EDGE_ROOT_DIR . '/assets/css/style_dynamic.css') && onschedule_edge_is_css_folder_writable() && !is_multisite()) {
            wp_enqueue_style('onschedule-edge-style-style-dynamic', EDGE_ASSETS_ROOT . '/css/style_dynamic.css', $style_dynamic_deps_array, filemtime(EDGE_ROOT_DIR . '/assets/css/style_dynamic.css')); //it must be included after woocommerce styles so it can override it
        } else if(file_exists(EDGE_ROOT_DIR.'/assets/css/style_dynamic_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css') && onschedule_edge_is_css_folder_writable() && is_multisite()) {
	        wp_enqueue_style('onschedule-edge-style-style-dynamic', EDGE_ASSETS_ROOT.'/css/style_dynamic_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css', $style_dynamic_deps_array, filemtime(EDGE_ROOT_DIR.'/assets/css/style_dynamic_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css')); //it must be included after woocommerce styles so it can override it
        }

        //is responsive option turned on?
        if (onschedule_edge_is_responsive_on()) {
            wp_enqueue_style('onschedule-edge-style-modules-responsive', EDGE_ASSETS_ROOT . '/css/modules-responsive.min.css');

            //is woocommerce installed?
            if (onschedule_edge_is_woocommerce_installed()) {
                if (onschedule_edge_load_woo_assets()) {

                    //include theme's woocommerce responsive styles
                    wp_enqueue_style('onschedule-edge-style-woo-responsive', EDGE_ASSETS_ROOT . '/css/woocommerce-responsive.min.css');
                }
            }

            //include proper styles
            if (file_exists(EDGE_ROOT_DIR . '/assets/css/style_dynamic_responsive.css') && onschedule_edge_is_css_folder_writable() && !is_multisite()) {
                wp_enqueue_style('onschedule-edge-style-style-dynamic-responsive', EDGE_ASSETS_ROOT . '/css/style_dynamic_responsive.css', array(), filemtime(EDGE_ROOT_DIR . '/assets/css/style_dynamic_responsive.css'));
            } else if(file_exists(EDGE_ROOT_DIR.'/assets/css/style_dynamic_responsive_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css') && onschedule_edge_is_css_folder_writable() && is_multisite()) {
	            wp_enqueue_style('onschedule-edge-style-style-dynamic-responsive', EDGE_ASSETS_ROOT.'/css/style_dynamic_responsive_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css', array(), filemtime(EDGE_ROOT_DIR.'/assets/css/style_dynamic_responsive_ms_id_'. onschedule_edge_get_multisite_blog_id() .'.css'));
            }
        }

        //include Visual Composer styles
        if (onschedule_edge_visual_composer_installed()) {
            wp_enqueue_style('js_composer_front');
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_styles');
}

if (!function_exists('onschedule_edge_google_fonts_styles')) {
    /**
     * Function that includes google fonts defined anywhere in the theme
     */
    function onschedule_edge_google_fonts_styles() {
        $font_simple_field_array = onschedule_edge_options()->getOptionsByType('fontsimple');
        if (!(is_array($font_simple_field_array) && count($font_simple_field_array) > 0)) {
            $font_simple_field_array = array();
        }

        $font_field_array = onschedule_edge_options()->getOptionsByType('font');
        if (!(is_array($font_field_array) && count($font_field_array) > 0)) {
            $font_field_array = array();
        }

        $available_font_options = array_merge($font_simple_field_array, $font_field_array);

        $google_font_weight_array = onschedule_edge_options()->getOptionValue('google_font_weight');
        if (!empty($google_font_weight_array)) {
            $google_font_weight_array = array_slice(onschedule_edge_options()->getOptionValue('google_font_weight'), 1);
        }

        $font_weight_str = '400,500,600,700';
        if (!empty($google_font_weight_array) && $google_font_weight_array !== '') {
            $font_weight_str = implode(',', $google_font_weight_array);
        }

        $google_font_subset_array = onschedule_edge_options()->getOptionValue('google_font_subset');
        if (!empty($google_font_subset_array)) {
            $google_font_subset_array = array_slice(onschedule_edge_options()->getOptionValue('google_font_subset'), 1);
        }

        $font_subset_str = 'latin-ext';
        if (!empty($google_font_subset_array) && $google_font_subset_array !== '') {
            $font_subset_str = implode(',', $google_font_subset_array);
        }

        //define available font options array
        $fonts_array = array();
        foreach ($available_font_options as $font_option) {
            //is font set and not set to default and not empty?
            $font_option_value = onschedule_edge_options()->getOptionValue($font_option);
            if (onschedule_edge_is_font_option_valid($font_option_value) && !onschedule_edge_is_native_font($font_option_value)) {
                $font_option_string = $font_option_value . ':' . $font_weight_str;
                if (!in_array($font_option_string, $fonts_array)) {
                    $fonts_array[] = $font_option_string;
                }
            }
        }

        $fonts_array = array_diff($fonts_array, array('-1:' . $font_weight_str));
        $google_fonts_string = implode('|', $fonts_array);

        //default fonts
        $default_font_string = 'Raleway:' . $font_weight_str.'|Roboto:'.$font_weight_str.'|Montserrat:'.$font_weight_str;
        $protocol = is_ssl() ? 'https:' : 'http:';

        //is google font option checked anywhere in theme?
        if (count($fonts_array) > 0) {

            //include all checked fonts
            $fonts_full_list = $default_font_string . '|' . str_replace('+', ' ', $google_fonts_string);
            $fonts_full_list_args = array(
                'family' => urlencode($fonts_full_list),
                'subset' => urlencode($font_subset_str),
            );

            $onschedule_edge_global_fonts = add_query_arg($fonts_full_list_args, $protocol . '//fonts.googleapis.com/css');
            wp_enqueue_style('onschedule-edge-style-google-fonts', esc_url_raw($onschedule_edge_global_fonts), array(), '1.0.0');

        } else {
            //include default google font that theme is using
            $default_fonts_args = array(
                'family' => urlencode($default_font_string),
                'subset' => urlencode($font_subset_str),
            );
            $onschedule_edge_global_fonts = add_query_arg($default_fonts_args, $protocol . '//fonts.googleapis.com/css');
            wp_enqueue_style('onschedule-edge-style-google-fonts', esc_url_raw($onschedule_edge_global_fonts), array(), '1.0.0');
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_google_fonts_styles');
}

if (!function_exists('onschedule_edge_scripts')) {
    /**
     * Function that includes all necessary scripts
     */
    function onschedule_edge_scripts() {
        global $wp_scripts;

        //init theme core scripts
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-accordion');
	    wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('wp-mediaelement');

        // 3rd party JavaScripts that we used in our theme
        wp_enqueue_script('appear', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.appear.js', array('jquery'), false, true);
        wp_enqueue_script('modernizr', EDGE_ASSETS_ROOT . '/js/modules/plugins/modernizr.min.js', array('jquery'), false, true);
		wp_enqueue_script('hoverIntent');
        wp_enqueue_script('jquery-plugin', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.plugin.js', array('jquery'), false, true);
        wp_enqueue_script('countdown', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.countdown.min.js', array('jquery'), false, true);
        wp_enqueue_script('owl-carousel', EDGE_ASSETS_ROOT . '/js/modules/plugins/owl.carousel.min.js', array('jquery'), false, true);
        wp_enqueue_script('parallax', EDGE_ASSETS_ROOT . '/js/modules/plugins/parallax.min.js', array('jquery'), false, true);
        wp_enqueue_script('easypiechart', EDGE_ASSETS_ROOT . '/js/modules/plugins/easypiechart.js', array('jquery'), false, true);
        wp_enqueue_script('waypoints', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.waypoints.min.js', array('jquery'), false, true);
        wp_enqueue_script('chart', EDGE_ASSETS_ROOT . '/js/modules/plugins/Chart.min.js', array('jquery'), false, true);
        wp_enqueue_script('counter', EDGE_ASSETS_ROOT . '/js/modules/plugins/counter.js', array('jquery'), false, true);
        wp_enqueue_script('absoluteCounter', EDGE_ASSETS_ROOT . '/js/modules/plugins/absoluteCounter.min.js', array('jquery'), false, true);
        wp_enqueue_script('fluidvids', EDGE_ASSETS_ROOT . '/js/modules/plugins/fluidvids.min.js', array('jquery'), false, true);
        wp_enqueue_script('prettyPhoto', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.prettyPhoto.js', array('jquery'), false, true);
        wp_enqueue_script('nicescroll', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.nicescroll.min.js', array('jquery'), false, true);
        wp_enqueue_script('scroll-to-plugin', EDGE_ASSETS_ROOT . '/js/modules/plugins/ScrollToPlugin.min.js', array('jquery'), false, true);
        wp_enqueue_script('waitforimages', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.waitforimages.js', array('jquery'), false, true);
        wp_enqueue_script('jquery-easing-1.3', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.easing.1.3.js', array('jquery'), false, true);
        wp_enqueue_script('multiscroll', EDGE_ASSETS_ROOT . '/js/modules/plugins/jquery.multiscroll.min.js', array('jquery'), false, true);
        wp_enqueue_script('isotope', EDGE_ASSETS_ROOT . '/js/modules/plugins/isotope.pkgd.min.js', array('jquery'), false, true);
        wp_enqueue_script('packery', EDGE_ASSETS_ROOT . '/js/modules/plugins/packery-mode.pkgd.min.js', array('jquery'), false, true);

        if (onschedule_edge_is_woocommerce_installed()) {
            wp_enqueue_script('select2');
        }

        //include google map api script
        $google_maps_api_key = onschedule_edge_options()->getOptionValue('google_maps_api_key');
        if (!empty($google_maps_api_key)) {
            wp_enqueue_script('onschedule-edge-google-map-api', '//maps.googleapis.com/maps/api/js?key=' . $google_maps_api_key, array(), false, true);
        }

        wp_enqueue_script('onschedule-edge-script-modules', EDGE_ASSETS_ROOT . '/js/modules.min.js', array('jquery'), false, true);

        //include comment reply script
        $wp_scripts->add_data('comment-reply', 'group', 1);
        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        //include Visual Composer script
        if (onschedule_edge_visual_composer_installed()) {
            wp_enqueue_script('wpb_composer_front_js');
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_scripts');
}

//defined content width variable
if (!isset($content_width)) $content_width = 1060;

if (!function_exists('onschedule_edge_theme_setup')) {
    /**
     * Function that adds various features to theme. Also defines image sizes that are used in a theme
     */
    function onschedule_edge_theme_setup() {
        //add support for feed links
        add_theme_support('automatic-feed-links');

        //add support for post formats
        add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));

        //add theme support for post thumbnails
        add_theme_support('post-thumbnails');

        //add theme support for title tag
        add_theme_support('title-tag');

        //define thumbnail sizes
        add_image_size('onschedule_edge_square', 550, 550, true);
        add_image_size('onschedule_edge_landscape', 1100, 550, true);
        add_image_size('onschedule_edge_portrait', 550, 1100, true);
        add_image_size('onschedule_edge_huge', 1100, 1100, true);

        load_theme_textdomain('onschedule', get_template_directory() . '/languages');
    }

    add_action('after_setup_theme', 'onschedule_edge_theme_setup');
}

if ( ! function_exists( 'onschedule_edge_enqueue_editor_customizer_styles' ) ) {
	/**
	 * Enqueue supplemental block editor styles
	 */
	function dieter_edge_enqueue_editor_customizer_styles() {
		wp_enqueue_style( 'onschedule-style-modules-admin-styles', EDGE_FRAMEWORK_ADMIN_ASSETS_ROOT . '/css/edgtf-modules-admin.css' );
		wp_enqueue_style( 'onschedule-style-handle-editor-customizer-styles', EDGE_FRAMEWORK_ADMIN_ASSETS_ROOT . '/css/editor-customizer-style.css' );
	}

	// add google font
	add_action( 'enqueue_block_editor_assets', 'onschedule_edge_google_fonts_styles' );
	// add action
	add_action( 'enqueue_block_editor_assets', 'onschedule_edge_enqueue_editor_customizer_styles' );
}

if (!function_exists('onschedule_edge_is_responsive_on')) {
    /**
     * Checks whether responsive mode is enabled in theme options
     * @return bool
     */
    function onschedule_edge_is_responsive_on() {
        return onschedule_edge_options()->getOptionValue('responsiveness') !== 'no';
    }
}

if (!function_exists('onschedule_edge_rgba_color')) {
    /**
     * Function that generates rgba part of css color property
     *
     * @param $color string hex color
     * @param $transparency float transparency value between 0 and 1
     *
     * @return string generated rgba string
     */
    function onschedule_edge_rgba_color($color, $transparency) {
        if ($color !== '' && $transparency !== '') {
            $rgba_color = '';

            $rgb_color_array = onschedule_edge_hex2rgb($color);
            $rgba_color .= 'rgba(' . implode(', ', $rgb_color_array) . ', ' . $transparency . ')';

            return $rgba_color;
        }
    }
}

if (!function_exists('onschedule_edge_header_meta')) {
    /**
     * Function that echoes meta data if our seo is enabled
     */
    function onschedule_edge_header_meta() { ?>

        <meta charset="<?php bloginfo('charset'); ?>"/>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>

    <?php }

    add_action('onschedule_edge_action_header_meta', 'onschedule_edge_header_meta');
}

if (!function_exists('onschedule_edge_user_scalable_meta')) {
    /**
     * Function that outputs user scalable meta if responsiveness is turned on
     * Hooked to onschedule_edge_action_header_meta action
     */
    function onschedule_edge_user_scalable_meta() {
        //is responsiveness option is chosen?
        if (onschedule_edge_is_responsive_on()) { ?>
            <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes">
        <?php } else { ?>
            <meta name="viewport" content="width=1200,user-scalable=yes">
        <?php }
    }

    add_action('onschedule_edge_action_header_meta', 'onschedule_edge_user_scalable_meta');
}

if (!function_exists('onschedule_edge_smooth_page_transitions')) {
	/**
	 * Function that outputs smooth page transitions html if smooth page transitions functionality is turned on
	 * Hooked to onschedule_edge_action_after_body_tag action
	 */
	function onschedule_edge_smooth_page_transitions() {
		$id = onschedule_edge_get_page_id();
		
		if(onschedule_edge_get_meta_field_intersect('smooth_page_transitions',$id) === 'yes' &&
		   onschedule_edge_get_meta_field_intersect('page_transition_preloader',$id) === 'yes') { ?>
			<div class="edgtf-smooth-transition-loader edgtf-mimic-ajax">
				<div class="edgtf-st-loader">
					<div class="edgtf-st-loader1">
						<?php onschedule_edge_loading_spinners(); ?>
					</div>
				</div>
			</div>
		<?php }
	}
	
	add_action('onschedule_edge_action_after_body_tag', 'onschedule_edge_smooth_page_transitions', 10);
}

if (!function_exists('onschedule_edge_back_to_top_button')) {
	/**
	 * Function that outputs back to top button html if back to top functionality is turned on
	 * Hooked to onschedule_edge_action_after_header_area action
	 */
	function onschedule_edge_back_to_top_button() {
		if (onschedule_edge_options()->getOptionValue('show_back_button') == 'yes') { ?>
			<a id='edgtf-back-to-top' href='#'>
                <span class="edgtf-icon-stack">
                     <?php onschedule_edge_icon_collections()->getBackToTopIcon('font_awesome');?>
                </span>
			</a>
		<?php }
	}
	
	add_action('onschedule_edge_action_after_header_area', 'onschedule_edge_back_to_top_button', 10);
}

if (!function_exists('onschedule_edge_get_page_id')) {
    /**
     * Function that returns current page / post id.
     * Checks if current page is woocommerce page and returns that id if it is.
     * Checks if current page is any archive page (category, tag, date, author etc.) and returns -1 because that isn't
     * page that is created in WP admin.
     *
     * @return int
     *
     * @version 0.1
     *
     * @see onschedule_edge_is_woocommerce_installed()
     * @see onschedule_edge_is_woocommerce_shop()
     */
    function onschedule_edge_get_page_id() {
        if (onschedule_edge_is_woocommerce_installed() && onschedule_edge_is_woocommerce_shop()) {
            return onschedule_edge_get_woo_shop_page_id();
        }

        if (onschedule_edge_is_default_wp_template()) {
            return -1;
        }

        return get_queried_object_id();
    }
}

if (!function_exists('onschedule_edge_get_multisite_blog_id')) {
    /**
     * Check is multisite and return blog id
     *
     * @return int
     */
    function onschedule_edge_get_multisite_blog_id() {
        if(is_multisite()){
            return get_blog_details()->blog_id;
        }
    }
}

if (!function_exists('onschedule_edge_is_default_wp_template')) {
    /**
     * Function that checks if current page archive page, search, 404 or default home blog page
     * @return bool
     *
     * @see is_archive()
     * @see is_search()
     * @see is_404()
     * @see is_front_page()
     * @see is_home()
     */
    function onschedule_edge_is_default_wp_template() {
        return is_archive() || is_search() || is_404() || (is_front_page() && is_home());
    }
}

if (!function_exists('onschedule_edge_has_shortcode')) {
    /**
     * Function that checks whether shortcode exists on current page / post
     *
     * @param string shortcode to find
     * @param string content to check. If isn't passed current post content will be used
     *
     * @return bool whether content has shortcode or not
     */
    function onschedule_edge_has_shortcode($shortcode, $content = '') {
        $has_shortcode = false;

        if ($shortcode) {
            //if content variable isn't past
            if ($content == '') {
                //take content from current post
                $page_id = onschedule_edge_get_page_id();
                if (!empty($page_id)) {
                    $current_post = get_post($page_id);

                    if (is_object($current_post) && property_exists($current_post, 'post_content')) {
                        $content = $current_post->post_content;
                    }
                }
            }

            //does content has shortcode added?
            if (stripos($content, '[' . $shortcode) !== false) {
                $has_shortcode = true;
            }
        }

        return $has_shortcode;
    }
}

if (!function_exists('onschedule_edge_page_custom_style')) {
    /**
     * Function that print custom page style
     */
    function onschedule_edge_page_custom_style() {

        $style = apply_filters('onschedule_edge_filter_add_page_custom_style', $style = array());

        if ($style !== '') {
            wp_add_inline_style('onschedule-edge-style-modules', $style);
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_page_custom_style');
}

if (!function_exists('onschedule_edge_container_style')) {
    /**
     * Function that return container style
     */
    function onschedule_edge_container_style($style) {
        $id = onschedule_edge_get_page_id();
        $class_id = onschedule_edge_get_page_id();
        if (onschedule_edge_is_woocommerce_installed() && is_product()) {
            $class_id = get_the_ID();
        }

        $class_prefix = onschedule_edge_get_unique_page_class($class_id);

        $container_selector = array(
            $class_prefix . ' .edgtf-content .edgtf-content-inner > .edgtf-container',
            $class_prefix . ' .edgtf-content .edgtf-content-inner > .edgtf-full-width',
        );

        $container_class = array();
        $page_backgorund_color = get_post_meta($id, "edgtf_page_background_color_meta", true);

        if ($page_backgorund_color) {
            $container_class['background-color'] = $page_backgorund_color;
        }

        $current_style = onschedule_edge_dynamic_css($container_selector, $container_class);
        $style[] = $current_style;

        return $current_style;
    }

    add_filter('onschedule_edge_filter_add_page_custom_style', 'onschedule_edge_container_style');
}

if (!function_exists('onschedule_edge_content_padding_top')) {
    /**
     * Function that return padding for content
     */
    function onschedule_edge_content_padding_top($style) {
        $id = onschedule_edge_get_page_id();
        $class_id = onschedule_edge_get_page_id();
        if (onschedule_edge_is_woocommerce_installed() && is_product()) {
            $class_id = get_the_ID();
        }

        $class_prefix = onschedule_edge_get_unique_page_class($class_id);

        $current_style = '';

        $content_selector = array(
            $class_prefix . ' .edgtf-content .edgtf-content-inner > .edgtf-container > .edgtf-container-inner',
            $class_prefix . ' .edgtf-content .edgtf-content-inner > .edgtf-full-width > .edgtf-full-width-inner',
        );

        $content_class = array();

        $page_padding_top = get_post_meta($id, "edgtf_page_content_top_padding", true);

        if ($page_padding_top !== '') {
            if (get_post_meta($id, "edgtf_page_content_top_padding_mobile", true) == 'yes') {
                $content_class['padding-top'] = onschedule_edge_filter_px($page_padding_top) . 'px !important';
            } else {
                $content_class['padding-top'] = onschedule_edge_filter_px($page_padding_top) . 'px';
            }
            $current_style .= onschedule_edge_dynamic_css($content_selector, $content_class);
        }

        $current_style = $current_style . $style;

        return $current_style;
    }

    add_filter('onschedule_edge_filter_add_page_custom_style', 'onschedule_edge_content_padding_top');
}

if (!function_exists('onschedule_edge_get_unique_page_class')) {
    /**
     * Returns unique page class based on post type and page id
     *
     * @return string
     */
    function onschedule_edge_get_unique_page_class($id) {
        $page_class = '';

        if (is_single()) {
            $page_class = '.postid-' . $id;
        } elseif ($id === onschedule_edge_get_woo_shop_page_id()) {
            $page_class = '.archive';
        } elseif (is_home()) {
            $page_class .= '.home';
        } else {
            $page_class .= '.page-id-' . $id;
        }

        return $page_class;
    }
}

if (!function_exists('onschedule_edge_sort_page_inline_style')) {
	/**
	 * Returns the array with prefix to the inputed styles
	 *
	 * @return array
	 */
	function onschedule_edge_sort_page_inline_style($prefix, $selectors, $join_selectors = false) {
		$prefixed = array();

		foreach( $selectors as $selector ) {
			
			if($join_selectors) {
				$prefixed[] = $prefix.$selector;
			} else {
				$prefixed[] = $prefix.' '.$selector;
			}
		}

		return $prefixed;
	}
}

if (!function_exists('onschedule_edge_print_custom_css')) {
    /**
     * Prints out custom css from theme options
     */
    function onschedule_edge_print_custom_css() {
        $custom_css = onschedule_edge_options()->getOptionValue('custom_css');

        if (!empty($custom_css)) {
            wp_add_inline_style('onschedule-edge-style-modules', $custom_css);
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_print_custom_css');
}

if (!function_exists('onschedule_edge_print_custom_js')) {
    /**
     * Prints out custom css from theme options
     */
    function onschedule_edge_print_custom_js() {
        $custom_js = onschedule_edge_options()->getOptionValue('custom_js');

        if (!empty($custom_js)) {
            wp_add_inline_script('onschedule-edge-script-modules', $custom_js);
        }
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_print_custom_js');
}

if (!function_exists('onschedule_edge_get_global_variables')) {
    /**
     * Function that generates global variables and put them in array so they could be used in the theme
     */
    function onschedule_edge_get_global_variables() {
        $global_variables = array();

        $global_variables['edgtfAddForAdminBar'] = is_admin_bar_showing() ? 32 : 0;
        $global_variables['edgtfElementAppearAmount'] = -100;
        $global_variables['edgtfAddingToCart'] = 'Adding to cart...';

        $global_variables = apply_filters('onschedule_edge_filter_js_global_variables', $global_variables);

        wp_localize_script('onschedule-edge-script-modules', 'edgtfGlobalVars', array(
            'vars' => $global_variables
        ));
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_get_global_variables');
}

if (!function_exists('onschedule_edge_per_page_js_variables')) {
    /**
     * Outputs global JS variable that holds page settings
     */
    function onschedule_edge_per_page_js_variables() {
        $per_page_js_vars = apply_filters('onschedule_edge_filter_per_page_js_vars', array());

        wp_localize_script('onschedule-edge-script-modules', 'edgtfPerPageVars', array(
            'vars' => $per_page_js_vars
        ));
    }

    add_action('wp_enqueue_scripts', 'onschedule_edge_per_page_js_variables');
}

if (!function_exists('onschedule_edge_content_elem_style_attr')) {
    /**
     * Defines filter for adding custom styles to content HTML element
     */
    function onschedule_edge_content_elem_style_attr() {
        $styles = apply_filters('onschedule_edge_filter_content_elem_style_attr', array());

        onschedule_edge_inline_style($styles);
    }
}

if (!function_exists('onschedule_edge_open_graph')) {
    /*
     * function that echoes open graph meta tags if enabled
     * hooked to onschedule_edge_header_meta action
     */
    function onschedule_edge_open_graph() {

        if (onschedule_edge_option_get_value('enable_open_graph') === 'yes') {

            // get the id
            $id = get_queried_object_id();

            // default type is article, override it with product if page is woo single product
            $type = 'article';
            $description = '';

            // check if page is generic wp page w/o page id
            if (onschedule_edge_is_default_wp_template()) {
                $id = 0;
            }

            // check if page is woocommerce shop page
            if (onschedule_edge_is_woocommerce_installed() && (function_exists('is_shop') && is_shop())) {
                $shop_page_id = get_option('woocommerce_shop_page_id');

                if (!empty($shop_page_id)) {
                    $id = $shop_page_id;
                    // set flag
                    $description = 'woocommerce-shop';
                }
            }

            if (function_exists('is_product') && is_product()) {
                $type = 'product';
            }

            // if id exist use wp template tags
            if (!empty($id)) {
                $url = get_permalink($id);
                $title = get_the_title($id);

                // apply bloginfo description to woocommerce shop page instead of first product item description
                if ($description === 'woocommerce-shop') {
                    $description = get_bloginfo('description');
                } else {
                    $description = strip_tags(apply_filters('the_excerpt', get_post_field('post_excerpt', $id)));
                }

                // has featured image
                if (get_post_thumbnail_id($id) !== '') {
                    $image = wp_get_attachment_url(get_post_thumbnail_id($id));
                } else {
                    $image = onschedule_edge_option_get_value('open_graph_image');
                }
            } else {
                global $wp;
                $url = home_url(add_query_arg(array(), $wp->request));
                $title = get_bloginfo('name');
                $description = get_bloginfo('description');
                $image = onschedule_edge_option_get_value('open_graph_image');
            }

            ?>

            <meta property="og:url" content="<?php echo esc_url($url); ?>"/>
            <meta property="og:type" content="<?php echo esc_html($type); ?>"/>
            <meta property="og:title" content="<?php echo esc_html($title); ?>"/>
            <meta property="og:description" content="<?php echo esc_html($description); ?>"/>
            <meta property="og:image" content="<?php echo esc_url($image); ?>"/>

        <?php }
    }

    add_action('wp_head', 'onschedule_edge_open_graph');
}

if (!function_exists('onschedule_edge_is_woocommerce_installed')) {
    /**
     * Function that checks if woocommerce is installed
     * @return bool
     */
    function onschedule_edge_is_woocommerce_installed() {
        return function_exists('is_woocommerce');
    }
}

if (!function_exists('onschedule_edge_core_plugin_installed')) {
    //is Edge CPT installed?
    function onschedule_edge_core_plugin_installed() {
        return defined('EDGE_CORE_VERSION');
    }
}

if (!function_exists('onschedule_edge_visual_composer_installed')) {
    /**
     * Function that checks if visual composer installed
     * @return bool
     */
    function onschedule_edge_visual_composer_installed() {
        //is Visual Composer installed?
        if (class_exists('WPBakeryVisualComposerAbstract')) {
            return true;
        }

        return false;
    }
}

if (!function_exists('onschedule_edge_contact_form_7_installed')) {
    /**
     * Function that checks if contact form 7 installed
     * @return bool
     */
    function onschedule_edge_contact_form_7_installed() {
        //is Contact Form 7 installed?
        if (defined('WPCF7_VERSION')) {
            return true;
        }

        return false;
    }
}

if (!function_exists('onschedule_edge_is_wpml_installed')) {
    /**
     * Function that checks if WPML plugin is installed
     * @return bool
     *
     * @version 0.1
     */
    function onschedule_edge_is_wpml_installed() {
        return defined('ICL_SITEPRESS_VERSION');
    }
}

if(!function_exists('onschedule_edge_restaurant_installed')) {
	/**
	 * Checks if restaurant plugin is installed
	 *
	 * @return bool
	 */
	function onschedule_edge_restaurant_installed() {
		return defined('EDGE_RESTAURANT_VERSION');
	}
}

if (!function_exists('onschedule_edge_is_booked_calendar_installed')) {
    /**
     * Function that checks if Booked plugin is installed
     * @return bool
     *
     * @version 0.1
     */
    function onschedule_edge_is_booked_calendar_installed() {
        return defined('BOOKED_VERSION');
    }
}

if (!function_exists('onschedule_edge_max_image_width_srcset')) {
    /**
     * Set max width for srcset to 1920
     *
     * @return int
     */
    function onschedule_edge_max_image_width_srcset() {
        return 1920;
    }

    add_filter('max_srcset_image_width', 'onschedule_edge_max_image_width_srcset');
}

if ( ! function_exists( 'edge_fn_onschedule_is_gutenberg_installed' ) ) {
    /**
     * Function that checks if Gutenberg plugin installed
     * @return bool
     */
    function edge_fn_onschedule_is_gutenberg_installed() {
        return function_exists( 'is_gutenberg_page' ) && is_gutenberg_page();
    }
}

if ( ! function_exists( 'onschedule_edge_get_module_part' ) ) {
	function onschedule_edge_get_module_part( $module ) {
		return $module;
	}
}

//Function to show services shortcode [show_booked_service]

function custom_booked_service(  ) { 
//$calendar = 77;
global $wpdb;
$booked_products = $wpdb->get_col("SELECT DISTINCT `posts`.`ID` FROM `{$wpdb->posts}` AS `posts`
INNER JOIN `{$wpdb->postmeta}` AS `meta` ON ( `posts`.`ID` = `meta`.`post_id` AND `meta`.`meta_key` = '_booked_appointment' )
WHERE `posts`.`post_type` = 'product'
AND `posts`.`post_status` = 'publish'
AND `meta`.`meta_value` = 'yes'
ORDER BY `posts`.`ID` DESC
");
$calendars = get_terms( array(
    'taxonomy' => 'booked_custom_calendars',
    'hide_empty' => false
) );

        echo '<div class="woocommerce columns-4 "><ul class="products columns-4">';
        foreach($booked_products as $booked_product){
            
            $title = get_the_title($booked_product);
            echo '<li class="product type-product status-publish first instock product_cat-flowers has-post-thumbnail shipping-taxable purchasable product-type-simple servicelist"><div>';           
            $product_image = get_the_post_thumbnail_url($booked_product);
            $currency = get_woocommerce_currency_symbol();
            $price = get_post_meta( $booked_product, '_regular_price', true);

            echo '<div class="edgtf-pl-inner"><div class="edgtf-pl-image"> <img src="'.$product_image.'"></div></div>';
            echo '<div class="edgtf-pl-text-wrapper"><h6 class="edgtf-product-list-title">'.$title.'</h6>';
            $content1 = get_post_field('post_content', $booked_product);
            echo mb_strimwidth($content1, 0, 320, '… <a href="'. get_permalink($booked_product) . '">' . 'Read More &raquo;' . '</a>');
            $homepage_url = get_home_url();
            ?><h6 style="font-size: 14px;">Ready to book this service? Choose a Mentor <a  style="color: white;" href="<?php echo $homepage_url?>/our-mentors">Here</a> Or let us help you choose a mentor <a style="color: white;" href="<?php echo $homepage_url?>/select-right-coach/">Here</a></h6><?php

            if ( ! empty( $calendars ) && ! is_wp_error( $calendars ) ) {
                foreach ( $calendars as $calendar ) {

                    $calendar_id = $calendar->term_id;
                    $custom_fields = json_decode(stripslashes(get_option('booked_custom_fields_'.$calendar_id)),true);
                    $product_id_current = $booked_product;
                    $product_id_arr = array();
                    if ($custom_fields) {
                    
                        foreach($custom_fields as $custom_field){
                            $product_id = $custom_field['value'];
                            if (is_numeric($product_id)) {
                                $product_id_arr[] = $product_id;
                            }
                            
                        }
                    }
                    if (in_array($product_id_current, $product_id_arr)){
                    echo '<p class="service_userlist"><a href="' . esc_url( get_term_link( $calendar ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $calendar->name ) ) . '">' . $calendar->name . '</a></p>';
                    }else{
                        //echo '<p>No Agent Added to this service</p>';
                    }
                }

        ?><p class="product-price-tickr"><?php echo $currency; echo $price; ?></p> <?php
            }
        echo '</div></div></li>';

        }    
        echo '</ul></div>';   
}       
// add the action 
function custom_fancy_product() {

add_shortcode( 'show_booked_service', 'custom_booked_service' );
}
add_action( 'init', 'custom_fancy_product' );

//User meta field
add_action('show_user_profile', 'my_user_profile_edit_action');
add_action('edit_user_profile', 'my_user_profile_edit_action');
function my_user_profile_edit_action($user) {
  //$checked = isset($user->user_timezome);
$checked = get_user_meta( $user->id, 'user_timezome' , true);
  ?><pre><?php //print_r($user);?></pre><?php
 // print_r($user->ID);
  if( is_user_logged_in() ) {
    $user = wp_get_current_user();
    $user_role = $user->roles;
    //print_r($user_role);
    if ( $user_role[0] == 'booked_booking_agent') { ?>

  <h3>Select your timezone</h3>
  <label for="user_timezome">
    <select name="user_timezome" placeholder="TimeZone" id="user_timezome" >
            <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="<?php if($checked){echo $checked;} ?>">Selected-- <?php if($checked){echo $checked;} ?></option>
            <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="(GMT-12:00) International Date Line West">(GMT-12:00) International Date Line West</option>
            <option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="(GMT-11:00) Midway Island, Samoa">(GMT-11:00) Midway Island, Samoa</option>
            <option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="(GMT-10:00) Hawaii">(GMT-10:00) Hawaii</option>
            <option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="(GMT-09:00) Alaska">(GMT-09:00) Alaska</option>
            <option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="(GMT-08:00) Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US & Canada)</option>
            <option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="(GMT-08:00) Tijuana, Baja California">(GMT-08:00) Tijuana, Baja California</option>
            <option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="(GMT-07:00) Arizona">(GMT-07:00) Arizona</option>
            <option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="(GMT-07:00) Chihuahua, La Paz, Mazatlan">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
            <option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="(GMT-07:00) Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US & Canada)</option>
            <option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="(GMT-06:00) Central America">(GMT-06:00) Central America</option>
            <option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="(GMT-06:00) Central Time (US & Canada)">(GMT-06:00) Central Time (US & Canada)</option>
            <option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="(GMT-06:00) Guadalajara, Mexico City, Monterrey">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
            <option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="(GMT-06:00) Saskatchewan">(GMT-06:00) Saskatchewan</option>
            <option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="(GMT-05:00) Bogota, Lima, Quito, Rio Branco">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
            <option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="(GMT-05:00) Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US & Canada)</option>
            <option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="(GMT-05:00) Indiana (East)">(GMT-05:00) Indiana (East)</option>
            <option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="(GMT-04:00) Atlantic Time (Canada)">(GMT-04:00) Atlantic Time (Canada)</option>
            <option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="(GMT-04:00) Caracas, La Paz">(GMT-04:00) Caracas, La Paz</option>
            <option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="(GMT-04:00) Manaus">(GMT-04:00) Manaus</option>
            <option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="(GMT-04:00) Santiago">(GMT-04:00) Santiago</option>
            <option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="(GMT-03:30) Newfoundland">(GMT-03:30) Newfoundland</option>
            <option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Brasilia">(GMT-03:00) Brasilia</option>
            <option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="(GMT-03:00) Buenos Aires, Georgetown">(GMT-03:00) Buenos Aires, Georgetown</option>
            <option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Greenland">(GMT-03:00) Greenland</option>
            <option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Montevideo">(GMT-03:00) Montevideo</option>
            <option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="(GMT-02:00) Mid-Atlantic">(GMT-02:00) Mid-Atlantic</option>
            <option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="(GMT-01:00) Cape Verde Is.">(GMT-01:00) Cape Verde Is.</option>
            <option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="(GMT-01:00) Azores">(GMT-01:00) Azores</option>
            <option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="(GMT+00:00) Casablanca, Monrovia, Reykjavik">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
            <option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
            <option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
            <option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
            <option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Brussels, Copenhagen, Madrid, Paris">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
            <option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
            <option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) West Central Africa">(GMT+01:00) West Central Africa</option>
            <option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Amman">(GMT+02:00) Amman</option>
            <option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Athens, Bucharest, Istanbul">(GMT+02:00) Athens, Bucharest, Istanbul</option>
            <option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Beirut">(GMT+02:00) Beirut</option>
            <option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Cairo">(GMT+02:00) Cairo</option>
            <option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="(GMT+02:00) Harare, Pretoria">(GMT+02:00) Harare, Pretoria</option>
            <option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
            <option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Jerusalem">(GMT+02:00) Jerusalem</option>
            <option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Minsk">(GMT+02:00) Minsk</option>
            <option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Windhoek">(GMT+02:00) Windhoek</option>
            <option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Kuwait, Riyadh, Baghdad">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
            <option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="(GMT+03:00) Moscow, St. Petersburg, Volgograd">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
            <option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Nairobi">(GMT+03:00) Nairobi</option>
            <option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Tbilisi">(GMT+03:00) Tbilisi</option>
            <option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="(GMT+03:30) Tehran">(GMT+03:30) Tehran</option>
            <option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="(GMT+04:00) Abu Dhabi, Muscat">(GMT+04:00) Abu Dhabi, Muscat</option>
            <option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="(GMT+04:00) Baku">(GMT+04:00) Baku</option>
            <option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="(GMT+04:00) Yerevan">(GMT+04:00) Yerevan</option>
            <option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="(GMT+04:30) Kabul">(GMT+04:30) Kabul</option>
            <option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="(GMT+05:00) Yekaterinburg">(GMT+05:00) Yekaterinburg</option>
            <option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="(GMT+05:00) Islamabad, Karachi, Tashkent">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
            <option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="(GMT+05:30) Sri Jayawardenapura">(GMT+05:30) Sri Jayawardenapura</option>
            <option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
            <option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="(GMT+05:45) Kathmandu">(GMT+05:45) Kathmandu</option>
            <option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="(GMT+06:00) Almaty, Novosibirsk">(GMT+06:00) Almaty, Novosibirsk</option>
            <option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="(GMT+06:00) Astana, Dhaka">(GMT+06:00) Astana, Dhaka</option>
            <option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="(GMT+06:30) Yangon (Rangoon)">(GMT+06:30) Yangon (Rangoon)</option>
            <option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="(GMT+07:00) Bangkok, Hanoi, Jakarta">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
            <option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="(GMT+07:00) Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
            <option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
            <option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Kuala Lumpur, Singapore">(GMT+08:00) Kuala Lumpur, Singapore</option>
            <option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Irkutsk, Ulaan Bataar">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
            <option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Perth">(GMT+08:00) Perth</option>
            <option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Taipei">(GMT+08:00) Taipei</option>
            <option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="(GMT+09:00) Osaka, Sapporo, Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
            <option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="(GMT+09:00) Seoul">(GMT+09:00) Seoul</option>
            <option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="(GMT+09:00) Yakutsk">(GMT+09:00) Yakutsk</option>
            <option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="(GMT+09:30) Adelaide">(GMT+09:30) Adelaide</option>
            <option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="(GMT+09:30) Darwin">(GMT+09:30) Darwin</option>
            <option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="(GMT+10:00) Brisbane">(GMT+10:00) Brisbane</option>
            <option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Canberra, Melbourne, Sydney">(GMT+10:00) Canberra, Melbourne, Sydney</option>
            <option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Hobart">(GMT+10:00) Hobart</option>
            <option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="(GMT+10:00) Guam, Port Moresby">(GMT+10:00) Guam, Port Moresby</option>
            <option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Vladivostok">(GMT+10:00) Vladivostok</option>
            <option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="(GMT+11:00) Magadan, Solomon Is., New Caledonia">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
            <option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="(GMT+12:00) Auckland, Wellington">(GMT+12:00) Auckland, Wellington</option>
            <option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="(GMT+12:00) Fiji, Kamchatka, Marshall Is">(GMT+12:00) Fiji, Kamchatka, Marshall Is</option>
            <option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="(GMT+13:00) Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
    </select>    
  </label>
<?php 
 }
  } 
}
add_action('personal_options_update', 'my_user_profile_update_action');
add_action('edit_user_profile_update', 'my_user_profile_update_action');
function my_user_profile_update_action($user_id) {
    $timezone_get = $_POST['user_timezome'];
  update_user_meta($user_id, 'user_timezome', $timezone_get);
}


