<div class="field">
	<label class="field-label"><?php esc_html_e("Registration:","booked"); ?><i class="required-asterisk booked-icon booked-icon-required"></i></label>
	<p class="field-small-p"><?php esc_html_e('Please enter your name, your email address and choose a password to get started.','booked'); ?></p>
</div>

<?php
	$name_requirements = get_option('booked_registration_name_requirements',array('require_name'));
	$name_requirements = ( isset($name_requirements[0]) ? $name_requirements[0] : false );
?>

<?php if ( $name_requirements == 'require_surname' ): ?>
	<div class="field">
		<input value="" placeholder="<?php esc_html_e('First Name','booked'); ?>..." type="text" class="textfield" name="booked_appt_name" />
		<input value="" placeholder="<?php esc_html_e('Last Name','booked'); ?>..." type="text" class="textfield" name="booked_appt_surname" />
	</div>
<?php else: ?>
	<div class="field">
		<input value="" placeholder="<?php esc_html_e('Name','booked'); ?>..." type="text" class="large textfield" name="booked_appt_name" />
	</div>
<?php endif; ?>

<div class="field">
	<input value="" placeholder="<?php esc_html_e('Email Address','booked'); ?>..." type="email" class="textfield" name="booked_appt_email" />
	<input value="" placeholder="<?php esc_html_e('Choose a password','booked'); ?>..." type="password" class="textfield" name="booked_appt_password" />
	<!-- <input value="" id="timezone" placeholder="timezone" type="text" class="textfield" name="booked_timezone" />-->
</div>


<script type="text/javascript">
	//var appointment_time = jQuery(".appointment-info").text();
	//var current_time = moment().format("MMMM Do YYYY, h:mm:ss a");
	//var diffrence = current_time.diff(appointment_time); // 86400000
	//var current_time = Intl.DateTimeFormat().resolvedOptions().timeZone;


//jstz
jQuery(document).ready(function() {
	var tz = jstz.determine();
	var timezone = tz.name();
	jQuery("#tz").html(timezone);

	// display current time based on user location
	var current_time =  moment().tz(timezone).format('MMMM Do YYYY, h:mm:ss a');
	jQuery("#time").html(current_time);
	var full_timezone = timezone.concat(current_time);
	jQuery(".Timezone").val(full_timezone); 

});

</script>
<div class="title" >Your Timezone: <span id="tz" style="color: #6D37B0;"></span> Date/Time: <span id="time" style="color: #6D37B0;"></span></div>
