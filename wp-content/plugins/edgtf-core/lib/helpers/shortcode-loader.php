<?php
namespace OnScheduleEdgeNamespace\Modules\Shortcodes\Lib;

use OnScheduleEdgeNamespace\Modules\Shortcodes\Accordion\Accordion;
use OnScheduleEdgeNamespace\Modules\Shortcodes\AccordionTab\AccordionTab;
use OnScheduleEdgeNamespace\Modules\Shortcodes\AnimationHolder\AnimationHolder;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Banner\Banner;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BlogList\BlogList;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BlogSlider\BlogSlider;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BookedAppointments\BookedAppointments;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BookedCalendar\BookedCalendar;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BookedLogin\BookedLogin;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BookedProfile\BookedProfile;
use OnScheduleEdgeNamespace\Modules\Shortcodes\BookedSlider\BookedSlider;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Button\Button;
use OnScheduleEdgeNamespace\Modules\Shortcodes\CallToAction\CallToAction;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Charts\Charts;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ClientsCarousel\ClientsCarousel;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ClientsCarouselItem\ClientsCarouselItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Countdown\Countdown;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Counter\Counter;
use OnScheduleEdgeNamespace\Modules\Shortcodes\CustomFont\CustomFont;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Dropcaps\Dropcaps;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ElementsHolder\ElementsHolder;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ElementsHolderItem\ElementsHolderItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ExpandedImageGallery\ExpandedImageGallery;
use OnScheduleEdgeNamespace\Modules\Shortcodes\FrameSlide\FrameSlide;
use OnScheduleEdgeNamespace\Modules\Shortcodes\FrameSlider\FrameSlider;
use OnScheduleEdgeNamespace\Modules\Shortcodes\GalleryBlocks\GalleryBlocks;
use OnScheduleEdgeNamespace\Modules\Shortcodes\GoogleMap\GoogleMap;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Highlight\Highlight;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Icon\Icon;
use OnScheduleEdgeNamespace\Modules\Shortcodes\IconListItem\IconListItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\IconWithText\IconWithText;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ImageGallery\ImageGallery;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ImageSlider\ImageSlider;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ImageSliderItem\ImageSliderItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ImageWithText\ImageWithText;
use OnScheduleEdgeNamespace\Modules\Shortcodes\InfoCards\InfoCards;
use OnScheduleEdgeNamespace\Modules\Shortcodes\InfoCardsItem\InfoCardsItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ItemShowcase\ItemShowcase;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ItemShowcaseItem\ItemShowcaseItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Parallax\Parallax;
use OnScheduleEdgeNamespace\Modules\Shortcodes\PieChart\PieChart;
use OnScheduleEdgeNamespace\Modules\Shortcodes\PricingList\PricingList;
use OnScheduleEdgeNamespace\Modules\Shortcodes\PricingListItem\PricingListItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\PricingTables\PricingTables;
use OnScheduleEdgeNamespace\Modules\Shortcodes\PricingTable\PricingTable;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ProgressBar\ProgressBar;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ProductInfo\ProductInfo;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ProductList\ProductList;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ProductListCarousel\ProductListCarousel;
use OnScheduleEdgeNamespace\Modules\Shortcodes\ProductListSimple\ProductListSimple;
use OnScheduleEdgeNamespace\Modules\Shortcodes\SectionTitle\SectionTitle;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Separator\Separator;
use OnScheduleEdgeNamespace\Modules\Shortcodes\SocialShare\SocialShare;
use OnScheduleEdgeNamespace\Modules\Shortcodes\StackedImages\StackedImages;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Tabs\Tabs;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Tab\Tab;
use OnScheduleEdgeNamespace\Modules\Shortcodes\Team\Team;
use OnScheduleEdgeNamespace\Modules\Shortcodes\VerticalSplitSlider\VerticalSplitSlider;
use OnScheduleEdgeNamespace\Modules\Shortcodes\VerticalSplitSliderLeftPanel\VerticalSplitSliderLeftPanel;
use OnScheduleEdgeNamespace\Modules\Shortcodes\VerticalSplitSliderRightPanel\VerticalSplitSliderRightPanel;
use OnScheduleEdgeNamespace\Modules\Shortcodes\VerticalSplitSliderContentItem\VerticalSplitSliderContentItem;
use OnScheduleEdgeNamespace\Modules\Shortcodes\VideoButton\VideoButton;

/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader {
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();
	
	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {}
	
	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {}
	
	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {}
	
	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}
		
		return self::$instance;
	}
	
	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}
	
	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new AnimationHolder());
		$this->addShortcode(new Banner());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new BlogSlider());
		if(onschedule_edge_is_booked_calendar_installed()){
			$this->addShortcode(new BookedAppointments());
			$this->addShortcode(new BookedCalendar());
			$this->addShortcode(new BookedLogin());
			$this->addShortcode(new BookedProfile());
			$this->addShortcode(new BookedSlider());
		}
		$this->addShortcode(new Button());
		$this->addShortcode(new CallToAction());
		$this->addShortcode(new Charts());
		$this->addShortcode(new ClientsCarousel());
		$this->addShortcode(new ClientsCarouselItem());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new Counter());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new ExpandedImageGallery());
		$this->addShortcode(new FrameSlider());
		$this->addShortcode(new FrameSlide());
		$this->addShortcode(new GalleryBlocks());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new Icon());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new ImageSlider());
		$this->addShortcode(new ImageSliderItem());
		$this->addShortcode(new ImageWithText());
		$this->addShortcode(new InfoCards());
		$this->addShortcode(new InfoCardsItem());
		$this->addShortcode(new ItemShowcase());
		$this->addShortcode(new ItemShowcaseItem());
		$this->addShortcode(new Parallax());
		$this->addShortcode(new PieChart());
		$this->addShortcode(new PricingList());
		$this->addShortcode(new PricingListItem());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new ProgressBar());
		if(onschedule_edge_is_woocommerce_installed()){
			$this->addShortcode(new ProductInfo());
			$this->addShortcode(new ProductList());
			$this->addShortcode(new ProductListCarousel());
			$this->addShortcode(new ProductListSimple());
		}
		$this->addShortcode(new SectionTitle());
		$this->addShortcode(new Separator());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new StackedImages());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new Team());
		$this->addShortcode(new VerticalSplitSlider());
		$this->addShortcode(new VerticalSplitSliderLeftPanel());
		$this->addShortcode(new VerticalSplitSliderRightPanel());
		$this->addShortcode(new VerticalSplitSliderContentItem());
		$this->addShortcode(new VideoButton());
	}
	
	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();
		
		foreach ($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}
	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();