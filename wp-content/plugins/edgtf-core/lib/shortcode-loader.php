<?php
namespace EdgeCore\Lib;

/**
 * Class ShortcodeLoader
 * @package EdgeCore\Lib
 */
class ShortcodeLoader {
    /**
     * @var private instance of current class
     */
    private static $instance;
    /**
     * @var array
     */
    private $loadedShortcodes = array();

    /**
     * Private constuct because of Singletone
     */
    private function __construct() {}

    /**
     * Private sleep because of Singletone
     */
    private function __wakeup() {}

    /**
     * Private clone because of Singletone
     */
    private function __clone() {}

    /**
     * Returns current instance of class
     * @return ShortcodeLoader
     */
    public static function getInstance() {
        if(self::$instance == null) {
            return new self;
        }

        return self::$instance;
    }

    /**
     * Adds new shortcode. Object that it takes must implement ShortcodeInterface
     * @param ShortcodeInterface $shortcode
     */
    private function addShortcode(ShortcodeInterface $shortcode) {
        if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
            $this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
        }
    }

    /**
     * Adds all shortcodes.
     *
     * @see ShortcodeLoader::addShortcode()
     */
    private function addShortcodes() {
	    $post_type_file_name_path = EDGE_CORE_CPT_PATH . '/*';
    	foreach (glob($post_type_file_name_path, GLOB_ONLYDIR) as $post_type_file_name) {
		    $post_type = str_replace(' ', '', ucwords(str_replace('-', ' ', basename($post_type_file_name))));
		    foreach (glob($post_type_file_name . '/shortcodes/*.php') as $shortcode_file_name) {
			    $class_path = str_replace(' ', '', 'EdgeCore\CPT\ '.$post_type.'\Shortcodes\ ' . ucwords(str_replace('-', ' ', basename($shortcode_file_name, '.php'))));
			
			    $this->addShortcode(new $class_path);
		    }
	    }
    }

    /**
     * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
     * of each shortcode object
     */
    public function load() {
        $this->addShortcodes();

        foreach ($this->loadedShortcodes as $shortcode) {
            add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
        }
    }
}