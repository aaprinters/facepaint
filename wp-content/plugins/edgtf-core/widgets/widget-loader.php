<?php

if (!function_exists('onschedule_edge_register_widgets')) {

	function onschedule_edge_register_widgets() {

		$widgets = array(
			'OnScheduleEdgeClassBlogListWidget',
			'OnScheduleEdgeClassButtonWidget',
			'OnScheduleEdgeClassFullScreenMenuOpener',
			'OnScheduleEdgeClassImageWidget',
			'OnScheduleEdgeClassImageSliderWidget',
			'OnScheduleEdgeClassRawHTMLWidget',
			'OnScheduleEdgeClassSearchOpener',
			'OnScheduleEdgeClassSeparatorWidget',
			'OnScheduleEdgeClassSideAreaOpener',
			'OnScheduleEdgeClassSocialIconWidget'
		);

		if ( onschedule_edge_is_woocommerce_installed() ) {
			$widgets[] = 'OnScheduleEdgeClassWoocommerceDropdownCart';
		}

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'onschedule_edge_register_widgets');