<?php get_header(); ?>
<?php onschedule_edge_get_title(); ?>
<div class="edgtf-container edgtf-default-page-template">
	<?php do_action('onschedule_edge_action_after_container_open'); ?>
	<div class="edgtf-container-inner clearfix">
		<?php
			$edgef_taxonomy_id = get_queried_object_id();
			$edgef_taxonomy_type = is_tax( 'portfolio-tag' ) ? 'portfolio-tag' : 'portfolio-category';
			$edgef_taxonomy	= !empty( $edgef_taxonomy_id ) ? get_term_by( 'id', $edgef_taxonomy_id, $edgef_taxonomy_type) : '';
			$edgef_taxonomy_slug = !empty($edgef_taxonomy) ? $edgef_taxonomy->slug : '';
			$edgef_taxonomy_name = !empty($edgef_taxonomy) ? $edgef_taxonomy->taxonomy : '';
		
			onschedule_edge_get_archive_portfolio_list($edgef_taxonomy_slug, $edgef_taxonomy_name);
		?>
	</div>
	<?php do_action('onschedule_edge_action_before_container_close'); ?>
</div>
<?php get_footer(); ?>
