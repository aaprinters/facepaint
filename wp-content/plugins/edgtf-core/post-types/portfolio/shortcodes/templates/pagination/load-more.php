<?php if($query_results->max_num_pages > 1) { ?>
	<div class="edgtf-pl-loading">
		<div class="edgtf-pl-loading-bounce1"></div>
		<div class="edgtf-pl-loading-bounce2"></div>
		<div class="edgtf-pl-loading-bounce3"></div>
	</div>
	<div class="edgtf-pl-load-more-holder">
		<div class="edgtf-pl-load-more">
			<?php 
				echo onschedule_edge_get_button_html(array(
					'link'         => 'javascript: void(0)',
					'text'         => esc_html__('LOAD MORE', 'edgtf-core'),
					'custom_class' => 'edgtf-btn-custom-hover-bg edgtf-btn-custom-border-hover edgtf-btn-custom-hover-color'
				));
			?>
		</div>
	</div>
<?php }