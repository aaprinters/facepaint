<?php echo edgtf_core_get_shortcode_module_template_part('portfolio', 'parts/image', $item_layout, $params); ?>

<div class="edgtf-pli-text-holder">
	<div class="edgtf-pli-text-wrapper">
		<div class="edgtf-pli-text">
			<?php echo edgtf_core_get_shortcode_module_template_part('portfolio', 'parts/title', $item_layout, $params); ?>
			
			<?php echo edgtf_core_get_shortcode_module_template_part('portfolio', 'parts/category', $item_layout, $params); ?>
			
			<?php echo edgtf_core_get_shortcode_module_template_part('portfolio', 'parts/excerpt', $item_layout, $params); ?>
		</div>
	</div>
</div>