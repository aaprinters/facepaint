<?php
/**
 * The Template for opening the wrapper for Author listings
 *
 * Override this template by copying it to yourtheme/simple-user-listing/open-author.php
 *
 * @author 		helgatheviking
 * @package 	Simple-User-Listing/Templates
 * @version     1.8.0
 *
 */
?>
<script type="text/javascript">

jQuery( document ).ready(function() {

	// Get the button that opens the modal
	jQuery(".user-list-wrap .author-block .coachimage").click(function() { 
	    var btnid = jQuery(this).attr('id');
	    jQuery(".user-list-wrap .author-block " +"."+btnid).show();
	});

	// Get the modal
	var modal = document.getElementById("model-"+'btnid');

	// When the user clicks on <span> (x), close the modal
	jQuery(".user-list-wrap .author-block .modal .close").click(function() { 
	  jQuery(this).parent().parent().css( "display", "none" );
	});
	
});	
</script>
<div class="user-list-wrap">