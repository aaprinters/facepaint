<?php 

/**
 * Simple User Listing Core Hooks. 
 * 
 * @package     Simple User Listing/Functions/Templates
 * @author      Kathy Darling
 * @copyright   Copyright (c) 2020, Kathy Darling
 * @license     http://opensource.org/licenses/gpl-3.0.php GNU Public License  
 */

add_action( 'sul_before_user_loop_author',       'sul_template_loop_author_link_open' );
add_action( 'sul_before_user_loop_author_title', 'sul_template_loop_author_avatar' );
add_action( 'sul_user_loop_author_title',        'sul_template_loop_author_name' );
add_action( 'sul_user_loop_author_title',        'sul_template_loop_author_link_close', 5 );
add_action( 'sul_after_user_loop_author',        'sul_template_loop_author_description' );
