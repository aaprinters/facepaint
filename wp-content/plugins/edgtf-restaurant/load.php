<?php

require_once 'const.php';
require_once 'helpers.php';

//load lib
require_once 'lib/post-type-interface.php';
require_once 'lib/shortcode-interface.php';
require_once 'lib/menu-query.php';

//load post-post-types
require_once 'post-types/menu/menu-register.php';
require_once 'post-types/menu/helper-functions.php';
require_once 'post-types/post-types-register.php'; //this has to be loaded last

//load shortcodes
require_once 'shortcodes/reservation-form/reservation-form.php';
require_once 'shortcodes/working-hours/working-hours.php';
require_once 'post-types/menu/shortcodes/menu-list/menu-list.php';
require_once 'post-types/menu/shortcodes/menu-grid/menu-grid.php';

//load shortcodes inteface
require_once 'lib/shortcode-loader.php';

//load admin
if(!function_exists('edgtf_restaurant_load_admin')) {
	function edgtf_restaurant_load_admin() {
		require_once 'admin/options/restaurant/map.php';
		require_once 'admin/meta-boxes/menu/map.php';
	}

	add_action('onschedule_edge_action_before_options_map', 'edgtf_restaurant_load_admin');
}
