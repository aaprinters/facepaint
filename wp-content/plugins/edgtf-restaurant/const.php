<?php

define('EDGE_RESTAURANT_VERSION', '1.0');
define('EDGE_RESTAURANT_ABS_PATH', dirname(__FILE__));
define('EDGE_RESTAURANT_REL_PATH', dirname(plugin_basename(__FILE__ )));
define('EDGE_RESTAURANT_CPT_PATH', EDGE_RESTAURANT_ABS_PATH.'/post-types');