<?php

if(edgtf_restaurant_theme_installed()) {
	if(!function_exists('edgtf_restaurant_map_map')) {
		/**
		 * Adds admin page for OpenTable integration
		 */
		function edgtf_restaurant_map_map() {
			onschedule_edge_add_admin_page(array(
				'title' => esc_html__('Restaurant', 'edgtf-restaurant'),
				'slug'  => '_restaurant',
				'icon'  => 'fa fa-cutlery'
			));

			//#OpenTable Panel
			$panel_open_table = onschedule_edge_add_admin_panel(array(
				'page'  => '_restaurant',
				'name'  => 'panel_open_table',
				'title' => esc_html__('OpenTable', 'edgtf-restaurant')
			));

			onschedule_edge_add_admin_field(array(
				'name'        => 'open_table_id',
				'type'        => 'text',
				'label'       => esc_html__('OpenTable ID', 'edgtf-restaurant'),
				'description' => esc_html__('Add your restaurant\'s OpenTable ID', 'edgtf-restaurant'),
				'parent'      => $panel_open_table,
				'args'        => array(
					'col_width' => 3
				)
			));

			//#Working Hours panel
			$panel_working_hours = onschedule_edge_add_admin_panel(array(
				'page'  => '_restaurant',
				'name'  => 'panel_working_hours',
				'title' => esc_html__('Working Hours', 'edgtf-restaurant')
			));

			$monday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'monday_group',
				'title'       => esc_html__('Monday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Monday', 'edgtf-restaurant')
			));

			$monday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'monday_row',
				'parent' => $monday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_monday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $monday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_monday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $monday_row
			));

			$tuesday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'tuesday_group',
				'title'       => esc_html__('Tuesday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Tuesday', 'edgtf-restaurant')
			));

			$tuesday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'tuesday_row',
				'parent' => $tuesday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_tuesday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $tuesday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_tuesday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $tuesday_row
			));

			$wednesday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'wednesday_group',
				'title'       => esc_html__('Wednesday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Wednesday', 'edgtf-restaurant')
			));

			$wednesday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'wednesday_row',
				'parent' => $wednesday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_wednesday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $wednesday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_wednesday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $wednesday_row
			));

			$thursday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'thursday_group',
				'title'       => esc_html__('Thursday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Thursday', 'edgtf-restaurant')
			));

			$thursday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'thursday_row',
				'parent' => $thursday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_thursday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $thursday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_thursday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $thursday_row
			));

			$friday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'friday_group',
				'title'       => esc_html__('Friday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Friday', 'edgtf-restaurant')
			));

			$friday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'friday_row',
				'parent' => $friday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_friday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $friday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_friday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $friday_row
			));

			$saturday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'saturday_group',
				'title'       => esc_html__('Saturday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Saturday', 'edgtf-restaurant')
			));

			$saturday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'saturday_row',
				'parent' => $saturday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_saturday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $saturday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_saturday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $saturday_row
			));

			$sunday_group = onschedule_edge_add_admin_group(array(
				'name'        => 'sunday_group',
				'title'       => esc_html__('Sunday', 'edgtf-restaurant'),
				'parent'      => $panel_working_hours,
				'description' => esc_html__('Working hours for Sunday', 'edgtf-restaurant')
			));

			$sunday_row = onschedule_edge_add_admin_row(array(
				'name'   => 'sunday_row',
				'parent' => $sunday_group
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_sunday_from',
				'type'   => 'textsimple',
				'label'  => esc_html__('From', 'edgtf-restaurant'),
				'parent' => $sunday_row
			));

			onschedule_edge_add_admin_field(array(
				'name'   => 'wh_sunday_to',
				'type'   => 'textsimple',
				'label'  => esc_html__('To', 'edgtf-restaurant'),
				'parent' => $sunday_row
			));
		}

		add_action('onschedule_edge_action_options_map', 'edgtf_restaurant_map_map', 22);
	}
}

if(!function_exists('edgtf_restaurant_menu_item_single_map')) {
	function edgtf_restaurant_menu_item_single_map() {
		//#OpenTable Panel
		$panel_single = onschedule_edge_add_admin_panel(array(
			'page'  => '_restaurant',
			'name'  => 'panel_menu_item_single',
			'title' => esc_html__('Menu Item Single Page', 'edgtf-restaurant')
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_show_navigation',
			'type'          => 'yesno',
			'default_value' => 'yes',
			'label'         => esc_html__('Enable Navigation Through Posts', 'edgtf-restaurant'),
			'description'   => esc_html__('Enabling this option will display previous and next posts links', 'edgtf-restaurant'),
			'parent'        => $panel_single
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_show_author_info',
			'type'          => 'yesno',
			'default_value' => 'yes',
			'label'         => esc_html__('Show Author Info Section', 'edgtf-restaurant'),
			'description'   => esc_html__('Enabling this option will display author info section', 'edgtf-restaurant'),
			'parent'        => $panel_single
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_enable_comments',
			'type'          => 'yesno',
			'default_value' => 'yes',
			'label'         => esc_html__('Enable Comments', 'edgtf-restaurant'),
			'description'   => esc_html__('Enabling this option will display comments', 'edgtf-restaurant'),
			'parent'        => $panel_single
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_show_preparation_info',
			'type'          => 'yesno',
			'default_value' => 'yes',
			'label'         => esc_html__('Show Preparation Info Section', 'edgtf-restaurant'),
			'description'   => esc_html__('Enabling this option will display preparation info section in sidebar', 'edgtf-restaurant'),
			'parent'        => $panel_single
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_show_related',
			'type'          => 'yesno',
			'default_value' => 'yes',
			'label'         => esc_html__('Show Related Menu Items', 'edgtf-restaurant'),
			'description'   => esc_html__('Enabling this option will display related menu items in sidebar', 'edgtf-restaurant'),
			'parent'        => $panel_single,
			'args'          => array(
				'dependence'             => true,
				'dependence_show_on_yes' => '#edgtf_menu_item_related_container'
			)
		));

		$related_container = onschedule_edge_add_admin_container(array(
			'parent'          => $panel_single,
			'name'            => 'menu_item_related_container',
			'hidden_property' => 'menu_item_show_related',
			'hidden_value'    => 'no'
		));

		onschedule_edge_add_admin_field(array(
			'name'          => 'menu_item_related_image_size',
			'type'          => 'select',
			'default_value' => 'portfolio-landscape',
			'label'         => esc_html__('Related Menu Item Image Proportion', 'edgtf-restaurant'),
			'description'   => esc_html__('Choose related menu item image proportion', 'edgtf-restaurant'),
			'parent'        => $related_container,
			'options'       => array(
				'full'                      => esc_html__('Original', 'edgtf-restaurant'),
				'onschedule_edge_square'    => esc_html__('Square', 'edgtf-restaurant'),
				'onschedule_edge_landscape' => esc_html__('Landscape', 'edgtf-restaurant'),
			    'onschedule_edge_portrait'  => esc_html__('Portrait', 'edgtf-restaurant')
			)
		));
	}

	add_action('onschedule_edge_action_options_map', 'edgtf_restaurant_menu_item_single_map', 22);
}