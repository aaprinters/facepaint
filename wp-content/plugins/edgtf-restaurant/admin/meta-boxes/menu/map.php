<?php

if(!function_exists('edgtf_restaurant_menu_map')) {
	function edgtf_restaurant_menu_map() {
		$general_meta_box = onschedule_edge_create_meta_box(array(
			'scope' => array('restaurant-menu-item'),
			'title' => esc_html__('Menu Item Settings', 'edgtf-restaurant'),
			'name'  => 'menu_item_settings'
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_price_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Price', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter price for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_label',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Label', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter label for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_prep_time_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Preparation Time', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter preparation time for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_cook_time_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Cooking Time', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter cooking time for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_servings_num_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Number of Servings', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter number of servings for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_calories_num_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Calories', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter number of calories for this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_ready_in_meta',
			'type'          => 'text',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Ready In', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter necessary time to prepare this menu item', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));

		onschedule_edge_create_meta_box_field(array(
			'name'          => 'edgtf_menu_item_ingredients_meta',
			'type'          => 'textarea',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Ingredients', 'edgtf-restaurant'),
			'description'   => esc_html__('Enter menu item ingredients. Separate each ingredient with new line (enter).', 'edgtf-restaurant'),
			'parent'        => $general_meta_box,
			'args'          => array(
				'col_width' => '3'
			)
		));
	}

	add_action('onschedule_edge_action_meta_boxes_map', 'edgtf_restaurant_menu_map');
}

if(!function_exists('edgtf_restaurant_menu_item_gallery_map')) {
	function edgtf_restaurant_menu_item_gallery_map() {
		$meta_box = onschedule_edge_create_meta_box(array(
			'scope' => array('restaurant-menu-item'),
			'title' => esc_html__('Menu Item Gallery', 'edgtf-restaurant'),
			'name'  => 'menu_item_gallery'
		));

		onschedule_edge_create_meta_box_field(array(
			'type'          => 'multipleimages',
			'name'          => 'edgtf_menu_item_gallery_meta',
			'default_value' => '',
			'label'         => esc_html__('Menu Item Gallery', 'edgtf-restaurant'),
			'description'   => esc_html__('Choose gallery images for this menu item', 'edgtf-restaurant'),
			'parent'        => $meta_box
		));
	}

	add_action('onschedule_edge_action_meta_boxes_map', 'edgtf_restaurant_menu_item_gallery_map');
}