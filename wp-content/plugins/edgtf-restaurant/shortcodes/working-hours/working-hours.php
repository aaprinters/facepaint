<?php
namespace EdgeRestaurant\Shortcodes\WorkingHours;

use EdgeRestaurant\Lib\ShortcodeInterface;

class WorkingHours implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'edgtf_working_hours';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Working Hours', 'edgtf-restaurant'),
			'base'                      => $this->base,
			'category'                  => esc_html__('by EDGE', 'edgtf-restaurant'),
			'icon'                      => 'icon-wpb-working-hours extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__('Title', 'edgtf-restaurant')
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'title_accent_word',
					'heading'     => esc_html__('Title Accent Word', 'edgtf-restaurant')
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'enable_frame',
					'heading'     => esc_html__('Enable Frame', 'edgtf-restaurant'),
					'description' => esc_html__('Enabling this option will display dark frame around working hours', 'edgtf-restaurant'),
					'admin_label' => true,
					'value'       => array(
						esc_html__('Default', 'edgtf-restaurant')   => '',
						esc_html__('Yes', 'edgtf-restaurant')       => 'yes',
						esc_html__('No', 'edgtf-restaurant')        => 'no'
					),
					'save_always' => true
				),
				array(
					'type'        => 'attach_image',
					'param_name'  => 'bg_image',
					'heading'     => esc_html__('Background Image', 'edgtf-restaurant'),
					'admin_label' => true,
					'value'       => array(
						esc_html__('Default', 'edgtf-restaurant')   => '',
						esc_html__('Yes', 'edgtf-restaurant')       => 'yes',
						esc_html__('No', 'edgtf-restaurant')        => 'no'
					),
					'save_always' => true,
					'dependency'  => array('element' => 'enable_frame', 'value' => 'yes')
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'title'             => '',
			'title_accent_word' => '',
			'enable_frame'      => '',
			'bg_image'          => ''
		);

		$params = shortcode_atts($default_atts, $atts);

		$params['working_hours']  = $this->getWorkingHours();
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['holder_styles']  = $this->getHolderStyles($params);

		return edgtf_restaurant_get_template_part('shortcodes/working-hours/templates/working-hours-template', '', $params, true);
	}

	private function getWorkingHours() {
		$workingHours = array();

		if(edgtf_restaurant_theme_installed()) {
			//monday
			if(onschedule_edge_options()->getOptionValue('wh_monday_from') !== '') {
				$workingHours['monday']['label'] = __('Monday', 'edgtf-restaurant');
				$workingHours['monday']['from']  = onschedule_edge_options()->getOptionValue('wh_monday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_monday_to') !== '') {
				$workingHours['monday']['to'] = onschedule_edge_options()->getOptionValue('wh_monday_to');
			}

			//tuesday
			if(onschedule_edge_options()->getOptionValue('wh_tuesday_from') !== '') {
				$workingHours['tuesday']['label'] = __('Tuesday', 'edgtf-restaurant');
				$workingHours['tuesday']['from']  = onschedule_edge_options()->getOptionValue('wh_tuesday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_tuesday_to') !== '') {
				$workingHours['tuesday']['to'] = onschedule_edge_options()->getOptionValue('wh_tuesday_to');
			}

			//wednesday
			if(onschedule_edge_options()->getOptionValue('wh_wednesday_from') !== '') {
				$workingHours['wednesday']['label'] = __('Wednesday', 'edgtf-restaurant');
				$workingHours['wednesday']['from']  = onschedule_edge_options()->getOptionValue('wh_wednesday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_wednesday_to') !== '') {
				$workingHours['wednesday']['to'] = onschedule_edge_options()->getOptionValue('wh_wednesday_to');
			}

			//thursday
			if(onschedule_edge_options()->getOptionValue('wh_thursday_from') !== '') {
				$workingHours['thursday']['label'] = __('Thursday', 'edgtf-restaurant');
				$workingHours['thursday']['from']  = onschedule_edge_options()->getOptionValue('wh_thursday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_thursday_to') !== '') {
				$workingHours['thursday']['to'] = onschedule_edge_options()->getOptionValue('wh_thursday_to');
			}

			//friday
			if(onschedule_edge_options()->getOptionValue('wh_friday_from') !== '') {
				$workingHours['friday']['label'] = __('Friday', 'edgtf-restaurant');
				$workingHours['friday']['from']  = onschedule_edge_options()->getOptionValue('wh_friday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_friday_to') !== '') {
				$workingHours['friday']['to'] = onschedule_edge_options()->getOptionValue('wh_friday_to');
			}

			//saturday
			if(onschedule_edge_options()->getOptionValue('wh_saturday_from') !== '') {
				$workingHours['saturday']['label'] = __('Saturday', 'edgtf-restaurant');
				$workingHours['saturday']['from']  = onschedule_edge_options()->getOptionValue('wh_saturday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_saturday_to') !== '') {
				$workingHours['saturday']['to'] = onschedule_edge_options()->getOptionValue('wh_saturday_to');
			}

			//sunday
			if(onschedule_edge_options()->getOptionValue('wh_sunday_from') !== '') {
				$workingHours['sunday']['label'] = __('Sunday', 'edgtf-restaurant');
				$workingHours['sunday']['from']  = onschedule_edge_options()->getOptionValue('wh_sunday_from');
			}

			if(onschedule_edge_options()->getOptionValue('wh_sunday_to') !== '') {
				$workingHours['sunday']['to'] = onschedule_edge_options()->getOptionValue('wh_sunday_to');
			}
		}

		return $workingHours;
	}

	private function getHolderClasses($params) {
		$classes = array('edgtf-working-hours-holder');

		if(isset($params['enable_frame']) && $params['enable_frame'] === 'yes') {
			$classes[] = 'edgtf-wh-with-frame';
		}

		if(isset($params['bg_image']) && $params['bg_image'] !== '') {
			$classes[] = 'edgtf-wh-with-bg-image';
		}

		return $classes;
	}

	private function getHolderStyles($params) {
		$styles = array();

		if($params['bg_image'] !== '') {
			$bg_url = wp_get_attachment_url($params['bg_image']);

			if(!empty($bg_url)) {
				$styles[] = 'background-image: url('.$bg_url.')';
			}
		}

		return $styles;
	}
}
