<?php
namespace EdgeRestaurant\Shortcodes;

use EdgeRestaurant\Lib\ShortcodeInterface;

class ReservationForm implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'edgtf_reservation_form';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Reservation Form', 'edgtf-restaurant'),
			'base'                      => $this->base,
			'category'                  => esc_html__('by EDGE', 'edgtf-restaurant'),
			'icon'                      => 'icon-wpb-reservation-form extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'        => 'textfield',
					'param_name'  => 'open_table_id',
					'heading'     => esc_html__('OpenTable ID', 'edgtf-restaurant'),
					'admin_label' => true
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'open_table_id' => ''
		);

		$params = shortcode_atts($default_atts, $atts);

		if($params['open_table_id'] === '' && edgtf_restaurant_theme_installed()) {
			$params['open_table_id'] = onschedule_edge_options()->getOptionValue('open_table_id');
		}

		return edgtf_restaurant_get_template_part('shortcodes/reservation-form/templates/reservation-form', '', $params, true);
	}
}