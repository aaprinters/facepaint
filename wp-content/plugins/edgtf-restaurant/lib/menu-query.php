<?php
namespace EdgeRestaurant\Lib;

/**
 * Class MenuQuery
 * @package EdgeRestaurant\Lib
 */
class MenuQuery {
	/**
	 * @var private instance of current class
	 */
	private static $instance;

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {
	}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {
	}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {
	}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 *
	 *
	 * @return array
	 */
	public function queryVCParams() {
		return array(
			array(
				'type'        => 'dropdown',
				'param_name'  => 'order_by',
				'heading'     => esc_html__('Order By', 'edgtf-restaurant'),
				'value'       => array(
					esc_html__('Menu Order', 'edgtf-restaurant') => 'menu_order',
					esc_html__('Title', 'edgtf-restaurant')      => 'title',
					esc_html__('Date', 'edgtf-restaurant')       => 'date'
				),
				'group'       => esc_html__('Query', 'edgtf-restaurant')
			),
			array(
				'type'        => 'dropdown',
				'param_name'  => 'order',
				'heading'     => esc_html__('Order', 'edgtf-restaurant'),
				'value'       => array(
					esc_html__('ASC', 'edgtf-restaurant')  => 'ASC',
					esc_html__('DESC', 'edgtf-restaurant') => 'DESC',
				),
				'group'       => esc_html__('Query', 'edgtf-restaurant')
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'menu_section',
				'heading'     => esc_html__('Menu Section', 'edgtf-restaurant'),
				'description' => esc_html__('Enter one menu section slug (leave empty for showing all menu sections)', 'edgtf-restaurant'),
				'group'       => esc_html__('Query', 'edgtf-restaurant')
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'number',
				'heading'     => esc_html__('Number of Menu Items', 'edgtf-restaurant'),
				'value'       => '-1',
				'group'       => esc_html__('Query', 'edgtf-restaurant')
			)
		);
	}

	/**
	 * Returns array of parameters to be used inside shortcodes
	 *
	 * @return array
	 */
	public function getShortcodeAtts() {
		return array(
			'order_by'     => '',
			'order'        => '',
			'menu_section' => '',
			'number'       => ''
		);
	}

	/**
	 * Returns an instance of WP_Query class based on proved params
	 *
	 * @param $params
	 *
	 * @return \WP_Query
	 */
	public function buildQueryObject($params) {
		$queryParams = array(
			'post_type'      => 'restaurant-menu-item',
			'orderby'        => $params['order_by'],
			'order'          => $params['order'],
			'posts_per_page' => $params['number']
		);

		$orderBy      = !empty($params['order_by']) ? $params['order_by'] : 'date';
		$order        = !empty($params['order']) ? $params['order'] : 'DESC';
		$postsPerPage = !empty($params['number']) ? $params['number'] : '';

		$queryParams['orderby'] = $orderBy;
		$queryParams['order']   = $order;

		if($postsPerPage !== '') {
			$queryParams['posts_per_page'] = $postsPerPage;
		}

		if(!empty($params['menu_section'])) {
			$queryParams['restaurant-menu-section'] = $params['menu_section'];
		}

		return new \WP_Query($queryParams);
	}
}