<?php
namespace EdgeRestaurant\Lib;

/**
 * interface PostTypeInterface
 * @package EdgeRestaurant\Lib;
 */
interface PostTypeInterface {
    /**
     * @return string
     */
    public function getBase();

    /**
     * Registers custom post type with WordPress
     */
    public function register();
}