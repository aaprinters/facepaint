<?php

if(!function_exists('edgtf_restaurant_menu_meta_box_save')) {
	function edgtf_restaurant_menu_meta_box_save($post_types) {
		$post_types[] = 'restaurant-menu-item';

		return $post_types;
	}

	add_filter('onschedule_edge_meta_box_post_types_save', 'edgtf_restaurant_menu_meta_box_save');
	add_filter('onschedule_edge_meta_box_post_types_remove', 'edgtf_restaurant_menu_meta_box_save');
}

if(!function_exists('edgtf_restaurant_get_menu_meta')) {
    function edgtf_restaurant_get_menu_meta() {
        $meta = array();

	    $price = get_post_meta(get_the_ID(), 'edgtf_menu_item_price_meta', true);
	    $meta['price'] = $price;

	    $label = get_post_meta(get_the_ID(), 'edgtf_menu_item_label', true);
	    $meta['label'] = $label;

	    $ingredients_text = get_post_meta(get_the_ID(), 'edgtf_menu_item_ingredients_meta', true);
	    $ingredients_list = '';
	    if($ingredients_text !== '') {
		    $ingredients_list = explode("\r\n", $ingredients_text);
	    }
	    $meta['ingridients'] = $ingredients_list;

	    return $meta;
    }
}