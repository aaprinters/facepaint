<?php
namespace EdgeRestaurant\CPT\Menu;

use EdgeRestaurant\Lib\PostTypeInterface;

class Menu implements PostTypeInterface {
	/**
	 * @var string
	 */
	private $base;
	/**
	 * @var string
	 */
	private $taxBase;

	public function __construct() {
		$this->base    = 'restaurant-menu-item';
		$this->taxBase = 'restaurant-menu-section';
	}

	/**
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function register() {
		$this->registerPostType();
		$this->registerTax();
	}

	/**
	 * Regsiters custom post type with WordPress
	 */
	private function registerPostType() {
		$menuPosition = 5;
		$menuIcon     = 'dashicons-admin-post';

		register_post_type($this->base,
			array(
				'labels'        => array(
					'name'          => esc_html__('Edge Restaurant Menu', 'edgtf-restaurant'),
					'menu_name'     => esc_html__('Edge Restaurant Menu', 'edgtf-restaurant'),
					'all_items'     => esc_html__('Menu Items', 'edgtf-restaurant'),
					'add_new'       => esc_html__('Add New Menu Item', 'edgtf-restaurant'),
					'singular_name' => esc_html__('Menu Item', 'edgtf-restaurant'),
					'add_item'      => esc_html__('New Menu Item', 'edgtf-restaurant'),
					'add_new_item'  => esc_html__('Add New Menu Item', 'edgtf-restaurant'),
					'edit_item'     => esc_html__('Edit Menu Item', 'edgtf-restaurant')
				),
				'public'        => true,
				'show_in_menu'  => true,
				'rewrite'       => array('slug' => 'menu-item'),
				'menu_position' => $menuPosition,
				'show_ui'       => true,
				'has_archive'   => false,
				'hierarchical'  => false,
				'supports'      => array('title', 'thumbnail', 'excerpt', 'editor', 'comments', 'author'),
				'menu_icon'     => $menuIcon
			)
		);
	}

	/**
	 * Registers custom taxonomy with WordPress
	 */
	private function registerTax() {
		$labels = array(
			'name'              => esc_html__('Menu Section', 'edgtf-restaurant'),
			'singular_name'     => esc_html__('Menu Section', 'edgtf-restaurant'),
			'search_items'      => esc_html__('Search Menu Sections', 'edgtf-restaurant'),
			'all_items'         => esc_html__('All Menu Sections', 'edgtf-restaurant'),
			'parent_item'       => esc_html__('Parent Menu Section', 'edgtf-restaurant'),
			'parent_item_colon' => esc_html__('Parent Menu Section:', 'edgtf-restaurant'),
			'edit_item'         => esc_html__('Edit Menu Section', 'edgtf-restaurant'),
			'update_item'       => esc_html__('Update Menu Section', 'edgtf-restaurant'),
			'add_new_item'      => esc_html__('Add New Menu Section', 'edgtf-restaurant'),
			'new_item_name'     => esc_html__('New Menu Section Name', 'edgtf-restaurant'),
			'menu_name'         => esc_html__('Menu Sections', 'edgtf-restaurant'),
		);

		register_taxonomy($this->taxBase, array($this->base), array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'query_var'         => true,
			'show_admin_column' => true,
			'rewrite'           => array('slug' => 'menu-section'),
		));
	}
}