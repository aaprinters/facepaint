<?php
extract(edgtf_restaurant_get_menu_meta());
$price_class = !empty($price) ? 'edgtf-mg-with-price' : '';
?>
<li <?php post_class($price_class); ?>>
	<?php if(has_post_thumbnail()) : ?>
		<div class="edgtf-mg-image-holder">
			<?php if(!empty($price)) : ?>
				<div class="edgtf-mg-price-holder">
					<span class="edgtf-mg-price"><?php echo esc_html($price); ?></span>
				</div>
			<?php endif; ?>
			<a itemprop="image" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" data-rel="prettyPhotoMenuGrid[<?php echo esc_attr($pretty_id); ?>]">
				<?php the_post_thumbnail($image_proportion); ?>
				<?php if(!empty($label)) : ?>
					<span class="edgtf-mg-label-holder"><?php echo esc_html($label); ?></span>
				<?php endif; ?>
			</a>
		</div>
	<?php endif; ?>
	<div class="edgtf-mg-content-holder">
		<h6 itemprop="name" class="edgtf-mg-title entry-title"><a itemprop="url" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
		<?php if(is_array($ingridients) && count($ingridients)): ?>
			<ul class="edgtf-smi-ingredients-list">
				<?php foreach($ingridients as $ingredient) : ?>
					<li><?php echo esc_html($ingredient); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</li>