<?php
namespace EdgeRestaurant\CPT\Menu\Shortcodes\MenuGrid;

use EdgeRestaurant\Lib\ShortcodeInterface;
use EdgeRestaurant\Lib\MenuQuery;

class MenuGrid implements ShortcodeInterface {
	private $base;

	/**
	 * MenuGrid constructor.
	 */
	public function __construct() {
		$this->base = 'edgtf_restaurant_menu_grid';

		add_action('vc_before_init', array($this, 'vcMap'));
	}


	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Menu Grid', 'edgtf-restaurant'),
			'base'                      => $this->base,
			'category'                  => esc_html__('by EDGE', 'edgtf-restaurant'),
			'icon'                      => 'icon-wpb-menu-grid extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'number_of_cols',
						'heading'     => esc_html__('Number of Columns', 'edgtf-restaurant'),
						'value'       => array(
							esc_html__('One Column', 'edgtf-restaurant')    => 'one-col',
							esc_html__('Two Columns', 'edgtf-restaurant')   => 'two-cols',
							esc_html__('Three Columns', 'edgtf-restaurant') => 'three-cols',
							esc_html__('Four Columns', 'edgtf-restaurant')  => 'four-cols'
						)
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'image_proportions',
						'heading'     => esc_html__('Image Proportions', 'edgtf-restaurant'),
						'value'       => array(
							esc_html__('Original', 'edgtf-restaurant')  => 'full',
							esc_html__('Square', 'edgtf-restaurant')    => 'square',
							esc_html__('Landscape', 'edgtf-restaurant') => 'landscape',
							esc_html__('Portrait', 'edgtf-restaurant')  => 'portrait'
						)
					)
				),
				MenuQuery::getInstance()->queryVCParams()
			)
		));
	}

	public function render($atts, $content = null) {
		$defaultAtts = array(
			'number_of_cols' => '',
			'image_proportions' => ''
		);

		$defaultAtts = array_merge($defaultAtts, MenuQuery::getInstance()->getShortcodeAtts());

		$params = shortcode_atts($defaultAtts, $atts);
		$query  = MenuQuery::getInstance()->buildQueryObject($params);
		$gridItemParams = array(
			'image_proportion' => $this->getImageProportionName($params),
			'pretty_id'           => rand(0, 1000)
		);

		$holderClasses = $this->getHolderClasses($params);

		$html = '<div '.edgtf_restaurant_get_class_attribute($holderClasses).'>';

		if($query->have_posts()) {
			$html .= '<ul class="edgtf-mg-holder clearfix">';

			while($query->have_posts()) {
				$query->the_post();
				$html .= edgtf_restaurant_get_shortcode_module_template_part('menu', 'menu-grid/templates/menu-grid-item', '', $gridItemParams);
			}

			$html .= '</ul>';

			wp_reset_postdata();
		} else {
			$html .= '<p>'.esc_html__('No menu items match your query', 'edgtf-restaurant').'</p>';
		}

		$html .= '</div>';

		return $html;
	}

	private function getImageProportionName($params) {
		$imageProportion = 'full';

		if(!empty($params['image_proportions'])) {
			switch($params['image_proportions']) {
				case 'square':
					$imageProportion = 'portfolio-square';
					break;
				case 'portrait':
					$imageProportion = 'portfolio-portrait';
					break;
				case 'landscape':
					$imageProportion = 'portfolio-landscape';
					break;
				default:
					$imageProportion = 'full';
					break;
			}
		}

		return $imageProportion;
	}

	private function getHolderClasses($params) {
		$classes = array('edgtf-menu-grid');

		if(!empty($params['number_of_cols'])) {
			$classes[] = 'edgtf-mg-'.$params['number_of_cols'];
		}

		return $classes;
	}
}

