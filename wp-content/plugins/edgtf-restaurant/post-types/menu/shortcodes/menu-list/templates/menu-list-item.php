<?php extract(edgtf_restaurant_get_menu_meta()); ?>
<li class="edgtf-ml-item clearfix">
	<?php if($show_featured_image === 'yes') : ?>
		<div class="edgtf-ml-item-image">
			<a itemprop="image" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" data-rel="prettyPhoto[<?php echo esc_attr($pretty_id); ?>]">
				<?php the_post_thumbnail('thumbnail'); ?>
			</a>
		</div>
	<?php endif; ?>
	<div class="edgtf-ml-item-content">
		<div class="edgtf-ml-top-holder">
			<div class="edgtf-ml-title-holder">
				<h5 itemprop="name" class="edgtf-ml-title entry-title">
					<a itemprop="url" href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
					<?php if(!empty($label)) : ?>
						<span class="edgtf-ml-label-holder">
							<span class="edgtf-ml-label"><?php echo esc_html($label); ?></span>
						</span>
					<?php endif; ?>
				</h5>
			</div>
			<div class="edgtf-ml-dots"></div>

			<?php if(!empty($price)) : ?>
				<div class="edgtf-ml-price-holder">
					<h5 class="edgtf-ml-price"><?php echo esc_html($price); ?></h5>
				</div>
			<?php endif; ?>
		</div>
		<div class="edgtf-ml-bottom-holder clearfix">
			<?php if(is_array($ingridients) && count($ingridients)): ?>
			<ul class="edgtf-smi-ingredients-list">
				<?php foreach($ingridients as $ingredient) : ?>
					<li><?php echo esc_html($ingredient); ?></li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
	</div>
</li>
