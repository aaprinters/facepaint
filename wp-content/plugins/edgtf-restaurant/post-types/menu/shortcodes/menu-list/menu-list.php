<?php
namespace EdgeRestaurant\CPT\Menu\Shortcodes\MenuList;

use EdgeRestaurant\Lib\ShortcodeInterface;
use EdgeRestaurant\Lib\MenuQuery;

class MenuList implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'edgtf_restaurant_menu_list';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function vcMap() {
		vc_map(array(
			'name'                      => esc_html__('Edge Menu List', 'edgtf-restaurant'),
			'base'                      => $this->base,
			'category'                  => esc_html__('by EDGE', 'edgtf-restaurant'),
			'icon'                      => 'icon-wpb-menu-list extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'textfield',
						'param_name'  => 'title',
						'heading'     => esc_html__('Title', 'edgtf-restaurant')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_position',
						'heading'     => esc_html__('Title Position', 'edgtf-restaurant'),
						'value'       => array(
							esc_html__('Left', 'edgtf-restaurant')   => '',
							esc_html__('Center', 'edgtf-restaurant') => 'center'
						),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'show_featured_image',
						'heading'     => esc_html__('Show Featured Image?', 'edgtf-restaurant'),
						'value'       => array(
							esc_html__('Default', 'edgtf-restaurant')   => '',
							esc_html__('Yes', 'edgtf-restaurant')       => 'yes',
							esc_html__('No', 'edgtf-restaurant')        => 'no'
						),
						'admin_label' => true,
						'description' => esc_html__('Use this option to show featured image of menu items', 'edgtf-restaurant'),
					)
				),
				MenuQuery::getInstance()->queryVCParams()
			)
		));
	}

	public function getBase() {
		return $this->base;
	}

	public function render($atts, $content = null) {
		$defaultAtts = array(
			'show_featured_image' => '',
			'title'               => '',
			'title_position'      => ''
		);

		$defaultAtts = array_merge($defaultAtts, MenuQuery::getInstance()->getShortcodeAtts());

		$params = shortcode_atts($defaultAtts, $atts);
		$query  = MenuQuery::getInstance()->buildQueryObject($params);

		$listItemParams = array(
			'show_featured_image' => $params['show_featured_image'],
			'pretty_id'           => rand(0, 1000)
		);

		$holderClasses = $this->getHolderClasses($params);
		$titleClasses = array(
			'edgtf-mlw-title-holder'
		);

		$titleClasses[] = 'edgtf-mlw-title-holder-'.$params['title_position'];

		$html = '<div '.edgtf_restaurant_get_class_attribute($holderClasses).'>';

		if($params['title'] !== '') {
			$html .= '<div '.edgtf_restaurant_get_class_attribute($titleClasses).'>';
			$html .= '<h3 class="edgtf-mlw-title">'.esc_html($params['title']).'</h3>';
			$html .= '</div>';
		}

		if($query->have_posts()) {
			$html .= '<ul class="edgtf-ml-holder">';

			while($query->have_posts()) {
				$query->the_post();
				$html .= edgtf_restaurant_get_shortcode_module_template_part('menu', 'menu-list/templates/menu-list-item', '', $listItemParams);
			}

			$html .= '</ul>';

			wp_reset_postdata();
		} else {
			$html .= '<p>'.esc_html__('No menu items match your query', 'edgtf-restaurant').'</p>';
		}

		$html .= '</div>';

		return $html;
	}

	private function getHolderClasses($params) {
		$classes = array('edgtf-menu-list');

		if($params['show_featured_image'] === 'yes') {
			$classes[] = 'edgtf-ml-with-featured-image';
		}

		return $classes;
	}

}