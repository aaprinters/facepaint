<?php
/*
Plugin Name: Edge Restaurant
Description: Plugin that adds all restaurant post types, shortcodes and OpenTable integration
Author: Edge Themes
Version: 1.0
*/

require_once 'load.php';

use EdgeRestaurant\CPT;
use EdgeRestaurant\Lib;

add_action('after_setup_theme', array(CPT\PostTypesRegister::getInstance(), 'register'));

Lib\ShortcodeLoader::getInstance()->load();

if(!function_exists('edgtf_restaurant_activation')) {
	/**
	 * Triggers when plugin is activated. It calls flush_rewrite_rules
	 * and defines edgtf_restaurant_on_activate action
	 */
	function edgtf_restaurant_activation() {
		do_action('edgtf_restaurant_on_activate');
		
		EdgeRestaurant\CPT\PostTypesRegister::getInstance()->register();
		flush_rewrite_rules();
	}

	register_activation_hook(__FILE__, 'edgtf_restaurant_activation');
}

if(!function_exists('edgtf_restaurant_text_domain')) {
	/**
	 * Loads plugin text domain so it can be used in translation
	 */
	function edgtf_restaurant_text_domain() {
		load_plugin_textdomain('edgtf-restaurant', false, EDGE_RESTAURANT_REL_PATH.'/languages');
	}

	add_action('plugins_loaded', 'edgtf_restaurant_text_domain');
}